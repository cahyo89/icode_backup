<?php
#create by andesta@ymail.com
#15 juli 2013
set_time_limit(0);
	
	class push{

		var $path;
		var $fileNameLog;
		var $folderLog;
		var $db;
		var $folderDb;
		var $folderDbFailed;
		var $folderBackupDb;
		
		var $fileList;
		var $pemisah;
		
		var $dataAni;
		var $dataBatch;
		
		var $tableName;
		
		var $dataBlackList;
		var $fileCommandSmpp;
		
		
		
		function loadNumberBlackList(){
			$query  = "SELECT subscriber_number from tbl_global_blacklist_asli";
			$result = $this->db->query($query);
			if($result){
				while($rw = $result->fetch_array(MYSQLI_ASSOC)){
					$this->dataBlackList[trim($rw['subscriber_number'])] = trim($rw['subscriber_number']);
				}
				$this->writeLog("query success ".$query);
				$this->writeLog("size blacklist : ".count($this->dataBlackList));
			}
			else{
				$this->writeLog("query failed ".$query);
			}			
		}
		
		function __construct(){
			
			include "/nfs_data/projectmobads_development/utils/schedulerSMS/configPush.php";
			$mysqli = new mysqli($hostDb,$userDb,$passDb,$database);
			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error());
			    exit();
			}else{
				$this->dataBlackList    = array();
				$this->db  				= $mysqli;
				$this->path 			= $path;
				$this->fileNameLog 		= $fileNameLog;
				$this->folderLog		= $folderLog;
				
				$this->fileList 		= array();
				$this->pemisah 			= $pemisah; 
				
				$this->folderDb			= $folderDb;
				$this->folderDbFailed 	= $folderDbFailed; 
				$this->folderBackupDb	= $folderBackupDb;
				$this->tableName 			= "tbl_daily_history_".date("Ymd");
				$this->fileCommandSmpp 	 	= $fileCommandSmpp; 
				
			}
		}
		
		function createTableDailyHistory(){
           $query = "SHOW TABLES like 'tbl_daily_history_".date("Ymd")."'";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				if($result->num_rows == 0){
					$queryCreate = " CREATE TABLE `tbl_daily_history_".date("Ymd")."` ( ";
					$queryCreate .= " `id` int(11) NOT NULL AUTO_INCREMENT, ";
					$queryCreate .= " `request_time` datetime DEFAULT NULL, ";
					$queryCreate .= " `response_time` datetime DEFAULT NULL,";
					$queryCreate .= " `ip_request` varchar(15) DEFAULT NULL, ";
					$queryCreate .= " `id_batch` int(11) DEFAULT NULL, ";
					$queryCreate .= " `uid` int(11) DEFAULT NULL, ";
					$queryCreate .= " `url_on_click` blob DEFAULT NULL, ";
					$queryCreate .= " `url_requested` blob DEFAULT NULL, ";
					$queryCreate .= " `lac` int(11) DEFAULT NULL, ";
					$queryCreate .= " `ci` int(11) DEFAULT NULL, ";
					$queryCreate .= " `msisdn` varchar(30) DEFAULT NULL, ";
					$queryCreate .= " `tipe` int(11) DEFAULT NULL, ";
					$queryCreate .= " `channel_category` int(11) DEFAULT NULL, ";
					$queryCreate .= " `result` int(11) DEFAULT NULL, ";
					$queryCreate .= " PRIMARY KEY (`id`), ";
					$queryCreate .= " KEY `request_time` (`request_time`), ";
					$queryCreate .= " KEY `response_time` (`response_time`), ";
					$queryCreate .= " KEY `id_batch` (`id_batch`), ";
					$queryCreate .= " KEY `lac` (`lac`), ";
					$queryCreate .= " KEY `ci` (`ci`), ";
					$queryCreate .= " KEY `tipe` (`tipe`), ";
					$queryCreate .= " KEY `msisdn` (`msisdn`), ";
					$queryCreate .= " KEY `channel_category` (`channel_category`), ";
					$queryCreate .= " KEY `result` (`result`) ";
					$queryCreate .= " ) ENGINE=MYISAM";
					$resultCreate = $this->db->query($queryCreate);
					if($resultCreate){
						//$this->writeLog("query success : ".$queryCreate);
					}
					else{
						$this->writeLog("query failed : ".$queryCreate);
					}
				}
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
 		}
		
		function getLastDay($now){
			
			$date 		= explode(" ",$now);
			$tanggal 	= explode("-",$date[0]);
			$waktu 		= explode(":",$date[1]);
			$y = $tanggal[0];
			$m = $tanggal[1];
			$d = $tanggal[2];
			$h = $waktu[0];
			$i = $waktu[1];
			$s = $waktu[2];
			// hour menit second month day year
			$lastDay  =  date("Ymd", mktime(intval($h),intval($i),intval($s),intval($tanggal[1]),intval($tanggal[2])-1, intval($tanggal[0])));
			return $lastDay;
		
		}
		
		function parseTimeDatabase($date){
			$y= substr($date,0,4);
			$m= substr($date,4,2);
			$d= substr($date,6,2);
			$h= substr($date,8,2);
			$i= substr($date,10,2);
			$s= substr($date,12,2);
			return $y."-".$m."-".$d." ".$h.":".$i.":".$s;
		}
		
		function insertIntoTableDailyHistory($data){
			// data['time']
			$tanggalTable = substr($data['time'],0,8);
			$query = "INSERT INTO tbl_daily_history_".$tanggalTable." ";
			$query .= " ( ";
			$query .= " id_batch, ";
			$query .= " msisdn, ";
			$query .= " channel_category, ";
			$query .= " request_time, ";
			$query .= " response_time, ";
			$query .= " result ";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " ".$data['id_batch'].", ";
			$query .= " '".$data['number']."', ";
			$query .= " 0, ";
			$query .= " '".date("Y-m-d H:i:s")."', ";
			$query .= " NULL,  ";
			$query .= " 0  ";
			$query .= " ) ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else
			{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		
		function getJobsIdByIdBatch($idBatch){
			$jobsId = "";
			$query  = "SELECT jobs_id FROM tbl_lba_batch where id_batch = ".$idBatch." ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				$rw = $result->fetch_array(MYSQLI_ASSOC);
				$jobsId = $rw['jobs_id'];
			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
			return $jobsId;
		}	
		
		function insertIntoJobsId($data){
			$jobsId = $this->getJobsIdByIdBatch($data['id_batch']);
			$query  = "INSERT INTO `".$jobsId."` ";
			$query .= " ( ";
			$query .= " msisdn, ";	
			$query .= " channel_category, ";
			$query .= " result, ";
			$query .= " request_time, ";
			$query .= " response_time ";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " '".$data['number']."', ";	
			$query .= " 0, ";
			$query .= " 0, ";
			$query .= " '".date("Y-m-d H:i:s")."', ";
			$query .= " NULL ";
			$query .= " ) ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		
		
		function deConvert($message){
			$message = str_replace("#$OD","\r",$message);
			$message = str_replace("#$OA","\n",$message);
			$message = str_replace("#$2E",".",$message);
			$message = str_replace("#$2C",",",$message);
			return $message;
		}
		
		function readDirDbLBA(){
		$this->fileList = array();
			if(is_dir($this->folderDb))
			{
				if ($handle = opendir($this->folderDb)) {
				   $i = 0;
				   while (false !== ($file = readdir($handle))) {
						if ($file != "." && $file != "..") { 
							@clearstatcache();
							$this->fileList[$file] = date ("Y-m-d H:i:s", filemtime($this->folderDb.$this->path.$file));
							if($i == 100)break;
						}
					}
					   asort($this->fileList);
					   closedir($handle);
				}		
			}
	    }
		
		function readAllowFile(){
			foreach($this->fileList as $idx => $value)
			{
					if(substr_count($idx, ".reading") > 0){
						unset($this->fileList[$idx]);
					}else if(substr_count($idx,"lba_0") == 0){
							unset($this->fileList[$idx]);
					}else if((mktime() - strtotime($value)) <= 5){
						unset($this->fileList[$idx]);
					}
			}
		}
		
		function replaceSmsText($sms){
			$sms = str_replace(",",'#$2C',$sms);
			$sms = str_replace("\r",'#$OD',$sms);
			$sms = str_replace("\n",'#$OD',$sms);
			return $sms;
		}
		
		function parseSmsDynamic($idBatch,$sms,$msisdn){
			$jobsId = $this->getJobsIdByIdBatch($idBatch);
			$query  = "SELECT dynamic_argument from `".$jobsId."` where msisdn = '".$msisdn."' ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				$rw = $result->fetch_array(MYSQLI_ASSOC);
				$dynamicArgument = $this->replaceSmsText($rw['dynamic_argument']);
				
				if($dynamicArgument != ""){
					if(substr_count($dynamicArgument,";") > 0){
						$array = explode(";",$dynamicArgument);
						$ctr = 1;
						foreach($array as $idx => $value){
							$sms = str_replace("%".$ctr."",$value,$sms);
							$ctr++;
						}
					}
					else{
						$sms = str_replace("%1",$dynamicArgument,$sms);
					}
				}
				
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
			return $sms;
		}
		
		
		function post($idBatch,$msisdn,$sms,$ani,$dataAni,$idCustomer){
				if(!isset($this->dataBlackList[trim($msisdn)])){
					/*
						sender
						dest
						message
						seq_num
						ref_num
						totSegment
						encoding : (kalo mau OTA, encoding = 4)
						otherInfo
						sms_type (0=sms biasa, 1= sms OTA)
					*/
					
					$sms = $this->parseSmsDynamic($idBatch,$sms,$msisdn);
					
					$command = "sendsms,".$ani.",".$msisdn.",".$sms.",,,1,0,".$idBatch.",0\r\n";
					$handle = fopen($this->fileCommandSmpp,"a");
					fwrite($handle,$command);
					fclose($handle);
					
				}
				else{
					$this->writeLog("nomor ".$msisdn." blacklist ");
					$response = "blacklist";
				}
				$this->writeLog($response);
				return $response;
		}
		
		function writeLog($message){
			$handle = fopen($this->folderLog.$this->path.$this->fileNameLog,"a");
			fwrite($handle,date("Y-m-d H:i:s")." : ".$message."\r\n");
			fclose($handle);
		}
		
		function getArrayDbContent($dataku){
			
			$data  = array();
			$array =  explode($this->pemisah,$dataku);
			// data ada 8
			$data['batch_id'] 			= $array[0];
			$data['batch_seq_no']		= $array[1];
			$data['message_id']			= $array[2];
			$data['ani']				= $array[3];
			$data['dnis']				= $array[4];
			$data['time']				= $array[5];
			$data['connection_id']		= $array[6];
			$data['feature_counter']	= $array[7];
			$data['text']				= $array[8];
			$data['discard']			= $array[9];	
			
			if($data['message_id'] == "")
			$data['message_id'] = date("YmdHis")."_".mt_rand(100,999)."_".$data['batch_seq_no']."_".mt_rand(10000,99999);
			return $data;
		}
		
		
		function getDataAniCms(){
			$array = array();
			$query = "SELECT * FROM tbl_ani where media_type = 0  AND status = 1";
			$result = $this->db->query($query);
			if($result){
				if( ($result->num_rows) >0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$array[$rw['ani']][$rw['id_customer']]['cpName']     = $rw['cp_name'];
						$array[$rw['ani']][$rw['id_customer']]['cpPassword'] = $rw['cp_password'];
						$array[$rw['ani']][$rw['id_customer']]['cpSid']		 = $rw['cp_sid'];		
					}
				}
			}else{
				$this->writeLog("query failed :".$query);
			}
			return $array;
		}
		
		function getDataBatch(){
			$array = array();
			$query 	= "SELECT * FROM tbl_lba_batch where status_batch not in(0,11) AND channel_category = 0 order by id_batch desc limit 0,5000";
			$result = $this->db->query($query);
			if($result){
				if(($result->num_rows)>0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$array[$rw['id_batch']] = $rw;
					}
				}
			}else{
				$this->writeLog("query failed : ".$query);
			}
			return $array;	
		}
		
		function countSms($message){
			$lenMessage = strlen($this->deConvert($message));
			return ceil($lenMessage/160);
		}
	
		function processDb(){
			
			if(count($this->fileList) > 0){
				foreach($this->fileList as $idx => $value){
					clearstatcache();
					if(is_file($this->folderDb.$this->path.$idx) == true){
						$rand = mt_rand(100000,900000);
						$renFile = rename($this->folderDb.$this->path.$idx,$this->folderDb.$this->path.$idx.$rand.".reading");
						if($renFile){
							usleep(200000);
							$content = file($this->folderDb.$this->path.$idx.$rand.".reading");
							if($content !== false){
								foreach($content as $row){
									$arrayData = array();
									$value = trim($row);
									if($value != ""){
										$arrayData = $this->getArrayDbContent($value);
										
										if($this->dataBatch[$arrayData['batch_id']]['id_customer'] == ""){
											$this->dataBatch = $this->getDataBatch();
										}
										if($this->dataAni[$arrayData['ani']][$idCustomer]['cpName'] == "" || $this->dataAni[$arrayData['ani']][$idCustomer]['cpPassword'] == "" || $this->dataAni[$arrayData['ani']][$idCustomer]['cpSid'] == ""){
											$this->dataAni = $this->getDataAniCms();
										}
										$idCustomer = $this->dataBatch[$arrayData['batch_id']]['id_customer'];
										$return = $this->post($arrayData['batch_id'],$arrayData['dnis'],$arrayData['text'],$arrayData['ani'],$this->dataAni,$idCustomer);
										$insert['sms_text']		=	$arrayData['text'];
										$insert['result']		=  "";
										$insert['error_code']	=  "";
										$insert['sender_masking'] = $arrayData['ani'];
										$insert['number'] 		= 	$arrayData['dnis'];
										$insert['sent_time'] 	= 	$arrayData['time'];
										$insert['id_batch'] 	=   $arrayData['batch_id'];
										$insert['time']         =   $arrayData['time']; 
										
										$this->insertIntoTableDailyHistory($insert);
										//$this->insertIntoJobsId($insert);
										
								   	}
								}
								// move file to backup folder
								$move = rename($this->folderDb.$this->path.$idx.$rand.".reading",$this->folderBackupDb.$this->path.$idx);
								if($move){
									//$this->writeLog("move file db to backup");
								}else{	
									$this->writeLog("move file db to backup folder failed");
								}
								
								
							}else{ // db failed
								rename($this->folderDb.$this->path.$idx.$rand.".reading",$this->folderDbFailed.$this->path.$idx);
								$this->writeLog("gagal baca file : ".$this->folderDb.$this->path.$idx.$rand.".reading pindahin ke ".$this->folderDbFailed.$this->path.$idx."");
							}
						}else{
							rename($this->folderDb.$this->path.$idx.$rand.".reading",$this->folderDbFailed.$this->path.$idx);
							$this->writeLog("gagal rename file : ".$this->folderDb.$this->path.$idx.$rand.".reading pindahin ke ".$this->folderDbFailed.$this->path.$idx."");
						}
					}else{
						$this->writeLog("file tidak ada :".$this->folderDb.$this->path.$idx."");
					}
					unset($this->fileList[$idx]);
					break;
				}
			}
		}
		
		function process(){
				
				$this->run_start_time = mktime();
				$this->dataAni = $this->getDataAniCms();
				$this->dataBatch = $this->getDataBatch();
				$this->createTableDailyHistory();
				$this->loadNumberBlackList();
				
				while(true)
				{
					if((mktime() - $this->run_start_time) > 1000)
						break;
					if(count($this->fileList) == 0)
					{
						//$this->createTableDailyHistory();
						$this->readDirDbLBA();
						$this->readAllowFile();
						if(count($this->fileList) == 0)
						{
	                  				if((mktime() - $this->run_start_time) > 56) {
	                          				if(isset($idle)) {
	                                  				break;
	                          				}
	                          				else {
	                                  				sleep(rand(1, 3));
	                                  				$idle = true;
	                          				} 
	                  				}
	  					}
						else {
	                  				unset($idle);
	               				}
					}
					$this->processDb();
					//print "jalankan process() .. \n";
					usleep(200000); //tidur 2 detik <wuaaaaaah>
				}
		}
		
		function __destruct(){
		
		}
	}
	$push = new push();
	$push->process();
	
?>
