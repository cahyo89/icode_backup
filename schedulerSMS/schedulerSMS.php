<?php
/*
Create by cahyo89@gmail.com
23 agustus 2013
.: Man Jadda Wa Jada :. 
*/
	set_time_limit(0);
	ini_set('memory_limit', '-1');
	class sendSMS{
		var $dataLast;
		
		function get_last(){
			
			$query  = " SELECT * from tbl_request_single_command  where start_date<=CURDATE() and last_update IS NULL ";
			$result = $this->db->query($query);
			if($result){
				if(($result->num_rows)>0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$this->dataLast[$rw['trc_id']] = $rw;
					}
				$this->writeLog("Got the data !!!");
				}
				$this->writeLog("query success ".$query);
			}
			else{
				$this->writeLog("query failed ".$query);
			}	
			
			//print_r( $this->dataLast);
		}
		
		function __construct(){
			clearstatcache();
			include "/projectmobads/utils/schedulerCahyo/configPush.php";
			$mysqli = new mysqli($hostDb,$userDb,$passDb,$database);

			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error());
			    exit();
			}else{
				$this->dataLast    			= array();
				$this->db					= $mysqli;
				$this->path 				= $path;
				$this->fileNameLog 			= $fileNameLog;
				$this->folderLog			= $folderLog;
				$this->folderDb				= $folderDb;
				$this->folderDbFailed 		= $folderDbFailed; 
				$this->folderBackupDb		= $folderBackupDb;
				$this->fileCommandSmpp 		= $fileCommandSmpp; 
				
			}
		}
		
		function replaceSMS($message){
			$message = str_replace("\r","#\$OD",$message);
			$message = str_replace("\n","#\$OA",$message);
			$message = str_replace(",","#\$2C",$message);
			return $message;
		}
		
		
		function delete_recod_last($id){
			$query  = "delete from `tbl_request_single_command` where trc_id = ".$id." ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);

			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
		}	
		
		function update_status($id,$msisdn){
			$suffix=substr($msisdn, -1);;
			$query  = "update `tbl_history_single_command_".$suffix."` set status=1 ,last_update=NOW() where trc_id ='".$id."' and dnis='".$msisdn."'";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);

			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
		}	
		
		function write_file($data){
		$i=1;
			if(count($data)>0){
				$my_file = date("Y-m-d").'_file_ayo_.cmd';
				foreach($data as $a){
					$handle = fopen($this->fileCommandSmpp.$my_file, 'a') or die('Cannot open file:'.$my_file);
					
					if($i==1){
						$data = 'sendsms,'.$a['ani'].','.$a['dnis'].','.$this->replaceSMS($a['sms_text']).',,,1,0,'.$a['trc_id'].',0'."\r\n";
						fwrite($handle, $data);
					}else{
						$new_data ="\n".'sendsms,'.$a['ani'].','.$a['dnis'].','.$this->replaceSMS($a['sms_text']).',,,1,0,'.$a['trc_id'].',0'."\r\n";
						fwrite($handle, $new_data); 
					}
					
					fclose($handle);
					$this->delete_recod_last($a['trc_id']);
					
					$this->update_status($a['trc_id'],$a['dnis']);
					$i++;
				}
			}else{
				$this->writeLog("no data ");
			}
		}
		
		function writeLog($message){
			$handle = fopen($this->folderLog.$this->path.$this->fileNameLog,"a");
			fwrite($handle,date("Y-m-d H:i:s")." : ".$message."\r\n");
			fclose($handle);
		}
		
		
		function proses(){
			
			$this->get_last();
			$this->write_file($this->dataLast);
		}
		
		function __destruct(){
		
		}
	}
	
	$sendSMS = new sendSMS();
	$sendSMS->proses();
	//print_r($this->dataLast);
	
?>
