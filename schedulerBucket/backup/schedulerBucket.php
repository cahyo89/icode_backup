<?php
/*
Create by cahyo89@gmail.com
10 September 2013
..........hwaiting.........
     ayo al-hazim
.: Man Jadda Wa Jada :. 
*/

	class schedulerBucket{
		function __construct(){
			clearstatcache();
			include  "/projectlbaxl_development/utils/schedulerBucket/configBucket.php";
			require_once  "/projectlbaxl_development/utils/sendMail/sendMail.php";
			$mysqli = new mysqli($hostDb,$userDb,$passDb,$db);

			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error());
			    exit();
			}else{
				$this->data_customer		= array();
				$this->data_reseller		= array();
				$this->db					= $mysqli;
				$this->fileNameLog 			= $fileNameLog;
				$this->folderLog			= $folderLog;
				$this->balance_value    	= $balance_value;
				$this->sender    			= $sender;
				$this->subject    			= $subject;
				$this->Message    			= $Message;
				$this->max_mail    			= $max_mail;
			}
		}
		
		
		
		function send_email($emailnya){
			$destination=array("".$emailnya."");
			
			$ayo = new sendEmail();
			$ayo->sendMailnya($this->sender,$this->subject,$destination,$this->Message );
		}
		
		function update_bucket_reseller($id_customer,$tipe,$channel){
			
			$query = "UPDATE tbl_bucket_status set count_mail =count_mail-1 WHERE id_akun = ".$id_customer." and tipe_akun='".$tipe."' and channel=".$channel; 
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
			
		}
		function update_bucket_customer($id_customer,$tipe,$channel,$id_parent){
			
			$query = "UPDATE tbl_bucket_status set count_mail =count_mail-1 WHERE id_akun = ".$id_customer." and tipe_akun='".$tipe."' and channel=".$channel." and id_parent =".$id_parent; 
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
			
		}
		
		function cek_data_bucket_reseller($id_customer,$tipe,$channel){
			$query  = "SELECT *  from tbl_bucket_status  where id_akun=".$id_customer." and tipe_akun='".$tipe."' and channel=".$channel;
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success :".$query);
				if(($result->num_rows)>0){
					$rw = $result->fetch_array(MYSQLI_ASSOC); 
					return $rw['count_mail'];
				}else{
					return "FALSE";
				}
			}
			else{
				$this->writeLog("query failed : ".$query);
			}	
			
		}
		
		
		
		function cek_data_bucket_customer($id_customer,$tipe,$channel,$id_parent){
			$query  = "SELECT *  from tbl_bucket_status  where id_akun=".$id_customer." and tipe_akun='".$tipe."' and channel=".$channel." and id_parent=".$id_parent;
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success :".$query);
				if(($result->num_rows)>0){
					$rw = $result->fetch_array(MYSQLI_ASSOC); 
					return $rw['count_mail'];
				}else{
					return "FALSE";
				}
			}
			else{
				$this->writeLog("query failed : ".$query);
			}	
			
		}
		
		
		function cek_bucket_customer(){
			
			$query  = " SELECT a.*, b.pic_email  from tbl_customer_balance a join tbl_customer b on a.id_customer = b.id_customer where a.bal_value<".$this->balance_value." or a.expired_date<=NOW() ";
			$result = $this->db->query($query);
			if($result){
				if(($result->num_rows)>0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$this->data_customer[$rw['id']] = $rw;
					}
				$this->writeLog("Got the data !!!");
				}
				$this->writeLog("query success ".$query);
			}
			else{
				$this->writeLog("query failed ".$query);
			}	
			
			//print_r( $this->data_customer);
		}
		
		function cek_bucket_reseller(){
			
			$query  = "SELECT a.*, b.pic_email from tbl_reseller_balance a join tbl_reseller b on a.id_parent = b.id  where a.bal_value<".$this->balance_value." or a.expired_date<=NOW() ";
			$result = $this->db->query($query);
			if($result){
				if(($result->num_rows)>0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$this->data_reseller[$rw['id']] = $rw;
					}
				$this->writeLog("Got the data !!!");
				}
				$this->writeLog("query success ".$query);
			}
			else{
				$this->writeLog("query failed ".$query);
			}	
			
		}
		
		function insert_bucket_stat_customer($tipe_akun,$id_akun,$email,$channel,$id_parent){
			$query = "INSERT INTO tbl_bucket_status ";
			$query .= " ( ";
			$query .= " id_akun, ";
			$query .= " tipe_akun, ";
			$query .= " email, ";
			$query .= " count_mail, ";
			$query .= " channel,"; 
			$query .= " id_parent";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " ".$id_akun.", ";
			$query .= " '".$tipe_akun."', ";
			$query .= " '".$email."', ";
			$query .= " ".$this->max_mail.", ";
			$query .= " ".$channel.",";
			$query .= " ".$id_parent."";
			$query .= " ) ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function insert_bucket_stat_reseller($tipe_akun,$id_akun,$email,$channel){
			$query = "INSERT INTO tbl_bucket_status ";
			$query .= " ( ";
			$query .= " id_akun, ";
			$query .= " tipe_akun, ";
			$query .= " email, ";
			$query .= " count_mail, ";
			$query .= " channel ";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " ".$id_akun.", ";
			$query .= " '".$tipe_akun."', ";
			$query .= " '".$email."', ";
			$query .= " ".$this->max_mail.", ";
			$query .= " ".$channel."";
			$query .= " ) ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function prosesnya(){
			$this->cek_bucket_reseller();
			$this->cek_bucket_customer();
			
			if(!empty( $this->data_reseller)){
				foreach($this->data_reseller as $r) {
					$temp0=$this->cek_data_bucket_reseller($r['id'],"reseller",$r['channel']);
					if($temp0=="FALSE"){
						$this->insert_bucket_stat_reseller("reseller",$r['id'],$r['pic_email'],$r['channel']);
						$this->writeLog("insert data ".$r['pic_email']);
						$this->send_email($r['pic_email ']);
						$this->writeLog("Send Email Reseller to ".$r['pic_email']);
						$this->update_bucket_reseller($r['id'],"reseller",$r['channel']);
					}else{
						if($temp0>0){
							$this->send_email($r['pic_email ']);
							$this->writeLog("Send Email Reseller to ".$r['pic_email']);
							$this->update_bucket_reseller($r['id'],"reseller",$r['channel']);
						}
					}
					
				}
			}
			
			if(!empty($this->data_customer)){
				foreach($this->data_customer as $c) {
					$temp=$this->cek_data_bucket_customer($c['id_customer'],"customer",$c['channel'],$c['id_parent']);
					if($temp=="FALSE"){
						$this->insert_bucket_stat_customer("customer",$c['id_customer'],$c['pic_email'],$c['channel'],$c['id_parent']);
						$this->writeLog("insert data ".$c['pic_email']);
						$this->send_email($c['pic_email']);
						$this->writeLog("Send Email Customer to ".$c['pic_email']);
						$this->update_bucket_customer($c['id_customer'],"customer",$c['channel'],$c['id_parent']);
					}else{
						if($temp>0){
							$this->send_email($c['pic_email']);
							$this->writeLog("Send Email Customer to ".$c['pic_email']);
							$this->update_bucket_customer($c['id_customer'],"customer",$c['channel'],$c['id_parent']);
						}
					}
				}
			}
		}
		
		
		
		
		function writeLog($message){
			$handle = fopen($this->folderLog.$this->fileNameLog,"a");
			if($handle){
				fwrite($handle,date("Y-m-d H:i:s")." : ".$message."\r\n");
				fclose($handle);
			}
		}
		
		
		
		function __destruct(){
		} 
	}
	
	$ayo = new schedulerBucket();
	$ayo->prosesnya();
	//$ayo->send_email("cahyo89@gmail.com"); 
?>