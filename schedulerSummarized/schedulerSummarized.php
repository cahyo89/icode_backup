<?php
set_time_limit(0);

/*
	CREATE TABLE `tbl_summary_transaction_hourly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `id_batch` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `tipe` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tanggal` (`tanggal`),
  KEY `id_batch` (`id_batch`),
  KEY `id_customer` (`id_customer`),
  KEY `tipe` (`tipe`)
) ENGINE=MyISAM 


CREATE TABLE `tbl_scheduler_status` (
  `description` varchar(20) NOT NULL DEFAULT '',
  `scheduler_name` varchar(50) DEFAULT NULL,
  `module_name` varchar(50) DEFAULT NULL,
  `desc_view` varchar(50) DEFAULT NULL,
  `bypass_exec_id` int(11) DEFAULT NULL,
  `last_exec_id` int(11) DEFAULT NULL,
  `last_exec_time` datetime DEFAULT NULL,
  `next_end_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `last_exec_time_file` datetime DEFAULT NULL,
  `add_desc` varchar(255) DEFAULT NULL,
  `next_execute_time` datetime DEFAULT NULL,
  `process_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`description`)
) ENGINE=MyISAM

*/	
	
	class tools{
		
		var $fileNameLog;
		var $db;
		var $varr;
		
		function __construct(){
			include_once "/projectmobads/utils/schedulerSummary/config.php";
			$mysqli = new mysqli($hostDatabase,$userNameDatabase,$passwordDatabase,$database);
			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error());
			    exit();
			}
			else{
				$this->db 			= $mysqli;
				$this->fileNameLog	= $fileNameLog;
				$this->varr 		= $varr;
			}
		}	
		
		
		function writeLog($message){
			$handle = fopen($this->fileNameLog,"a");
			if($handle){
				fwrite($handle,date("Y-m-d H:i:s")." : ".$message."\r\n");
				fclose($handle);
			}
		}
		
		
		function  setTime(){
			
			$date 		= explode(" ",$this->nextExecute);
			$tanggal 	= explode("-",$date[0]);
			$waktu 		= explode(":",$date[1]);
			
			$y = $tanggal[0];
			$m = $tanggal[1];
			$d = $tanggal[2];
			
			$h = $waktu[0];
			$i = $waktu[1];
			$s = $waktu[2];
			
			// hour menit second month day year
			$awal  =  date("Y-m-d H:i:s", mktime(intval($h)-1,intval($i),intval($s),intval($tanggal[1]),intval($tanggal[2]), intval($tanggal[0])));
			$akhir =  date("Y-m-d H:i:s", mktime(intval($h),intval($i),intval($s),intval($tanggal[1]),intval($tanggal[2]), intval($tanggal[0])));
			
			$tanggalDb  =  date("Ymd", mktime(intval($h)-1,intval($i),intval($s),intval($tanggal[1]),intval($tanggal[2]), intval($tanggal[0])));
			
			//$date = explode(" ",$awal);
			$this->tanggalDb = $tanggalDb;
			$this->awal = $awal;
			$this->akhir = $akhir;
		
		}
		
		function updateStatus($status){
			$query = "UPDATE tbl_scheduler_status set status = 0 ";
			$query .= " WHERE description = '".$this->varr."' ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function updateScheduler($time){
			
			$query = "UPDATE tbl_scheduler_status set last_exec_time = '".date("Y-m-d H:i:s")."',status = 1,next_execute_time = '".$time."' ";
			$query .= " WHERE description = '".$this->varr."' ";
			
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function updateSmsSuccess($idBatch,$total){
			$query = "UPDATE tbl_lba_batch set sms_success = sms_success+".$total." WHERE id_batch = ".$idBatch."";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
			
		}
		
		function cekStatus(){
			$query  = "SELECT * from tbl_scheduler_status where description = '".$this->varr."'";
			$result = $this->db->query($query);
				if($result){
					$rw = $result->fetch_array(MYSQLI_ASSOC); 
					
					if($rw['status'] == 1){
						$this->writeLog("scheduler already running");
						return "FALSE";
					}
				    else{
						
						$a = $rw['next_execute_time'];
						$mkTimeNext = strtotime($a);
						
						$data = explode(" ",$a);
						$this->nextExecute = $a; 
						
						$tanggal = explode("-",$data[0]);
						$waktu   = explode(":",$data[1]);
						
						$y = $tanggal[0];
						$m = $tanggal[1];
						$d = $tanggal[2];
						
						$h = $waktu[0];
						$i = $waktu[1];
						$s = $waktu[2];	
						
						// hour menit second month day year
						
						$time = date("Y-m-d H:i:s", mktime(intval($waktu[0])+1, intval($waktu[1]), intval($waktu[2]), intval($tanggal[1]), intval($tanggal[2]), intval($tanggal[0])));
						
						if( mktime() > $mkTimeNext  ){
							$this->updateScheduler($time);
							return "TRUE";	
						}else{
							return "FALSE";
						}
					}
				}else{
					$this->writeLog("query FAILED");
					return "FALSE";
				}
		} 
		
		
		function getIdCustomerByIdBatch($idBatch){
			$idCustomer = 0;
			$query  = "SELECT id_customer FROM tbl_lba_batch where id_batch = ".$idBatch." ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				$rw = $result->fetch_array(MYSQLI_ASSOC);	
				$idCustomer = $rw['id_customer'];
			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
			return $idCustomer;
		}
		
		function insertSummaryHourly($idBatch,$tipe,$total){
			/*
			CREATE TABLE `tbl_summary_transaction_hourly` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`tanggal` datetime DEFAULT NULL,
				`id_batch` int(11) DEFAULT NULL,
				`id_customer` int(11) DEFAULT NULL,
				`tipe` int(11) DEFAULT NULL,
				`total` int(11) DEFAULT NULL,
			*/
			
			$idCustomer = $this->getIdCustomerByIdBatch($idBatch);
			if($idCustomer == "") $idCustomer = "-1";
			
			$query = "INSERT INTO tbl_summary_transaction_hourly ";
			$query .= " ( ";
			$query .= " tanggal, ";
			$query .= " id_batch, ";
			$query .= " id_customer, ";
			$query .= " tipe, ";
			$query .= " total ";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " '".$this->awal."', ";
			$query .= " ".$idBatch.", ";
			$query .= " ".$idCustomer.", ";
			$query .= " ".$tipe.", ";
			$query .= " ".$total." ";
			$query .= " ) ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function getTypeBatch($idBatch){
			$type = 0;
			$query = "select http_intercept_ads_type from tbl_lba_batch WHERE id_batch = ".$idBatch." ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				$rw = $result->fetch_array(MYSQLI_ASSOC);	
				$type = $rw['http_intercept_ads_type'];
			}
			else{
				$this->writeLog("query failed : ".$query);
			}
			return $type;
		}
		
		function loadDataDaily(){
			$query  = "SELECT count(msisdn) as total,id_batch,tipe FROM tbl_daily_history_".$this->tanggalDb." where request_time >='".$this->awal."' AND request_time <= '".$this->akhir."' group by id_batch,tipe";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
				while($rw = $result->fetch_array(MYSQLI_ASSOC)){
					$this->insertSummaryHourly($rw['id_batch'],$rw['tipe'],$rw['total']);
					
					if( $this->getTypeBatch($rw['id_batch']) == $rw['tipe']){
						$this->updateSmsSuccess($rw['id_batch'],$rw['total']);
					}
					
				}
			}			
			else{
				$this->writeLog("query failed : ".$query);
			}
		
		}
		
		
		function __destruct(){
		
		
		}
	}
		
		$do = new tools();
		if($do->cekStatus() == "TRUE"){
			$do->setTime();
			$do->loadDataDaily();
			$do->updateStatus(0);
		}	
	

?>