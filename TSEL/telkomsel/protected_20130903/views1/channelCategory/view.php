<?php
$this->breadcrumbs=array(
	'Basic Channel Rate'=>array('index'),
	$model->name,
);

?>

<h1>View Basic Channel Rate #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'push_price',
		'response_price',
	),
)); ?>


<?php 
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-channel-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'nama',
		array(
			'name'=>'tipe_customer',
			'type'=>'raw',
			'value'=>'Controller::getConstanta("tipe_customer",$data->tipe_customer)',
		),
		array(
			'name'=>'category_customer',
			'value'=>'@$data->TreeId->nama_perangkat',
		),
		array(
			'name'=>'tipe_paket',
			'value'=>'@$data->Paket->jenis_paket',
		),
		'prepaid_value',
	),
));

 ?>