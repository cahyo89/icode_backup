<?php
$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->nama=>array('view','id'=>$model->id_customer),
	'Update',
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Customer #<?php echo $model->nama; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'customer/view','img'=>'/images/view.png','id'=>$model->id_customer,'conf'=>''),
	   array('label'=>'List','link'=>'customer/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'customer/view','img'=>'/images/view.png','id'=>$model->id_customer,'conf'=>''),
	   array('label'=>'List','link'=>'customer/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>