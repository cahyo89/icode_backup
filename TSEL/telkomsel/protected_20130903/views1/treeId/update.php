<?php
$this->breadcrumbs=array(
	'Customer Category'=>array('index'),
	$model->nama_perangkat=>array('view','id'=>$model->tree_id),
	'Update',
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Customer Category #<?php echo $model->nama_perangkat; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'treeId/view','img'=>'/images/view.png','id'=>$model->tree_id,'conf'=>''),
	   array('label'=>'List','link'=>'treeId/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'treeId/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'treeId/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'treeId/view','img'=>'/images/view.png','id'=>$model->tree_id,'conf'=>''),
	   array('label'=>'List','link'=>'treeId/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>