<?php

class DetailPrepaidController extends Controller
{

	public function filters()
	{
		return array(
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionIndex()
	{
		$model=new DetailPrepaid;
		$model->unsetAttributes();
		
		if(isset($_GET['DetailPrepaid']))
		{	
			$this->redirect(array('search','dateStart'=>$_GET['dateStart'],'dateEnd'=>$_GET['dateEnd'],'customer'=>$_GET['DetailPrepaid']['id_customer']));
		}
		
		$this->render('search',array(
			'model'=>$model,
		));
	}
	
	public function actionSearch()
	{
		/*
		$criteria = new CDbCriteria;
		//$criteria->addCondition("status between 1 and 2");
		$criteria->addCondition("status = 1");
		$model = new CActiveDataProvider('DetailPrepaid',array(
					'criteria'=>$criteria,
					'pagination'=>array(
						'pageSize'=>Yii::app()->params['paging'],
					),
				));
		//$model->unsetAttributes();  // clear any default values
		*/
		$dateStart = $_GET['dateStart'];
		$dateEnd = $_GET['dateEnd'];
		$customer = $_GET['customer'];
		$where="";
		if($dateStart != "")
		{
			$where .= " and first_update > '$dateStart 00:00:00'";
		}
		if($dateEnd != "")
		{
			$where .= " and first_update < '$dateEnd 23:59:59'";
		}
		if($customer != "")
		{
			$where .= " and id_customer = '$customer'";
		}
		$sql = "select sum(bayar) as bayar,first_update from tbl_detail_prepaid where status = 1 ".$where." group by first_update";

		$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM ('.$sql.') tbl')->queryScalar();
		$model=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
		if(isset($_GET['DetailPrepaid']))
			$model->attributes=$_GET['DetailPrepaid'];
		$this->render('index',array(
			'model'=>$model,
		));
	}
	
	public function actionView($id)
	{
		$model1 = Yii::app()->db->createCommand("select sum(prepaid_value) as prepaid_value, sum(bayar) as bayar, sum(harga) as harga, first_update,first_user,first_ip,last_update,last_user,last_ip from tbl_detail_prepaid where id in (".$id.") group by first_update")->queryRow();
		//$model = DetailPrepaid::model()->findAll($id);
		$sql = "select * from tbl_detail_prepaid where id in (".$id.")";
		$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM ('.$sql.') tbl')->queryScalar();
		$model=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
		$this->render('view',array(
			//'model'=>$this->loadModel($id),
			'model'=>$model,
			'model1'=>$model1,
			'poid'=>$id,
		));
	}
	
	public function actionApp($id,$reason)
	{
		if(empty($reason))
		{
			echo "<script>alert('Reason must be filled');</script>";
			$this->redirect(array('view','id'=>$id));
		}
		else
		{
			$ids = explode(',',$id);
			$idbulk = "BPO_".date('Ymd').rand(10,100);
			foreach($ids as $id)
			{
				$current = DetailPrepaid::model()->findByPk($id);
				$current->id_bulk = $idbulk;
				$current->reason = $reason;
				$current->status = 2;
				$current->last_user = Yii::app()->user->first_name;
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_update =  date('Y-m-d h:i:s');
				$current->update();
				
				$expdate = 0;
				if($current->discount < 10)
				{
					$expdate = 3;
				}
				else if($current->discount >= 10 && $current->discount < 20)
				{
					$expdate = 6;
				}
				else
				{
					$expdate = 12;
				}

				$currentS= SettingBalance::model()->findByPk($current->id_customer);
				if($currentS == null)
				{
					continue;
				}
				$modelPaketExpired = CHtml::listData(Paket::model()->findAll(),'id_paket','expired_paket');
				$currentS->approved_prepaid = 1;
				$currentS->reason_prepaid = NULL;
				$currentS->prepaid_value  = $currentS->prepaid_value+$current->prepaid_value;

				
				$currentS->temp_prepaid = 0;
				$currentS->last_ip = CHttpRequest::getUserHostAddress();
				$currentS->last_user = Yii::app()->user->first_name;
				$currentS->update();
				
				$currentVR= new VoucherOrder;				
				$currentVR->id_customer = $current->id_customer;
				$currentVR->voucher_type = $currentS->tipe_paket;
				$currentVR->total = $currentS->prepaid_value; 
				$currentVR->remain = $currentS->prepaid_value;
				$currentVR->start = date('Y-m-d'); 
				
				$dateNow = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " +".$expdate." day");
				$dateExp = strtotime($model->expired_date);
					
					if($dateNow > $dateExp)
						$model->expired_date = date('Y-m-d',$dateNow);
						
				if($currentS->tipe_customer ==0)
					$currentVR->end = $currentS->expired_date;
					
				$currentVR->first_user = Yii::app()->user->first_name;
				$currentVR->first_ip = CHttpRequest::getUserHostAddress();
				$currentVR->first_update = date('Y-m-d h:i:s');
				$currentVR->last_user = Yii::app()->user->first_name;
				$currentVR->last_ip = CHttpRequest::getUserHostAddress();
				$currentVR->last_update =  date('Y-m-d h:i:s');
				$currentVR->save();
				
				$berita = "Approved Setting Balance Pertama untuk Customer = ".$current->Customer->nama." Oleh ".Yii::app()->user->first_name;
				Controller::addberita($berita);
			}
			$this->redirect(array('index'));
		}

	}
	
	public function actionReject($id,$reason)
	{
		if(empty($reason))
		{
			echo "<script>alert('Reason must be filled');</script>";
			$this->redirect(array('view','id'=>$id));
		}
		else
		{
			$ids = explode(',',$id);
			$idbulk = "BPO_".date('Ymd').rand(10,100);
			foreach($ids as $id)
			{
				$current = DetailPrepaid::model()->findByPk($id);
				$current->status = 3;
				$current->reason = $reason;
				$current->last_user = Yii::app()->user->first_name;
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_update =  date('Y-m-d h:i:s');
				$current->update();
				$berita = "Rejected Setting Balance Pertama untuk Customer = ".$current->Customer->nama." Oleh ".Yii::app()->user->first_name;
				Controller::addberita($berita);
			}
			$this->redirect(array('index'));
		}

	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Discount the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DetailPrepaid::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}