<?php

class BatchDailyHourlyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		
		
		
			
		if(isset($_GET['excel'])){
			$count=Yii::app()->db->createCommand('SELECT COUNT(DISTINCT(DATE_FORMAT(tanggal,"%Y-%m-%d %H"))) FROM '.$id.'')->queryScalar();
			$sql='SELECT id,sum(case when success = 1 then 1 else 0 end) as success,sum(case when failed = 1 then 1 else 0 end) as failed,DATE_FORMAT(tanggal,"%Y-%m-%d %H") as waktu FROM '.$id.' group by waktu';
			$dataProvider=new CSqlDataProvider($sql, array(
				'totalItemCount'=>$count,
				'pagination'=>array(
					'pageSize'=>999999,
				),
			));
			
			Yii::app()->request->sendFile('BatchDailyHourly_'.$id.'.xls',
				$this->renderPartial('excel',array(
						'model'=>$dataProvider,
					),true)
			);
		}
		else{
			$count=Yii::app()->db->createCommand('SELECT COUNT(DISTINCT(DATE_FORMAT(tanggal,"%Y-%m-%d %H"))) FROM '.$id.'')->queryScalar();
			$sql='SELECT id,sum(case when success = 1 then 1 else 0 end) as success,sum(case when failed = 1 then 1 else 0 end) as failed,DATE_FORMAT(tanggal,"%Y-%m-%d %H") as waktu FROM '.$id.' group by waktu';
			$dataProvider=new CSqlDataProvider($sql, array(
				'totalItemCount'=>$count,
				'pagination'=>array(
					'pageSize'=>Yii::app()->params['paging'],
				),
			));
			
			$this->render('view',array(
					'dataProvider'=>$dataProvider,
			));
		}
	}


	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new BatchDailyHourly('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BatchDailyHourly'])){
			$model->attributes=$_GET['BatchDailyHourly'];
			if($model->validate())
				$this->redirect(array('view','id'=>$model->jobs_id));
		}
		$this->render('index',array(
			'model'=>$model,
		));
	}
	
}
