<?php

class Ani2Controller extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	 
	public function actionCekDataType()
        {
                //Handler for _form updates when perspective_id is changed.
                if (Yii::app()->request->isAjaxRequest)
                {
                        
                        //pick off the parameter value
                        $dataType = Yii::app()->request->getParam( 'dataType' );
                        if($dataType == 5)
                        {
                        //we are going to hide the insider_div block on _form.
                        //by changing the style attribute on the form block div.
                        echo "display:none";
                        Yii::app()->end();
                        }
                        else
                        {
                        //display the insider_div  block on _form
                        echo "display:block";
                        Yii::app()->end();
                        }
                        
                }
        }
	 
	public function actionApp($id)
	{
		$current= Ani2::model()->findByPk($id);
		$current->status = 1;
		$berita = "Approved Ani dengan Ani = $current->ani ";
		$current->last_ip = CHttpRequest::getUserHostAddress();
		$current->last_user = Yii::app()->user->first_name;
		$current->update();
		Controller::addberita($berita);
		
		$this->redirect(array('index'));
	}
	
	public function actionReject($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Ani2']))
		{
			$model->attributes=$_POST['Ani2'];
			if($model->save()){
				$current= Ani2::model()->findByPk($id);
				$current->status = 2;
				$berita = "Reject Ani dengan Ani = $current->ani ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}

		$this->render('reject',array(
			'model'=>$model,
		));
	}
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Ani2;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ani2']))
		{
			$model->attributes=$_POST['Ani2'];
			$modelCeks = CHtml::listData(Ani2::model()->findAllByAttributes(array('ani'=>$model->ani,'flag_deleted'=>1)),'ani','id');
			$model->status = 0 ;
			if($model->media_type  == 5){
				if($model->svc_code !== "0")
					$model->ani = $model->vas_id.$model->svc_code;
				else
					$model->ani = $model->vas_id;
			}
			
			if($model->id_customer == "")
				$model->id_customer = -1;
				
			if($model->id_customer == -1)
				$customer = "ALL";
			else
				$customer = $model->Customer->nama;
				
			if(!isset($modelCeks[$model->ani])){
				
				$model->first_ip = CHttpRequest::getUserHostAddress();
				$model->first_user = Yii::app()->user->first_name;
				$model->first_update = date("Y-m-d H:i:s");
				$model->last_ip = CHttpRequest::getUserHostAddress();
				$model->last_user = Yii::app()->user->first_name;
				$model->last_update = date("Y-m-d H:i:s");
				
				
				if($model->save()){
					//Controller::afterCreate("Ani2",$model->id);
					Controller::addberita("Create Customer Shortcode : Ani : $model->ani , Customer : ".$customer." ");
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			else
			{
				$modelU->last_ip = CHttpRequest::getUserHostAddress();
				$modelU->last_user = Yii::app()->user->first_name;
				$modelU->last_update = date("Y-m-d H:i:s");
				
				$modelU=$this->loadModel($modelCeks[$model->ani]);
				$modelU->attributes=$_POST['Ani2'];
				$modelU->status = 0 ;
				$modelU->flag_deleted = 0; 
				if($modelU->save()){
					
					//Controller::afterCreate("Ani2",$modelU->id);
					
					Controller::addberita("Create Customer Shortcode : Ani : $model->ani , Customer : ".$customer." ");
					$this->redirect(array('view','id'=>$modelU->id));
				}
			
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ani2']))
		{
			$model->attributes=$_POST['Ani2'];
			
			if($model->media_type  == 5){
				if($model->svc_code !== "0")
					$model->ani = $model->vas_id.$model->svc_code;
				else
					$model->ani = $model->vas_id;
			}
			
			if($model->id_customer == "")
				$model->id_customer = -1;
			
			if($model->id_customer == -1)
				$customer = "ALL";
			else
				$customer = $model->Customer->nama;
				
			if($modelOld->id_customer == -1)
				$customerOld = "ALL";
			else
				$customerOld = $modelOld->Customer->nama;
				
				
				$model->last_ip = CHttpRequest::getUserHostAddress();
				$model->last_user = Yii::app()->user->first_name;
				$model->last_update = date("Y-m-d H:i:s");
				
			if($model->save()){
				//Controller::afterUpdate("Ani2",$model->id);
				Controller::addberita("Update Customer Shortcode : Ani : $modelOld->ani -> $model->ani , Customer : ".$customerOld." -> ".$customer."");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if(Controller::cekExist($id,"customer_ani","Customer")){
				$model=$this->loadModel($id);
				
				if($model->id_customer == -1)
					$customer = "ALL";
				else
					$customer = $model->Customer->nama;
				
				Controller::beforeDelete('Ani2',$id);
				Controller::deleted('Ani2',$id);
				Controller::addberita("Delete Customer Shortcode : Ani : $model->ani , Customer : ".$customer." ");
					
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Ani2;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Ani2']))
			$model->attributes=$_GET['Ani2'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Ani2::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ani-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
