<?php

class GlobalWhitelistController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new GlobalWhitelist;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GlobalWhitelist']))
		{
			$model->attributes=$_POST['GlobalWhitelist'];
			if($model->save()){
				$G = GlobalConfigurationSubs::model()->find();
				
				$modelHistory = new HistoryGlobalblackwhitelist;
				$modelHistory->msisdn = $model->subscriber_number;
				$modelHistory->shortcode = $G->shortcode;
				$modelHistory->sms_text = $G->keyword_on_whitelist;
				$modelHistory->action = 1;
				$modelHistory->first_user = Yii::app()->user->first_name;
				$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
				$modelHistory->first_update = date('Y-m-d h:i:s');
				$modelHistory->last_user = Yii::app()->user->first_name;
				$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
				$modelHistory->last_update =  date('Y-m-d h:i:s');
				$modelHistory->save();
				
				Controller::afterCreate("GlobalWhitelist",$model->id);
				Controller::addberita("Create Global Whitelist : Number : $model->subscriber_number");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GlobalWhitelist']))
		{
			$model->attributes=$_POST['GlobalWhitelist'];
			if($model->save()){
				Controller::afterUpdate("GlobalWhitelist",$model->id);
				Controller::addberita("Create Global Whitelist : Number : $modelOld->subscriber_number -> $model->subscriber_number");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			Controller::beforeDelete('GlobalWhitelist',$id);
			Controller::addberita("Delete Global Whitelist : Number : $model->subscriber_number");
			$G = GlobalConfigurationSubs::model()->find();
			
			//$model = $this->loadModel($id);
			$modelHistory = new HistoryGlobalblackwhitelist;
			$modelHistory->msisdn = $model->subscriber_number;
			$modelHistory->shortcode = $G->shortcode;
			$modelHistory->sms_text = $G->keyword_off_whitelist;
			$modelHistory->action = 2;
			$modelHistory->first_user = Yii::app()->user->first_name;
			$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
			$modelHistory->first_update = date('Y-m-d h:i:s');
			$modelHistory->last_user = Yii::app()->user->first_name;
			$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
			$modelHistory->last_update =  date('Y-m-d h:i:s');
			$modelHistory->save();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new GlobalWhitelist('search');
		//$model->unsetAttributes();  // clear any default values
		if(isset($_POST['GlobalWhitelist'])){
			$model->attributes=$_POST['GlobalWhitelist'];
			//echo "ddodododo";
			$itu=CUploadedFile::getInstance($model,'filee');
			//echo $itu;
			if (substr($itu,-3) == "xls") {
				require_once('/nfs_data/projectmobads/program/web_admin/protected/extensions/php-excel-reader-2.21/excel_reader2.php');
				
				
				$path=Yii::app()->basePath.'/dataTemp/'. $itu;
				//$model->image->saveAs(Yii::app()->basePath . '/../images/' . $model->image);
				$itu->saveAs($path);
				$data = new Spreadsheet_Excel_Reader($path);
				$subscriber_number=array();
				$nama=array();
				for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++) 
				{
					if(isset($data->sheets[0]['cells'][$j][1])){
						$dataExcel[$j]['num'] = $data->sheets[0]['cells'][$j][1];
					}
				}
				
				
				$G = GlobalConfigurationSubs::model()->find();
				foreach($dataExcel as $id=>$value)
				{
				
					
					$modelW=new GlobalWhitelist;
					
					$modelW->subscriber_number=$value['num'];
					$modelW->first_user = Yii::app()->user->first_name;
					$modelW->first_ip = CHttpRequest::getUserHostAddress();
					$modelW->first_update = date('Y-m-d h:i:s');
					$modelW->last_user = Yii::app()->user->first_name;
					$modelW->last_ip = CHttpRequest::getUserHostAddress();
					$modelW->last_update =  date('Y-m-d h:i:s');
					if($modelW->save()){
						Controller::addberita("Create Global Whitelist : Number : $modelW->subscriber_number");
						$modelHistory = new HistoryGlobalblackwhitelist;
						$modelHistory->msisdn = $value['num'];
						$modelHistory->shortcode = $G->shortcode;
						$modelHistory->sms_text = $G->keyword_on_whitelist;
						$modelHistory->action = 1;
						$modelHistory->first_user = Yii::app()->user->first_name;
						$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
						$modelHistory->first_update = date('Y-m-d h:i:s');
						$modelHistory->last_user = Yii::app()->user->first_name;
						$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
						$modelHistory->last_update =  date('Y-m-d h:i:s');
						$modelHistory->save();
					}
				}
			   
				unlink($path);
			}		
			else
			{
				Yii::app()->user->setFlash('error', "File must Excel 2003(*.xls)!");
			}
			
		}
		
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionExport()
	{	
		$model=GlobalWhitelist::model()->findAll();
			
			Yii::app()->request->sendFile('GlobalWhitelist.xls',
				$this->renderPartial('excel',array(
						'model'=>$model,
					),true)
			);
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GlobalWhitelist::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='global-whitelist-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
