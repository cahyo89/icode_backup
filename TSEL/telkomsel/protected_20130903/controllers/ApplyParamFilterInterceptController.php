<?php

class mysocketNMS
	{
		var $state=array();
		var $state2=array(array());
		var $history=array(array());
		var $fp;
		var $dataCoumplete;
		
		function __construct($cfgServer, $cfgPort, $cfgTimeOut)
		{
			//return true;
			$errno = "ERRNO";
			$errstr = "ERRSTR";
			$this->fp = fsockopen($cfgServer, $cfgPort, $errno, $errstr, $cfgTimeOut);
			if (!$this->fp) {
				//echo "$errstr ($errno)<br />\n";
				//exit;
				return false;
			}
			return true;
		}
		function getStateCoumplete($cmd)
		{
			if($this->fp)
			{
				@fwrite($this->fp, $cmd);
				while (!@feof($this->fp)) 
					$this->dataCoumplete .= @fgets($this->fp, 128);
				$this->dataCoumplete = strstr($this->dataCoumplete, "\n");
				//tree_id,state,localize	
				@fclose($this->fp);
				return true;
			}
			else
			{
			//	print "Cannot execute state command";
				return false;
			}
		}
		function getStateComplete($cmd)
		{
			return $this->getStateCoumplete($cmd);
		}
	}


class ApplyParamFilterInterceptController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	


	

	/**
	 * Lists all models.
	 */
	 
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$modela=new ApplyParam;
		
			if(isset($_GET['ApplyParam'])){
				ini_set("memory_limit","500M");
				$pathParamKeyword = "/nfs_data/projectmobads/commandlba/"."keyword.lst";
				$pathLocationLst = "/nfs_data/projectmobads/commandlba/"."lacList.lst";
				$pathUrlLst = "/nfs_data/projectmobads/commandlba/"."urlList.lst";
				$pathUserAgentLst = "/nfs_data/projectmobads/commandlba/"."uaHandsetList.lst";
				
				$this->writeParamKeyword($pathParamKeyword);
				
				$this->writeLocationLst($pathLocationLst);
				//echo "d";
				$this->writeUrlLst($pathUrlLst);
				//echo "c";
				$this->writeUserAgentLst($pathUserAgentLst);
				//echo "z";
				
				/*
				$lokasiapply = $pathCommand."prs1.cmd";
				
					$this->writeFile($lokasiapply,"reload\n","a+");
				
				$lokasiapply = $pathCommand."prs2.cmd";
				
					$this->writeFile($lokasiapply,"reload\n","a+");
				*/
				
				$cfgServer = "10.10.227.171";
				$cfgPort = 8023;
				$cfgTimeOut = 10;
	
				$mysocket = new mysocketNMS($cfgServer, $cfgPort, $cfgTimeOut);
				//$mysocket->getStateComplete("reinit\r\n");
				$mysocket->getStateComplete("reinit\r\nq\r\n");
				
				$cfgServer = "10.10.227.172";
				$cfgPort = 8023;
				$cfgTimeOut = 10;
	
				$mysocket = new mysocketNMS($cfgServer, $cfgPort, $cfgTimeOut);
				//$mysocket->getStateComplete("reinit\r\n");
				$mysocket->getStateComplete("reinit\r\nq\r\n");
				
				
				
				Yii::app()->user->setFlash('error', "Success Apply Parameter.");
				Controller::addberita("Success Apply Parameter [Filter Intercept]");
				$this->redirect(array('index'));
				
			}
		$this->render('index',array(
			'model'=>$modela,
		));
	}
	
	public function writeParamKeyword ($fileKeywordLst){
		
		$content="";
		$model = ParamKeyword::model()->findAll();
		$handle = fopen($fileKeywordLst,"w");
		if($handle){
			foreach($model as $rw)
			{
				$explode = array();
				$explode = explode(",",$rw['param_name'].",");
				foreach($explode as $idx => $value){
					if($value != "")
						fwrite($handle,str_replace(",","#$2C",$value).",".$rw['id'].",".$rw['flag_blacklist']."\r\n");	
				}
			}
			fclose($handle);
			@chmod($fileKeywordLst, 0777);
		}
	
	}
	
	public function writeUserAgentLst($fileUALst){
		/*
			CREATE TABLE `tbl_param_user_agent` (
			  `id` int(11) NOT NULL auto_increment,
			  `name` varchar(50) default NULL,
			  `param_name` blob,
			  `first_user` varchar(50) default NULL,
			  `first_update` datetime default NULL,
			  `first_ip` varchar(25) default NULL,
			  `last_user` varchar(50) default NULL,
			  `last_update` datetime default NULL,
			  `last_ip` varchar(25) default NULL,
			  PRIMARY KEY  (`id`)
			) ENGINE=MyISAM
		*/	
			$content="";
			$model = ParamUserAgent::model()->findAll();
			$handle = fopen($fileUALst,"w");
			if($handle){
				foreach($model as $rw)
				{
					//$explode = array();
					//$explode = explode(",",$rw['param_name'].",");
					//foreach($explode as $idx => $value){
						//if($value != "")
							fwrite($handle, str_replace(",","#$2C",$rw['param_name']).",".$rw['id'].",".$rw['script_name'].",".$rw['flag_blacklist']."\r\n");
					//}
				}
				fclose($handle);
				@chmod($fileUALst, 0777);
			}
	}		
	
	
	function writeUrlLst($fileUrlLst){
			/*
				CREATE TABLE `tbl_filter_url_intercept` (
			  `id` int(11) NOT NULL auto_increment,
			  `url_name` varchar(100) NOT NULL,
			  `url_detail` blob NOT NULL,
			  `first_user` varchar(100) NOT NULL,
			  `first_update` datetime NOT NULL,
			  `first_ip` varchar(50) NOT NULL,
			  `last_user` varchar(100) NOT NULL,
			  `last_update` datetime NOT NULL,
			  `last_ip` varchar(50) NOT NULL,
			  `flag_deleted` int(11) NOT NULL,
			  PRIMARY KEY  (`id`)
			)	 
			*/
			$content="";
			$model = FilterUrlIntercept::model()->findAll();
				$arrayNewUrl 	= array();
				$arrayBlackList = array();
				foreach($model as $rw)
				{
					$explode = array();
					$explode = explode(",",$rw['url_detail'].",");
					
					
					foreach($explode as $idx => $value){
						if($value != ""){
							if(!isset($arrayNewUrl[$value])){
								$arrayNewUrl[$value] = $rw['id'];
							}
							else{
								$arrayNewUrl[$value] = $arrayNewUrl[$value].";".$rw['id'].";";
							}
							$arrayBlackList[$value]	= $rw['flag_blacklist'];
						}
					}
				}
				
				$handle = fopen($fileUrlLst,"w");
				if($handle){
					foreach($arrayNewUrl as $idx => $value){
					fwrite($handle,$idx.",".rtrim($value,";").",".$arrayBlackList[$idx]."\r\n");
					}
				fclose($handle);
				@chmod($fileUrlLst, 0777);
				}			
	}
	
	
	function writeLocationLst($fileLocationLst){
			// format  lac,ci,groupId
			
			$arrayLacCi = array();
			$model = Bts::model()->findAll("type = 0 and(flag_deleted <> 1 or flag_deleted is null)");
			foreach($model as $rw)
			{
				$arrayLacCi[$rw['id']] 			= $rw['lac'].",".$rw['bts_id'];
				$arrayFlagBlacklist[$rw['id']]	= $rw['flag_blacklist'];
				$arrayLongLat[$rw['id']]		= $rw['longitude'].",".$rw['latitude'].",".str_replace(",",":",$rw['bts_name']);
			}
			
			$arrayGroup = array();
			$model = BtsGroupDetail::model()->findAll();
			
			foreach($model as $rw)
			{
				if(!isset($arrayGroup[$rw['id_bts']])){
						$arrayGroup[$rw['id_bts']] = $rw['id_group']."";
					}
					else{
						if( substr_count($arrayGroup[$rw['id_bts']],$rw['id_group']) == 0 ){
							$arrayGroup[$rw['id_bts']] = $arrayGroup[$rw['id_bts']].";".$rw['id_group'];
					    }
					}
			}
			
			$handle = fopen($fileLocationLst,"w");
			if($handle){
				if(count($arrayGroup) > 0){	
					foreach($arrayGroup as $idx => $value){
						fwrite($handle,$arrayLacCi[$idx].",".$value.",".$arrayFlagBlacklist[$idx].",".$arrayLongLat[$idx]."\r\n");
					}
				}
				fclose($handle);
				@chmod($fileLocationLst, 0777);
			}
		}
	
	
		
		public function writeFile($location,$text,$method)
		{
			$openfile = fopen($location,$method);
			if(is_writeable($location))
			{
				fwrite($openfile,$text);
			}
			fclose($openfile);
			@chmod($location, 0777);
		}
}
