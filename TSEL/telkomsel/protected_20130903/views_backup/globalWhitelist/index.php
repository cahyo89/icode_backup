<?php
$this->breadcrumbs=array(
	'Global Whitelists',
);

$this->menu=array(
	array('label'=>'Create GlobalWhitelist', 'url'=>array('create')),
	array('label'=>'Manage GlobalWhitelist', 'url'=>array('admin')),
);
?>

<h1>Global Whitelists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
