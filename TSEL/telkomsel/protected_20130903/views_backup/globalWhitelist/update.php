<?php
$this->breadcrumbs=array(
	'Global Whitelists'=>array('index'),
	$model->subscriber_number=>array('view','id'=>$model->id),
	'Update',
);

?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Global Whitelist #<?php echo $model->subscriber_number; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'globalWhitelist/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'globalWhitelist/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'globalWhitelist/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'globalWhitelist/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'globalWhitelist/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'globalWhitelist/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>