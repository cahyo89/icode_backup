<?php
$this->breadcrumbs=array(
	'Lac Profiles'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('lac-profile-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Lac Profile</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'lacProfile/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'lac-profile-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'profile_name',
		'description',
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update} {delete} {detail}',
			'buttons' => array(
				
				'delete' => array
		        (
		        	'visible'=>'Controller::cekExist($data->id,"id_profile","LacProfileDetail") ? true : false',
		        ),
				
				'detail' => array(
					'label'=>'Detail',
					'url' => 'Yii::app()->createUrl("/lacProfileDetail/index", array("LPid"=>$data->id))',
					'imageUrl' => Yii::app()->request->baseUrl . '/images/listGrid.png',
				),
			),
			'htmlOptions' => array('width'=>'80'),
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
		
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'lacProfile/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>
