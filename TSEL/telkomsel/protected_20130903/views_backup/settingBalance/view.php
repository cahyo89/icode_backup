<?php
$this->breadcrumbs=array(
	'Balance'=>array('index'),
	$model->nama,
);


?>

<h1>View Balance #<?php echo $model->nama; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nama',
		array(
			'name'=>'tipe_customer',
			'type'=>'raw',
			'value'=>Controller::getConstanta("tipe_customer",$model->tipe_customer),
		),
		array(
			'label'=>'tipe_paket',
			'name'=>'Paket.jenis_paket',
		),
		'temp_prepaid',
		'reason_prepaid',
		
	),
)); 
Controller::createInfo($model);
if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->getUsermode()==0){
	if($model->approved_prepaid == 0 || $model->approved_prepaid == 2){
		//echo CHtml::button('Reject', array('submit' => array('reject','id'=>$model->id_customer),'confirm'=>'Are you sure want to REJECT this Setting Balance?')); 
		//echo CHtml::button('Approve', array('submit' => array('app','id'=>$model->id_customer),'confirm'=>'Are you sure want to APPROVE this Setting Balance?')); 
	}
}
?>
