<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="row">
		<?php echo $form->label($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipe_customer'); ?>
		<?php echo $form->textField($model,'tipe_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prepaid_value'); ?>
		<?php echo $form->textField($model,'prepaid_value'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->label($model,'approved_prepaid'); ?>
		<?php echo $form->dropDownList($model,'approved_prepaid',Yii::app()->params['tipe_customer'],array('empty'=>'--Please Select One--')); ?>
		</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->