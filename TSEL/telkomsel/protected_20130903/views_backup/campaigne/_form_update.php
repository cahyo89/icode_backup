<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'campaigne-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Campaigne Id'); ?>
		<div class="read">
			<?php echo Chtml::activeTextField($model,'jobs_id',array('size'=>60,'maxlength'=>200,'readonly'=>true)); ?>
		</div>
		<?php echo $form->error($model,'jobs_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Topic'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100))."*"; ?>
		<?php echo $form->error($model,'topik'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'Start Periode'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
				    'model'=>$model,
				    'name'=>'start_periode',
				));
				echo "*";
				?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Content Expired'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
				    'model'=>$model,
				    'name'=>'content_expired',
				));
				echo "*";
				?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Total Requested Number'); ?>
		<?php echo $form->textField($model,'total_number')."*"; ?>
		<?php echo $form->error($model,'total_number'); ?>
	</div>
	
	<div class="hidden">
		<?php echo $form->labelEx($model,'Campaign SMS Text'); ?>
		<?php echo $form->textArea($model,'sms_text',array('cols'=>50,'rows'=>10,'onkeyup'=>'{onEventSMSText();}'))."*"; ?>
		<?php echo $form->error($model,'sms_text'); ?>
	</div>
	
	
	
	<div class="hidden">
		Length SMS <input name="Campaigne[length_text]" value="0" size="3" id="Campaigne_length_text" readonly="true"  type="text">
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
	
	
	<input name="Campaigne[message_type]" value="1" id="Campaigne_message_type" style="display:none" type="text">
	<input name="Campaigne[message_bit]" value="7" id="Campaigne_message_bit" style="display:none" type="text">
	<input name="Campaigne[binary_data]" value="0" id="Campaigne_binary_data" style="display:none" type="text">
	<input name="Campaigne[promotion_media]" value="0" id="Campaigne_promotion_media" style="display:none" type="text">
	<input name="Campaigne[sex_category]" value="0" id="Campaigne_sex_category" style="display:none" type="text">
	<input name="Campaigne[age_category]" value="0" id="Campaigne_age_category" style="display:none" type="text">
	<input name="Campaigne[status_category]" value="0" id="Campaigne_status_category" style="display:none" type="text">
	<input name="Campaigne[filter_prefix]" value="0" id="Campaigne_filter_prefix" style="display:none" type="text">
	<input name="Campaigne[education_category]" value="0" id="Campaigne_education_category" style="display:none" type="text">
	<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
	<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
	<input name="Campaigne[quota]" value="0" id="Campaigne_quota" style="display:none" type="text">
	<input name="Campaigne[vip]" value="1" id="Campaigne_vip" style="display:none" type="text">
	<input name="Campaigne[increase_counter]" value="1" id="Campaigne_increase_counter" style="display:none" type="text">
	<input name="Campaigne[unique_subscriber_per_batch]" value="1" id="Campaigne_unique_subscriber_per_batch" style="display:none" type="text">
	<input name="Campaigne[flag_hour]" value="0" id="Campaigne_flag_hour" style="display:none" type="text">
	<input name="Campaigne[multimedia_type]" value="0" id="Campaigne_multimedia_type" style="display:none" type="text">
	<input name="Campaigne[flag_file]" value="0" id="Campaigne_flag_file" style="display:none" type="text">
	
	
<script type="text/javascript">
	
	
	
	function onEventSMSText(){
		var smsText =  document.getElementById('Campaigne_sms_text').value;
		document.getElementById('Campaigne_length_text').value = smsText.length;
	}
	
	
	function onChangeChannelCategory(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeChannelCategory'),
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_customer_ani").html(data.customer_ani);
	                 }
	                 ',
	                ))?>;
					
					if(document.getElementById('Campaigne_channel_category').value == 5){
						document.getElementById('Campaigne_mmsFile').disabled = false;
					}
					else{
						document.getElementById('Campaigne_mmsFile').disabled = true;
					}
					
	        return false;
	}
	
	function onChangeCampaigneType(){
		if(document.getElementById('Campaigne_batch_type').value == 1 || document.getElementById('Campaigne_batch_type').value == 5){
			document.getElementById('Campaigne_blackWhiteListFile').disabled = false;
		}
		else{
			document.getElementById('Campaigne_blackWhiteListFile').disabled = true;
		}
	}
	
	
	function onChangeCustomer(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeCustomer'),
	 
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_jobs_id").val(data.jobs_id);
						$("#Campaigne_customer_ani").html(data.customer_ani);
	                 }
	                 ',
	                ))?>;
					
	        return false;
	}

</script>

	
<?php $this->endWidget(); ?>
</div><!-- form -->