<?php
$this->breadcrumbs=array(
	'Campaignes'=>array('index'),
	$model->jobs_id,
);

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>View Campaign #<?php echo $model->jobs_id; ?></h1></td>
<td><div class="operatorRight"><?php  
	   if($model->status_batch == 0){	
		   Controller::createMenu(array(
		   array('label'=>'Update','link'=>'Campaigne/update','img'=>'/images/update.png','id'=>$model->id_batch,'conf'=>''),
		   array('label'=>'Delete','link'=>'Campaigne/delete','img'=>'/images/delete.png','id'=>$model->id_batch,'conf'=>$model->jobs_id),
		   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
			)); 
	   }
	   else{
		   Controller::createMenu(array(
		   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
			));	
	   }	   
		
		?></div></td>
					</tr></table>

<?php 					
$columns[] = array(
				'label'=>'Campaign Id',
				'name'=>'jobs_id'
				);	
$columns[] = array(
				'label'=>'Topic',
				'name'=>'topik'
				);
$columns[] = array(
				'label'=>'Start Periode',
				'name'=>'start_periode'
				);
$columns[] = array(
				'label'=>'Content Expired',
				'name'=>'content_expired'
				);
$columns[] = array(
				'label'=>'Status Batch',
				'value'=>Yii::app()->params['status_batch'][$model->status_batch]
				);
$columns[] = array(
				'label'=>'Total Number Requested',
				'name'=>'total_number'
				);
$columns[] = array(
				'label'=>'Campaign Type',
				'value'=>'Normal'
				);
					
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$columns,
	
)); 
Controller::createInfo($model);
?>
<div class="operatorLeft">
<?php  
		// cek menu
		if($model->status_batch == 0){
			Controller::createMenu(array(
			array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>''),
			array('label'=>'Update','link'=>'Campaigne/update','img'=>'/images/update.png','id'=>$model->id_batch,'conf'=>''),
		    array('label'=>'Delete','link'=>'Campaigne/delete','img'=>'/images/delete.png','id'=>$model->id_batch,'conf'=>$model->jobs_id)
		 	));
		}
		else{
			Controller::createMenu(array(
			array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		 	));
		}
		?>
</div>
