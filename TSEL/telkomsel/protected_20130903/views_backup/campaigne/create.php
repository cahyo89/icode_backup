<?php
$this->breadcrumbs=array(
	'Campaignes'=>array('index'),
	'Create',
);
?>


<div class="row">
<div class="span8">
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
<div class="span3">
	<div style="margin-bottom:15px;">
	<?php  Controller::createMenu(array(
		array('label'=>'List','link'=>'Campaigne/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	));  ?>
	</div>
<?php 		
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Basic', 'items' => array(
				array('label'=>'General Setting','icon' => 'cog','linkOptions'=>array('id'=>'generalSetting')),
				array('label'=>'Creative','icon' => 'leaf','linkOptions'=>array('id'=>'creative')),
			),
		),
		array(
			'label' => 'Targeting', 'items' => array(
				array('label'=>'Location & URL Filter','icon' => 'map-marker','linkOptions'=>array('id'=>'targeting')),
			),
		),
	),
));
?>
</div>
</div>
<script>

	
</script>