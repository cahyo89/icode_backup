<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PaketCampaign', 'url'=>array('index')),
	array('label'=>'Create PaketCampaign', 'url'=>array('create')),
	array('label'=>'Update PaketCampaign', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PaketCampaign', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaketCampaign', 'url'=>array('admin')),
);
?>

<h1>View PaketCampaign #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'harga',
		array(
		'name'=>'tipe',
		'value'=>Yii::app()->params['tipe_paket'][$model->tipe],
		),
		'total',
		array(
		'name'=>'posisi',
		'value'=>Yii::app()->params['http_intercept_ads_value'][$model->posisi],
		),
		'ads_start_time',
		'ads_time_out',
		array(
		'name'=>'ukuran gambar 1',
		'value'=>$model->ukuran_gambar1,
		),
		array(
		'name'=>'ukuran gambar 2',
		'value'=>$model->ukuran_gambar2,
		),
		array(
		'name'=>'ukuran gambar 3',
		'value'=>$model->ukuran_gambar3,
		),
		array(
		'name'=>'Priority',
		'value'=>Yii::app()->params['priority'][$model->priority],
		),
		'first_user',
		'first_update',
		'first_ip',
		'last_user',
		'last_update',
		'last_ip',
	),
)); ?>
