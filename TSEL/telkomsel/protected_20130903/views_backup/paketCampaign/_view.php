<?php
/* @var $this PaketCampaignController */
/* @var $data PaketCampaign */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe')); ?>:</b>
	<?php echo CHtml::encode(Yii::app()->params['tipe_paket'][$data->tipe]); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posisi')); ?>:</b>
	<?php echo CHtml::encode(Yii::app()->params['http_intercept_ads_value'][$data->posisi]); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ads_start_time')); ?>:</b>
	<?php echo CHtml::encode($data->ads_start_time); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ads_time_out')); ?>:</b>
	<?php echo CHtml::encode($data->ads_time_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_gambar1')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_gambar1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_gambar2')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_gambar2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukuran_gambar3')); ?>:</b>
	<?php echo CHtml::encode($data->ukuran_gambar3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
	<?php echo CHtml::encode($data->priority); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	*/ ?>

</div>