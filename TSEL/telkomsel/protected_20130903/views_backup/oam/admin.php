<?php
set_time_limit(0);
$this->breadcrumbs=array(
	'Monitoring'=>array('index'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('oam-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerCss(uniqid(),".grid-view table.items{	font-size:11px;}");



// get total sent
	function getTotalSent($jobsId){
		if($jobsId != ""){
			/*
			if(file_exist("/nfs_data/projectlbaxl/program/lba_server/mem/".$idBatch."")){
				$myFile = "/nfs_data/projectlbaxl/program/lba_server/mem/".$idBatch."";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				$arrayData = explode(",",$theData);
				$x = intval($arrayData[0]);
			}
			else if(file_exist("/nfs_data/projectlbaxl/program/batch_module/temp/".$idBatch."")){
				$myFile = "/nfs_data/projectlbaxl/program/batch_module/temp/".$idBatch."";
				$fh = fopen($myFile, 'r');
				$theData = fread($fh, filesize($myFile));
				fclose($fh);
				$arrayData = explode(",",$theData);
				$x = intval($arrayData[0]);
			}
			else{
			*/
				$sql 		= "select number from `".$jobsId."` group by number";
				$connection = Yii::app()->db;
				$dataReader = $connection->createCommand($sql);
				//$row 		= $dataReader->query();
				$x = $dataReader->query()->getRowCount();
				/*
				$count 		= count($row);
				$x = 0;
				while($x < $count)
				{
					$x++;
				}
				*/
			//}
			return $x;
		}
		else{
			return 0;
		}
	}
	
	// get total sms yang gak dapet dr
	function getTotalSmsNoDr($jobsId){
		if($jobsId != ""){
			
			$total = getTotalSent($jobsId);
			
			$sql 		= "select distinct(number) from `".$jobsId."` where success = 1 ";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			/*
			$row 		= $dataReader->query();
			
			$count 		= count($row);
			$x = 0;
			while($x < $count)
			{
				$x++;
			}
			*/
			$x = $dataReader->query()->getRowCount();
			
			$sql 		= "select distinct(number) from `".$jobsId."` where failed = 1 ";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			/*
			$row 		= $dataReader->query();
			$count 		= count($row);
			$y = 0;
			while($y < $count)
			{
				$y++;
			}
			*/
			
			$y = $dataReader->query()->getRowCount();
			return $total- ($x+$y);
		
		}
		else{
			return 0;
		}
	}


?>

<h1>Waiting List Campaign</h1>
<!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'waiting-list-campaigne-grid',
	'dataProvider'=>$model->searchWaitingList(),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'jobs_id',
		'value'=>'CHtml::link(str_replace("_","<br>",$data->jobs_id),array("oam/viewWaitingList","id"=>$data->id_batch))',
		'type'=>'raw'
		),
		array(
		'name'=>'status_batch',
		'value'=>'Controller::getStatusCampaigne($data->status_batch)'),
		//array(
		//'name'=>'Priority Category',
		//'value'=>'Controller::getPriorityCampaigne($data->priority_category)'
		//),
		array(
		'name'=>'nama',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->Customer->nama)',
		
		),
		array(
		'name'=>'bts_name',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->BtsGroup->bts_name)',
		),
		array(
		'name'=>'topik',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->topik)',
		),
		'start_periode',
		'content_expired',
		'total_number',
		'last_update',
		'last_user',

	),
));
?> 
<h1>Progress List Campaign</h1>
 <?
 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'progress-list-campaigne-grid',
	'dataProvider'=>$model2->searchProgressList(),
	'filter'=>$model2,
	'columns'=>array(
		array('name'=>'jobs_id',
		'type'=>'raw',
		'value'=>'CHtml::link(str_replace("_","<br>",$data->jobs_id),array("oam/viewProgressList","id"=>$data->id_batch))',
		),
		array(
		'name'=>'status_batch',
		'value'=>'Controller::getStatusCampaigne($data->status_batch)'),
		//array(
		//'name'=>'Priority Category',
		//'value'=>'Controller::getPriorityCampaigne($data->priority_category)'
		//),
		array(
		'name'=>'nama',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->Customer->nama)',
		),
		array(
		'name'=>'bts_name',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->BtsGroup->bts_name)',
		),
		array(
		'name'=>'topik',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->topik)',
		),
		'start_periode',
		'total_number',
		/*
		array(
			'name'=>'sms_success',
			'type'=>'raw',
			'value'=>'CHtml::link($data->sms_success,array("oam/excel","jobs_id"=>$data->jobs_id,"flag"=>"success"))',
					
		),
		*/
		'sms_success',
		'last_update',
		'last_user',
	),
));
?>
<h1>Finish List Campaign</h1>
<?
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'finish-list-campaigne-grid',
	'dataProvider'=>$model3->searchFinishList(),
	'filter'=>$model3,
	'columns'=>array(
		array('name'=>'jobs_id',
		'type'=>'raw',
		'value'=>'CHtml::link(str_replace("_","<br>",$data->jobs_id),array("oam/viewFinishList","id"=>$data->id_batch))',
		),
		array(
		'name'=>'status_batch',
		'value'=>'Controller::getStatusCampaigne($data->status_batch)'),
		array(
		'name'=>'nama',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->Customer->nama)'
		),
		array(
		'name'=>'bts_name',
		'value'=>'str_replace("_","<br>",$data->BtsGroup->bts_name)',
		'type'=>'raw',
		),
		array(
		'name'=>'topik',
		'type'=>'raw',
		'value'=>'str_replace("_","<br>",$data->topik)',
		),
		'start_periode',
		'total_number',
		/*
		array(
			'name'=>'sms_success',
			'type'=>'raw',
			'value'=>'CHtml::link($data->sms_success,array("oam/excel","jobs_id"=>$data->jobs_id,"flag"=>"success"))',
					
		),
		*/
		'sms_success',
		'last_update',
		'last_user',
		
	),
));

?>
