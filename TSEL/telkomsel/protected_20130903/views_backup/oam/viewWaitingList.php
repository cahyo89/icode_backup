<?php
$this->breadcrumbs=array(
	'Monitoring'=>array('index'),
	$model->jobs_id,
);
?>

<h1>View Waiting List Campaign #<?php echo $model->jobs_id; ?></h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'oam',
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); 
?>

<?php 
		
function getBatchListed($idBatch){
	if($idBatch != ""){
			$sql 		= "select mdn from `tbl_batch_listed_".$idBatch."`";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$x = $dataReader->query()->getRowCount();
		return $x;
	}
	else{
		return 0;
	}
}

if($model->batch_type > 0){
	$totalBatchListed = " total data black white list : ".getBatchListed($model->id_batch);
}
else{
	$totalBatchListed = "0";
}		
		
$arrayMessageType		= array(1=>"non interactive campaign",2=>"interactive campaign");
$arrayBalancing			= array(""=>"Off",0=>"Off",1=>"On");
$arrayStatusCampaign 	= array(0=>"Queue",1=>"Approved",2=>"Progress",3=>"Resuming",4=>"Pausing",5=>"Paused",6=>"Stopping",7=>"Stopped",8=>"Retry",9=>"Finish",11=>"Scheduled");
$arrayChannelCategory   = array(0=>"SMS",5=>"MMS",6=>"Statistic",7=>"Url interface",8=>"USSD",9=>"WAP");
$arrayAst				= array(0=>" ON Entry ",1=>" On Location");
$arrayVip				= array(0=>" NOT VIP",1=>"VIP");
$arrayPriority			= array(0=>"LOW",1=>"MEDIUM",2=>"HIGH");
$arrayCampaignType		= array(0=>"Normal",1=>"blacklist File",2=>"whitelist File",3=>"blacklist profile",4=>"whitelist profile");
$arrayGlobalWhiteList	= array(""=>"Off",0=>"Off",1=>"On");			
		
$columns[] = array(
				'label'=>'Campaign Id',
				'name'=>'jobs_id'
				);	
$columns[] = array(
				'label'=>'Id Batch',
				'name'=>'id_batch'
				);				
$columns[] = array(
				'label'=>'Customer',
				'name'=>'Customer.nama',
				);

$columns[] = array(
				'label'=>'Topic',
				'name'=>'topik'
				);
$columns[] = array(
				'label'=>'Start Periode',
				'name'=>'start_periode'
				);
$columns[] = array(
				'label'=>'Content Expired',
				'name'=>'content_expired'
				);				
$columns[] = array(
				'label'=>'Status Campaign',
				'value'=>Controller::getStatusCampaigne($model->status_batch)
				);
$columns[] = array(
				'label'=>'Cell Group Area',
				'name'=>'BtsGroup.bts_name'
				);		
$columns[] = array(
				'label'=>'Cell Id',
				'name'=>'cell'
				);				
$columns[] = array(
				'label'=>'Campaign Type',
				'value'=>Controller::getCampaigneType($model->batch_type)
				);
$columns[] = array(
				'label'=>'total black/white list file',
				'value'=>$totalBatchListed
);				
			
/*$columns[] = array(
				'label'=>'first_user',
				'name'=>'first_user'
				);
$columns[] = array(
				'label'=>'first_ip',
				'name'=>'first_ip'
				);
$columns[] = array(
				'label'=>'first_update',
				'name'=>'first_update'
				);
$columns[] = array(
				'label'=>'last_user',
				'name'=>'last_user'
				);
$columns[] = array(
				'label'=>'last_ip',
				'name'=>'last_ip'
				);
$columns[] = array(
				'label'=>'last_update',
				'name'=>'last_update'
				);*/
/*				
$columns[] = array(
				'label'=>'Campaign SMS Text',
				'type'=>'raw',
				'value'=>Controller::getSms($model->sms_text)
				);
*/				
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$columns,
));

 ?>

 <?php  $this->endWidget();

Controller::createInfo($model);
if($model->status_batch == 0 || $model->status_batch == 1 || $model->status_batch == 11){
	if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('superUser')){
		echo CHtml::button('Reject', array('submit' => array('reject','id'=>$model->id_batch),'confirm'=>'Are you sure want to REJECT this Campaign?')); 
	}
}
if($model->status_batch == 0){
	if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('superUser')){	
		echo CHtml::button('Approve', array('submit' => array('app','id'=>$model->id_batch),'confirm'=>'Are you sure want to APPROVE this Campaign?')); 
	}
}
?>

<?php 
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_batch',
		'id_customer',
		'jobs_id',
		'start_periode',
		'stop_periode',
		'sms_send_limit',
		'sms_sent',
		'sms_text',
		'message_type',
		'message_bit',
		'status_batch',
		'filename',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'topik',
		'approved_time',
		'sms_success',
		'sms_failed',
		'deleted',
		'schedule_delivery',
		'process_date',
		'retry',
		'comment',
		'id_invoice',
		'batch_no',
		'last_value',
		'content_expired',
		'divre',
		'filenameupload',
		'broadcast_type',
		'total_number',
		'booking',
		'usage',
		'last_filestatus_modif',
		'customer_ani',
		'binary_data',
		'prefix_detail',
		'sms_text_view',
		'instant_failed',
		'tipe_batch',
		'promotion_media',
		'sex_category',
		'age_category',
		'status_category',
		'filter_prefix',
		'education_category',
		'channel_category',
		'priority_category',
		'flag_deleted',
		'ast',
		'cell',
		'flag_lba',
		'flag_file',
		'multimedia_type',
		'quota',
		'profile',
		'batch_type',
		'vip',
		'increase_counter',
		'unique_subscriber_per_batch',
		'flag_hour',
		'flag_multi_batch',
		'slot_multi_batch',
		'id_batch_reference',
		'black_list_profile',
		'flag_template_message',
		'profile_prouction',
		'profile_gender',
		'profile_gprs',
		'profile_kesehatan',
		'profile_usia',
		'profile_lifestyle',
		'profile_sians',
		'profile_travel',
		'profile_arpu',
		'param_arpu',
		'param_usia',
		'profile_usia2',
		'profile_arpu2',
		'flag_master_batch',
		'interface_channel',
	),
)); 
*/
?>
