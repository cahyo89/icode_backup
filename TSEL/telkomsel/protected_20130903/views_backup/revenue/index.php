<h1>Report Campaign  Filter</h1>
<?php
$this->breadcrumbs=array(
	'Report Campaign Filter',
);
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
        'Campaign by Time'=>$this->renderPartial("_formBatch", array('model' => $modelB), $this),
        // panel 3 contains the content rendered by a partial view
    ),
    // additional javascript options for the tabs plugin
    'options'=>array(
        'collapsible'=>true,
    ),
));
?>