<?php
$this->breadcrumbs=array(
	'Tree Ids',
);

$this->menu=array(
	array('label'=>'Create TreeId', 'url'=>array('create')),
	array('label'=>'Manage TreeId', 'url'=>array('admin')),
);
?>

<h1>Tree Ids</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
