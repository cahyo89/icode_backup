<div class="well form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'GET',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<?php echo $form->errorSummary($model); ?>
	
		<div class="well-small">
		<?php echo $form->labelEx($model,'dateStart'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateStart', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			'yearRange'=>'1920:2012',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	
	<div class="well-small">
		<?php echo $form->labelEx($model,'customer'); ?>
		<?php 
			if(Yii::app()->user->checkAccess('Admin'))
			{
				$customer = Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
			}
			else
			{
				$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
				$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
			}
			if(Yii::app()->user->getUserMode()==0){
				echo $form->dropDownList($model,'customer',CHtml::listData($customer,'id_customer','nama'),
						array('empty'=>'All',
								'ajax'=>array(
												'type'=>'post',
												'dataType'=>'json',
												'url'=>CController::createUrl('cekCampaign'),
												'data'=>array(	'dataCus' => 'js:this.value',
																'dataDateStart' => 'js:$(\'#dateStart\').val()'
																),
												'success' => 'function(data)
												{
													$("#StaticBatchLba_campaign").html(data.campaignD);
												}',
												
												'error'=> 'function(){alert(\'Bad AJAX\');}',
										)	
							)	
		
		
			);}
				else{
					echo $form->dropDownList($model,'customer',CHtml::listData(Customer::model()->findAll(array('condition'=>'(id_customer ='.Yii::app()->user->id.' ) and flag_deleted IS NULL or flag_deleted <> 1','order'=>'nama')),'id_customer','nama'),
						array('empty'=>'All',
								'ajax'=>array(
												'type'=>'post',
												'dataType'=>'json',
												'url'=>CController::createUrl('cekCampaign'),
												'data'=>array(	'dataCus' => 'js:this.value',
																'dataDateStart' => 'js:$(\'#dateStart\').val()',
																),
												'success' => 'function(data)
												{
													$("#StaticBatchLba_campaign").html(data.campaignD);
												}',
												
												'error'=> 'function(){alert(\'Bad AJAX\');}',
										)	
							)	
		
		
						);
		}
		?>
		<?php echo $form->error($model,'customer'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'campaign'); ?>
		<?php echo $form->dropDownList($model,'campaign',array(''=>'All')); ?>
		<?php echo $form->error($model,'campaign'); ?>
	</div>
	
	<div class="well-small">
		<?php //echo $form->labelEx($model,'Type'); ?>
		<?php //echo $form->dropDownList($model,'tipe',array(''=>'All','1'=>'view','2'=>'click')); ?>
		<?php //echo $form->error($model,'tipe'); ?>
	</div>
	
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'MSISDN'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'URL'); ?>
		<?php echo $form->textField($model,'url',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>
	
	
	<div class="well-small submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->