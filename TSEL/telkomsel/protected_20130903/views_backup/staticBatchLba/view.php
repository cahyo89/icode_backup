<?php
$this->breadcrumbs=array(
	'Report Campaign Filter'=>array('index'),
);

$dateStart = $_GET['dateStart'];
$customer = $_GET['customer'];
$campaign = $_GET['campaign'];
$url = $_GET['url'];
$msisdn = $_GET['msisdn'];

$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
$modelCampaign = CHtml::listData(Campaigne::model()->findAll(),'id_batch','jobs_id');
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Report Campaign</h1></td>
<td><div class="operatorRight"><?php  if($dataView == 0 ){  echo CHtml::link('Export Report to CSV',array('staticBatchLba/excel',
										'excel'=>'1',
										'dateStart'=>$dateStart,
										'customer'=>$customer,
										'campaign'=>$campaign,
										'url' => $url,
										'msisdn' => $msisdn
										)); } ?></div></td>
					</tr></table>

<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

<h4>Customer : <?php echo $customer == "" ?  'All' :  $modeCustomer[$customer] ;  ?></h4>

<h4>Campaign : <?php echo $campaign == "" ?  'All' : $modelCampaign[$campaign] ;  ?></h4>
<?php if($dataView == 0){ 

	$success =0;
	$failed = 0;
	$queue = 0;
	$total = 0;
	$qr = 0;
	$sr = 0;
	$fr = 0;
	
	//$dataM = $dataProvider->getData();

	?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'static-error-code-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'request_time',
		'msisdn',
		array(
			'name'=>'tipe',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["tipe_paket"][$data["tipe"]]',
		),
		array(
			'name'=>'Media Seller',
			'type'=>'raw',
			'value'=>'@Controller::getCustomer($data["id_customer"])',
		),
		array(
			'name'=>'Advertiser',
			'type'=>'raw',
			'value'=>'@Controller::getCustomer($data["parent_id"])',
		),
		array(
			'name'=>'Advertising model',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["http_intercept_ads_value"][$data["http_intercept_ads_value"]]',
		),
		'url_on_click',
		'url_requested',
		'jobs_id',
		
		/*
		'result',
		'error_code',
		*/
		
	),
)); 

?>
<?php }else{ ?>
	
	<?php
		$this->widget(
		   'application.extensions.OpenFlashChart2Widget.OpenFlashChart2Widget',
		   array(
		     'chart' => $chart,
		     'width' => '100%',
			'height' => '400'
		   )
		 );
	?>

<?php } ?>
