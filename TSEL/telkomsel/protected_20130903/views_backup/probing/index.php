<?php
$this->breadcrumbs=array(
	'Probings',
);

$this->menu=array(
	array('label'=>'Create Probing', 'url'=>array('create')),
	array('label'=>'Manage Probing', 'url'=>array('admin')),
);
?>

<h1>Probings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
