tected static function extract($value, $expression)
	{
		preg_match('/@layout(\s*\(.*\))(\s*)/', $value, $matches);

		return str_replace(array("('", "')"), '', $matches[1]);
	}

	/**
	 * Rewrites Blade comments into PHP comments.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protected static function compile_comments($value)
	{
		$value = preg_replace('/\{\{--(.+?)(--\}\})?\n/', "<?php // $1 ?>", $value);

		return preg_replace('/\{\{--((.|\s)*?)--\}\}/', "<?php /* $1 */ ?>\n", $value);
	}

	/**
	 * Rewrites Blade echo statements into PHP echo statements.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protected static function compile_echos($value)
	{
		return preg_replace('/\{\{(.+?)\}\}/', '<?php echo $1; ?>'