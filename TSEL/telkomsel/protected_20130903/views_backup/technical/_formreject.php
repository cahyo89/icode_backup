<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-app-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>50,'maxlength'=>50,'readonly'=>true)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'reason_prepaid'); ?>
		<?php echo $form->textField($model,'reason_prepaid',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'reason_prepaid'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->