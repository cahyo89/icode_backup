<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setting-balance-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30 ,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<?php
		if($model->tipe_customer == 0 ){
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'tipe_paket'); ?>
		<?php $model->tipe_paket = ""; echo $form->dropDownList($model,'tipe_paket',CHtml::listData(Paket::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_paket', 'jenis_paket')); ?>
		<?php echo $form->error($model,'tipe_paket'); ?>
	</div>
	<?php
		}
		else
		{
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'temp_prepaid'); ?>
		<?php $model->temp_prepaid = 0; echo $form->textField($model,'temp_prepaid'); ?>Token
		<?php echo $form->error($model,'temp_prepaid'); ?>
	</div>
	<?php
		}
	?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->