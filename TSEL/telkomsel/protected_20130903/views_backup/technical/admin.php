<?php
$this->breadcrumbs=array(
	'Balance'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('setting-balance-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1> Balance Management</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'setting-balance-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'nama',
			'type'=>'raw',
			'value'=>'CHtml::link($data->nama,array("technical/view","id"=>$data->id_customer))',
		),
		array(
			'name'=>'tipe_customer',
			'type'=>'raw',
			'value'=>'Yii::app()->params[\'tipe_customer\'][$data->tipe_customer]',
		),
		'prepaid_value',
		array(
			'name'=>'approved_prepaid',
			'type'=>'raw',
			'value'=>'Yii::app()->params[\'approved\'][$data->approved_prepaid]',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		    'buttons'=>array
		    (
		        'update' => array
		        (
		            'label'=>'Edit',
		            'imageUrl'=>Yii::app()->request->baseUrl.'/images/update.png',
		            'url'=>'Yii::app()->createUrl("technical/view", array("id"=>$data->id_customer))',
					'visible'=>'$data->approved_prepaid == 2 ? false : true',
		        ),
		        'delete' => array
		        (
		            'label'=>'Relase',
		            'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
		            'url'=>'Yii::app()->createUrl("technical/delete", array("id"=>$data->id_customer))',
					'visible'=>'$data->approved_prepaid == 2 ? false : true',
					
		        ),
		    ),
			'deleteConfirmation'=>"js:'Are you sure you want to Reset this Balance Value??\\n\\nCustomer Name :' +
	        $(this).parents('tr').children(':eq(0)').text() +
	        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to Proceed, or \"Cancel\" to Abort this action.\\n\\n'",
		),
	),
)); ?>
