<?php
$this->breadcrumbs=array(
	'Transaction Reports'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TransactionReport', 'url'=>array('index')),
	array('label'=>'Manage TransactionReport', 'url'=>array('admin')),
);
?>

<h1>Create TransactionReport</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>