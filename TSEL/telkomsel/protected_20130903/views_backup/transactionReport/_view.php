<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_batch')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_batch), array('view', 'id'=>$data->id_batch)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_customer')); ?>:</b>
	<?php echo CHtml::encode($data->id_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jobs_id')); ?>:</b>
	<?php echo CHtml::encode($data->jobs_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_periode')); ?>:</b>
	<?php echo CHtml::encode($data->start_periode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stop_periode')); ?>:</b>
	<?php echo CHtml::encode($data->stop_periode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_send_limit')); ?>:</b>
	<?php echo CHtml::encode($data->sms_send_limit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_sent')); ?>:</b>
	<?php echo CHtml::encode($data->sms_sent); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_text')); ?>:</b>
	<?php echo CHtml::encode($data->sms_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message_type')); ?>:</b>
	<?php echo CHtml::encode($data->message_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message_bit')); ?>:</b>
	<?php echo CHtml::encode($data->message_bit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_batch')); ?>:</b>
	<?php echo CHtml::encode($data->status_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('filename')); ?>:</b>
	<?php echo CHtml::encode($data->filename); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('frist_update')); ?>:</b>
	<?php echo CHtml::encode($data->frist_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('topik')); ?>:</b>
	<?php echo CHtml::encode($data->topik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_time')); ?>:</b>
	<?php echo CHtml::encode($data->approved_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_success')); ?>:</b>
	<?php echo CHtml::encode($data->sms_success); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_failed')); ?>:</b>
	<?php echo CHtml::encode($data->sms_failed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted')); ?>:</b>
	<?php echo CHtml::encode($data->deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('schedule_delivery')); ?>:</b>
	<?php echo CHtml::encode($data->schedule_delivery); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('process_date')); ?>:</b>
	<?php echo CHtml::encode($data->process_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('retry')); ?>:</b>
	<?php echo CHtml::encode($data->retry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_invoice')); ?>:</b>
	<?php echo CHtml::encode($data->id_invoice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_no')); ?>:</b>
	<?php echo CHtml::encode($data->batch_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_value')); ?>:</b>
	<?php echo CHtml::encode($data->last_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_expired')); ?>:</b>
	<?php echo CHtml::encode($data->content_expired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('divre')); ?>:</b>
	<?php echo CHtml::encode($data->divre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('filenameupload')); ?>:</b>
	<?php echo CHtml::encode($data->filenameupload); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broadcast_type')); ?>:</b>
	<?php echo CHtml::encode($data->broadcast_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_number')); ?>:</b>
	<?php echo CHtml::encode($data->total_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('booking')); ?>:</b>
	<?php echo CHtml::encode($data->booking); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usage')); ?>:</b>
	<?php echo CHtml::encode($data->usage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_filestatus_modif')); ?>:</b>
	<?php echo CHtml::encode($data->last_filestatus_modif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_ani')); ?>:</b>
	<?php echo CHtml::encode($data->customer_ani); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('binary_data')); ?>:</b>
	<?php echo CHtml::encode($data->binary_data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_detail')); ?>:</b>
	<?php echo CHtml::encode($data->prefix_detail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_text_view')); ?>:</b>
	<?php echo CHtml::encode($data->sms_text_view); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instant_failed')); ?>:</b>
	<?php echo CHtml::encode($data->instant_failed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_batch')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('promotion_media')); ?>:</b>
	<?php echo CHtml::encode($data->promotion_media); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sex_category')); ?>:</b>
	<?php echo CHtml::encode($data->sex_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('age_category')); ?>:</b>
	<?php echo CHtml::encode($data->age_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_category')); ?>:</b>
	<?php echo CHtml::encode($data->status_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('filter_prefix')); ?>:</b>
	<?php echo CHtml::encode($data->filter_prefix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('education_category')); ?>:</b>
	<?php echo CHtml::encode($data->education_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel_category')); ?>:</b>
	<?php echo CHtml::encode($data->channel_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority_category')); ?>:</b>
	<?php echo CHtml::encode($data->priority_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->flag_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ast')); ?>:</b>
	<?php echo CHtml::encode($data->ast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cell')); ?>:</b>
	<?php echo CHtml::encode($data->cell); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_lba')); ?>:</b>
	<?php echo CHtml::encode($data->flag_lba); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_file')); ?>:</b>
	<?php echo CHtml::encode($data->flag_file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('multimedia_type')); ?>:</b>
	<?php echo CHtml::encode($data->multimedia_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quota')); ?>:</b>
	<?php echo CHtml::encode($data->quota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile')); ?>:</b>
	<?php echo CHtml::encode($data->profile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_type')); ?>:</b>
	<?php echo CHtml::encode($data->batch_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vip')); ?>:</b>
	<?php echo CHtml::encode($data->vip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('increase_counter')); ?>:</b>
	<?php echo CHtml::encode($data->increase_counter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unique_subscriber_per_batch')); ?>:</b>
	<?php echo CHtml::encode($data->unique_subscriber_per_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_hour')); ?>:</b>
	<?php echo CHtml::encode($data->flag_hour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_multi_batch')); ?>:</b>
	<?php echo CHtml::encode($data->flag_multi_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slot_multi_batch')); ?>:</b>
	<?php echo CHtml::encode($data->slot_multi_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_batch_reference')); ?>:</b>
	<?php echo CHtml::encode($data->id_batch_reference); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('black_list_profile')); ?>:</b>
	<?php echo CHtml::encode($data->black_list_profile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_template_message')); ?>:</b>
	<?php echo CHtml::encode($data->flag_template_message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_prouction')); ?>:</b>
	<?php echo CHtml::encode($data->profile_prouction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_gender')); ?>:</b>
	<?php echo CHtml::encode($data->profile_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_gprs')); ?>:</b>
	<?php echo CHtml::encode($data->profile_gprs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_kesehatan')); ?>:</b>
	<?php echo CHtml::encode($data->profile_kesehatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_usia')); ?>:</b>
	<?php echo CHtml::encode($data->profile_usia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_lifestyle')); ?>:</b>
	<?php echo CHtml::encode($data->profile_lifestyle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_sians')); ?>:</b>
	<?php echo CHtml::encode($data->profile_sians); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_travel')); ?>:</b>
	<?php echo CHtml::encode($data->profile_travel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_arpu')); ?>:</b>
	<?php echo CHtml::encode($data->profile_arpu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('param_arpu')); ?>:</b>
	<?php echo CHtml::encode($data->param_arpu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('param_usia')); ?>:</b>
	<?php echo CHtml::encode($data->param_usia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_usia2')); ?>:</b>
	<?php echo CHtml::encode($data->profile_usia2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_arpu2')); ?>:</b>
	<?php echo CHtml::encode($data->profile_arpu2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_master_batch')); ?>:</b>
	<?php echo CHtml::encode($data->flag_master_batch); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('interface_channel')); ?>:</b>
	<?php echo CHtml::encode($data->interface_channel); ?>
	<br />

	*/ ?>

</div>