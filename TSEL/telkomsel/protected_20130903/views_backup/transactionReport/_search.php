<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_batch'); ?>
		<?php echo $form->textField($model,'id_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_customer'); ?>
		<?php echo $form->textField($model,'id_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jobs_id'); ?>
		<?php echo $form->textField($model,'jobs_id',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_periode'); ?>
		<?php echo $form->textField($model,'start_periode'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stop_periode'); ?>
		<?php echo $form->textField($model,'stop_periode'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_send_limit'); ?>
		<?php echo $form->textField($model,'sms_send_limit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_sent'); ?>
		<?php echo $form->textField($model,'sms_sent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_text'); ?>
		<?php echo $form->textField($model,'sms_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'message_type'); ?>
		<?php echo $form->textField($model,'message_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'message_bit'); ?>
		<?php echo $form->textField($model,'message_bit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_batch'); ?>
		<?php echo $form->textField($model,'status_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'filename'); ?>
		<?php echo $form->textField($model,'filename',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_ip'); ?>
		<?php echo $form->textField($model,'first_ip',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_user'); ?>
		<?php echo $form->textField($model,'first_user',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'frist_update'); ?>
		<?php echo $form->textField($model,'frist_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_ip'); ?>
		<?php echo $form->textField($model,'last_ip',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_user'); ?>
		<?php echo $form->textField($model,'last_user',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_update'); ?>
		<?php echo $form->textField($model,'last_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'topik'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approved_time'); ?>
		<?php echo $form->textField($model,'approved_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_success'); ?>
		<?php echo $form->textField($model,'sms_success'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_failed'); ?>
		<?php echo $form->textField($model,'sms_failed'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deleted'); ?>
		<?php echo $form->textField($model,'deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'schedule_delivery'); ?>
		<?php echo $form->textField($model,'schedule_delivery'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'process_date'); ?>
		<?php echo $form->textField($model,'process_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'retry'); ?>
		<?php echo $form->textField($model,'retry'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_invoice'); ?>
		<?php echo $form->textField($model,'id_invoice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'batch_no'); ?>
		<?php echo $form->textField($model,'batch_no',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_value'); ?>
		<?php echo $form->textField($model,'last_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content_expired'); ?>
		<?php echo $form->textField($model,'content_expired'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'divre'); ?>
		<?php echo $form->textField($model,'divre',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'filenameupload'); ?>
		<?php echo $form->textField($model,'filenameupload',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'broadcast_type'); ?>
		<?php echo $form->textField($model,'broadcast_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_number'); ?>
		<?php echo $form->textField($model,'total_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'booking'); ?>
		<?php echo $form->textField($model,'booking'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usage'); ?>
		<?php echo $form->textField($model,'usage'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_filestatus_modif'); ?>
		<?php echo $form->textField($model,'last_filestatus_modif'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_ani'); ?>
		<?php echo $form->textField($model,'customer_ani',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'binary_data'); ?>
		<?php echo $form->textField($model,'binary_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prefix_detail'); ?>
		<?php echo $form->textField($model,'prefix_detail',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_text_view'); ?>
		<?php echo $form->textField($model,'sms_text_view'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'instant_failed'); ?>
		<?php echo $form->textField($model,'instant_failed'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipe_batch'); ?>
		<?php echo $form->textField($model,'tipe_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'promotion_media'); ?>
		<?php echo $form->textField($model,'promotion_media'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sex_category'); ?>
		<?php echo $form->textField($model,'sex_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'age_category'); ?>
		<?php echo $form->textField($model,'age_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_category'); ?>
		<?php echo $form->textField($model,'status_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'filter_prefix'); ?>
		<?php echo $form->textField($model,'filter_prefix',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'education_category'); ?>
		<?php echo $form->textField($model,'education_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'channel_category'); ?>
		<?php echo $form->textField($model,'channel_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priority_category'); ?>
		<?php echo $form->textField($model,'priority_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_deleted'); ?>
		<?php echo $form->textField($model,'flag_deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ast'); ?>
		<?php echo $form->textField($model,'ast'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cell'); ?>
		<?php echo $form->textField($model,'cell'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_lba'); ?>
		<?php echo $form->textField($model,'flag_lba'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_file'); ?>
		<?php echo $form->textField($model,'flag_file'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'multimedia_type'); ?>
		<?php echo $form->textField($model,'multimedia_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quota'); ?>
		<?php echo $form->textField($model,'quota'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile'); ?>
		<?php echo $form->textField($model,'profile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'batch_type'); ?>
		<?php echo $form->textField($model,'batch_type',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vip'); ?>
		<?php echo $form->textField($model,'vip'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'increase_counter'); ?>
		<?php echo $form->textField($model,'increase_counter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unique_subscriber_per_batch'); ?>
		<?php echo $form->textField($model,'unique_subscriber_per_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_hour'); ?>
		<?php echo $form->textField($model,'flag_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_multi_batch'); ?>
		<?php echo $form->textField($model,'flag_multi_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slot_multi_batch'); ?>
		<?php echo $form->textField($model,'slot_multi_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_batch_reference'); ?>
		<?php echo $form->textField($model,'id_batch_reference'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'black_list_profile'); ?>
		<?php echo $form->textField($model,'black_list_profile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_template_message'); ?>
		<?php echo $form->textField($model,'flag_template_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_prouction'); ?>
		<?php echo $form->textField($model,'profile_prouction',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_gender'); ?>
		<?php echo $form->textField($model,'profile_gender',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_gprs'); ?>
		<?php echo $form->textField($model,'profile_gprs',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_kesehatan'); ?>
		<?php echo $form->textField($model,'profile_kesehatan',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_usia'); ?>
		<?php echo $form->textField($model,'profile_usia',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_lifestyle'); ?>
		<?php echo $form->textField($model,'profile_lifestyle',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_sians'); ?>
		<?php echo $form->textField($model,'profile_sians',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_travel'); ?>
		<?php echo $form->textField($model,'profile_travel',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_arpu'); ?>
		<?php echo $form->textField($model,'profile_arpu',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'param_arpu'); ?>
		<?php echo $form->textField($model,'param_arpu'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'param_usia'); ?>
		<?php echo $form->textField($model,'param_usia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_usia2'); ?>
		<?php echo $form->textField($model,'profile_usia2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_arpu2'); ?>
		<?php echo $form->textField($model,'profile_arpu2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_master_batch'); ?>
		<?php echo $form->textField($model,'flag_master_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'interface_channel'); ?>
		<?php echo $form->textField($model,'interface_channel'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->