<?php
$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->nama,
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Customer #<?php echo $model->nama; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'customer/update','img'=>'/images/update.png','id'=>$model->id_customer,'conf'=>''),
	   array('label'=>'Delete','link'=>'customer/delete','img'=>'/images/delete.png','id'=>$model->id_customer,'conf'=>$model->nama),
	   array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nama',
		
		array(
			'name'=>'segmen_customer',
			'type'=>'raw',
			'value'=>@Yii::app()->params["segmen_customer"][$model->segmen_customer],
		),
		array(
			'name'=>'tipe_customer',
			'type'=>'raw',
			'value'=>@Yii::app()->params["tipe_customer"][$model->tipe_customer],
		),
		array(
			'label'=>'category_customer',
			'name'=>'TreeId.nama_perangkat',
		),
		array(
			'label'=>'tipe_paket',
			'name'=>'Paket.jenis_paket',
		),
		array(
			'name'=>'bypass_approval',
			'type'=>'raw',
			'value'=>@Yii::app()->params["bypass_approval"][$model->bypass_approval],
		),
		'prepaid_value',
		array(
			'name'=>'approved',
			'type'=>'raw',
			'value'=>@Yii::app()->params["approved"][$model->approved],
		),
		array(
			'name'=>'blocked',
			'type'=>'raw',
			'value'=>@Yii::app()->params["blocked"][$model->blocked],
		),
	),
));
if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('superUser')){
	if($model->approved != 2)
		echo CHtml::button('Reject', array('submit' => array('reject','id'=>$model->id_customer),'confirm'=>'Are you sure want to REJECT this Customer?')); 
	if($model->approved != 1)
		echo CHtml::button('Approve', array('submit' => array('app','id'=>$model->id_customer),'confirm'=>'Are you sure want to APPROVE this Customer?')); 
	if($model->blocked != 1)
		echo CHtml::button('Block', array('submit' => array('block','id'=>$model->id_customer),'confirm'=>'Are you sure want to BLOCK this Customer?')); 
	echo "<BR>";
}
Controller::createInfo($model);

 ?>
 
 <div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'customer/update','img'=>'/images/update.png','id'=>$model->id_customer,'conf'=>''),
	    array('label'=>'Delete','link'=>'customer/delete','img'=>'/images/delete.png','id'=>$model->id_customer,'conf'=>$model->nama)
	   
		));  ?>
</div>
