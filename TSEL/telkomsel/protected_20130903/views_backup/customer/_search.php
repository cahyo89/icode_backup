<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	

	<div class="row">
		<?php echo $form->label($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipe_customer'); ?>
		<?php echo $form->dropDownList($model,'tipe_customer',Controller::getConstanta('tipe_customer'),array('empty'=>'--Please Select One--')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'category_customer'); ?>
		<?php echo $form->dropDownList($model,'category_customer',CHtml::listData(TreeId::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'tree_id', 'nama_perangkat'),array('empty'=>'--Please Select One--')); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipe_paket'); ?>
		<?php echo $form->dropDownList($model,'tipe_paket',CHtml::listData(Paket::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_paket', 'jenis_paket'),array('empty'=>'--Please Select One--')); ?>
		
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'bypass_approval'); ?>
		<?php echo $form->dropDownList($model,'bypass_approval',Controller::getConstanta('bypass_approval'),array('empty'=>'--Please Select One--')); ?>
		
	</div>

	
	
	

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->