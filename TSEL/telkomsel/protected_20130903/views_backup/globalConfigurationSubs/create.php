<?php
$this->breadcrumbs=array(
	'Global Configuration'=>array('index'),
	'Create',
);
Yii::app()->clientScript->registerCss(uniqid(),"
div.form label {
    display: block;
    font-size: 1em;
    font-weight: bold;
    width: 270px;
}
");
?>

<h1>Create GlobalConfigurationSubs</h1>
<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Create Global Configuration</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'globalConfigurationSubs/index','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

						  
<div class="operatorLeft"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'globalConfigurationSubs/index','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div>