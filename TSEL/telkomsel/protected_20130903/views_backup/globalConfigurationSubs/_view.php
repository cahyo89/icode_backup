<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortcode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->shortcode), array('view', 'id'=>$data->shortcode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword_on_whitelist')); ?>:</b>
	<?php echo CHtml::encode($data->keyword_on_whitelist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword_off_whitelist')); ?>:</b>
	<?php echo CHtml::encode($data->keyword_off_whitelist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword_on_blacklist')); ?>:</b>
	<?php echo CHtml::encode($data->keyword_on_blacklist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword_off_blacklist')); ?>:</b>
	<?php echo CHtml::encode($data->keyword_off_blacklist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_success_on_whitelist_sub')); ?>:</b>
	<?php echo CHtml::encode($data->response_success_on_whitelist_sub); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_success_off_whitelist_unsub')); ?>:</b>
	<?php echo CHtml::encode($data->response_success_off_whitelist_unsub); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('response_already_on_whitelist')); ?>:</b>
	<?php echo CHtml::encode($data->response_already_on_whitelist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_already_off_whitelist')); ?>:</b>
	<?php echo CHtml::encode($data->response_already_off_whitelist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_success_on_blacklist_block')); ?>:</b>
	<?php echo CHtml::encode($data->response_success_on_blacklist_block); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_success_off_blacklist_unblock')); ?>:</b>
	<?php echo CHtml::encode($data->response_success_off_blacklist_unblock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_already_on_blacklist')); ?>:</b>
	<?php echo CHtml::encode($data->response_already_on_blacklist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_already_off_blacklist')); ?>:</b>
	<?php echo CHtml::encode($data->response_already_off_blacklist); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_keyword_not_valid')); ?>:</b>
	<?php echo CHtml::encode($data->response_keyword_not_valid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_shortcode_expired')); ?>:</b>
	<?php echo CHtml::encode($data->response_shortcode_expired); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_shortcode_already_response')); ?>:</b>
	<?php echo CHtml::encode($data->response_shortcode_already_response); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('response_shortcode_not_from_lba')); ?>:</b>
	<?php echo CHtml::encode($data->response_shortcode_not_from_lba); ?>
	<br />

	*/ ?>

</div>