<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'global-configuration-subs-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->textField($model,'shortcode',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keyword_on_whitelist'); ?>
		<?php echo $form->textField($model,'keyword_on_whitelist',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'keyword_on_whitelist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keyword_off_whitelist'); ?>
		<?php echo $form->textField($model,'keyword_off_whitelist',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'keyword_off_whitelist'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'response_success_on_whitelist_sub'); ?>
		<?php echo $form->textArea($model,'response_success_on_whitelist_sub',array('rows'=>3, 'cols'=>50,'maxlength'=>160)); ?>
		<?php echo $form->error($model,'response_success_on_whitelist_sub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'response_success_off_whitelist_unsub'); ?>
		<?php echo $form->textArea($model,'response_success_off_whitelist_unsub',array('rows'=>3, 'cols'=>50,'maxlength'=>160)); ?>
		<?php echo $form->error($model,'response_success_off_whitelist_unsub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'response_already_on_whitelist'); ?>
		<?php echo $form->textArea($model,'response_already_on_whitelist',array('rows'=>3, 'cols'=>50,'maxlength'=>160)); ?>
		<?php echo $form->error($model,'response_already_on_whitelist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'response_already_off_whitelist'); ?>
		<?php echo $form->textArea($model,'response_already_off_whitelist',array('rows'=>3, 'cols'=>50,'maxlength'=>160)); ?>
		<?php echo $form->error($model,'response_already_off_whitelist'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->