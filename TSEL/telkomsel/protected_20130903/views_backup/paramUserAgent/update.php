<?php
/* @var $this ParamUserAgentController */
/* @var $model ParamUserAgent */

$this->breadcrumbs=array(
	'Param User Agents'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ParamUserAgent', 'url'=>array('index')),
	array('label'=>'Create ParamUserAgent', 'url'=>array('create')),
	array('label'=>'View ParamUserAgent', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);
?>

<h1>Update ParamUserAgent <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>