<?php
/* @var $this ParamUserAgentController */
/* @var $model ParamUserAgent */

$this->breadcrumbs=array(
	'Param User Agents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ParamUserAgent', 'url'=>array('index')),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);
?>

<h1>Create ParamUserAgent</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>