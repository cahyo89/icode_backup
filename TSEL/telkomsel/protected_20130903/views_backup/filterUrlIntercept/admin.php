<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FilterUrlIntercept', 'url'=>array('index')),
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#filter-url-intercept-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Filter Url Intercepts</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="well">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'filter-url-intercept-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'url_name',
		array(
			'name'=>'url_detail',
			'htmlOptions'=>array('width'=>'100px'),
		),
		'first_user',
		'first_update',
		'first_ip',
		/*
		'last_user',
		'last_update',
		'last_ip',
		'flag_deleted',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<div>
