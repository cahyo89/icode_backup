<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FilterUrlIntercept', 'url'=>array('index')),
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
	array('label'=>'Update FilterUrlIntercept', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FilterUrlIntercept', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FilterUrlIntercept', 'url'=>array('admin')),
);
?>

<h1>View FilterUrlIntercept #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'url_name',
		'url_detail',
		'first_user',
		'first_update',
		'first_ip',
		'last_user',
		'last_update',
		'last_ip',
		'flag_deleted',
	),
)); ?>
