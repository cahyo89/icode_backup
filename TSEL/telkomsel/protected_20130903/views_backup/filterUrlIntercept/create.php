<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Filter Url Intercept', 'url'=>array('index')),
	array('label'=>'Manage Filter Url Intercept', 'url'=>array('admin')),
);
?>

<h1>Create Filter Url Intercept</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>