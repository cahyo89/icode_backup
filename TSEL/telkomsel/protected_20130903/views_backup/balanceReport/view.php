<?php
$this->breadcrumbs=array(
	'Report Campaign Filter'=>array('index'),
);

$dateStart = $_GET['dateStart'];
$dateEnd = $_GET['dateEnd'];

$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
$modelCampaign = CHtml::listData(Campaigne::model()->findAll(),'id_batch','jobs_id');
$modelUser = CHtml::listData(Users::model()->findAll(),'id','flag_free');
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Report Balance</h1></td>
<td><div class="operatorRight"><?php  if($dataView == 0 ){  echo CHtml::link('Export Report to CSV',array('balanceReport/excel',
										'excel'=>'1',
										'dateStart'=>$dateStart,
										'dateEnd'=>$dateEnd,
										)); } ?></div></td>
					</tr></table>

<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

<?php if($dataView == 0){ 

	$success =0;
	$failed = 0;
	$queue = 0;
	$total = 0;
	$qr = 0;
	$sr = 0;
	$fr = 0;
	
	//$dataM = $dataProvider->getData();

	?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'static-error-code-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name'=>'POID',
			'type'=>'raw',
			'value'=>'$data["idpo"]',
		),
		array(
			'name'=>'BulkPOID',
			'type'=>'raw',
			'value'=>'$data["id_bulk"]',
		),
		array(
			'name'=>'PO Created Date',
			'type'=>'raw',
			'value'=>'$data["create_date"]',
		),
		array(
			'name'=>'PO Approve Date',
			'type'=>'raw',
			'value'=>'$data["first_update"]',
		),
		array(
			'name'=>'PO Approve By',
			'type'=>'raw',
			'value'=>'$data["first_user"]',
		),
		array(
			'name'=>'PO Release Date',
			'type'=>'raw',
			'value'=>'$data["last_update"]',
		),
		array(
			'name'=>'PO Release By',
			'type'=>'raw',
			'value'=>'$data["last_user"]',
		),
		array(
			'name'=>'Media Seller',
			'type'=>'raw',
			'value'=>'@(empty($data["parent_id"])) ? Controller::getCustomer($data["id_customer"]) : Controller::getCustomer($data["parent_id"])',
		),
		array(
			'name'=>'Client',
			'type'=>'raw',
			'value'=>'@(empty($data["parent_id"])) ? "" : Controller::getCustomer($data["parent_id"])',
		),
		array(
			'name'=>'Token',
			'type'=>'raw',
			'value'=>'$data["prepaid_value"]',
		),
		array(
			'name'=>'Price per Token',
			'type'=>'raw',
			'value'=>'@"Rp. ".number_format($data["perharga"])',
		),
		array(
			'name'=>'Price',
			'type'=>'raw',
			'value'=>'@"Rp. ".number_format($data["harga"])',
		),
		array(
			'name'=>'Discount',
			'type'=>'raw',
			'value'=>'@$data["disc"]',
		),
		array(
			'name'=>'Net Price',
			'type'=>'raw',
			'value'=>'@"Rp. ".number_format($data["bayar"])',
		),
		array(
			'name'=>'Sharing Rev Axis (50%)',
			'type'=>'raw',
			'value'=>'@"Rp. ".number_format($data["RevAxis"])',
		),
		array(
			'name'=>'Sharing Rev ICode (50%)',
			'type'=>'raw',
			'value'=>'@"Rp. ".number_format($data["RevICode"])',
		),
		array(
			'name'=>'Token Status',
			'type'=>'raw',
			'value'=>'@Controller::getTokenStatus($data["user"])',
		),
		/*
		'result',
		'error_code',
		*/
		
	),
)); 

?>
<?php }else{ ?>
	
	<?php
		$this->widget(
		   'application.extensions.OpenFlashChart2Widget.OpenFlashChart2Widget',
		   array(
		     'chart' => $chart,
		     'width' => '100%',
			'height' => '400'
		   )
		 );
	?>

<?php } ?>
