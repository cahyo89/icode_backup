<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'get',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Report Opt-in</h1>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<?php echo $form->errorSummary($model); ?>
	
	
		<div class="row">
		<?php echo $form->labelEx($model,'dateStart'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateStart', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			'yearRange'=>'2010:'.date("Y").'',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	
	<div class="row" >
			<?php echo $form->labelEx($model,'dateEnd'); ?>
			<?php
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
				//'model'=>$model,
				'attribute'=>'start',
				'name'=>'dateEnd', // This is how it works for me.
				'value'=>date('Y-m-d'),
				'options'=>array('dateFormat'=>'yy-mm-dd',
				'altFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				'yearRange'=>'2010:'.date("Y").'',
				'showOn'=>'both',
				// 'buttonText'=>'...',
				'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				'buttonImageOnly' => true,
				),
				'htmlOptions'=>array('size'=>'15')
				));
			?>
			<?php echo $form->error($model,'dateEnd'); ?>
		</div>
	
		<div class="row" style="display:none">
			<?php echo $form->labelEx($model,'action'); ?>
			<?php echo $form->dropDownList($model,'action',Controller::getConstanta('dataHour')); ?>
			<?php echo $form->error($model,'action'); ?>
		</div>
	
	<div class="row submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->