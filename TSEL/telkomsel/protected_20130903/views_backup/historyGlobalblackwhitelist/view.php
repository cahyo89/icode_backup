<?php
$this->breadcrumbs=array(
	'Report Opt-in Filter'=>array('index')
);

$dateStart = $_GET['dateStart'];
$dateEnd = $_GET['dateEnd'];
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Report Opt-in</h1></td>
<td><div class="operatorRight"><?php  echo CHtml::link(Yii::app()->params['excel'],array('historyGlobalblackwhitelist/view',
										'excel'=>'1',
										'dateStart'=>$dateStart,
										'dateEnd'=>$dateEnd,
										));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'history-globalblackwhitelist-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name'=>'Date',
			'type'=>'raw',
			'value'=>'$data["last_update"]',
		),
		array(
			'name'=>'Net Subscribe',
			'type'=>'raw',
			'value'=>array($this,'gridNet'),
		),
		/*array(
			'name'=>'jumlahNumber',
			'type'=>'raw',
			'value'=>Controller::getNet($data["last_update"],$netz),
		),*/
		array(
			'name'=>'Subscribe',
			'type'=>'raw',
			'value'=>'$data["jumlahSub"]',
		),
		array(
			'name'=>'Unsubscribe',
			'type'=>'raw',
			'value'=>'$data["jumlahUnSub"]',
		),
		array(
			'name'=>'Block',
			'type'=>'raw',
			'value'=>'$data["jumlahBlock"]',
		),
		array(
			'name'=>'Unblock',
			'type'=>'raw',
			'value'=>'$data["jumlahUnBlock"]',
		),
		array(
			'name'=>'Invalid Subscribe',
			'type'=>'raw',
			'value'=>'$data["jumlahF"]',
		),
		/*
		'first_user',
		'first_ip',
		
		'last_user',
		'last_ip',
		*/
	),
)); ?>
