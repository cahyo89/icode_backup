<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'history-globalblackwhitelist-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shortcode'); ?>
		<?php echo $form->textField($model,'shortcode',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'shortcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_text'); ?>
		<?php echo $form->textField($model,'sms_text'); ?>
		<?php echo $form->error($model,'sms_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action'); ?>
		<?php echo $form->textField($model,'action'); ?>
		<?php echo $form->error($model,'action'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_update'); ?>
		<?php echo $form->textField($model,'first_update'); ?>
		<?php echo $form->error($model,'first_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_user'); ?>
		<?php echo $form->textField($model,'first_user',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_ip'); ?>
		<?php echo $form->textField($model,'first_ip',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_ip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_update'); ?>
		<?php echo $form->textField($model,'last_update'); ?>
		<?php echo $form->error($model,'last_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_user'); ?>
		<?php echo $form->textField($model,'last_user',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_ip'); ?>
		<?php echo $form->textField($model,'last_ip',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'last_ip'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->