<?php
$this->breadcrumbs=array(
	'History Globalblackwhitelists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HistoryGlobalblackwhitelist', 'url'=>array('index')),
	array('label'=>'Manage HistoryGlobalblackwhitelist', 'url'=>array('admin')),
);
?>

<h1>Create HistoryGlobalblackwhitelist</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>