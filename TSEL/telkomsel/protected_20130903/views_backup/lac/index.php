<?php
$this->breadcrumbs=array(
	'Lacs',
);

$this->menu=array(
	array('label'=>'Create Lac', 'url'=>array('create')),
	array('label'=>'Manage Lac', 'url'=>array('admin')),
);
?>

<h1>Lacs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
