<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bts_id')); ?>:</b>
	<?php echo CHtml::encode($data->bts_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lac')); ?>:</b>
	<?php echo CHtml::encode($data->lac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('show_flag')); ?>:</b>
	<?php echo CHtml::encode($data->show_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_stat_ip')); ?>:</b>
	<?php echo CHtml::encode($data->loc_stat_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->flag_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_loc_stat')); ?>:</b>
	<?php echo CHtml::encode($data->id_loc_stat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gis_id')); ?>:</b>
	<?php echo CHtml::encode($data->gis_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gis_name')); ?>:</b>
	<?php echo CHtml::encode($data->gis_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gis_date')); ?>:</b>
	<?php echo CHtml::encode($data->gis_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sector_type')); ?>:</b>
	<?php echo CHtml::encode($data->sector_type); ?>
	<br />

	*/ ?>

</div>