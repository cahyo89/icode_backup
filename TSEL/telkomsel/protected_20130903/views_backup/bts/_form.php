<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bts-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	

	<div class="row">
		<?php echo $form->labelEx($model,'bts_name'); ?>
		<?php echo $form->textField($model,'bts_name',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'bts_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'cellname'); ?>
		<?php echo $form->textField($model,'cellname',array('size'=>50,'maxlength'=>50)); //,'readonly'=>$model->isNewRecord ? false:true ?>
		<?php echo $form->error($model,'cellname'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'site_id'); ?>
		<?php echo $form->textField($model,'site_id',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'site_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'site_type'); ?>
		<?php echo $form->dropDownList($model,'site_type',Yii::app()->params["site_type"],array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'site_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bts_id'); ?>
		<?php echo $form->textField($model,'bts_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'bts_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'lac'); ?>
		<?php echo $form->dropDownList($model,'lac',CHtml::listData(Lac::model()->findAll(array('condition'=>'flag_deleted IS NULL or flag_deleted <> 1','order'=>'lac_name')), 'lac_id', 'lac_name'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'lac'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'latitude'); ?>
	</div>

	


	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->