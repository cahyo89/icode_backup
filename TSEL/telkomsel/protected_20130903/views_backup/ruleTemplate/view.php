<?php
$this->breadcrumbs=array(
	'Rule Templates'=>array('index'),
	$model->rule_name,
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Rule Template #<?php echo $model->rule_name; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'ruleTemplate/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'Delete','link'=>'ruleTemplate/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->rule_name),
	   array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rule_name',
		array(
			'label'=>'Customer',
			'name'=>'Customer.nama',
		),
		'pause_time',
		'resume_time',
	),
)); 
Controller::createInfo($model);
?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'ruleTemplate/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	    array('label'=>'Delete','link'=>'ruleTemplate/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->rule_name)
	   
		));  ?>
</div>
