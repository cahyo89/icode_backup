<?php
$this->breadcrumbs=array(
	'Rule Templates'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rule-template-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Rule Templates</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rule-template-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'rule_name',
			'type'=>'raw',
			'value'=>'CHtml::link($data->rule_name,array("ruleTemplate/view","id"=>$data->id))',
		),
		array(
			'name'=>'id_customer',
			'value'=>'@$data->Customer->nama'
		),
		'pause_time',
		'resume_time',
		/*
		'description',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'reason_reject',
		'flag',
		*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\Rule Template :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>
