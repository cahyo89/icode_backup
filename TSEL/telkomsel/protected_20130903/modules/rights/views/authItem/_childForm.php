<div class="form">

<?php $form=$this->beginWidget('CActiveForm',array('id'=>'itemchild'));?>
	
	<div class="row">
		<?php echo $form->hiddenField($model,'name',array('value'=>$usertype,'type'=>"hidden",'size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->dropDownList($model, 'temp', $itemnameSelectOptions,array('multiple'=>true ,'style'=>'width:400px;','size'=>'25')); ?>
		<?php echo $form->dropDownList($model, 'itemname', $currChildList,array('multiple'=>true ,'style'=>'width:400px;','size'=>'25')); ?>
		<?php echo $form->error($model, 'itemname'); ?>	
	</div>
	 
	<div class="row buttons">
		<input id="buttonAdd" type="button" value="Add" />
		<input id="buttonRemove" type="button" value="Remove" />
		<?php echo CHtml::submitButton(Rights::t('core', 'Submit'),array('onclick'=>'$("#AuthChildForm_itemname option").attr("selected","selected");')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
