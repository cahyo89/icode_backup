
<script>
$(function(){
	$("#buttonAdd").click(function(){
		$("#AuthChildForm_temp option:selected").each(function(){
			$(this).remove().appendTo("#AuthChildForm_itemname");
		});
	});
	
	$("#buttonRemove").click(function(){
		$("#AuthChildForm_itemname option:selected").each(function(){
		var flag = 0;
		var data=$("#itemchild").serialize();
		var temp = $("#AuthChildForm_itemname option:selected").val();
		/*
		$.ajax({
			type: 'POST',
			url: '<?php echo Yii::app()->createAbsoluteUrl("rights/authItem/updatechild"); ?>',
			data:data,
			error: function(data) { // if error occured
				alert("Error occured.please try again");
				flag = 1;
			},
			dataType:'html'
		});
		*/
			if(flag == 0)
			{
				$(this).remove().appendTo("#AuthChildForm_temp");
			}
			else
			{
				$("#AuthChildForm_itemname option:selected").appendTo(temp);
			}
		});
	});
});
</script>

<?php $this->breadcrumbs = array(
	Rights::getAuthItemTypeNamePlural($model->type)=>Rights::getAuthItemRoute($model->type),
	$model->name,
); ?>

<div id="updatedAuthItem">

	<h2><?php echo Rights::t('core', 'Update :name', array(
		':name'=>$model->name,
		':type'=>Rights::getAuthItemTypeName($model->type),
	)); ?></h2>

	<?php $this->renderPartial('_form', array('model'=>$formModel)); ?>

	<div class="relations span-11 last">

		<h3><?php echo Rights::t('core', 'Relations'); ?></h3>

		<?php if( $model->name!==Rights::module()->superuserName ): ?>

			<div class="parents">

				<h4><?php echo Rights::t('core', 'Parents'); ?></h4>

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'dataProvider'=>$parentDataProvider,
					'template'=>'{items}',
					'hideHeader'=>true,
					'emptyText'=>Rights::t('core', 'This item has no parents.'),
					'htmlOptions'=>array('class'=>'grid-view parent-table mini'),
					'columns'=>array(
    					array(
    						'name'=>'name',
    						'header'=>Rights::t('core', 'Name'),
    						'type'=>'raw',
    						'htmlOptions'=>array('class'=>'name-column'),
    						'value'=>'$data->getNameLink()',
    					),
    					array(
    						'name'=>'type',
    						'header'=>Rights::t('core', 'Type'),
    						'type'=>'raw',
    						'htmlOptions'=>array('class'=>'type-column'),
    						'value'=>'$data->getTypeText()',
    					),
    					array(
    						'header'=>'&nbsp;',
    						'type'=>'raw',
    						'htmlOptions'=>array('class'=>'actions-column'),
    						'value'=>'$data',
    					),
					)
				));?>

			</div>

			<div class="addChild">

				<h5><?php echo Rights::t('core', 'Add Child'); ?></h5>

				<?php if( $childFormModel!==null ): ?>
					<?php $this->renderPartial('_childForm', array(
						'usertype'=>$model->name,
						'model'=>$childFormModel,
						'itemnameSelectOptions'=>$childSelectOptions,
						'currChildList'=>$currChildList,
					)); ?>

				<?php else: ?>

					<p class="info"><?php echo Rights::t('core', 'No children available to be added to this item.'); ?>

				<?php endif; ?>

			</div>

		<?php else: ?>

			<p class="info">
				<?php echo Rights::t('core', 'No relations need to be set for the superuser role.'); ?><br />
				<?php echo Rights::t('core', 'Super users are always granted access implicitly.'); ?>
			</p>

		<?php endif; ?>

	</div>

</div>