<?php
if(Rights::getAuthorizer()->isSuperusera(Yii::app()->user->name) == false){
echo "<br>";
}
else
{
$this->widget('zii.widgets.CMenu', array(
	'firstItemCssClass'=>'first',
	'lastItemCssClass'=>'last',
	'htmlOptions'=>array('class'=>'actions'),
	'items'=>array(
		array(
			'label'=>Rights::t('core', 'Assignments'),
			'url'=>array('assignment/view'),
			'itemOptions'=>array('class'=>'item-assignments'),
		),
		array(
			'label'=>Rights::t('core', 'Permissions'),
			'url'=>array('authItem/permissions'),
			'itemOptions'=>array('class'=>'item-permissions'),
		),
		array(
			'label'=>Rights::t('core', 'Roles'),
			'url'=>array('authItem/roles'),
			'itemOptions'=>array('class'=>'item-roles'),
		),
		array(
			'label'=>Rights::t('core', 'Tasks'),
			'url'=>array('authItem/tasks'),
			//'visible'=>Rights::getAuthorizer()->isSuperusera(Yii::app()->user->name),
			'itemOptions'=>array('class'=>'item-tasks'),
		),
		array(
			'label'=>Rights::t('core', 'Operations'),
			'url'=>array('authItem/operations'),
			//'visible'=>Rights::getAuthorizer()->isSuperusera(Yii::app()->user->name),
			'itemOptions'=>array('class'=>'item-operations'),
		),
		array(
			'label'=>Rights::t('core', 'Generate'),
			'url'=>array('authItem/generate'),
			//'visible'=>Rights::getAuthorizer()->isSuperusera(Yii::app()->user->name),
			'itemOptions'=>array('class'=>'item-operations'),
		),
	),
));	
}
?>