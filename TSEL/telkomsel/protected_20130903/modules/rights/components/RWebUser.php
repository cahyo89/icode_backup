<?php
/**
* Rights web user class file.
*
* @author Christoffer Niska <cniska@live.com>
* @copyright Copyright &copy; 2010 Christoffer Niska
* @since 0.5
*/
class RWebUser extends CWebUser
{
	/**
	* Actions to be taken after logging in.
	* Overloads the parent method in order to mark superusers.
	* @param boolean $fromCookie whether the login is based on cookie.
	*/
	public function afterLogin($fromCookie)
	{
		parent::afterLogin($fromCookie);

		// Mark the user as a superuser if necessary.
		if( Rights::getAuthorizer()->isSuperuser($this->getId())===true )
			$this->isSuperuser = true;
	}

	/**
	* Performs access check for this user.
	* Overloads the parent method in order to allow superusers access implicitly.
	* @param string $operation the name of the operation that need access check.
	* @param array $params name-value pairs that would be passed to business rules associated
	* with the tasks and roles assigned to the user.
	* @param boolean $allowCaching whether to allow caching the result of access checki.
	* This parameter has been available since version 1.0.5. When this parameter
	* is true (default), if the access check of an operation was performed before,
	* its result will be directly returned when calling this method to check the same operation.
	* If this parameter is false, this method will always call {@link CAuthManager::checkAccess}
	* to obtain the up-to-date access result. Note that this caching is effective
	* only within the same request.
	* @return boolean whether the operations can be performed by this user.
	*/
	public function checkAccess($operation, $params=array(), $allowCaching=true)
	{
		// Allow superusers access implicitly and do CWebUser::checkAccess for others.
		if(!Yii::app()->user->isGuest)
		{
			$user = $this->loadUser(Yii::app()->user->id);
			if($user->usermode == 0)
			return true;
			else
			return $this->isSuperuser===true ? true : parent::checkAccess($operation, $params, $allowCaching);
		}
		else
			parent::checkAccess($operation, $params, $allowCaching);
	}

	/**
	* @param boolean $value whether the user is a superuser.
	*/
	public function setIsSuperuser($value)
	{
		$this->setState('Rights_isSuperuser', $value);
	}

	/**
	* @return boolean whether the user is a superuser.
	*/
	public function getIsSuperuser()
	{
		return $this->getState('Rights_isSuperuser');
	}
	
	/**
	 * @param array $value return url.
	 */
	public function setRightsReturnUrl($value)
	{
		$this->setState('Rights_returnUrl', $value);
	}
	
	/**
	 * Returns the URL that the user should be redirected to 
	 * after updating an authorization item.
	 * @param string $defaultUrl the default return URL in case it was not set previously. If this is null,
	 * the application entry URL will be considered as the default return URL.
	 * @return string the URL that the user should be redirected to 
	 * after updating an authorization item.
	 */
	public function getRightsReturnUrl($defaultUrl=null)
	{
		if( ($returnUrl = $this->getState('Rights_returnUrl'))!==null )
			$this->returnUrl = null;
		
		return $returnUrl!==null ? CHtml::normalizeUrl($returnUrl) : CHtml::normalizeUrl($defaultUrl);
	}
	 private $_model;
	 
	 
	 
 ///tambahan di luar RIGHTS
	  // Return first name.
	  // access it by Yii::app()->user->first_name
	  function getFirst_Name(){
	    $user = $this->loadUser($this->id);
	    return $user->username;
	  }
	  function getIdAdvertiser(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return $user->id_advertiser;
	  }
	   function getIdMediaSeller(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return $user->id_media_seller;
	  }
	   function getIdUser(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return $user->id;
	  }
	  function getSuperuser()
	  {
		if(Yii::app()->user->id != ""){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->superuser;
		}
		else
		return false;
	  }
	   function getIdCustomer(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return $user->id_customer;
	  }
	 function getLastAction(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return $user->last_action;
	  }
	  function getUserMode()
	  {

		$user = $this->loadUser(Yii::app()->user->id);
	    return $user->usermode;
	  }
	   function setLastAction(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    $user->last_action = date("Y-m-d H:i:s");
		$user->update();
		//return $user->id_customer;
	  }
	 
	  // This is a function that checks the field 'role'
	  // in the User model to be equal to 1, that means it's admin
	  // access it by Yii::app()->user->isAdmin()
	  function isAdmin(){
	    $user = $this->loadUser(Yii::app()->user->id);
	    return intval($user->superuser) == 1;
	  }
	   function isGuest($param=false){
	    return $param;
	  }
	 
	  // Load user model.
	  protected function loadUser($id=null)
	    {
	        if($this->_model===null)
	        {
	            if($id!==null)
	                $this->_model=User::model()->findByPk($id);
	        }
	        return $this->_model;
	    }
	
	public $authExpires;            // let authentication expire if user is inactive for this number of seconds
 
	public function getIsGuest()
	{
	    $isGuest=$this->getState('__id')===null;
	    $expires=$this->getState('__expires');

	    if (!$isGuest && $this->authExpires!==null)
	    {
	        if ($expires!==null && $expires < time())  // authentication expired
	        {
	            // TBD:
	            //   - Either always (true) or never (false) destroys session data! Not what everyone wants...
	            //   - Make sure __expires is also cleared from session in logout()
				//Yii::app()->user->logout();
				$connection=Yii::app()->db;
				$berita = "Session Time Out";
				$dodo = $this->id;
				$sql= "update tbl_users set flag = 0,ip = 0 where id = '".$this->id."'";
				$command=$connection->createCommand($sql)->execute();
				$sql= "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".$this->id."','".$this->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
				$command=$connection->createCommand($sql)->execute();
	            $this->logout();
	            $isGuest=true;
	        }
	        else                    // update expiration timestamp
	        {
	            $this->setState('__expires',time()+$this->authExpires);
	        }
	    }
	    return $isGuest;
	}
	
	
}
