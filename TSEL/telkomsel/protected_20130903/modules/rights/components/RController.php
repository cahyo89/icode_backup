<?php
/**
* Rights base controller class file.
*
* @author Christoffer Niska <cniska@live.com>
* @copyright Copyright &copy; 2010 Christoffer Niska
* @since 0.6
*/
class RController extends CController
{
	/**
	* @property string the default layout for the controller view. Defaults to '//layouts/column1',
	* meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	*/
	public $layout='//layouts/column1';
	/**
	* @property array context menu items. This property will be assigned to {@link CMenu::items}.
	*/
	public $menu=array();
	/**
	* @property array the breadcrumbs of the current page. The value of this property will
	* be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	* for more details on how to specify this property.
	*/
	public $breadcrumbs=array();

	/**
	* The filter method for 'rights' access filter.
	* This filter is a wrapper of {@link CAccessControlFilter}.
	* @param CFilterChain $filterChain the filter chain that the filter is on.
	*/
	public function filterRights($filterChain)
	{
		$filter = new RightsFilter;
		$filter->allowedActions = $this->allowedActions();
		$filter->filter($filterChain);
	}

	/**
	* @return string the actions that are always allowed separated by commas.
	*/
	public function allowedActions()
	{
		return '';
	}

	/**
	* Denies the access of the user.
	* @param string $message the message to display to the user.
	* This method may be invoked when access check fails.
	* @throws CHttpException when called unless login is required.
	*/
	public function accessDenied($message=null)
	{
		if( $message===null )
			$message = Rights::t('core', 'You are not authorized to perform this action.');

		$user = Yii::app()->getUser();
		if( $user->isGuest===true )
			$user->loginRequired();
		else
			throw new CHttpException(403, $message);
	}
	
	//get user roles for menu
	function getRoles()
	{
		$roles = Rights::getAssignedRoles(Yii::app()->user->Id);
		$rolein = "(";
		foreach($roles as $role)
		{
			$rolein .= "'".$role->name."',";
		}
		$rolein = rtrim($rolein, ",");
		$rolein .= ")";
		return $rolein;
	}
	
	/**
		List Menu User
	*/
	function display_menu($rolein,$parent=0) {
		$menu;
		if($parent==0)
		$sql ="SELECT a.id, a.title, a.link, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent . " order by a.`order`";
		else
		$sql ="SELECT a.id, a.title, a.link, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id JOIN `AuthItemChild` aic on (substring(replace(link,'/','.'),2,LENGTH(link)-1) = lower(aic.child) or link = '#') WHERE a.parent_id=" . $parent . " and aic.parent in ".$rolein." GROUP BY id order by a.`order`";
		
		//group by link ori nya
		
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$rows=$command->queryAll();
		foreach($rows as $row)
		{
			if ($row['Count'] > 0) 
			{
				$cekItem = $this->display_menu($rolein,$row['id']);
				
				if($cekItem > 0)
				{
					$menu[] = array('label'=>$row['title'], 'dropdown' => true, 'url'=>array($row['link']), 'items' => $this->display_menu($rolein,$row['id']),'submenuOptions'=>array('class'=>'sub-menu'));
				}
			} elseif ($row['Count']==0) 
			{
				$urls = explode('/',$row['link']);
				$ceks = "select count(1) as ttl from AuthItemChild where child like '%".$urls[1]."%' and parent in ".$rolein;
				$exec = $connection->createCommand($ceks);
				$rt = $exec->queryScalar();
				
				//if($rt['ttl'] > 0 || $row['title'] == 'Dashboard')
				if($rt['ttl'] > 0 || $urls[1] == 'site')
				{
					$menu[] = array('label'=>$row['title'], 'url'=>array($row['link']));
				}
			} else;
		}
		return $menu;
	}
	
	/**
		List Menu Admin
	*/
	function display_menu_admin($parent=0) {
		$menu;
		$sql ="SELECT a.id, a.title, a.link, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent_id, COUNT(*) AS Count FROM `menu` GROUP BY parent_id) Deriv1 ON a.id = Deriv1.parent_id WHERE a.parent_id=" . $parent . "  order by a.`order`";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$rows=$command->queryAll();
		foreach($rows as $row)
		{
			if ($row['Count'] > 0) {
				$menu[] = array('label'=>$row['title'], 'dropdown' => true, 'url'=>array($row['link']), 'items' => $this->display_menu_admin($row['id']),'submenuOptions'=>array('class'=>'sub-menu'));
			} elseif ($row['Count']==0) {
				$menu[] = array('label'=>$row['title'], 'url'=>array($row['link']));
			} else;
		}
		return $menu;
	}
}
