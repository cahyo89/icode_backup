<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

?>



<div class="row">
	<div class="span8">
		<h1>Update Menus <?php echo $model->id; ?></h1>
		<?php echo $this->renderPartial('_form', array('model'=>$model,'controllerlink'=>$controllerlink,'parent'=>$parent)); ?>
	</div>
	<div class="span3">
		<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
				'label' => 'Side Menu', 'items' => array(
					array('label'=>'List Menu', 'url'=>array('index')),
					array('label'=>'Create Menu', 'url'=>array('create')),
					array('label'=>'View Menu', 'url'=>array('view', 'id'=>$model->id)),
					array('label'=>'Manage Menu', 'url'=>array('admin')),
				),
			),
		),
	));
	?>
	</div>
</div>
