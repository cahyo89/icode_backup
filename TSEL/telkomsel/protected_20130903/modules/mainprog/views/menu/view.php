<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	$model->title,
);

$this->menu=array(
);
?>
<div class="row">
<div class="span8">
	<h1>View Menu "<?php echo $model->title; ?>"</h1>

	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'link',
			'title',
		),
	)); ?>

</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label'=>'List Menu', 'url'=>array('index')),
				array('label'=>'Create Menu', 'url'=>array('create')),
				array('label'=>'Update Menu', 'url'=>array('update', 'id'=>$model->id)),
				array('label'=>'Delete Menu', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
				array('label'=>'Manage Menu', 'url'=>array('admin')),
			),
		),
	),
));
?>
</div>
</div>