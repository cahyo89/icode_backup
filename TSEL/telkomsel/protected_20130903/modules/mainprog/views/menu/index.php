<?php
/* @var $this ModulesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modules',
);

$this->menu=array(
	
);
?>




<div class="row">
<div class="span8">
	<h1>Menus</h1>

	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
	)); ?>
</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label'=>'Create Menu', 'url'=>array('create'),'icon' => 'plus'),
				array('label'=>'Manage Menu', 'url'=>array('admin'),'icon' => 'cog'),
			),
		),
	),
));
?>
</div>

<div class="row">
	<div class="span8">
	</div>
	<div class="span3">
	</div>
</div>