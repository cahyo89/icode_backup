<?php
/* @var $this ModulesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Modules',
);

$this->menu=array(
	
);
?>




<div class="row">
<div class="span8">
	<h1>Modules</h1>

	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
	)); ?>
</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label'=>'Create Modules', 'url'=>array('create'),'icon' => 'plus'),
				array('label'=>'Manage Modules', 'url'=>array('admin'),'icon' => 'cog'),
			),
		),
	),
));
?>
</div>