<?php
/* @var $this ModulesController */
/* @var $model Modules */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'modules-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>30,'class'=>"input-xlarge",'placeholder'=>"Type Format Module.Controller")); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="buttons" class="btn btn-danger">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array("class"=>"btn btn-danger")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->