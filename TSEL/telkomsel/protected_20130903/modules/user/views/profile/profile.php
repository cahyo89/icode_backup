<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("Profile"),
);
?>
<?php //echo $this->renderPartial('menu'); ?>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<table width="100%"><tr><td><h2>Your Profile</h2></td>
	<td><div class="operatorRight pull-right"><?php Controller::createMenu(array(
	//array('label'=>'Edit','link'=>'edit','img'=>'/images/update.png','id'=>'','conf'=>''),
	array('label'=>'Change Password','link'=>'changepassword','img'=>'/images/key.png','id'=>'','conf'=>'')
	)); ?></div></td>
	</tr>
</table>

<table class="dataGrid">
<tr>
	<th style="text-align:left;min-width:150px;"><?php echo CHtml::encode($model->getAttributeLabel('username')); ?></th>
    <td class="label"><?php echo CHtml::encode($model->username); ?></td>
</tr>
<?php /*
		$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
		if ($profileFields) {
			foreach($profileFields as $field) {
				//echo "<pre>"; print_r($profile); die();
			?>
<tr>
	<th class="label"><?php echo CHtml::encode(UserModule::t($field->title)); ?>
</th>
    <td><?php //echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?>
</td>
</tr>
			<?php
			}//$profile->getAttribute($field->varname)
		}*/
?>
<tr>
	<th style="text-align:left;"><?php echo CHtml::encode($model->getAttributeLabel('role')); ?></th>
    <td class="label"><?php echo Controller::getRoleUser($model->id); ?></td>
</tr>

<tr>
	<th style="text-align:left;"><?php echo CHtml::encode($model->getAttributeLabel('createtime')); ?></th>
    <td class="label"><?php echo date("d.m.Y H:i:s",$model->createtime); ?></td>
</tr>
<tr>
	<th style="text-align:left;"><?php echo CHtml::encode($model->getAttributeLabel('lastvisit')); ?></th>
    <td class="label"><?php echo date("d.m.Y H:i:s",$model->lastvisit); ?></td>
</tr>
<tr>
	<th style="text-align:left;"><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></th>
    <td class="label"><?php echo CHtml::encode(User::itemAlias("UserStatus",$model->status));?></td>
</tr>
</table>