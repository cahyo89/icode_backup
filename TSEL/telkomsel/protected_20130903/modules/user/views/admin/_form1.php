<div class="form">

<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo CHtml::errorSummary(array($model,$profile)); ?>

	<div class="row">
	
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo CHtml::error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo CHtml::error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'email'); ?>
		<?php echo CHtml::activeTextField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo CHtml::error($model,'email'); ?>
	</div>
	
	
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'superuser'); ?>
		<?php echo CHtml::activeDropDownList($model,'superuser',User::itemAlias('AdminStatus')); ?>
		<?php echo CHtml::error($model,'superuser'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'status'); ?>
		<?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus')); ?>
		<?php echo CHtml::error($model,'status'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'id_advertiser'); ?>
		<?php echo CHtml::activedropDownList($model,'id_advertiser',CHtml::listData(Advertiser::model()->findAll('deleted IS NULL or deleted <> :deleted',array(':deleted'=>1)), 'id', 'name'),array('empty'=>'--None--')); ?>
		<?php echo CHtml::error($model,'id_advertiser'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'active_date'); ?>
		<?php 
                $this->widget('zii.widgets.jui.CJuiDatePicker', 
                        array(
                                'model' => $model,
                                'attribute' => 'active_date',
								'language'=>'',
                                'options' => array(
								
                                        'showAnim' => 'fold',
                                        'dateFormat' => 'yy-mm-dd', 
                                        'defaultDate' => $model->active_date,
                                        'changeYear' => true,
                                        'changeMonth' => true,
                                ),
                ));
        ?><?php echo CHtml::error($model,'active_date'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'rtries_count'); ?>
		<?php echo CHtml::activeTextField($model,'rtries_count',array('size'=>11,'maxlength'=>10)); ?>
		<?php echo CHtml::error($model,'rtries_count'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'rtries_count_use'); ?>
		<?php echo CHtml::activeTextField($model,'rtries_count_use',array('size'=>11,'maxlength'=>10)); ?>
		<?php echo CHtml::error($model,'rtries_count_use'); ?>
	</div>
	
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'passage1'); ?>
		<?php echo CHtml::activeTextField($model,'passage1',array('size'=>11,'maxlength'=>10)); ?>
		<?php echo CHtml::error($model,'passage1'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'passage2'); ?>
		<?php echo CHtml::activeTextField($model,'passage2',array('size'=>11,'maxlength'=>10)); ?>
		<?php echo CHtml::error($model,'passage2'); ?>
	</div>
	
	
	
	
	
	
	
<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="row">
		<?php echo CHtml::activeLabelEx($profile,$field->varname); ?>
		<?php 
		if ($field->widgetEdit($profile)) {
			echo $field->widgetEdit($profile);
		} elseif ($field->range) {
			echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo CHtml::activeTextField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
		}
		 ?>
		<?php echo CHtml::error($profile,$field->varname); ?>
	</div>	
			<?php
			}
		}
?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php echo CHtml::endForm(); ?>

</div><!-- form -->