<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserLogin extends CFormModel
{
	public $username;
	public $password;
	private $_identity;
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array();
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		$userD=User::model()->findByAttributes(
	   	 array('username'=>$this->username)
		);
		if(!$this->hasErrors())  // we only want to authenticate when no input errors
		{
			$identity=new UserIdentity($this->username,$this->password);
			$identity->authenticate();
			
			switch($identity->errorCode)
			{
				case UserIdentity::ERROR_DOBLE_LOGIN:
					$this->addError("status",UserModule::t("Can't doble Login"));
					break;
				case UserIdentity::ERROR_NONE:
					$duration = 0; // 30 days
					Yii::app()->user->login($identity,$duration);
					break;
				case UserIdentity::ERROR_EMAIL_INVALID:
					$this->addError("username",UserModule::t("<span class=\"label label-important\">Email is incorrect.</span>"));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError("username",UserModule::t("<span class=\"label label-important\">Username is incorrect.</span>"));
					break;
				case UserIdentity::ERROR_STATUS_NOTACTIV:
					$this->addError("password",UserModule::t("<span class=\"label label-important\">You account is not activated.</span>"));
					break;
				case UserIdentity::ERROR_STATUS_BAN:
					$this->addError("password",UserModule::t("<span class=\"label label-important\">You account is blocked.</span>"));
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError("password",UserModule::t("<span class=\"label label-important\">Password is incorrect ,Try  remaining = ".($userD->rtries_count_use-1)." </span>"));
					break;
				
			}
		}
	}
}
