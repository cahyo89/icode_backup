<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if (strpos($this->username,"@")) {
			$user=Users::model()->findByAttributes(array('email'=>$this->username));
			//$user=User::model()->notsafe()->findByAttributes(array('email'=>$this->username));//<<harusnya seperti ini tapi ga dpt nilai kl $user->counter
			
		} else {
			$user=Users::model()->findByAttributes(array('username'=>$this->username));
			//$user=Users::model()->notsafe()->findByAttributes(array('username'=>$this->username));//<<harusnya seperti ini tapi ga dpt nilai kl $user->counter
		}
		

		
		/*if($user->rtries_count_use <= 0)
		{
		$sql="";
		$sql = "update tbl_users set status = 0 where username = '".$user->username."'";
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql)->execute();
		
		}*/
		
	
		if($user===null)
			if (strpos($this->username,"@")) {
				$this->errorCode=self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}
	
		else if(Yii::app()->getModule('user')->encrypting($this->password)!==$user->password ){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
			$sql = "update tbl_users set rtries_count_use = rtries_count_use-1 where username = '".$user->username."'";
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql)->execute();
			}
	
		else if($user->status==0&&Yii::app()->getModule('user')->loginNotActiv==false)
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		else if($user->rtries_count_use <= 0)
			$this->errorCode=self::ERROR_STATUS_BAN;
		else if($user->status==-1)
			$this->errorCode=self::ERROR_STATUS_BAN;
		elseif($user->active_date > date("Y-m-d"))
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		else {
			$sql = "update tbl_users set rtries_count_use = rtries_count where username = '".$user->username."'";
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql)->execute();
			$session=new CHttpSession;
			$session->open();
			//$session['nama']=$user->username;
			$session['id']=$user->id;
			yii::app()->user->setState('userSessionTimeout', time()+Yii::app()->params['sessionTimeoutSeconds']    );
			$session['name'] = 2;
			//$session = 1;
			$session['$user->id'] = $user->id;
			$connection=Yii::app()->db;
			$berita = "LOGIN";
			$sql= "update tbl_users set flag = 1 where id = $user->id";
			$command=$connection->createCommand($sql)->execute();
			$sql= "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".$user->id."','".$user->username."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
			$command=$connection->createCommand($sql)->execute();
			$this->_id=$user->id;
			$this->username=$user->username;
			$this->errorCode=self::ERROR_NONE;
			
		}
		return !$this->errorCode;
	}
    
    /**
    * @return integer the ID of the user record
    */
	public function getId()
	{
		return $this->_id;
	}
}