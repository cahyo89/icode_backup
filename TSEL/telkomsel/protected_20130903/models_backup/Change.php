<?php
/**
 * UserChangePassword class.
 * UserChangePassword is the data structure for keeping
 * user change password form data. It is used by the 'changepassword' action of 'UserController'.
 */
class Change extends CFormModel {
	public $id;
	public $password;
	public $verifyPassword;
	public $oldpassword;
	
	public function rules() {
		return array(
			array('password, verifyPassword,oldpassword', 'required'),
			array('oldpassword','cekPassword'),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' =>"Incorrect password (minimal length 4 symbols)."),
			array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => "Retype Password is incorrect."),
			array('password', 'compare', 'compareAttribute'=>'oldpassword','strict'=>true,'operator'=>'!=','message'=>'Old Password are same with New Password'),
		);
	}

	public function GetPassword ($id){
		$currentPassword = Yii::app()->db->createCommand('select password from tbl_users where id = '.$id.'')->queryScalar();

		return $currentPassword;
	}

	public function cekPassword($attribute,$params)
	{
		$passW = $this->GetPassword($this->id);
		User::model()->findbyPk($this->id);

		if($passW != UserModule::encrypting($this->oldpassword)){
			$this->addError('oldpassword',"Old Password Wrong.");
		}
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'password'=>"password",
			'verifyPassword'=>"Retype Password",
			'oldpassword'=>"Old Password",
		);
	}
} 