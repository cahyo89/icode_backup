<?php

/**
 * This is the model class for table "{{customer}}".
 *
 * The followings are the available columns in table '{{customer}}':
 * @property integer $id_customer
 * @property string $nama
 * @property integer $tipe_customer
 * @property integer $prepaid_value
 * @property string $customer_ani
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 * @property integer $flag_deleted
 * @property integer $usage
 * @property integer $booking
 * @property integer $category_customer
 * @property integer $tipe_paket
 * @property string $expired_date
 * @property integer $approved
 * @property string $reason
 * @property integer $temp_prepaid
 * @property string $reason_prepaid
 * @property integer $approved_prepaid
 * @property integer $segmen_customer
 * @property integer $id_user
 * @property integer $bypass_approval
 * @property integer $quota_periode
 * @property integer $quota_value
 * @property integer $blocked
 * @property string $blocked_reason
 */
class SettingBalance extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SettingBalance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('temp_prepaid', 'required'),
			array('temp_prepaid', 'numerical', 'integerOnly'=>true),
			array('tipe_customer, prepaid_value, flag_deleted, usage, booking, category_customer, tipe_paket, approved, temp_prepaid, approved_prepaid, segmen_customer, id_user, bypass_approval, quota_periode, quota_value, blocked', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>30),
			array('customer_ani, first_ip, last_ip', 'length', 'max'=>20),
			array('first_user, last_user', 'length', 'max'=>50),
			array('reason, reason_prepaid, blocked_reason', 'length', 'max'=>200),
			array('first_update, last_update, expired_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_customer, nama, tipe_customer, prepaid_value, customer_ani, first_user, first_ip, first_update, last_user, last_ip, last_update, flag_deleted, usage, booking, category_customer, tipe_paket, expired_date, approved, reason, temp_prepaid, reason_prepaid, approved_prepaid, segmen_customer, id_user, bypass_approval, quota_periode, quota_value, blocked, blocked_reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Paket'=>array(self::BELONGS_TO, 'Paket', 'tipe_paket'),
			'TreeId'=>array(self::BELONGS_TO, 'TreeId', 'category_customer')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_customer' => 'Id Customer',
			'nama' => 'Name',
			'tipe_customer' => 'Type Customer',
			'prepaid_value' => 'Prepaid Value',
			'customer_ani' => 'Customer Ani',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
			'flag_deleted' => 'Flag Deleted',
			'usage' => 'Usage',
			'booking' => 'Booking',
			'category_customer' => 'Category Customer',
			'tipe_paket' => 'Type Paket',
			'expired_date' => 'Expired Date',
			'approved' => 'Approved',
			'reason' => 'Reason',
			'temp_prepaid' => 'Balance',
			'reason_prepaid' => 'Reason Reject',
			'approved_prepaid' => 'Approved Balance',
			'segmen_customer' => 'Segmen Customer',
			'id_user' => 'Id User',
			'bypass_approval' => 'Bypass Approval',
			'quota_periode' => 'Quota Periode',
			'quota_value' => 'Quota Value',
			'blocked' => 'Blocked',
			'blocked_reason' => 'Blocked Reason',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		
		if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser')){
			$criteria->condition=' id_customer = '.Yii::app()->user->idCustomer.' ';
			$criteria->condition=' (segmen_customer is null or segmen_customer <> 0) and approved = 1 and blocked = 0 and (flag_deleted IS NULL or flag_deleted <>:deleted)';
			$criteria->params=array(':deleted'=>1);	
		}
		else
		{
			$criteria->condition='(segmen_customer is null or segmen_customer <> 0) and  (approved = 1 and blocked = 0) and (flag_deleted IS NULL or flag_deleted <>:deleted)';
			$criteria->params=array(':deleted'=>1);	
		}
		
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tipe_customer',$this->tipe_customer);
		$criteria->compare('prepaid_value',$this->prepaid_value);
		$criteria->compare('customer_ani',$this->customer_ani,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('usage',$this->usage);
		$criteria->compare('booking',$this->booking);
		$criteria->compare('category_customer',$this->category_customer);
		$criteria->compare('tipe_paket',$this->tipe_paket);
		$criteria->compare('expired_date',$this->expired_date,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('temp_prepaid',$this->temp_prepaid);
		$criteria->compare('reason_prepaid',$this->reason_prepaid,true);
		$criteria->compare('approved_prepaid',$this->approved_prepaid);
		$criteria->compare('segmen_customer',$this->segmen_customer);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('bypass_approval',$this->bypass_approval);
		$criteria->compare('quota_periode',$this->quota_periode);
		$criteria->compare('quota_value',$this->quota_value);
		$criteria->compare('blocked',$this->blocked);
		$criteria->compare('blocked_reason',$this->blocked_reason,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}