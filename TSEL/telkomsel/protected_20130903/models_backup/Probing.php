<?php

/**
 * This is the model class for table "{{probing}}".
 *
 * The followings are the available columns in table '{{probing}}':
 * @property integer $id
 * @property string $name
 * @property string $ip
 * @property integer $port
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class Probing extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Probing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{probing}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('probing_name,port,ip', 'required'),
			array('port, flag_deleted', 'numerical', 'integerOnly'=>true),
			array('probing_name', 'length', 'max'=>250),
			array('probing_name', 'cekUnique'),
			array('ip, first_user, last_user', 'length', 'max'=>50),
			array('ip', 'application.extensions.ipvalidator.IPValidator', 'version' => 'v4'),
			array('first_ip, last_ip', 'length', 'max'=>20),
			array('first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_probing, probing_name, ip, port, flag_deleted, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}
	public function cekUnique($attribute,$params)
	{
		$current = Probing::model()->findByAttributes(array('probing_name'=>$this->probing_name));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id_probing != $this->id_probing)
			$this->addError('probing_name',"$attribute '$this->probing_name' has already been taken.");
		}
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_probing' => 'ID',
			'Probing_name' => 'Name',
			'ip' => 'Ip',
			'port' => 'Port',
			'flag_deleted' => 'Flag Deleted',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->condition=' id in ('.Controller::getIdCustomerByIdUserAndesta().') ';
		$criteria->condition=' flag_deleted IS NULL or flag_deleted <>:deleted';
		$criteria->params=array(':deleted'=>1);	
		
		$criteria->compare('id_probing',$this->id_probing);
		$criteria->compare('probing_name',$this->probing_name,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}