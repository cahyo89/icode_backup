<?php

/**
 * This is the model class for table "{{lba_batch}}".
 *
 * The followings are the available columns in table '{{lba_batch}}':
 * @property integer $id_batch
 * @property integer $id_customer
 * @property string $jobs_id
 * @property string $start_periode
 * @property string $stop_periode
 * @property integer $sms_send_limit
 * @property integer $sms_sent
 * @property string $sms_text
 * @property integer $message_type
 * @property integer $message_bit
 * @property integer $status_batch
 * @property string $filename
 * @property string $first_ip
 * @property string $first_user
 * @property string $frist_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $topik
 * @property string $approved_time
 * @property integer $sms_success
 * @property integer $sms_failed
 * @property integer $deleted
 * @property string $schedule_delivery
 * @property string $process_date
 * @property integer $retry
 * @property string $comment
 * @property integer $id_invoice
 * @property string $batch_no
 * @property integer $last_value
 * @property string $content_expired
 * @property string $divre
 * @property string $filenameupload
 * @property integer $broadcast_type
 * @property integer $total_number
 * @property integer $booking
 * @property integer $usage
 * @property integer $last_filestatus_modif
 * @property string $customer_ani
 * @property integer $binary_data
 * @property string $prefix_detail
 * @property string $sms_text_view
 * @property integer $instant_failed
 * @property integer $tipe_batch
 * @property integer $promotion_media
 * @property integer $sex_category
 * @property integer $age_category
 * @property integer $status_category
 * @property string $filter_prefix
 * @property integer $education_category
 * @property integer $channel_category
 * @property integer $priority_category
 * @property integer $flag_deleted
 * @property integer $ast
 * @property string $cell
 * @property integer $flag_lba
 * @property integer $flag_file
 * @property integer $multimedia_type
 * @property integer $quota
 * @property string $profile
 * @property string $batch_type
 * @property integer $vip
 * @property integer $increase_counter
 * @property integer $unique_subscriber_per_batch
 * @property integer $flag_hour
 * @property integer $flag_multi_batch
 * @property string $slot_multi_batch
 * @property integer $id_batch_reference
 * @property string $black_list_profile
 * @property integer $flag_template_message
 * @property string $profile_prouction
 * @property string $profile_gender
 * @property string $profile_gprs
 * @property string $profile_kesehatan
 * @property string $profile_usia
 * @property string $profile_lifestyle
 * @property string $profile_sians
 * @property string $profile_travel
 * @property string $profile_arpu
 * @property integer $param_arpu
 * @property integer $param_usia
 * @property integer $profile_usia2
 * @property integer $profile_arpu2
 * @property integer $flag_master_batch
 * @property integer $interface_channel
 */
class Revenue extends CActiveRecord
{
    public $dateStart;
	public $customer;
	public $campaign;
	public $msisdn;
	public $url;
	
	public $qr;
	public $sr;
	public $fr;
	/**
	 * Returns the static model of the specified AR class.
	 * @return StaticBatchLba the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{lba_batch}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer, sms_send_limit, sms_sent, message_type, message_bit, status_batch, sms_success, sms_failed, deleted, retry, id_invoice, last_value, broadcast_type, total_number, booking, usage, last_filestatus_modif, binary_data, instant_failed, tipe_batch, promotion_media, sex_category, age_category, status_category, education_category, channel_category, priority_category, flag_deleted, ast, flag_lba, flag_file, multimedia_type, quota, vip, increase_counter, unique_subscriber_per_batch, flag_hour, flag_multi_batch, id_batch_reference, flag_template_message, param_arpu, param_usia, profile_usia2, profile_arpu2, flag_master_batch, interface_channel', 'numerical', 'integerOnly'=>true),
			array('jobs_id', 'length', 'max'=>200),
			array('filename, topik, divre, filenameupload, prefix_detail', 'length', 'max'=>100),
			array('first_ip, first_user, last_ip, last_user', 'length', 'max'=>30),
			array('batch_no', 'length', 'max'=>10),
			array('customer_ani', 'length', 'max'=>20),
			array('filter_prefix, profile_prouction, profile_gender, profile_gprs, profile_kesehatan, profile_usia, profile_lifestyle, profile_sians, profile_travel, profile_arpu', 'length', 'max'=>50),
			array('batch_type', 'length', 'max'=>250),
			array('start_periode, stop_periode, sms_text, frist_update, last_update, approved_time, schedule_delivery, process_date, comment, content_expired, sms_text_view, cell, profile, slot_multi_batch, black_list_profile', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_batch, id_customer, jobs_id, start_periode, stop_periode, sms_send_limit, sms_sent, sms_text, message_type, message_bit, status_batch, filename, first_ip, first_user, frist_update, last_ip, last_user, last_update, topik, approved_time, sms_success, sms_failed, deleted, schedule_delivery, process_date, retry, comment, id_invoice, batch_no, last_value, content_expired, divre, filenameupload, broadcast_type, total_number, booking, usage, last_filestatus_modif, customer_ani, binary_data, prefix_detail, sms_text_view, instant_failed, tipe_batch, promotion_media, sex_category, age_category, status_category, filter_prefix, education_category, channel_category, priority_category, flag_deleted, ast, cell, flag_lba, flag_file, multimedia_type, quota, profile, batch_type, vip, increase_counter, unique_subscriber_per_batch, flag_hour, flag_multi_batch, slot_multi_batch, id_batch_reference, black_list_profile, flag_template_message, profile_prouction, profile_gender, profile_gprs, profile_kesehatan, profile_usia, profile_lifestyle, profile_sians, profile_travel, profile_arpu, param_arpu, param_usia, profile_usia2, profile_arpu2, flag_master_batch, interface_channel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Customer'=>array(self::BELONGS_TO, 'Customer', 'id_customer'),
			'ChannelCategory'=>array(self::BELONGS_TO, 'ChannelCategory', 'channel_category'),
			'Bts'=>array(self::BELONGS_TO, 'Bts', 'cell')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_batch' => 'Id Batch',
			'id_customer' => 'Id Customer',
			'jobs_id' => 'Jobs',
			'start_periode' => 'Start Periode',
			'stop_periode' => 'Stop Periode',
			'sms_send_limit' => 'Total',
			'sms_sent' => 'Sms Sent',
			'sms_text' => 'Sms Text',
			'message_type' => 'Message Type',
			'message_bit' => 'Message Bit',
			'status_batch' => 'Status Batch',
			'filename' => 'Filename',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'frist_update' => 'Frist Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'topik' => 'Topik',
			'approved_time' => 'Approved Time',
			'sms_success' => ' Success',
			'sms_failed' => ' Failed',
			'deleted' => 'Deleted',
			'schedule_delivery' => 'Schedule Delivery',
			'process_date' => 'Process Date',
			'retry' => 'Retry',
			'comment' => 'Comment',
			'id_invoice' => 'Berita Acara',
			'batch_no' => 'Batch No',
			'last_value' => 'Last Value',
			'content_expired' => 'Content Expired',
			'divre' => 'Divre',
			'filenameupload' => 'Filenameupload',
			'broadcast_type' => 'Broadcast Type',
			'total_number' => 'Total Number',
			'booking' => 'Booking',
			'usage' => 'Usage',
			'last_filestatus_modif' => 'Last Filestatus Modif',
			'customer_ani' => 'Customer Ani',
			'binary_data' => 'Binary Data',
			'prefix_detail' => 'Prefix Detail',
			'sms_text_view' => 'Sms Text View',
			'instant_failed' => 'Instant Failed',
			'tipe_batch' => 'Tipe Batch',
			'promotion_media' => 'Promotion Media',
			'sex_category' => 'Sex Category',
			'age_category' => 'Age Category',
			'status_category' => 'Status Category',
			'filter_prefix' => 'Filter Prefix',
			'education_category' => 'Education Category',
			'channel_category' => 'Channel Category',
			'priority_category' => 'Priority Category',
			'flag_deleted' => 'Flag Deleted',
			'ast' => 'Ast',
			'cell' => 'Cell',
			'flag_lba' => 'Date by',
			'flag_file' => 'Flag File',
			'multimedia_type' => 'Multimedia Type',
			'quota' => 'Quota',
			'profile' => 'Profile',
			'batch_type' => 'Batch Type',
			'vip' => 'Vip',
			'increase_counter' => 'Increase Counter',
			'unique_subscriber_per_batch' => 'Unique Subscriber Per Batch',
			'flag_hour' => 'Flag Hour',
			'flag_multi_batch' => 'Flag Multi Batch',
			'slot_multi_batch' => 'Slot Multi Batch',
			'id_batch_reference' => 'Id Batch Reference',
			'black_list_profile' => 'Black List Profile',
			'flag_template_message' => 'Flag Template Message',
			'profile_prouction' => 'Profile Prouction',
			'profile_gender' => 'Profile Gender',
			'profile_gprs' => 'Profile Gprs',
			'profile_kesehatan' => 'Profile Kesehatan',
			'profile_usia' => 'Profile Usia',
			'profile_lifestyle' => 'Profile Lifestyle',
			'profile_sians' => 'Profile Sians',
			'profile_travel' => 'Profile Travel',
			'profile_arpu' => 'Profile Arpu',
			'param_arpu' => 'Param Arpu',
			'param_usia' => 'Param Usia',
			'profile_usia2' => 'Profile Usia2',
			'profile_arpu2' => 'Profile Arpu2',
			'flag_master_batch' => 'Flag Master Batch',
			'interface_channel' => 'Interface Channel',
			'qr' => 'Queue Rate',
			'sr' => 'Success Rate',
			'fr' => 'Failed Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_batch',$this->id_batch);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('jobs_id',$this->jobs_id,true);
		$criteria->compare('start_periode',$this->start_periode,true);
		$criteria->compare('stop_periode',$this->stop_periode,true);
		$criteria->compare('sms_send_limit',$this->sms_send_limit);
		$criteria->compare('sms_sent',$this->sms_sent);
		$criteria->compare('sms_text',$this->sms_text,true);
		$criteria->compare('message_type',$this->message_type);
		$criteria->compare('message_bit',$this->message_bit);
		$criteria->compare('status_batch',$this->status_batch);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('frist_update',$this->frist_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('topik',$this->topik,true);
		$criteria->compare('approved_time',$this->approved_time,true);
		$criteria->compare('sms_success',$this->sms_success);
		$criteria->compare('sms_failed',$this->sms_failed);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('schedule_delivery',$this->schedule_delivery,true);
		$criteria->compare('process_date',$this->process_date,true);
		$criteria->compare('retry',$this->retry);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('id_invoice',$this->id_invoice);
		$criteria->compare('batch_no',$this->batch_no,true);
		$criteria->compare('last_value',$this->last_value);
		$criteria->compare('content_expired',$this->content_expired,true);
		$criteria->compare('divre',$this->divre,true);
		$criteria->compare('filenameupload',$this->filenameupload,true);
		$criteria->compare('broadcast_type',$this->broadcast_type);
		$criteria->compare('total_number',$this->total_number);
		$criteria->compare('booking',$this->booking);
		$criteria->compare('usage',$this->usage);
		$criteria->compare('last_filestatus_modif',$this->last_filestatus_modif);
		$criteria->compare('customer_ani',$this->customer_ani,true);
		$criteria->compare('binary_data',$this->binary_data);
		$criteria->compare('prefix_detail',$this->prefix_detail,true);
		$criteria->compare('sms_text_view',$this->sms_text_view,true);
		$criteria->compare('instant_failed',$this->instant_failed);
		$criteria->compare('tipe_batch',$this->tipe_batch);
		$criteria->compare('promotion_media',$this->promotion_media);
		$criteria->compare('sex_category',$this->sex_category);
		$criteria->compare('age_category',$this->age_category);
		$criteria->compare('status_category',$this->status_category);
		$criteria->compare('filter_prefix',$this->filter_prefix,true);
		$criteria->compare('education_category',$this->education_category);
		$criteria->compare('channel_category',$this->channel_category);
		$criteria->compare('priority_category',$this->priority_category);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('ast',$this->ast);
		$criteria->compare('cell',$this->cell,true);
		$criteria->compare('flag_lba',$this->flag_lba);
		$criteria->compare('flag_file',$this->flag_file);
		$criteria->compare('multimedia_type',$this->multimedia_type);
		$criteria->compare('quota',$this->quota);
		$criteria->compare('profile',$this->profile,true);
		$criteria->compare('batch_type',$this->batch_type,true);
		$criteria->compare('vip',$this->vip);
		$criteria->compare('increase_counter',$this->increase_counter);
		$criteria->compare('unique_subscriber_per_batch',$this->unique_subscriber_per_batch);
		$criteria->compare('flag_hour',$this->flag_hour);
		$criteria->compare('flag_multi_batch',$this->flag_multi_batch);
		$criteria->compare('slot_multi_batch',$this->slot_multi_batch,true);
		$criteria->compare('id_batch_reference',$this->id_batch_reference);
		$criteria->compare('black_list_profile',$this->black_list_profile,true);
		$criteria->compare('flag_template_message',$this->flag_template_message);
		$criteria->compare('profile_prouction',$this->profile_prouction,true);
		$criteria->compare('profile_gender',$this->profile_gender,true);
		$criteria->compare('profile_gprs',$this->profile_gprs,true);
		$criteria->compare('profile_kesehatan',$this->profile_kesehatan,true);
		$criteria->compare('profile_usia',$this->profile_usia,true);
		$criteria->compare('profile_lifestyle',$this->profile_lifestyle,true);
		$criteria->compare('profile_sians',$this->profile_sians,true);
		$criteria->compare('profile_travel',$this->profile_travel,true);
		$criteria->compare('profile_arpu',$this->profile_arpu,true);
		$criteria->compare('param_arpu',$this->param_arpu);
		$criteria->compare('param_usia',$this->param_usia);
		$criteria->compare('profile_usia2',$this->profile_usia2);
		$criteria->compare('profile_arpu2',$this->profile_arpu2);
		$criteria->compare('flag_master_batch',$this->flag_master_batch);
		$criteria->compare('interface_channel',$this->interface_channel);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}