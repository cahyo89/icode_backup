<?php

/**
 * This is the model class for table "{{bts}}".
 *
 * The followings are the available columns in table '{{bts}}':
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $description
 * @property string $bts_id
 * @property string $longitude
 * @property string $latitude
 * @property string $first_ip
 * @property string $first_user
 * @property string $first_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $lac
 * @property integer $show_flag
 * @property string $loc_stat_ip
 * @property integer $flag_deleted
 * @property integer $id_loc_stat
 * @property string $gis_id
 * @property string $gis_name
 * @property string $gis_date
 * @property integer $sector_type
 */
class BtsGroup extends CActiveRecord
{
	var $slot;
	var $temp;
	var $searchT;
	var $searchLac;
	var $searchCi;
	var $searchName;
	public $flag_blacklist;
	/**
	 * Returns the static model of the specified AR class.
	 * @return BtsGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flag_blacklist, bts_name', 'required'),
			array('bts_name', 'cekUnique'),
			array('type, show_flag, flag_deleted, id_loc_stat, sector_type', 'numerical', 'integerOnly'=>true),
			array(' first_user, last_user', 'length', 'max'=>50),
			array('bts_name,description', 'length', 'max'=>100),
			array('bts_id', 'length', 'max'=>10),
			array('first_ip, last_ip, lac', 'length', 'max'=>20),
			array('loc_stat_ip, gis_id, gis_name', 'length', 'max'=>30),
			array('longitude, latitude, first_update, last_update, gis_date,slot', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, bts_name, description, bts_id, longitude, latitude, first_ip, first_user, first_update, last_ip, last_user, last_update, lac, show_flag, loc_stat_ip, flag_deleted, id_loc_stat, gis_id, gis_name, gis_date, sector_type', 'safe', 'on'=>'search'),
		);
	}

	public function cekUnique($attribute,$params)
	{
		$current = BtsGroup::model()->findByAttributes(array('bts_name'=>$this->bts_name,'type'=>1));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id != $this->id)
			$this->addError('bts_name',"$attribute '$this->bts_name' has already been taken.");
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'LocationStat'=>array(self::BELONGS_TO, 'LocationStat', 'id_loc_stat')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'bts_name' => 'Name',
			'description' => 'Description',
			'bts_id' => 'CI',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'lac' => 'Lac',
			'show_flag' => 'Show Flag',
			'loc_stat_ip' => 'Location Statistic',
			'flag_deleted' => 'Flag Deleted',
			'id_loc_stat' => 'Location Statistic',
			'gis_id' => 'Gis',
			'gis_name' => 'Gis Name',
			'gis_date' => 'Gis Date',
			'sector_type' => 'Sector Type',
			'flag_blacklist'=> 'Flag Blacklist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition=' type = 1 and (flag_deleted IS NULL or flag_deleted <>:deleted)';
		$criteria->params=array(':deleted'=>1);	
		
		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('bts_name',$this->bts_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('bts_id',$this->bts_id,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('lac',$this->lac,true);
		$criteria->compare('show_flag',$this->show_flag);
		$criteria->compare('loc_stat_ip',$this->loc_stat_ip,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('id_loc_stat',$this->id_loc_stat);
		$criteria->compare('gis_id',$this->gis_id,true);
		$criteria->compare('gis_name',$this->gis_name,true);
		$criteria->compare('gis_date',$this->gis_date,true);
		$criteria->compare('sector_type',$this->sector_type);
		$criteria->compare('flag_blacklist',$this->flag_blacklist);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}