<?php

/**
 * This is the model class for table "{{lac}}".
 *
 * The followings are the available columns in table '{{lac}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $lac_id
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 * @property string $host
 * @property integer $port
 * @property integer $id_probing
 */
class Lac extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Lac the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{lac}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lac_name,id_probing', 'required'),
			array('lac_name', 'cekUnique'),
			array('flag_deleted, port, id_probing', 'numerical', 'integerOnly'=>true),
			array('lac_name', 'length', 'max'=>255),
			array('description', 'length', 'max'=>250),
			array('lac_id, first_ip, last_ip', 'length', 'max'=>20),
			array('first_user, last_user', 'length', 'max'=>50),
			array('host', 'length', 'max'=>100),
			array('first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, lac_name, description, lac_id, flag_deleted, first_user, first_ip, first_update, last_user, last_ip, last_update, host, port, id_probing', 'safe', 'on'=>'search'),
		);
	}
	
	public function cekUnique($attribute,$params)
	{
		$current = Lac::model()->findByAttributes(array('lac_name'=>$this->lac_name));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id != $this->id)
			$this->addError('lac_name',"$attribute '$this->lac_name' has already been taken.");
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Probing'=>array(self::BELONGS_TO, 'Probing', 'id_probing')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lac_name' => 'Name',
			'description' => 'Description',
			'lac_id' => 'Lac ID',
			'flag_deleted' => 'Flag Deleted',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
			'host' => 'Host',
			'port' => 'Port',
			'id_probing' => 'Probing',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition=' flag_deleted IS NULL or flag_deleted <>:deleted';
		$criteria->params=array(':deleted'=>1);	
		
		$criteria->compare('id',$this->id);
		$criteria->compare('lac_name',$this->lac_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('lac_id',$this->lac_id,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('id_probing',$this->id_probing);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}