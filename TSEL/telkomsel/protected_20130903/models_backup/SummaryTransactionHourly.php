<?php

/**
 * This is the model class for table "{{summary_transaction_hourly}}".
 *
 * The followings are the available columns in table '{{summary_transaction_hourly}}':
 * @property integer $id
 * @property string $tanggal
 * @property integer $id_batch
 * @property integer $id_customer
 * @property integer $tipe
 */
class SummaryTransactionHourly extends CActiveRecord
{

	public $dataType;
    public $dateStart;
	public $dateEnd;
	public $hourStart;
	public $hourEnd;
	public $customer;
	public $campaign;
	public $tipe;
	public $total;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SummaryTransactionHourly the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{summary_transaction_hourly}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('total,id_batch, id_customer, tipe', 'numerical', 'integerOnly'=>true),
			array('tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tanggal, id_batch, id_customer, tipe, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Customer'=>array(self::BELONGS_TO, 'Customer', 'id_customer'),
			'ChannelCategory'=>array(self::BELONGS_TO, 'ChannelCategory', 'media_type'),
			'Bts'=>array(self::BELONGS_TO, 'Bts', 'id_group'),
			'Campaigne'=>array(self::BELONGS_TO, 'Campaigne', 'id_batch')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'id_batch' => 'Batch',
			'id_customer' => 'Customer',
			'tipe' => 'Tipe',
			'total' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('id_batch',$this->id_batch);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('tipe',$this->tipe);
		$criteria->compare('total',$this->tipe);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}