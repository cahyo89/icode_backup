<?php

/**
 * This is the model class for table "{{paket_campaign}}".
 *
 * The followings are the available columns in table '{{paket_campaign}}':
 * @property integer $id
 * @property string $nama
 * @property integer $harga
 * @property integer $tipe
 * @property integer $total
 * @property string $posisi
 * @property integer $ads_start_time
 * @property integer $ads_time_out
 * @property string $ukuran_gambar1
 * @property string $ukuran_gambar2
 * @property string $ukuran_gambar3
 * @property integer $priority
 * @property string $first_user
 * @property string $first_update
 * @property string $first_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $last_ip
 */
class PaketCampaign extends CActiveRecord
{
	public $http_intercept_ads_value;
	public $site;
	public $location;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaketCampaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{paket_campaign}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama,site,location, harga, tipe, total, posisi, ads_start_time, ads_time_out, ukuran_gambar1, ukuran_gambar2, ukuran_gambar3, priority, first_user, first_update, first_ip, last_user, last_update, last_ip', 'required'),
			array('harga,http_intercept_ads_value,site,location, tipe, total, ads_start_time, ads_time_out, priority', 'numerical', 'integerOnly'=>true),
			array('nama, first_user, last_user', 'length', 'max'=>100),
			array('posisi, ukuran_gambar1, ukuran_gambar2, ukuran_gambar3, first_ip, last_ip', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,http_intercept_ads_value,site,location, nama, harga, tipe, total, posisi, ads_start_time, ads_time_out, ukuran_gambar1, ukuran_gambar2, ukuran_gambar3, priority, first_user, first_update, first_ip, last_user, last_update, last_ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'harga' => 'Harga',
			'tipe' => 'Tipe',
			'total' => 'Total',
			'posisi' => 'Ads Type',
			'http_intercept_ads_value'=>"Ads Position",
			'site' => "Site Enable",
			"location"=>"Location Enable",
			'ads_start_time' => 'Ads Start Time',
			'ads_time_out' => 'Ads Time Out',
			'ukuran_gambar1' => 'Ukuran Gambar1',
			'ukuran_gambar2' => 'Ukuran Gambar2',
			'ukuran_gambar3' => 'Ukuran Gambar3',
			'priority' => 'Priority',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'first_ip' => 'First Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'last_ip' => 'Last Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('tipe',$this->tipe);
		$criteria->compare('total',$this->total);
		$criteria->compare('posisi',$this->posisi,true);
		$criteria->compare('ads_start_time',$this->ads_start_time);
		$criteria->compare('ads_time_out',$this->ads_time_out);
		$criteria->compare('ukuran_gambar1',$this->ukuran_gambar1,true);
		$criteria->compare('ukuran_gambar2',$this->ukuran_gambar2,true);
		$criteria->compare('ukuran_gambar3',$this->ukuran_gambar3,true);
		$criteria->compare('priority',$this->priority);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}