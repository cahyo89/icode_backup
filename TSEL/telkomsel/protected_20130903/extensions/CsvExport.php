<?php

class CsvExport {
	
	public static $rows;
	
	public static function addRow($row)
    {
		self::$rows = $row;
	}
	
    public static function export($data)
    {
        $endLine = "\r\n";
		header("Content-Type: application/force-download");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=test.csv");
		
		echo "Tanggal,Campaign,Customer,Type,Total".$endLine;
		foreach($data as $row)
		{
			echo "$row->tanggal,$row->id_batch,$row->id_customer,".Yii::app()->params['tipe_paket'][$row->tipe].",$row->total".$endLine;
		}
		
		return;
    }
	
	public static function exportDetail($data)
    {
		$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
        $endLine = "\r\n";
		header("Content-Type: application/force-download");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=test.csv");
		
		echo "DateTime,MSISDN,Tipe,Customer,Ads Code, URL Click, URL Response, Parent, Jobs ID".$endLine;
		foreach($data as $row)
		{
			echo "$row[request_time],$row[msisdn]".Yii::app()->params['tipe_paket'][$row['tipe']].$modeCustomer[$row['id_customer']]."$row[http_intercept_ads_value],$row[url_on_click],$row[url_requested],".$modeCustomer[$row['parent_id']].",$row[jobs_id]".$endLine;			
		}
		
		return;
    }
	
	
	public static function exportRevenue($data)
    {
		$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
        $endLine = "\r\n";
		header("Content-Type: application/force-download");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=test.csv");
		
		echo "Topik,Tipe,Customer,Nama Paket,Harga,Jobs ID".$endLine;
		foreach($data as $row)
		{
			echo "$row[topik],".Yii::app()->params['tipe_paket'][$row['http_intercept_ads_type']].",".$modeCustomer[$row['id_customer']]."$row[nama],$row[harga],$row[jobs_id]".$endLine;			
		}
		
		return;
    }
}