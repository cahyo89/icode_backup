<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends RController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
/*	 public function beforeAction(){
            // Check only when the user is logged in
            if ( !Yii::app()->user->isGuest)  {
               if ( yii::app()->user->getState('userSessionTimeout') < time() ) {
                   // timeout
                   Yii::app()->user->logout();
                   $this->redirect(array('/site/SessionTimeout'));  //
               } else {
                   yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
                   return true; 
               }
            } else {
                return true;
            }
        }
*/
public function aftersave($nama,$tanda,$id,$set=false,$data='',$namas='')
	{
			$connection = Yii::app()->db;
			$tabel = $this->tablename($tanda);
			$mod = call_user_func(array($nama,'model'));
			$current = $mod->findByPk($id);
		
			$sql="";
			$sql="update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip = '".CHttpRequest::getUserHostAddress()."',first_ip = '".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
			
			if($set)	
			{
				if($tanda == 2)
				{
					if($data['channel_use'] != "")
					{
						if(is_numeric($data['channel_use']))
						{
							$sql1 = "select channel_use from tbl_product_category where channel_use = '".$data['channel_use']."' ";
							$cek = $connection->createCommand($sql1)->queryScalar();
						
							if($cek == null)
							{
								$sql = "update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."', last_ip = '".CHttpRequest::getUserHostAddress()."', first_ip = '".CHttpRequest::getUserHostAddress()."', last_update = NOW(), first_update = NOW(), deleted = 0, `desc`='".$data['desc']."',channel_use = '".$data['channel_use']."' where id = $id";
								$berita = "Create ".$nama." : Name : $current->name, desc : ".$data['desc'].", deleted : 0, channel_use : ".$data['channel_use']." ";
							}				
							else
							{
								$error=1;
								return $error;
							}
						}
						else
						{
							$error=3;
							return $error;
						}
					}
					else
					{
						$error=2;
						return $error;
					}
				}
				else if($tanda == 3)
				{
					$sql = "update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip = '".CHttpRequest::getUserHostAddress()."', first_ip = '".CHttpRequest::getUserHostAddress()."', last_update = NOW(), first_update = NOW(),deleted = 0,id_media_seller = '".$data['id_media_seller']."' ,`desc`='".$data['desc']."',adv_type='".$data['adv_type']."' where id = $id";
					$berita = "Create ".$nama." : Name : $current->name, desc : ".$data['desc'].", deleted : 0, id media seller : ".$data['id_media_seller'].", adv type : ".$data['adv_type']." ";
				}
				else if($tanda == 4)
				{
					$sql= "update tbl_bsc set deleted = 0,`desc` = '".$data['desc']."',type = '".$data['type']."',ip_address = '".$data['ip_address']."',port = '".$data['port']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create ".$nama." : Name :$current->name , desc:".$data['desc']." , type :".$data['type']." , IP :".$data['ip_address']." , port :".$data['port']." , deleted :0";
				}
				else if($tanda == 5)
				{
					$sql= "update tbl_bsc_type set deleted = 0,`desc` = '".$data['desc']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create ".$nama." : Name :$current->name , desc:".$data['desc']." , Deleted :0 ";
				}
				else if($tanda == 6)
				{
					$sql= "update tbl_bts set deleted = 0,`desc` = '".$data['desc']."',cell_id = '".$data['cell_id']."',lac = '".$data['lac']."',latitude = '".$data['latitude']."',longitude = '".$data['longitude']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create  ".$nama." : BSC ID :".$data['bsc_id'].", Name:$current->name , Desc :".$data['desc']." , CELL ID :".$data['cell_id']." , LAC :".$data['lac']." , latitude :".$data['latitude']." , longitude :".$data['longitude']." , Deleted :0 ";
				}
				else if($tanda == 7)
				{
					$sql = "update tbl_bts_group set deleted = 0,`desc` = '".$data['desc']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create BTS GROUP : Name :$current->name , desc:".$data['desc'].", deleted :0 ";
				}
				else if($tanda == 8)
				{
					$sql= "update tbl_batch set deleted = 0,`desc` = '".$data['desc']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create Batch : id Advertiser :".$data['id_advertiser'].", id Media Seleer:".$data['id_media_seller'].", id Product Caterogry:".$data['id_product_category'].", Jobs ID :".$data['jobs_id']." , Topik :".$data['topik']." , Execution Date :".$data['execution_date']." , sms_text :".$data['sms_text']."  , Ani :".$data['ani']."    ";
				}
				else if($tanda == 9)
				{
					$sql = "update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW(),deleted = 0,`desc` = '".$data['desc']."', status = null where id = $id";
					$berita = "Create ".$nama." : Ani : $current->ani, desc : ".$data['desc'].", id_advertiser : ".$data['id_advertiser'].", deleted : 0";
				}
				else if($tanda == 10)
				{
					$sql= "update tbl_location_category set deleted = 0,`desc` = '".$data['desc']."',category_name = '".$data['category_name']."',category_price = '".$data['category_price']."',status = '".$data['status']."'
					,last_user = '".Yii::app()->user->first_name."' ,first_user = '".Yii::app()->user->first_name."' ,last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where id = $id";
					$berita = "Create Location Category : category_name :$current->category_name , Desc :$current->desc, Caterogry Price : $current->category_price , Status :$current->status  ";
				}
				else if($tanda == 11)
				{
					$sql= "update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip ='".CHttpRequest::getUserHostAddress()."',first_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW(),deleted = 0, status = null, packet_price = '".$data['packet_price']."', packet_unit = '".$data['packet_unit']."', packet_expired = '".$data['packet_expired']."' where id = $id";
					$berita = "Create ".$nama." : Packet Name : $current->packet_name, packet price : ".$data['packet_price'].", packet unit : ".$data['packet_unit'].", packet expired : ".$data['packet_expired'].", deleted : 0";
				}
				else
				{
					$sql = "update ".$tabel." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."', last_ip = '".CHttpRequest::getUserHostAddress()."', first_ip = '".CHttpRequest::getUserHostAddress()."', last_update = NOW(), first_update = NOW(),deleted = 0,`desc` = '".$data['desc']."' where id = $id";
					$berita = "Create ".$nama." : Name : $current->name, desc : ".$data['desc'].", deleted : 0 ";
				}
			}
			else
			{
				if($tanda == 2)
				{
					$berita = "Create ".$nama." : Name : $current->name, desc : $current->desc, deleted : $current->deleted, channel_use : $current->channel_use";
				}
				else if($tanda == 3)
				{
					$berita = "Create ".$nama." : Name : $current->name, desc : $current->desc, deleted : $current->deleted, id media seller : $current->id_media_seller, adv type : $current->adv_type";
				}
				else if($tanda == 4)
				{
					$berita = "Create ".$nama." : Name :$current->name , desc:$current->desc , type :$current->type , IP :$current->ip_address , port :$current->port,deleted : $current->deleted ";
				}
				else if($tanda == 5)
				{
					$berita = "Create ".$nama." : Name :$current->name , desc:$current->desc,deleted : $current->deleted ";
				}
				else if($tanda == 6)
				{
					$berita = "Create  ".$nama." : BSC ID :$current->bsc_id , Name:$current->name , Desc :$current->desc , CELL ID :$current->cell_id , LAC :$current->lac , latitude :$current->latitude , longitude :$current->longitude,deleted : $current->deleted ";
				}
				else if($tanda == 7)
				{
					$berita = "Create  ".$nama." :  Name:$current->name , Desc :$current->desc,deleted : $current->deleted ";
				}
				else if($tanda == 8)
				{
						$berita = "Create Batch : id Advertiser :$current->id_advertiser , id Media Seller: $current->id_media_seller, id Product Caterogry:$current->id_product_category , Jobs ID :$current->jobs_id , Topik :$current->topik , Execution Date :$current->execution_date , sms_text :$current->sms_text  , Ani :$current->ani    ";
				}
				else if($tanda == 9)
				{
					$berita = "Create ".$nama." : Ani : $current->ani, desc : $current->desc, id_advertiser : $current->id_advertiser,deleted : $current->deleted ";
				}
				else if($tanda == 10)
				{
					$berita = "Create Location Category : category_name :$current->category_name , Desc :$current->desc, Caterogry Price : $current->category_price , Status :$current->status  ";
				}
				else if($tanda == 11)
				{
					$berita = "Create ".$nama." : Packet Name : $current->packet_name, packet price : $current->packet_price, packet unit : $current->packet_unit, packet expired : $current->packet_expired,deleted : $current->deleted ";
				}
				else if($tanda == 13)
				{
					$berita = "Create ".$nama." :Name : $current->name";
				}
				else
				{
					$berita = "Create ".$nama." : Name : $current->name, desc : $current->desc, deleted : $current->deleted";
				}
			}
			$command = $connection->createCommand($sql)->execute();
			$this->addberita($berita);
	}
	public function afterdelete($data,$tanda)
	{
		$nama = $this->projectname($tanda);
		$data = explode(",",$data);
		$connection = Yii::app()->db;
		
		if($tanda == 2)
		{
			$berita = "Delete ".$nama." : Name : $data[0], desc : $data[1], channel use : $data[2]";
		}
		else if($tanda == 3)
		{
			$berita = "Delete ".$nama." : Name :$data[0], desc : $data[1], id media seller :$data[2], adv type : $data[3], ani : $data[4]";
		}
		else if($tanda == 4)
		{
			$berita = "Delete ".$nama." : Name :$data[0] , desc:$data[1] , type :$data[2]  , IP :$data[3] , port :$data[4] ";
		}
		else if($tanda == 5)
		{
			$berita = "Delete Bsc Type : Name :$data[0], desc:$data[1]";
		}
		else if($tanda == 6)
		{
			$berita = "Delete BTS : BSC ID :$data[0] , Name:$data[1]  , Desc :$data[2]  , CELL ID :$data[3]  , LAC :$data[4]  , latitude :$data[5], longitude :$data[5]";
		}
		else if($tanda == 7)
		{
			$berita = "Delete BTS GROUP : Name :$data[0]  , desc:$data[1]";
		}
		else if($tanda == 8)
		{
			$berita = "Delete Batch : id Advertiser :$data[0]  , id Media Seller:$data[1] id Product Caterogry:$data[2], Jobs ID :$data[3]  , Topik :$data[4] , Execution Date:$data[5] , sms_text :$data[6]   , Ani :$data[7]   ";
		}
		else if($tanda == 9)
		{
			$berita = "Delete ".$nama." : ani : $data[0], desc : $data[1]";
		}
		else if($tanda == 10)
		{
			$berita = "Delete Location Category : category_name :$data[0]  , Desc :$data[1] , Caterogry Price : $data[2] , Status :$data[3] ";
		}
		else if($tanda == 11)
		{
			$berita = "Delete ".$nama." : packet name : $data[0], packet price : $data[1], packet unit : $data[2], packet expired : $data[3]";
		}
		else if($tanda == 13)
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else
		{
			$berita = "Delete ".$nama." : Name : $data[0], desc : $data[1]";
		}
		$this->addberita($berita);
	}
	

 public function beforeAction(){
   if ( !Yii::app()->user->isGuest)  {
		$session=new CHttpSession;
		$session->open();
		$connection=Yii::app()->db;
		$sql = "select flag from tbl_users where id = '".$session['id']."' and ip = '".CHttpRequest::getUserHostAddress()."'";
		$sql1 = "select change_pass_date from tbl_users where id = '".$session['id']."'";
		
		$row1=$connection->createCommand($sql1)->queryScalar();
		$row=$connection->createCommand($sql)->queryScalar();
		
		//$this->redirect(array('/user/profile/changepassword'));
		if($row == 1){
		if ( yii::app()->user->getState('userSessionTimeout') < time() ) {
	            // timeout
	            Yii::app()->user->logout();
	            $this->redirect(array('/site/SessionTimeout'));  //
	        } else {
				Yii::app()->user->setLastAction();
	            yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
				$param= true; 
	  		}
		}
		else{
			Yii::app()->user->logout();
			$this->redirect(array('/user/login'));  //
			}
		return $param;	
		
	}
	else {
	return true;
	}
}
	

	public function afterCreate($nama,$id)
	{
			$connection = Yii::app()->db;
			$tabel = array();
			$tabel = $this->tablename($nama);
			$mod = call_user_func(array($nama,'model'));
			$current = $mod->findByPk($id);
			$sql="";
			$sql="update ".$tabel[0]." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip = '".CHttpRequest::getUserHostAddress()."',first_ip = '".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where ".$tabel[1]." = $id";
			$command = $connection->createCommand($sql)->execute();
		
				/*if($nama == 'Banned')
				{
					$berita = "Create ".$nama." : Banned : $current->banned_text ";
				}
				else if($nama == 'Bts')
				{
					$berita = "Create ".$nama." : Name : $current->bts_name, Type : $current->type";
				}
				else if($nama == 'Probing')
				{
					$berita = "Create ".$nama." : Name : $current->probing_name";
				}
				else if($nama == 'Lac')
				{
					$berita = "Create ".$nama." : Name : $current->lac_name";
				}
				else if($nama == 'LocationStat')
				{
					$berita = "Create ".$nama." : Name : $current->location_stat_name";
				}
				else if($nama == 'GlobalBlacklistAsli')
				{
					$berita = "Create ".$nama." : Name : $current->name";
				}
				else if($nama == 'TreeId')
				{
					$berita = "Create ".$nama." : Name : $current->nama_perangkat";
				}
				else if($nama == 'Paket')
				{
					$berita = "Create ".$nama." : Name : $current->jenis_paket";
				}
				else if($nama == 'Ani')
				{
					$berita = "Create ".$nama." : Name : $current->ani";
				}
				else if($nama == 'Customer')
				{
					$berita = "Create ".$nama." : Name : $current->nama";
				}
				else if($nama == 'ShortcodeConfiguration')
				{
					$berita = "Create ".$nama." : Name : $current->shortcode";
				}
				else if($nama == 'RuleTemplate')
				{
					$berita = "Create ".$nama." : Name : $current->rule_name";
				}
				else if($nama == 'InvoiceLba')
				{
					$berita = "Create ".$nama." : Invoice : $current->invoice_no";
				}
				else if($nama == 'UrlInterface')
				{
					$berita = "Create ".$nama." : Url : $current->url_interface";
				}
				else if($nama == 'GlobalWhitelist')
				{
					$berita = "Create ".$nama." : Number : $current->subscriber_number ";
				}
				else if($nama == 'GlobalConfigurationSubs')
				{
					$berita = "Create ".$nama." : Shortcode : $current->shortcode ";
				}
				else if($nama == 'SmppIn')
				{
					$berita = "Create ".$nama." : Name : $current->name ";
				}
				else if($nama == 'SmppOut')
				{
					$berita = "Create ".$nama." : Name : $current->smpp_conn_id ";
				}
			*/
			//$this->addberita($berita);
	}


	
	
 	
	
	public function afterUpdate($nama,$id)
	{
		$connection = Yii::app()->db;
		$tabel = array();
		$tabel = $this->tablename($nama);
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		$sql = "";
		$berita ="";
		$sql = "update ".$tabel[0]." set last_user = '".Yii::app()->user->first_name."', last_ip = '".CHttpRequest::getUserHostAddress()."', last_update = NOW() where ".$tabel[1]." = $id";
		$command = $connection->createCommand($sql)->execute();
		$data = $this->getData($nama,$id);
		/*if($nama == 'Banned')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : banned : $data[0]->$current->banned_text";
		}
		else if($nama == 'Bts')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->bts_name, type : $data[1]->$current->type";
		}
		else if($nama == 'Probing')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->probing_name";
		}
		else if($nama == 'Lac')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->lac_name";
		}
		else if($nama == 'LocationStat')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->location_stat_name";
		}
		else if($nama == 'GlobalBlacklistAsli')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->name";
		}
		else if($nama == 'TreeId')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->nama_perangkat";
		}
		else if($nama == 'Paket')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->jenis_paket";
		}
		else if($nama == 'Ani')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->ani";
		}
		else if($nama == 'Customer')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->nama";
		}
		else if($nama == 'ShortcodeConfiguration')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->shortcode";
		}
		else if($nama == 'RuleTemplate')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->rule_name";
		}
		else if($nama == 'InvoiceLba')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : invoice_no : $data[0]->$current->invoice_no";
		}
		else if($nama == 'UrlInterface')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Url : $data[0]->$current->url_interface";
		}
		else if($nama == 'GlobalWhitelist')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Number : $data[0]->$current->subscriber_number";
		}
		else if($nama == 'GlobalConfigurationSubs')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Shortcode : $data[0]->$current->shortcode";
		}
		else if($nama == 'SmppIn')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->name";
		}
		else if($nama == 'SmppOut')
		{
			$data = explode(",",$data);
			$berita = "Update ".$nama." : Name : $data[0]->$current->smpp_conn_id";
		}*/
		//$this->addberita($berita);
	}

	
	public function beforeDelete($nama,$id)
	{
		$data = $this->getData($nama,$id);
		$data = explode(",",$data);
		$connection = Yii::app()->db;
		
		if($nama == 'Banned')
		{
			$berita = "Delete ".$nama." : Banned Text : $data[0]";
		}
		else if($nama == 'Bts')
		{
			$berita = "Delete ".$nama." : Name : $data[0], type : $data[1]";
		}
		else if($nama == 'Probing')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Lac')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'LocationStat')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'GlobalBlacklistAsli')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'TreeId')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Paket')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Ani')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Customer')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'ShortcodeConfiguration')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'RuleTemplate')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'InvoiceLba')
		{
			$berita = "Delete ".$nama." : invoice_no : $data[0]";
		}
		else if($nama == 'UrlInterface')
		{
			$berita = "Delete ".$nama." : Url : $data[0]";
		}
		else if($nama == 'GlobalWhitelist')
		{
			$berita = "Delete ".$nama." : Number : $data[0]";
		}
		else if($nama == 'GlobalConfigurationSubs')
		{
			$berita = "Delete ".$nama." : Shortcode : $data[0]";
		}
		else if($nama == 'SmppIn')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'SmppOut')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		//$this->addberita($berita);
	}
	
	
	public function deleted($nama,$id)
	{
		//$nama = $this->projectname($tanda);
		//$current = $nama::model()->findByPk($id);
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		$current->flag_deleted = 1;
		if($nama == "Ani")
		{
			$current->status = null;
		}
		else if($nama == "Packet")
		{
			$current->status = null;
		}
		else if($nama == "Advertiser")
		{
			$current->approved_packet = null;
		}
		else if($nama == "ProductCategory")
		{
			$current->channel_use = null;
		}
		$current->update();
	}
	
	public function getData($nama,$id)
	{
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		if($nama == 'Banned')
		{
			$data = $current->banned_text;
		}
		else if($nama == 'Bts')
		{
			$data = $current->bts_name.','.$current->type;
		}
		else if($nama == 'Probing')
		{
			$data = $current->probing_name;
		}
		else if($nama == 'Lac')
		{
			$data = $current->lac_name;
		}
		else if($nama == 'LocationStat')
		{
			$data = $current->location_stat_name;
		}
		else if($nama == 'GlobalBlacklistAsli')
		{
			$data = $current->name;
		}
		else if($nama == 'TreeId')
		{
			$data = $current->nama_perangkat;
		}
		else if($nama == 'Paket')
		{
			$data = $current->jenis_paket;
		}
		else if($nama == 'Ani')
		{
			$data = $current->ani;
		}
		else if($nama == 'Customer')
		{
			$data = $current->nama;
		}
		else if($nama == 'ShortcodeConfiguration')
		{
			$data = $current->shortcode;
		}
		else if($nama == 'RuleTemplate')
		{
			$data = $current->rule_name;
		}
		else if($nama == 'InvoiceLba')
		{
			$data = $current->invoice_no;
		}
		else if($nama == 'UrlInterface')
		{
			$data = $current->url_interface;
		}
		else if($nama == 'GlobalWhitelist')
		{
			$data = $current->subscriber_number;
		}
		else if($nama == 'GlobalConfigurationSubs')
		{
			$data = $current->shortcode;
		}
		else if($nama == 'SmppIn')
		{
			$data = $current->name;
		}
		else if($nama == 'SmppOut')
		{
			$data = $current->smpp_conn_id;
		}
		return $data;
	}
	
	public function saveListbox($id,$tanda,$table,$nama,$tes)
	{
		$connection = Yii::app()->db;
		$tabel = $this->tablename($tanda);
		$sql = "select id,name from ".$tabel." where deleted is null or deleted <> 1";
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count = count($row);
		$x = 0;
		$cat = "";
			while($x < $count)
				{
					$cat[$row[$x]['name']] = $row[$x]['id'];
					$x++;
				}
		$x = 0;
		$count = count($_POST['cat']);
		$cos = "";
			while($x < $count)
				{
					$sqll="";
					$category[$x]=$cat[$_POST['cat'][$x]];
					$sqll = "insert into ".$table."  values($id,$category[$x])";
					$dataReader = $connection->createCommand($sqll)->execute();
					$sql = "select name from ".$tabel." where id = ".$category[$x]." ";
					$row = $connection->createCommand($sql)->queryScalar();
					$cos = $row.",".$cos;
					$x++;
				}
				$cos = substr($cos,0,strlen($cos)-1);
				$berita = " ".$nama." dengan nama ".$tes." Isi : ".$cos." ";
				$this->addberita($berita);
	}
	
	public function addberita($berita)
	{
		$connection=Yii::app()->db;
		$sql = "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
		$command = $connection->createCommand($sql)->execute();		
	}
	
	public function kueri($tanda,$model)
	{
		$tabel = $this->tablename($tanda);
		$connection = Yii::app()->db;
		$sql = "select id from ".$tabel." where name='$model->name' and deleted=1";
		$row = $connection->createCommand($sql)->queryScalar();
		return $row;
	}
	
	public function kueris($tanda,$model,$namas,$para='')
	{
		$tabel = $this->tablename($tanda);
		$connection = Yii::app()->db;
		$name = $model->$namas;
		if($para == "")
		{
			$sql = "select id from ".$tabel." where ".$namas."= '".$name."' and deleted=1";
		}
		else
		{
			$adv = $model->$para;
			$sql = "select id from ".$tabel." where ".$namas."= '".$name."' and id_advertiser = ".$adv."";
		}
		$row = $connection->createCommand($sql)->queryScalar();
		
		return $row;
	}
	
	public function cek1($tanda,$model,$nama,$para,$tnd)
	{
		$tabel = $this->tablename($tanda);
		$connection = Yii::app()->db;
		$name = $model->$nama;
		$adv = $model->$para;
		if($tnd == 1)
		{
			$sql = $sql = "select id from ".$tabel." where ".$nama."= '".$name."' and id_advertiser = ".$adv." and deleted = 1";
		}
		else if($tnd == 0)
		{
			$sql = $sql = "select id from ".$tabel." where ".$nama."= '".$name."' and id_advertiser = ".$adv." and (deleted <> 1 or deleted is null)";
		}
		
		$row = $connection->createCommand($sql)->queryScalar();
		return $row;
	}
	
	public function getsid($tanda,$model,$nama,$para)
	{
		$tabel = $this->tablename($tanda);
		$connection = Yii::app()->db;
		$name = $model->$nama;
		$adv = $model->$para;
		
		$sql = $sql = "select id from ".$tabel." where ".$nama."= '".$name."' and id_advertiser = ".$adv." and (deleted <> 1 or deleted is null)";
		
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		return $row[0]['id'];
	}
	
	public function cek($tanda,$model,$nama,$para,$tnd)
	{
		$tabel = $this->tablename($tanda);
		$connection = Yii::app()->db;
		$name = $model->$nama;
		$adv = $model->$para;
			if ($tnd == 1)
			{
				$sql = "select id from ".$tabel." where ".$nama." = '".$name."' and id_advertiser = ".$adv." and (deleted <> 1 or deleted is null)";
			}
			else if($tnd == 2)
			{
				$sql = "select id from ".$tabel." where deleted =1 and ".$nama." = '".$name."' and id_advertiser = ".$adv." ";
			}
			$row2 = $connection->createCommand($sql)->queryScalar();
		return $row2;
	}
	
	public function delListBox($id,$table,$del)
	{
		$connection = Yii::app()->db;
		$sql1 = "delete from ".$table." where ".$del." = $id";
		$command = $connection->createCommand($sql1)->execute();
	}
	
	public function projectname($flags)
	{	
		if($flags == "1")
		{
			$nama = "MediaSeller";
		}
		else if($flags == 2)
		{
			$nama = "ProductCategory";
		}
		else if($flags == 3)
		{
			$nama = "Advertiser";
		}
		else if($flags == 4)
		{
			$nama = "Bsc";
		}
		else if($flags == 5)
		{
			$nama = "BscTypes";
		}
		else if($flags == 6)
		{
			$nama = "Bts";
		}
		else if($flags == 7)
		{
			$nama = "BtsGroup";
		}
		else if($flags == 8)
		{
			$nama = "Batch";
		}
		else if($flags == 9)
		{
			$nama = "Ani";
		}
		else if($flags == 10)
		{
			$nama = "LocationCategory";
		}
		else if($flags == 11)
		{
			$nama = "Packet";
		}
		else if($flags == 13)
		{
			$nama = "Cbc";
		}
		return $nama;
	}
	
	function tablename($flags)
	{
		if($flags == 1)
		{
			$tabel = "tbl_media_seller";
		}
		else if($flags == 2)
		{
			$tabel = "tbl_product_category";
		}
		else if($flags == 3)
		{
			$tabel = "tbl_advertiser";
		}
		else if($flags == 4)
		{
			$tabel = "tbl_bsc";
		}
		else if($flags == 5)
		{
			$tabel = "tbl_bsc_type";
		}
		else if($flags == "Bts")
		{
			$tabel[0] = "tbl_bts";
			$tabel[1] = "id";
		}
		else if($flags == 7)
		{
			$tabel = "tbl_bts_group";
		}
		else if($flags == 8)
		{
			$tabel = "tbl_batch";
		}
		else if($flags == "Ani")
		{
			$tabel[0] = "tbl_ani";
			$tabel[1] = "id";
		}
		else if($flags == 10)
		{
			$tabel = "tbl_location_category";
		}
		else if($flags == 11)
		{
			$tabel = "tbl_packet";
		}
		else if($flags == 12)
		{
			$tabel = "tbl_advertiser";
		}
		else if($flags == 13)
		{
			$tabel = "tbl_cbc";
		}
		else if($flags == "Banned")
		{
			$tabel[0] = "tbl_banned";
			$tabel[1] = "id";
		}
		else if($flags == "Probing")
		{
			$tabel[0] = "tbl_probing";
			$tabel[1] = "id_probing";
		}
		else if($flags == "Lac")
		{
			$tabel[0] = "tbl_lac";
			$tabel[1] = "id";
		}
		else if($flags == "LocationStat")
		{
			$tabel[0] = "tbl_location_stat";
			$tabel[1] = "id_loc_stat";
		}
		else if($flags == "GlobalBlacklistAsli")
		{
			$tabel[0] = "tbl_global_blacklist_asli";
			$tabel[1] = "id";
		}
		else if($flags == "TreeId")
		{
			$tabel[0] = "tbl_tree_id";
			$tabel[1] = "tree_id";
		}
		else if($flags == "Paket")
		{
			$tabel[0] = "tbl_paket";
			$tabel[1] = "id_paket";
		}
		else if($flags == "Customer")
		{
			$tabel[0] = "tbl_customer";
			$tabel[1] = "id_customer";
		}
		else if($flags == "ShortcodeConfiguration")
		{
			$tabel[0] = "tbl_shortcode_configuration";
			$tabel[1] = "id";
		}
		else if($flags == "RuleTemplate")
		{
			$tabel[0] = "tbl_rule_template";
			$tabel[1] = "id";
		}
		else if($flags == "InvoiceLba")
		{
			$tabel[0] = "tbl_invoice_lba";
			$tabel[1] = "id_invoice";
		}
		else if($flags == "UrlInterface")
		{
			$tabel[0] = "tbl_url_interface";
			$tabel[1] = "id";
		}
		else if($flags == "GlobalWhitelist")
		{
			$tabel[0] = "tbl_global_whitelist";
			$tabel[1] = "id";
		}
		else if($flags == "GlobalConfigurationSubs")
		{
			$tabel[0] = "tbl_global_configuration_subs";
			$tabel[1] = "shortcode";
		}
		else if($flags == "SmppIn")
		{
			$tabel[0] = "tbl_smpp_in";
			$tabel[1] = "id";
		}
		else if($flags == "SmppOut")
		{
			$tabel[0] = "tbl_smpp_out";
			$tabel[1] = "id";
		}
		else if($flags == "BtsTest")
		{
			$tabel[0] = "tbl_bts_test";
			$tabel[1] = "id";
		}
		else if($flags == "BtsTest")
		{
			$tabel[0] = "tbl_bts_test";
			$tabel[1] = "id";
		}
		else if($flags == "LacProfile")
		{
			$tabel[0] = "tbl_lac_profile";
			$tabel[1] = "id";
		}
		else if($flags == "LacProfileDetail")
		{
			$tabel[0] = "tbl_lac_profile_detail";
			$tabel[1] = "id";
		}
		return $tabel;
	}
	
	/**
 * genRandomString - Generates random string
 * @author Macinville <http://macinville.blogspot.com>
 * @license MIT License <http://www.opensource.org/licenses/mit-license.php>
 * @param int $length Length of the return string.
 * @param string $chars User-defined set of characters to be used in randoming. If this is set, $type will be ignored.
 * @param array $type Type of the string to be randomed.Can be set by boolean values.
 * <ul>
 * <li><b>alphaSmall</b> - small letters, true by default</li>
 * <li><b>alphaBig</b> - big letters, true by default</li>
 * <li><b>num</b> - numbers, true by default</li>
 * <li><b>othr</b> - non-alphanumeric characters found on regular keyboard, false by default</li>
 * <li><b>duplicate</b> - allow duplicate use of characters, true by default</li>
 * </ul>
 * @return string The generated random string
 */

 
 
 function genRandomString()//$length=5, $chars='', $type=array()) 
 {
 /*   //initialize the characters
    $num = '0123456789';

    $characters = "";
    $string = '';  
    //defaults the array values if not set
    isset($type['num'])         ? $type['num']: $type['num'] = true;                //num - default true
    isset($type['duplicate'])   ? $type['duplicate']: $type['duplicate'] = true;    //duplicate - default true 
    
	$datetime = date("ymdHis");
    
    if (strlen(trim($chars)) == 0) { 
        $type['num'] ? $characters .= $num : $characters = $characters;     
    }
    else
        $characters = str_replace(' ', '', $chars);
      	
    
	if($type['duplicate'])
        for (; $length > 0 && strlen($characters) > 0; $length--) {
            $ctr = mt_rand(0, (strlen($characters)) - 1);
            $string .= $characters[$ctr];
			$random = "_".$datetime."_".$string;

        }
    else
        $string = substr (str_shuffle($characters), 0, $length);

		$random = "_".$datetime."_".$string;

    return $random; */
	
	$validCharacters = "0123456789";
    $validCharNumber = strlen($validCharacters);
    $result = "";
	$datetime = date("ymdHis");
	
    for ($i = 0; $i < 5; $i++) {
        $index = mt_rand(0, $validCharNumber - 1);
        $result .= $validCharacters[$index];
    }
	$random = "_".$datetime."_".$result;
    return $random;
}
				
 function getStatusBatch($id)
{
	if($id == 0 || $id == NULL)
		$status = 'Idle';
		else if($id == 1)
		$status = 'Approved';
		else if($id == 2)
		$status = 'Progres';
else if($id == 6)
			$status = 'Stopping';
		else if($id == 7)
		$status = 'Stoped / Rejected';
		else if($id == 9)
		$status = 'Finish';
		else if($id == 11)
		$status = 'Scheduled';
	return $status;
}				

public static function getStatusPacket($id)
{
   if($id == null )
		$status = 'New';
	else if($id == 0)
		$status = 'Not Approved';
	else if($id == 1)
		$status = 'Approved';
	return $status;
}

	function getStatusCombo()
	{
	return array(0=>'Idle',1=>'Approved',2=>'Progress',6=>'Stopping',7=>'Stopped / Rejected',9=>'Finish',11=>'Scheduled');
	}

function getPrice($id,$price)
	{
		$advertiser = Advertiser::model()->findByPk($id);
		if($advertiser->balance_value > $price)
		return true;
		else
		return false;
	}

	function getTime($id)
	{
		$advertiser = Advertiser::model()->findByPk($id);
		if($advertiser->expired_date != NULL && $advertiser->expired_date != '0000-00-00 00:00:00')
		{
			if(strtotime($advertiser->expired_date) < mktime())
			return false;
			else
			return true;
		}
		else
		return true;
	}

	function getStatusLocation($id)
	{
		$bts = BtsGroup::model()->findByPk($id);
		$location = LocationCategory::model()->findByPk($bts->location_category);
		if($location->status == 0)
		return false;
		else 
		return true;
	}

	function getLog($id)
	{
		$session=new CHttpSession;
		$session->open();
	
		if($session['id'] == $id)
		return false;
		else 
		return true;
	}
	
	function minBalanceValue($last)
	{
	$current = Batch::model()->findByPk($last);
 	$advertiser = Advertiser::model()->findByPk($current->id_advertiser);
	$advertiser->balance_value = $advertiser->balance_value - $current->price;
	$advertiser->update();	
	}
	
	function plusBalanceValue($id)
	{
	$current = Batch::model()->findByPk($id);
	$advertiser = Advertiser::model()->findByPk($current->id_advertiser);
	$advertiser->balance_value = $advertiser->balance_value + $current->price;
	$advertiser->update();
	}
	
	function cekSlot($batch,$slot,$idBatch = "",$cek = true)
	{
	if($slot != ""){
		
		if($cek == true){
			$connection = Yii::app()->db;
			$sql = "select slot_id,start_time from tbl_batch_slot where id_group = '".$batch['group_id']."' and start_time >= '".$batch['execution_date']." 00:00:00' and start_time <= '".$batch['execution_date']." 23:59:59' ";
			$row = $connection->createCommand($sql)->queryAll();
			$x=0;
			$status = 1;
			while($x<count($row)){
			$z= 0;
			while($z < count($slot))
			{
			if($slot[$z] == $row[$x]['slot_id'])
			$status = 0;
			$z++;
			}
			$x++;
			}
			return $status; 
		}
		else{
			$connection = Yii::app()->db;
			$x=0;
			$slotId= "";
			while($x < count($slot))
			{
			$slotId .= $slot[$x].",";
			$x++;
			}
			$slotId = substr($slotId,0,-1);
			$sql = "select id,start_time from tbl_batch_slot_time where id in($slotId)";
			$row = $connection->createCommand($sql)->queryAll();
			$z= 0;
			while($z < count($slot))
			{
			$slotc = new BatchSlot;
			$slotc->trans_date = new CDbExpression('NOW()');
			$slotc->batch_id = $idBatch;
			$slotc->id_group = $batch['group_id'];
			$slotc->start_time = $batch['execution_date']." ".$row[$z]['start_time'];
			$slotc->slot_id = $row[$z]['id'];
			$slotc->status = 0;
			$slotc->save();
			$z++;
			}
			
			
			}
	}
	return false;
	
	}
	
	function cekPrice($batch,$slot)
	{
	$advertiser = Advertiser::model()->findByPk($batch['id_advertiser']);
	if($advertiser != ""){
	$price = $batch['price'] * count($slot);
	if($advertiser->balance_value < $price)
	return false;
	else
	return true;
	}
	return true;
	}
	
	function getSubmit($batch,$slot)
	{
	$price = $batch['price'] * count($slot);
	$x = 0;
	$a = "";
	while($x < count($slot))
	{
	$a .= $slot[$x].',';
	$x++;
	}
	$a = substr($a,0,-1);
	$sql = "select * from tbl_batch_slot_time where id in ($a)";
	$connection = Yii::app()->db;
	$row = $connection->createCommand($sql)->queryAll();
	$z = 0;
	$nameSlot = "";
	while($z < count($row))
	{
	$nameSlot .= $row[$z]['name'].", ";
	$z++;
	}
	$nameSlot = substr($nameSlot,0,-2);
	return "Price :Rp".$price."\n Slot:".$nameSlot;
	}

	
	public function getUnit($nama)
	{
		$connection = Yii::app()->db;
		$sql = "";
		$sql = "select packet_unit from tbl_packet where id = '".$nama."'";
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		//$command = $connection->createCommand($sql)->execute();	
		return $row[0]['packet_unit'];
	}
	
	function getStatus()
	{
		return array(2=>'NEW',1=>'Approved',0=>'Not Approved');
	}

	function getStat()
	{
		return array(2=>'NEW',0=>'Not Approved');
	}
	
	function getGroup($id)
	{
		if($id == 0)
			$stats = 'Detail';
		else if($id == 1)
			$stats = 'Daily';
		else if($id == 2)
			$stats = 'Monthly';
		return $stats;
	}
	
	function getNama($id,$tanda)
	{
		$sql ="";
		$connection = Yii::app()->db;
		$tabel = $this->tablename($tanda);
		if($id != null)
		{
			$sql = "select name from ".$tabel." where id= '".$id."' ";
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			return $row[0]['name'];
		}
		else
		{
			$sql = "select id,name from ".$tabel." where deleted is null or deleted <> 1";
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			$count = count($row);
			$x = 0;
			$nama = "";
			while($x < $count)
			{
				$nama[$x]=$row[$x]['name'];
				$x++;
			}
				return $nama;
		}
	}
	
	function group()
	{
		return array(0=>'Detail',1=>'Daily',2=>'Monthly');
	}
function getScope($id)
		{
			if($id == 0)
				$scope = 'Immediate Cell wide';
				else if($id == 1)
				$scope = 'Normal PLMN wide';
				else if($id == 2)
				$scope = 'Normal Location Area wide in GSM,Service Area wide in UMTS';
				else if($id == 3)
				$scope = 'Normal Cell wide';
		
			return $scope;

		}	

	function getCast($id)
		{
			if($id == 0)
				$scope = 'National Wide';
				else if($id == 1)
				$scope = 'Specific Area';
			return $scope;

		}
	function getMsg($id)
		{
			if($id == 0)
				$scope = 'POP Up';
				else if($id == 1)
				$scope = 'Save to Inbox';
			return $scope;

		}
function getSms($sms)
		{
			 return nl2br($sms);
		}			
						


	function getTotalPrice($data)
	{
		$data = explode(",",$data);
		$connection = Yii::app()->db;
		if($data[2] != null and $data[3] != null and $data[4] != null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."' and id_advertiser = '".$data[3]."' and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] == null and $data[4] == null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') "; 
		}
		else if($data[2] == null and $data[3] == null and $data[4] != null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] != null and $data[4] != null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."' and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] != null and $data[4] == null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."'";
		}
		else if($data[2] != null and $data[3] == null and $data[4] == null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."' ";
		}
		else if($data[2] != null and $data[3] != null and $data[4] == null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."' and id_media_seller = '".$data[2]."' ";
		}
		else if($data[2] != null and $data[3] == null and $data[4] != null)
		{
			$sql = "select sum(price) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."' and id_product_category = '".$data[4]."' ";
		}
		
		$row = $connection->createCommand($sql)->queryAll();		
		return $row[0]['a'];
	}
	
	function getTotalBatch($data)
	{
		$data = explode(",",$data);
		$connection = Yii::app()->db;
		if($data[2] != null and $data[3] != null and $data[4] != null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."' and id_advertiser = '".$data[3]."' and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] == null and $data[4] == null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') "; 
		}
		else if($data[2] == null and $data[3] == null and $data[4] != null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] != null and $data[4] != null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."' and id_product_category = '".$data[4]."' ";
		}
		else if($data[2] == null and $data[3] != null and $data[4] == null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."'";
		}
		else if($data[2] != null and $data[3] == null and $data[4] == null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."'";
		}
		else if($data[2] != null and $data[3] != null and $data[4] == null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_advertiser = '".$data[3]."' and id_media_seller = '".$data[2]."' ";
		}
		else if($data[2] != null and $data[3] == null and $data[4] != null)
		{
			$sql = "select count(*) as a from tbl_batch where (deleted is null or deleted <> 1) and (execution_date >= '".$data[0]."' and execution_date <= '".$data[1]."') and id_media_seller = '".$data[2]."' and id_product_category = '".$data[4]."' ";
		}
		
		$row = $connection->createCommand($sql)->queryAll();		
		
			return $row[0]['a'];
		
	}
	
	function csv($data)
	{
		Yii::import('ext.CSVExport');
	
		$data=explode(",",$data);
		$to = $data[0];
		$from = $data[1];
		$media = $data[2];
		$adv = $data[3];
		$prod = $data[4];
		$group = $data[5];
		$title = $data[6];
		
		$tgl = $to."a".$from;
		$provider = null;
		$csv = null;
		$content = null;
		$data=$to.",".$from.",".$media.",".$adv.",".$prod;
		
		$batch = $this->getTotalBatch($data);
		$price = $this->getTotalPrice($data);
				
	if($group == 0)
	{
		if($media == null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id")->queryAll();
		}
		else if($media != null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_media_seller = '".$media."' and b.id_advertiser = '".$adv."' and b.id_product_category = '".$prod."'")->queryAll();
		}
		else if($media != null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_media_seller = '".$media."' and b.id_advertiser = '".$adv."'")->queryAll();
		}
		else if($media != null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_media_seller = '".$media."' and b.id_product_category = '".$prod."'")->queryAll();
		}
		else if($media == null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_advertiser = '".$adv."' and b.id_product_category = '".$prod."'")->queryAll();
		}
		else if($media == null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_product_category = '".$prod."'")->queryAll();
		}
		else if($media != null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_media_seller= '".$media."'")->queryAll();
		}
			else if($media == null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select b.execution_date,b.topik,m.name as id_media_seller,p.name as id_product_category,a.name as id_advertiser,bt.name as id_group,b.price from tbl_batch b,tbl_media_seller m,tbl_product_category p, tbl_advertiser a, tbl_bts_group bt where (execution_date >= '".$to."' and execution_date <= '".$from."') and (b.deleted is null or b.deleted <> 1) and a.id = b.id_advertiser and p.id = b.id_product_category and m.id = b.id_media_seller and bt.id = b.group_id and b.id_advertiser = '".$adv."'")->queryAll();
		}
	}
	else if($group == 1)
	{
		if($media == null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) group by execution_date order by execution_date ")->queryAll();
		}
		else if($media != null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_advertiser = '".$adv."' and id_product_category = '".$prod."' group by execution_date order by execution_date")->queryAll();
		}
		else if($media != null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_advertiser = '".$adv."' group by execution_date order by execution_date")->queryAll();
		}
		else if($media != null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_product_category = '".$prod."' group by execution_date order by execution_date")->queryAll();
		}
		else if($media == null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_advertiser = '".$adv."' and id_product_category = '".$prod."' group by execution_date order by execution_date")->queryAll();
		}
		else if($media == null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_product_category = '".$prod."' group by execution_date order by execution_date")->queryAll();
		}
		else if($media != null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' group by execution_date order by execution_date")->queryAll();
		}
			else if($media == null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select execution_date,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_advertiser = '".$adv."' group by execution_date order by execution_date")->queryAll();
		}
	}
	else if($group == 2)
	{
		if($media == null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media != null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_advertiser = '".$adv."' and id_product_category = '".$prod."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media != null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_advertiser = '".$adv."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media != null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' and id_product_category = '".$prod."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media == null and $adv != null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_advertiser = '".$adv."' and id_product_category = '".$prod."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media == null and $adv == null and $prod != null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_product_category = '".$prod."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
		else if($media != null and $adv == null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_media_seller = '".$media."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
			else if($media == null and $adv != null and $prod == null)
		{
			$provider = Yii::app()->db->createCommand("select month(execution_date) as bulan,year(execution_date) as tahun,count(id) as batch,sum(price) as price from tbl_batch where (execution_date >= '".$to."' and execution_date <= '".$from."') and (deleted is null or deleted <> 1) and id_advertiser = '".$adv."' group by tahun,bulan order by tahun,bulan")->queryAll();
		}
	}
	
	if($provider == null)
		{
			Yii::app()->user->setFlash('error', 'No Data to Export');
			$this->redirect(array('admin','date'=>$tgl,'media'=>$media,'adv'=>$adv,'prod'=>$prod,'group'=>$group));
		}
	else
		{
			$csv = new CSVExport($provider);
			if($group == 0)
			{
				$csv->headers = array('execution_date'=>'Date','topik'=>'Topik','id_media_seller'=>'Media Seller','id_advertiser'=>'Advertiser','id_product_category'=>'Product Category','id_group'=>'Group','price'=>'Price'); 
			}
			else if($group == 1)
			{
				$csv->headers = array('execution_date'=>'Date','batch'=>'Jumlah Batch','price'=>'Price'); 
			}
			else if($group == 2)
			{
				$csv->headers = array('execution_date'=>'Date','batch'=>'Jumlah Batch','price'=>'Price'); 
			}
			$csv->exportFull = true; 
			$content = $csv->toCSV();	
			$filename = $title.'.csv';
			$content .="\n\nTotal Batch,$batch\nTotal Price,$price\n";
			Yii::app()->getRequest()->sendFile(basename($filename), $content, "text/csv",false);
			exit();
		}
	}
function insertSlot($tbl,$idHeader,$idDetail,$model,$slot,$id,$status = false)//insertSlot($tbl,$idHeader,idDetail,$model)
	{	
		if($status){
		$connection = Yii::app()->db;
		$sql = "delete from $tbl where $idHeader = $id";
		$row = $connection->createCommand($sql)->execute();
		}
		$z= 0;
		while($z < count($slot))
		{
		$slotc = new $model;
		$slotc->$idHeader = $id;
		$slotc->$idDetail = $slot[$z];
		$slotc->first_user = Yii::app()->user->first_name;
		$slotc->first_ip = CHttpRequest::getUserHostAddress();
		$slotc->first_update = date('Y-m-d h:i:s');
		$slotc->last_user = Yii::app()->user->first_name;
		$slotc->last_ip = CHttpRequest::getUserHostAddress();
		$slotc->last_update =  date('Y-m-d h:i:s');
		$slotc->save();
		$z++;
		}
	}
function selectedItem($tbl,$select,$tbldetail,$idHeader,$id){
	$connection = Yii::app()->db;
	if($tbl =="tbl_customer")
		$idTbl = "id_customer";
	else
		$idTbl = "id";
	$sql = "select * from $tbl where $idTbl in(select $select from $tbldetail where $idHeader = $id)";
	$row = $connection->createCommand($sql)->queryAll();
	$x = 0;
	$pp = "";
	while($x < count($row))
	{
			 $pp[$row[$x][$idTbl]] = array('selected'=>'selected');
			 $x++;
	}
	return $pp;
	
	}
	
	function getChannel($id)
		{
			$product = ProductCategory::model()->findByPk($id);
			return $product->channel_use;

		}

function createMenu($param)
	{
	/*
	cara pake:
	Controller::createMenu(array(
	array('label'=>'xxx','link'=>'xxx/xxx'),
	array('label'=>'xxx','link'=>'xxx/xxx')
	));
	*/
	if(count($param) == 1)
	$width = 75;
	else if(count($param) == 2)
	$width = 150;
	else
	$width = 210;
	$menuO = "<table width=$width border='0' cellpadding='0' cellspacing='0'>
	<tr>";
	$x=0;
	$menu = "";
	while($x < count($param)){
	$label = $param[$x]['label'];
	$link = $param[$x]['link'];

	if($param[$x]['id']==""){
	$final=CHtml::link("$label",array("$link"));
	}
	else
	$final=CHtml::link("$label",array("$link",'id'=>$param[$x]['id']));

	if($param[$x]['conf'] != "")
	$final=CHtml::link("$label","#", array("submit"=>array("delete", 'id'=>$param[$x]['id']), 'confirm' => "Are you sure want to delete '".$param[$x]['conf']."' ?"));
	if($param[$x]['img'] == ""){
	$menu .="
	<td width='10'>$final</td>
	";
	}
	else{
	$menu .="
	<td width='10' align ='right'>&nbsp;&nbsp;<img src='".Yii::app()->request->baseUrl.$param[$x]['img']."'/></td><td width='10' valign='bottom'>$final</td>
	";
	}
	$x++;
	}
	$menu .="</tr></table>";
	echo $menuO.$menu;
	}

	function createInfo($model)
	{
	echo "<div class = 'info'>";
	echo "Create :".$model->first_user."(".$model->first_ip.")/ ".$model->first_update;echo "<br>";
	echo "Update :".$model->last_user."(".$model->last_ip.")/ ".$model->last_update;
	echo "</div>";
	echo "<br />";
	echo "<br />";
	}
function cekMenu($param)
	{
		
		$session=new CHttpSession;
		$session->open();
		$connection=Yii::app()->db;
		$sqlCel="select * from AuthAssignment where userid = (select id from tbl_users where id = '".$session['id']."') and itemname = 'Admin'";
		$commandCel=$connection->createCommand($sqlCel)->queryAll();
		if($commandCel == NULL){
			$panjang = strlen($param)+1;
			$param = $param.".";
			$sql = "select child from AuthItemChild where child in(select name from AuthItem where (type = 0 or type = 1) )
					and parent in(select itemname from AuthAssignment where userid = '".$session['id']."')
					and substring(child,1,'".$panjang."') ='".$param."'";
			$command=$connection->createCommand($sql)->queryAll();
			if($command == NULL)
			return false;
			else 
			return true;
		}
		else
		return true;
	}
function cekRole()
	{
	$session=new CHttpSession;
		$session->open();
	$connection=Yii::app()->db;
		$sqlCel="select itemname from AuthAssignment where userid =  '".$session['id']."' and itemname = 'superUser'";
		$commandCel=$connection->createCommand($sqlCel)->queryAll();
		if($commandCel == NULL)
		return false;
		else 
		return true;
		
	
	}
function dropDownJS($id,$array = false)
	{
	$data =CHtml::listData(ProductCategory::model()->findAll('deleted IS NULL or deleted <> :deleted',array(':deleted'=>1)), 'id', 'channel_use');
	if($array == false){
	$name = explode("_",$id);
	$return = "";
	$return = "<select id='$id' name='$name[0][$name[1]][$name[2]_$name[3]]'>";
	foreach($data as $key=>$value)
		{
		$return .= "<option value='$value'>$value($key)</option>";
		}
	$return .="</select>";
	}
	else{
	$return = "";
	foreach($data as $key=>$value)
		{
		$return[$value] ="$value($key)";
		}
	}
	return $return;
	}	

	function getConstanta($idx=null,$val=null){
		$array = array(
					'tipe_customer'   => array(0 => 'Prepaid', 1=> 'Postpaid'),
					'bypass_approval' => array(0 => 'No Bypass', 1 => 'Yes Bypass'),
					'blocked'         => array(0 => 'Not Blocked', 1 => 'Blocked'),
					'approved'        => array(-1 => 'Not Set',0 => 'New', 1 => 'Approved', 2 => 'Rejected'),
					'dataType'        => array(1 => 'Daily', 0 => 'Hourly'),
					'mediaType'       => array('All'=>'All',0=>"SMS",5=>"MMS",6=>"STATISTIC",7=>"Url Interface"),
					'media_type'       => array(0 => 'New SMS', 5 => 'New MMS',6=>"Statistic",7=>"Url Interface"),
					'grouping'        => array(0 => 'Time', 1 => 'Error Code' , 2 =>'Masking Customer'),
					'dataView'        => array(0 => 'Table', 1 => 'Graphic'),
					'dataHour'        => array("00"=>"00","01" =>"01","02" =>"02","03" =>"03","04" =>"04","05" =>"05","06" =>"06","07"=>"07","08" =>"08","09" =>"09","10" =>"10","11" =>"11","12" =>"12","13" =>"13","14" =>"14","15" =>"15","16" =>"16","17" =>"17","18" =>"18","19" =>"19","20" =>"20","21" =>"21","22" =>"22","23" =>"23"),
					'profile'         => array(1 => 'penduduk', 2 => 'pekerja', 3 => 'last_week', 4 => 'pernah_4_minggu', 5 => 'pernah_3_bulan',6 => 'mingguan', 7 => 'bulanan', 8=> 'full day settler', 9 => "last_day_visitor", 10 => "Sering",12 =>"Last 2 Week")
				);
		if(($idx!="")&&($val!="")){
			$x = $array[$idx][$val];
			$array = $x;
		}
		else if($idx!=""){
			$array = $array[$idx];
		}
		return $array;
	}
	
	function cekExist($id,$field,$model){
			$mod = call_user_func(array($model,'model'));
			if($model == "ShortcodeCampaigne")
				$findall = "$field = $id and (flag_deleted <> 1 or flag_deleted is null) and id_batch in (select id_batch from tbl_lba_batch where status_batch not in(7,9))";
			else if($model == "LacProfileDetail")
				$findall = "$field = $id ";
			else
				$findall = "$field = $id and (flag_deleted <> 1 or flag_deleted is null)";
			//echo $findall;
			$modelCek = $mod->findAll($findall);
			if(count($modelCek) == 0)
				$return = true;
			else
				$return = false;
		return $return;
	}
	
	//fungsi untuk profile downloder START
	
	function getLastWeek($prefix=NULL){
	//=============================
	// LAST WEEK START
	//+++++++++++++++++++++++++++++
	$yearweak = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - 8 , date('Y'))));
	if($yearweak == "53"){
		$lastWeek = "53";
	}
	else{
		$g = ($yearweak) % 13;
		$lastWeek = $g+1;
		if($lastWeek > 13)
			$lastWeek -= 13;
	}
	return $prefix.$lastWeek;
	//=============================
	// LAST WEEK FINISH
	//+++++++++++++++++++++++++++++	
	}

	function getFourWeeks($prefix=NULL){
		//=============================
		// PERNAH 4 MINGGU START
		//+++++++++++++++++++++++++++++
		$counter = 0;
		while($counter < 4){
			$yearweak = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - (($counter + 1) * 7) , date('Y'))));
			if($yearweak == "53"){
				$fourWeeks[$counter] = $prefix."53";
			}
			else{
				$g = ($yearweak) % 13;
				$fourWeeks[$counter] = $g+1;
				if($fourWeeks[$counter] > 13)
					$fourWeeks[$counter] -= 13;
				$fourWeeks[$counter] = $prefix.$fourWeeks[$counter];
			}
			$counter++;
		}
		return $fourWeeks;
		//=============================
		// PERNAH 4 MINGGU FINISH
		//+++++++++++++++++++++++++++++
	}

	function getThreeMonths($prefix=NULL){
		//=============================
		// PERNAH 3 BULAN START
		//+++++++++++++++++++++++++++++
		$counter = 0;
		while($counter < 12){
			$yearweak = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - (($counter + 1) * 7) , date('Y'))));
			if($yearweak == "53"){
				$threeMonths[$counter] = $prefix."53";
			}
			else{
				$g = ($yearweak) % 13;
				$threeMonths[$counter] = $g+1;
				if($threeMonths[$counter] > 13)
					$threeMonths[$counter] -= 13;
			}
			$threeMonths[$counter] = $prefix.$threeMonths[$counter];
			$counter++;
		}
		return $threeMonths;
		//=============================
		// PERNAH 3 BULAN FINISH
		//+++++++++++++++++++++++++++++
	}

	function getWeekly($prefix=NULL){
		//=============================
		// MINGGUAN START
		//+++++++++++++++++++++++++++++
		$counter = 0;
		while($counter < 4){
			$yearweak = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - (7 * ($counter + 1)) , date('Y'))));
			if($yearweak == "53"){
				$weekly[$counter] = $prefix."53";
			}
			else{
				$g = ($yearweak) % 13;
				$weekly[$counter] = $g+1;
				if($weekly[$counter] > 13)
					$weekly[$counter] -= 13;
			}
			$weekly[$counter] = $prefix.$weekly[$counter];
			$counter++;
		}
		return $weekly;
		//=============================
		// MINGGUAN FINISH
		//+++++++++++++++++++++++++++++
	}

	function getMonthly($prefix=NULL){
		//=============================
		// BULANAN START
		//+++++++++++++++++++++++++++++
		$counter = 0;
		while($counter < 4){
			$monthyear = intval(date('m', mktime(0,0,0,intval(date('m')) - ($counter + 1), date('d') , date('Y'))));
			$g = $monthyear % 6;
			$g += 1;
			if($g < 1)
				$g += 6;
			$monthly[$counter] = $g;
			if($monthly[$counter]< 1)
				$monthly[$counter] += 6; 
			$monthly[$counter] = $prefix.$monthly[$counter];
			$counter++;
		}
		return $monthly;
		//=============================
		// BULANAN FINISH
		//+++++++++++++++++++++++++++++
	}

	function getOften($prefix=NULL){
		//=============================
		// SERING START
		//+++++++++++++++++++++++++++++
		$counter = 0;
		while($counter < 4){
			$yearweak = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - (7 * ($counter + 1)) , date('Y'))));
			if($yearweak == "53"){
				$often[$counter] = "53";
			}
			else{
				$g = ($yearweak) % 13;
				$often[$counter] = $g+1;
				if($often[$counter] > 13)
					$often[$counter] -= 13;
			}
			$often[$counter] = $prefix.$often[$counter];
			$counter++;
		}
		return $often;
		//=============================
		// SERING END
		//+++++++++++++++++++++++++++++
	}

	function getWeek(){
		$yearweak_1 = intval(date('W', mktime(0,0,0,date('m'), intval(date('d')) - 7 , date('Y'))));
		$yearweak_1 = ($yearweak_1) % 4;
		$yearweak_1 = $yearweak_1 + 1;
		if($yearweak_1 < 1)
			$yearweak_1 += 4;
		return $yearweak_1;
	}

	function getWH(){
		$week = $this->getWeek();
		return "minggu".$week."_wh";
	}

	function getNWH(){
		$week = $this->getWeek();
		return "minggu".$week."_nwh";
	}
	
	function profileToQuery($profile){
	//print $profile;
		switch($profile){
			case "2": //pekerja
				//print "aaaaaaaaaaaaaaaaaaaaaa";
				$query = " AND ".$this->getWH()." >= 4 AND ".$this->getNWH()." < 4";
			break;
			case "-2": //bukan pekerja
				$query = " AND ".$this->getWH()." < 4";
			break;
			case "1": //penduduk
				$query = " AND ".$this->getNWH()." >= 4";
			break;
			case "-1": //bukan penduduk
				$query = " AND ".$this->getNWH()." < 4";
			break;
			case "3": //last_week
				$query = " AND ".$this->getLastWeek("minggu")." > 0";
			break;
			case "-3": //bukan last_week
				$query = " AND ".$this->getLastWeek("minggu")." = 0";
			break;
			case "4": //pernah 4 minggu
				$week = $this->getFourWeeks("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." > 0) OR ";
				}
				$query = substr($query, 0, strrpos($query, "OR"));
				$query .= ")";
			break;
			case "-4": //bukan pernah 4 minggu
				$week = $this->getFourWeeks("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." = 0) AND ";
				}
				$query = substr($query, 0, strrpos($query, "AND"));
				$query .= ")";
			break;
			case "5": //pernah 3 bulan
				$week = $this->getThreeMonths("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." > 0) OR ";
				}
				$query = substr($query, 0, strrpos($query, "OR"));
				$query .= ")";
			break;
			case "-5": //bukan pernah 3 bulan
				$week = $this->getThreeMonths("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." = 0) AND ";
				}
				$query = substr($query, 0, strrpos($query, "AND"));
				$query .= ")";
			break;
			case "6": //mingguan
				$week = $this->getWeekly("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." > 0) AND ";
				}
				$query = substr($query, 0, strrpos($query, "AND"));
				$query .= ")";
			break;
			case "-6": //bukan mingguan
				$week = $this->getWeekly("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." = 0) OR ";
				}
				$query = substr($query, 0, strrpos($query, "OR"));
				$query .= ")";
			break;
			case "7": //bulanan
				$week = $this->getMonthly("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." > 0) AND ";
				}
				$query = substr($query, 0, strrpos($query, "AND"));
				$query .= ")";
			break;
			case "-7": //bukan bulanan
				$week = $this->getMonthly("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." = 0) OR ";
				}
				$query = substr($query, 0, strrpos($query, "OR"));
				$query .= ")";
			break;
			case "8": //full_day_settler
				$query = " AND ((".$this->getWH()." >= 4) AND (".$this->getNWH()." >= 4))";
			break;
			case "-8": //bukan full_day_settler
				$query = " AND ((".$this->getWH()." < 4) OR (".$this->getNWH()." < 4))";
			break;
			case "9": //last_daye
				$query = " AND last_trans_datetime >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),intval(date('d')) -1,date('Y')))."'";
			break;
			case "-9": //bukan last_daye
				$query = " AND last_trans_datetime < '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),intval(date('d')) -1,date('Y')))."'";
			break;
			case "10": //sering
				$week = $this->getOften("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= "(".$val." > 1) AND";
				}
				$query = substr($query, 0, strrpos($query, "AND"));
				$query .= ")";
			break;
			case "-10": //bukan sering
				$week = $this->getOften("minggu");
				$query = " AND (";
				foreach($week as $idx => $val){
					$query .= $val." < 2 OR";
				}
				$query = substr($query, 0, strrpos($query, "OR"));
				$query .= ")";
			break;
		}
	return $query;
	}
	//fungsi untuk profile downloder END
	
	function autoScaleY($miny=0,$maxy=100,$step=10,$d=0){
	   $lower_bound = $miny;
	   $upper_bound = $maxy;
	   $range = $maxy - $miny;
	   $tick_range = $range / $step;
	   $i=0;
	   $n=1;
	   while($n>=1)
	   {
	      $n = $tick_range/pow(10,$i);
	      $i++;
	   }
	   $i--;
	   switch ($n){
	      case (0.1):
	         $n = 0.1;
	         break;
	      case ($n<0.2):
	         $n = 0.2;
	         break;   
	      case ($n<0.25):
	         $n = 0.25;
	         break;   
	      case ($n<0.3):
	         $n = 0.3;
	         break;   
	      case ($n<0.4):
	         $n = 0.4;
	         break;   
	      case ($n<0.5):
	         $n = 0.5;
	         break;   
	      case ($n<0.6):
	         $n = 0.6;
	         break;   
	      case ($n<0.7):
	         $n = 0.7;
	         break;   
	      case ($n<0.75):
	         $n = 0.75;
	         break;   
	      case ($n<0.8):
	         $n = 0.8;
	         break;   
	      case ($n<0.9):
	         $n = 0.9;
	         break;   
	      case ($n<1):
	         $n = 1;
	         break;
	      default:
	         print "Somethings is wrong !!! I don`t know what \n";
	         $d=1;
	         break;
	   }
	   
	   $nice_tick = $n * pow(10,$i);
	   $lower_bound = $nice_tick * floor($miny/$nice_tick);
	   $upper_bound = $nice_tick * round(1+$maxy/$nice_tick);
	   
	   if($d==1){
	      print "range = $range \n";
	      print "tick_range = $tick_range \n";
	      print "n = $n \n";
	      print "nice_tick = $nice_tick \n";
	      print "lower_bound = $lower_bound \n";
	      print "upper_bound = $upper_bound \n";
	   }
	   $scale["minY"] = $lower_bound;
	   $scale["maxY"] = $upper_bound;
	   
	   return $scale;
	}
	
	function getListTanggal($dateFrom,$dateTo,$type = null)
	{
		$dateList = array();
		$datefrom2 = $dateFrom;
		$dateto2 = $dateTo;
		while($datefrom2 <= $dateto2){
			//echo "<BR>".$datefrom2;
			$dateList[] = $datefrom2;
			if($type == 1){
				$dodo = explode (" ",$datefrom2);
				$date1 = explode("-",$dodo[0]);
				$date2 = explode(":",$dodo[1]);
			}
			else
				$date1 = explode("-",$datefrom2);
				
			if($type == 1){
				$datefrom2 = date("Y-m-d H:00", mktime(intval($date2[0])+1, 0, 0, $date1[1], $date1[2], $date1[0]));
			}
			else
				$datefrom2 = date("Y-m-d", mktime(0, 0, 0, $date1[1], intval($date1[2])+1, $date1[0]));
				
		}
		//print_r($dateList);
		//print "<pre>";print_r($dateList);
		return $dateList;
	}
	
	function getLacIc($id,$return= 0 ){
		$connection=Yii::app()->db;
		$icName  ="";
		$idBts = "";
		$idBts;
		$date="";
		$ic = "";
		$sql  = "SELECT * from tbl_btsgroup_detail where id_group = '".$id."' ";
		$RBtsGroupDetail = $connection->createCommand($sql)->query(); 

			foreach($RBtsGroupDetail as $rw){
				$ic .=$rw['id_bts'].",";
				if(strtotime($date) < strtotime($rw['first_update'])){
					$idBts = $rw['id_bts'];
					$date = $rw['first_update'];
				}
				
				if(strtotime($date) < strtotime($rw['last_update'])){
					$idBts = $rw['id_bts'];
					$date = $rw['last_update'];
				}
			}
		
		//echo "<br>Query Btsgroup: ".$query." ";
		$dateLac="";
		$lac ="";
		
		$sqlBts  = "SELECT * from tbl_bts where id = '".$idBts."' ";
		$RBts = $connection->createCommand($sqlBts)->query(); 
		
			foreach($RBts as $rwBts){
				//$ic .=$rwBts['id_bts'].",";
				if(strtotime($dateLac) < strtotime($rwBts['first_update'])){
					$lac = $rwBts['lac'];
					$dateLac = $rwBts['first_update'];
				}
				
				if(strtotime($dateLac) < strtotime($rwBts['last_update'])){
					$lac = $rwBts['lac'];
					$dateLac = $rwBts['last_update'];
				}
			}
		
		//echo "<BR>Query Bts: ".$queryBts." ";
		
		$dateIc="";
		if($ic != "")
			$icIn = substr($ic,0,-1);
		else
			$icIn = 0;
		$sqlIc  = "SELECT * from tbl_bts where id in( ".$icIn.") ";
		$RIc = $connection->createCommand($sqlIc)->query(); 
		
			foreach($RIc as $rwIc){
				if(strtotime($dateIc) < strtotime($rwIc['first_update'])){
					$ic = $rwIc['bts_id'];
					$icName = $rwIc['bts_name'];
					$dateIc = $rwIc['first_update'];
				}
				if(strtotime($dateIc) < strtotime($rwIc['last_update'])){
					$ic = $rwIc['bts_id'];
					$icName = $rwIc['bts_name'];
					$dateIc = $rwIc['last_update'];
				}
			}
		
		if($ic == "")
			$ic = "EMPTY";
		else
			$ic = $icName."/".$ic."(".date("Y-m-d",strtotime($dateIc)).")";
		//echo "<BR>Query Ic: ".$queryIc." ";
		//echo "IC :".$ic."<BR>";
		//echo "</BR>LAC".$lac."</BR>";
		if($lac == ""){
		$lac =0;
		}
		$lacId = "";
		$lacName = "";
		$dateLac = "";
		$sqlLac  = "SELECT * from tbl_lac where lac_id = ".$lac." ";
		$RLac = $connection->createCommand($sqlLac)->query(); 
		
			foreach($RLac as $rwLac){
				if(strtotime($dateLac) < strtotime($rwLac['first_update'])){
					$lacId = $rwLac['lac_id'];
					$lacName = $rwLac['description'];
					$dateLac = $rwLac['first_update'];
				}
				if(strtotime($dateLac) < strtotime($rwLac['last_update'])){
					$lacId = $rwLac['lac_id'];
					$lacName = $rwLac['description'];
					$dateLac = $rwLac['last_update'];
				}
			}
		
		if($lacId == "")
			$lac = "EMPTY";
		else
			$lac = $lacName."/".$lacId."(".date("Y-m-d",strtotime($dateLac)).")";
		return $lac."#".$ic;
	}
	
	/** andesta **/
	
	function getCampaigneTypeCombo(){
		return array(1=>"BLACK LIST FILE",2=>"WHITE LIST FILE",3=>"BLACK LIST PROFILE",4=>"WHITE LIST PROFILE");
	}

	
 function getCampaigneType($id){
	switch(intval($id)){
		case 0:
			return "NORMAL";
		break;
		case 1:
			return "BLACK LIST FILE";
		break;
		case 2:
			return "WHITE LIST FILE";
		break;
		case 3:
			return "BLACK LIST PROFILE";
		break;
		case 4:
			return "WHITE LIST PROFILE";
		break;
		DEFAULT:
			return "UNDEFINE";
		break;
	}
 }
 
 function getAdvertisingSendingTypeComboCampaigne(){
	/** return array(0=>"ON ENTRY",1=>"ON LOCATION",2=>"INSTANT PROFILE",3=>"INSTANT FILE");**/
	return array(-1=>"--Please Select One--",0=>"ON ENTRY",1=>"ON LOCATION");
 }
 
 function getAdvertisingSendingTypeCampaigne($id){
	switch(intval($id)){
		case 0:
			return "ON ENTRY";
		break;
		case 1:
			return "ON LOCATION";
		break;
		/*
		case 2:
			return "INSTANT PROFILE";
		break;
		case 3:
			return "INSTANT FILE";
		break;
		*/
		default:
			return "UNKNOWN";
		break;
	}
 }

 function getPriorityComboCampaigne(){
	return array(0=>"LOW",1=>"MID",2=>"HIGH");
 }
 
 function getPriorityCampaigne($id){
	switch(intval($id)){
		CASE 0:
			return "LOW";
		break;
		CASE 1:
			return "MID";
		break;
		CASE 2:
			return "HIGH";
		break;
		DEFAULT:
			return "UNDEFINED";
		break;
	}
 }
 
 function getChannelCategoryComboCampaigne($idx = ""){
	if($idx == "")
		return array(""=>"--Please Select One--",0=>"SMS",5=>"MMS",6=>"STATISTIC",7=>"URL INTERFACE",8=>"USSD",9=>"WAP",10=>"USSD MENU",11=>"SMS FLASH");
	else
		return array(""=>"--Please Select One--",0=>"SMS",5=>"MMS",6=>"STATISTIC",7=>"URL INTERFACE",8=>"USSD",9=>"WAP",10=>"USSD MENU",11=>"SMS FLASH");
 }
 
 function getChannelCategoryCampaigne($id){
	switch(intval($id)){
		CASE 0:
			return "SMS";
		break;
		CASE 5:
			return "MMS";
		break;
		CASE 6:
			return "STATISTIC";
		break;
		CASE 7:
			return "URL INTERFACE";
		break;
		CASE 8:
			return "USSD";
		break;
		
		CASE 9:
			return "WAP";
		break;
		
		CASE 10:
			return "USSD MENU";
		break;
		
		CASE 11:
			return "SMS FLASH";
		break;
		
		default:
				return "UNDEFINED";
			break;
	}
 }

 function getStatusComboCampaigne()
 {
	return array(0=>'QUEUE',1=>'APPROVED',2=>'PROGRESS',3=>'RESUMING',4=>'PAUSING',5=>'PAUSED',6=>'STOPPING',7=>'STOPPED',8=>'REJECTED',9=>'FINISH',11=>'SCHEDULED');
 } 
 
 function getStatusCampaigne($id)
{

	switch(intval($id)){
		case 0:
			return "QUEUE";
		break;
		case 1:
			return "APPROVED";
		break;
		case 2:
			return "PROGRESS";
		break;
		case 3:
			return "RESUMING";
		break;
		case 4:
			return "PAUSING";
		break;
		case 5:
			return "PAUSED";
		break;
		case 6:
			return "STOPPING";
		break;
		case 7:
			return "STOPPED";
		break;
		case 8:
			return "RETRY";
		break;
		case 9:
			return "FINISH";
		break;
		case 11:
			return "SCHEDULED";
		break;
		DEFAULT :
			return "UNDEFINED STATUS";
		break;
	}
}	

	function userMode(){
		$session=new CHttpSession;
		$session->open();
		
		$modelUser = Users::model()->findByPk($session['id']);
		if($modelUser->usermode == 0 || $modelUser->usermode == 1)
		{
			$return = false;
		}
		else
		{
			$return = true;
		}
		
		return $return;
		
	}
	

	function getIdCustomerByIdUserAndesta(){
		// cek kalau id usernya adalah admin berarti dia bisa semua
		if(Controller::userMode()){//Yii::app()->user->isAdmin()
			// kalau dia adalah operator / media seller atau     customer atau operator cek by usernya dia
			$sql = "select id_customer from tbl_customer where id_customer in ( select id_customer from tbl_users where id = ".Yii::app()->user->id." ) ";
			
		}
		else{
			$sql = "select id_customer from tbl_customer";
		}
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count = count($row);
		$x = 0;
		$listIdCustomer = "";
			while($x < $count)
			{
					$listIdCustomer .= $row[$x]['id_customer'].",";
					$x++;
			}
		if($listIdCustomer != "")
			return rtrim($listIdCustomer,",");	
		else
			return -1;
	}	
	
	 function generateCampaigneId()
 {
 	
	// generate  max id batch from tbl_lba_batch
	$sql 		= "select max(id_batch) as maxs from tbl_lba_batch";
	$connection = Yii::app()->db;
	$dataReader = $connection->createCommand($sql);
	$row = $dataReader->queryAll();
	$count = count($row);
	if($row[0]['maxs'] == ""){
		$maxIdBatch = 1;
		$batchNo 	= 1;
	}	
	else{
		$maxIdBatch = $row[0]['maxs'];
		$sql="SELECT batch_no,left(right(jobs_id,23),2) as bln FROM tbl_lba_batch WHERE id_batch=".$maxIdBatch;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
				 if(	$row[0]['bln'] != date('m'))
		         {
		             $batchNo=1;
		         } 
		         else
		         {
		             $batchNo=intval($row[0]['batch_no']) + 1;
		         }  
	}
	
	$formatBatchNo = sprintf('%03d',$batchNo);
	$campaigneId = "_".$formatBatchNo."_".date('mYdhis')."_".date("Ymd");
	return $campaigneId;
}

	function getProfileComboCampaigne(){
		//return array("1"=>"penduduk","-1"=>"bukan penduduk","2"=>"pegawai","-2"=>"bukan pegawai","3"=>"last week","3"=>"bukan last week","4"=>"pernah_4_minggu","-4"=>"bukan_pernah_4_minggu","5"=>"pernah_3_bulan","-5"=>"bukan_pernah_3_bulan","6"=>"mingguan","-6"=>"bukan mingguan","7"=>"bulanan","-7"=>"bukan bulanan","8"=>"full day settlet","-8"=>"bukan full day settlet","9"=>"last day average","-9"=>"bukan last day average","10"=>"sering","-10"=>"bukan sering");
		return array("1"=>"penduduk","2"=>"pegawai","3"=>"last week","4"=>"pernah_4_minggu","5"=>"pernah_3_bulan","6"=>"mingguan","7"=>"bulanan","8"=>"full day settler","9"=>"last day visitor","10"=>"sering");
	}
	
	function getProfileCampaigne($profile){
		switch(intval($profile)){
			case 0:
				return "normal";
			break;
			case 1:
				return "penduduk";
			break;
			case -1:
				return "bukan penduduk";
			break;
			case 2:
				return "pegawai";
			break;
			case -2:
				return	"bukan pegawai";
			break;
			case 3:
				return "Last week";
			break;
			case -3:
				return "bukan last week";
			break;
			case 4:
				return "pernah_4_minggu";
			break;
			case -4:
				return "bukan pernah_4_minggu";
			break;
			case 5:
				return "pernah_3_bulan";
			break;
			case -5:
				return "bukan_pernah_3_bulan";
			break;
			case 6:
				return "mingguan";
			break;
			case -6:
				return "bukan mingguan";
			break;
			case 7:
				return "bulanan";
			break;
			case -7:
				return "bukan bulanan";
			break;
			case 8:
				return "full day settler";
			break;
			case -8:	
				return "bukan full day settler";
			break;
			case 9:
				return "last_day_visitor";
			break;
			case -9:
				return "bukan_last_day_visitor";
			break;
			case 10:
				return "sering";
			break;
			
			case -10:
				return "bukan sering";
			break;
			default:
				return "undefined";
			break;
		}
	}
	
	function getMessageTypeComboCampaigne(){
		return array(1=>"NORMAL",2=>"INTERAKTIF");
	}
	
	function getMessageTypeCampaigne($id){
		switch(intval($id)){
			case 1:
				return "NON INTERAKTIF CAMPAIGN";
			break;
			case 2:
				return "INTERAKTIF CAMPAIGN";
			break;
			default :
				return "UNDEFINED";
			break;
		}		
	}
	
	function getShortCodeComboCampaigne(){
		$data = array();
		$sql 		= "select id,shortcode,name from tbl_shortcode_configuration where flag_deleted is null or flag_deleted = 0";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$x = 0;
		$count = count($row);
		while($x < $count){
			$data[$row[$x]['id']] = "[".$row[$x]['shortcode']."]".$row[$x]['name'];
			$x++;
		}
		return $data;
	}
	
	function getShortCodeCampaigne($id){
		$sql 		= "select shortcode from tbl_shortcode_configuration where id = ".$id."";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		return $row[0]['shortcode'];
	}
	
	function getShortCodeCampaigneByIdBatch($idBatch){
		$sql 		= "select id_configuration from tbl_shortcode_campaigne where id_batch = ".$idBatch."";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$idShortCode = $row[0]['id_configuration'];
		$sql 		= "select shortcode from tbl_shortcode_configuration where id = ".$idShortCode."";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		return $row[0]['shortcode'];
	}

	function getComboRecurring(){
		return array(1=>"daily",2=>"weekly");
	}
	
	
	function getRecurring($id){
		switch($id){
			case 1:
				return "DAILY";
			break ;
			case 2:
				return "WEEKLY";
			break;
			default:
				return "UNDEFINED";
			break;
		}
	}
	
	function getComboFlagRecurring(){
		return array(0=>"NEW",1=>"RUN",2=>"STOP");
	}
	
	function getFlagRecurring($id){
		switch($id){
			case 0:
				return "NEW";
			break;
			case 1:
				return "RUN";
			break;
			case 2:
				return "STOP";
			break;
			default:
				return "UNDEFINED";
			break;
		}
	}
	
	function getComboUrlInterface(){
		$data = array();
		$sql 		= "select id,url_interface from tbl_url_interface where status = 1";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$x = 0;
		$count = count($row);
		while($x < $count){
			$data[$row[$x]['id']] = $row[$x]['url_interface'];
			$x++;
		}
		return $data;
	}
	
	function getUrlInterface($id){
		$sql 		= "select url_interface from tbl_url_interface where status = 1 AND ID = ".$id."";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		return $row[0]['url_interface'];
	}
	
	function getFunctionBodyOnLoadByPage($title){
		if(substr_count($title,"Create CampaigneUrlInterface") > 0){
			return "onClickEventCapture();onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onChangeAutoPauseResume();onChangeCampaigneType();";
		}
		
		if(substr_count($title,"Create CampaigneInteractive") > 0){
			return "onClickEventCapture();onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();";
		}
		
		if(substr_count($title,"Update CampaigneAuto") > 0){
			return "onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();";
		}
		
		if(substr_count($title,"CampaigneAuto") > 0){
			return "onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();";
		}
		
		if(substr_count($title,"Create CampaigneStatistic") > 0){
			return "onClickEventCapture();onChangeCustomer();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();";
		}
		
		if(substr_count($title,"Create Campaigne") > 0){
			return "onClickEventCapture();onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();";
		}	
		if(substr_count($title,"Create Admin") > 0){
			return "onChangeRole();";
		}	
		if(substr_count($title,"Create MapGis") > 0){
			return "onClickEventCapture();onChangeCustomer();onChangeChannelCategory();onChangeGlobalWhiteList();onChangeAdvertisingSendingType();onEventSMSText();onChangeAutoPauseResume();onChangeCampaigneType();addElement();";
		}
		if(substr_count($title,"Create Ani2") > 0 || substr_count($title,"Update Ani2") > 0){
			return "onChangeMedia();";
		}	
	}
	
	function getComboEventCaptureName(){
		$data = array();
		$sql 		= "select * from tbl_event_capture";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$x = 0;
		$count = count($row);
		while($x < $count){
			$data[$row[$x]['jobs_id']] = $row[$x]['profile_name'];
			$x++;
		}
		return $data;
	}
	
	
	
	/** andesta **/
	
	function getRoleUser($id){
		$AuthAssignment = AuthAssignment::model()->findByAttributes(array('userid'=> $id));
		if($AuthAssignment !== null)
			$return = $AuthAssignment->itemname;
		else
			$return = "Not Set";
			
		return $return;
	}
	function getNotAdmin(){
		$AuthAssignment = AuthAssignment::model()->findAll('itemname = "Admin"');
		$return = "";
		foreach($AuthAssignment as $dataM){
			$return .= $dataM->userid.",";
		}
		return substr($return,0,-1);
	}
	function getCI($id){
		$bts = Bts::model()->findByPk($id);
		$lac = CHtml::listData(Lac::model()->findAll('(flag_deleted is null or flag_deleted <> 1)'),'lac_id','description');
		$return = "[".$bts->bts_id."|".$bts->lac."] ".$bts->bts_name." (".$lac[$bts->lac].")";
		return $return;
	}
	
	function getCustomer($id){
		$model = Customer::model()->findByPk($id);
		return $model->nama;
	}
	
	function getLocation($id){
		$model = Bts::model()->findByPk($id);
		return $model->bts_name;
	}
	
	function getAllLbaBatch($id,$param){
		$model = LbaBatch::model()->findByPk($id);
		if($model !== null)
			return $model->$param;
		else
			return "";
	}
	function getChannelProvisioning($id){
		$model = ChannelCategory::model()->findByPk($id);
		if($model !== null)
			return $model->name;
		else
			return "";
	}
	
	function getGridDaily($tanggal,$id_group){
		$start = $tanggal;
		$end =  date("Y-m-d",strtotime(date("Y-m-d", strtotime($tanggal)) . " -7 day"));
		$connection=Yii::app()->db;
		$sql = "select round(sum(total)/7) from tbl_profile_stat_daily where start_date >='".$end." 00:00:00' and start_date <= '".$start." 23:59:59' and group_id = '".$id_group."' and profile = 9   ";
		$command = $connection->createCommand($sql)->queryScalar();		
		
		return $command;
	}
	
	function getGrid($data){
		return str_replace("_"," ",$data);
	}
	
	
	
}
