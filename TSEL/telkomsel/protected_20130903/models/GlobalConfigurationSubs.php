<?php

/**
 * This is the model class for table "{{global_configuration_subs}}".
 *
 * The followings are the available columns in table '{{global_configuration_subs}}':
 * @property string $shortcode
 * @property string $keyword_on_whitelist
 * @property string $keyword_off_whitelist
 * @property string $keyword_on_blacklist
 * @property string $keyword_off_blacklist
 * @property string $response_success_on_whitelist_sub
 * @property string $response_success_off_whitelist_unsub
 * @property string $response_already_on_whitelist
 * @property string $response_already_off_whitelist
 * @property string $response_success_on_blacklist_block
 * @property string $response_success_off_blacklist_unblock
 * @property string $response_already_on_blacklist
 * @property string $response_already_off_blacklist
 * @property string $response_keyword_not_valid
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 * @property string $response_shortcode_expired
 * @property string $response_shortcode_already_response
 * @property string $response_shortcode_not_from_lba
 */
class GlobalConfigurationSubs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return GlobalConfigurationSubs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{global_configuration_subs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shortcode, keyword_on_whitelist, keyword_off_whitelist, keyword_on_blacklist, keyword_off_blacklist, response_success_on_whitelist_sub, response_success_off_whitelist_unsub, response_already_on_whitelist, response_already_off_whitelist, response_success_on_blacklist_block, response_success_off_blacklist_unblock, response_already_on_blacklist, response_already_off_blacklist, response_keyword_not_valid, response_shortcode_expired, response_shortcode_already_response, response_shortcode_not_from_lba', 'required'),
			array('shortcode', 'cekUnique'),
			array('shortcode, first_user, last_user', 'length', 'max'=>50),
			array('keyword_on_whitelist, keyword_off_whitelist, keyword_on_blacklist, keyword_off_blacklist', 'length', 'max'=>250),
			array('first_ip, last_ip', 'length', 'max'=>25),
			array('response_success_on_whitelist_sub, response_success_off_whitelist_unsub, response_already_on_whitelist, response_already_off_whitelist, response_success_on_blacklist_block, response_success_off_blacklist_unblock, response_already_on_blacklist, response_already_off_blacklist, response_keyword_not_valid, first_update, last_update, response_shortcode_expired, response_shortcode_already_response, response_shortcode_not_from_lba', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('shortcode, keyword_on_whitelist, keyword_off_whitelist, keyword_on_blacklist, keyword_off_blacklist, response_success_on_whitelist_sub, response_success_off_whitelist_unsub, response_already_on_whitelist, response_already_off_whitelist, response_success_on_blacklist_block, response_success_off_blacklist_unblock, response_already_on_blacklist, response_already_off_blacklist, response_keyword_not_valid, first_user, first_ip, first_update, last_user, last_ip, last_update, response_shortcode_expired, response_shortcode_already_response, response_shortcode_not_from_lba', 'safe', 'on'=>'search'),
		);
	}
	
	public function cekUnique($attribute,$params)
	{
		$current = ShortcodeConfiguration::model()->findByAttributes(array('shortcode'=>$this->shortcode));
		if($current != ""){
			$this->addError('shortcode',"$attribute '$this->shortcode' has already been taken in Shortcode Sms .");
		}
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'shortcode' => 'Shortcode',
			'keyword_on_whitelist' => 'Keyword On Whitelist',
			'keyword_off_whitelist' => 'Keyword Off Whitelist',
			'keyword_on_blacklist' => 'Keyword On Blacklist',
			'keyword_off_blacklist' => 'Keyword Off Blacklist',
			'response_success_on_whitelist_sub' => 'Response Success On Whitelist Sub',
			'response_success_off_whitelist_unsub' => 'Response Success Off Whitelist Unsub',
			'response_already_on_whitelist' => 'Response Already On Whitelist',
			'response_already_off_whitelist' => 'Response Already Off Whitelist',
			'response_success_on_blacklist_block' => 'Response Success On Blacklist Block',
			'response_success_off_blacklist_unblock' => 'Response Success Off Blacklist Unblock',
			'response_already_on_blacklist' => 'Response Already On Blacklist',
			'response_already_off_blacklist' => 'Response Already Off Blacklist',
			'response_keyword_not_valid' => 'Response Keyword Not Valid',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
			'response_shortcode_expired' => 'Response Shortcode Expired',
			'response_shortcode_already_response' => 'Response Shortcode Already Response',
			'response_shortcode_not_from_lba' => 'Response Shortcode Not From Lba',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('shortcode',$this->shortcode,true);
		$criteria->compare('keyword_on_whitelist',$this->keyword_on_whitelist,true);
		$criteria->compare('keyword_off_whitelist',$this->keyword_off_whitelist,true);
		$criteria->compare('keyword_on_blacklist',$this->keyword_on_blacklist,true);
		$criteria->compare('keyword_off_blacklist',$this->keyword_off_blacklist,true);
		$criteria->compare('response_success_on_whitelist_sub',$this->response_success_on_whitelist_sub,true);
		$criteria->compare('response_success_off_whitelist_unsub',$this->response_success_off_whitelist_unsub,true);
		$criteria->compare('response_already_on_whitelist',$this->response_already_on_whitelist,true);
		$criteria->compare('response_already_off_whitelist',$this->response_already_off_whitelist,true);
		$criteria->compare('response_success_on_blacklist_block',$this->response_success_on_blacklist_block,true);
		$criteria->compare('response_success_off_blacklist_unblock',$this->response_success_off_blacklist_unblock,true);
		$criteria->compare('response_already_on_blacklist',$this->response_already_on_blacklist,true);
		$criteria->compare('response_already_off_blacklist',$this->response_already_off_blacklist,true);
		$criteria->compare('response_keyword_not_valid',$this->response_keyword_not_valid,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('response_shortcode_expired',$this->response_shortcode_expired,true);
		$criteria->compare('response_shortcode_already_response',$this->response_shortcode_already_response,true);
		$criteria->compare('response_shortcode_not_from_lba',$this->response_shortcode_not_from_lba,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}