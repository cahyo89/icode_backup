<?php

/**
 * This is the model class for table "{{tree_id}}".
 *
 * The followings are the available columns in table '{{tree_id}}':
 * @property integer $tree_id
 * @property integer $usermode
 * @property string $nama_perangkat
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class TreeId extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TreeId the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tree_id}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_perangkat', 'required'),
			array('nama_perangkat', 'cekUnique'),
			array('usermode, flag_deleted', 'numerical', 'integerOnly'=>true),
			array('nama_perangkat, first_user, last_user', 'length', 'max'=>50),
			array('first_ip, last_ip', 'length', 'max'=>20),
			array('first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tree_id, usermode, nama_perangkat, flag_deleted, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}

	public function cekUnique($attribute,$params)
	{
		$current = TreeId::model()->findByAttributes(array('nama_perangkat'=>$this->nama_perangkat));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->tree_id != $this->tree_id)
			$this->addError('nama_perangkat',"$attribute '$this->nama_perangkat' has already been taken.");
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tree_id' => 'Tree',
			'usermode' => 'Usermode',
			'nama_perangkat' => 'Name',
			'flag_deleted' => 'Flag Deleted',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->condition='flag_deleted IS NULL or flag_deleted <>:deleted';
		$criteria->params=array(':deleted'=>1);	
		
		$criteria->compare('tree_id',$this->tree_id);
		$criteria->compare('usermode',$this->usermode);
		$criteria->compare('nama_perangkat',$this->nama_perangkat,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}