<?php

/**
 * This is the model class for table "{{ani}}".
 *
 * The followings are the available columns in table '{{ani}}':
 * @property integer $id
 * @property integer $id_customer
 * @property string $ani
 * @property integer $status
 * @property integer $media_type
 * @property string $cp_name
 * @property string $cp_password
 * @property string $cp_sid
 * @property string $cp_password_trx
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class Ani2 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Ani the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ani}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer', 'required'),
			array('ani', 'cekUnique'),
			array('ani', 'cekMms'),
			array('id_customer, status, media_type, flag_deleted, ani,vas_id,svc_code', 'numerical', 'integerOnly'=>true),
			array('ani', 'length', 'max'=>25),
			array('vas_id', 'length', 'min'=>3, 'max'=>4),
			array('svc_code', 'length', 'min'=>1),
			array('cp_name, cp_password, cp_password_trx, first_user, last_user', 'length', 'max'=>50),
			array('cp_sid,vas_id,svc_code', 'length', 'max'=>100),
			array('first_ip, last_ip', 'length', 'max'=>20),
			array('first_update, last_update,vas_id,svc_code', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,vas_id,svc_code, id_customer, ani, status,reason, media_type, cp_name, cp_password, cp_sid, cp_password_trx, flag_deleted, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}

	public function cekUnique($attribute,$params)
	{
		if($this->id_customer != -1 )
			$current = Ani::model()->findByAttributes(array('ani'=>$this->ani,'id_customer'=>$this->id_customer,'media_type'=>$this->media_type));
		else
			$current = Ani::model()->findByAttributes(array('ani'=>$this->ani,'media_type'=>$this->media_type));
			
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id != $this->id)
			$this->addError('ani',"SDC '$this->ani' has already been taken.");
		}
	}
	public function cekMms($attribute,$params)
	{
		if($this->media_type == 5 ){
			if($this->vas_id == "" || $this->vas_id == " ")
				$this->addError('vas_id'," Vas ID cannot be blank.");
				
			if($this->svc_code == "" || $this->svc_code == " ")
				$this->addError('svc_code'," Svc Code cannot be blank.");
		}
		else{
			if($this->ani == "" || $this->ani == " ")
				$this->addError('ani'," SDC cannot be blank.");
		}
	}
	
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Customer'=>array(self::BELONGS_TO,'Customer','id_customer'),
			'ChannelCategory'=>array(self::BELONGS_TO,'ChannelCategory','media_type')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_customer' => 'Customer',
			'ani' => 'SDC',
			'status' => 'Status',
			'media_type' => 'Media Type',
			'cp_name' => 'Cp Name',
			'cp_password' => 'Cp Password',
			'cp_sid' => 'Cp Sid',
			'cp_password_trx' => 'Cp Password Trx',
			'flag_deleted' => 'Flag Deleted',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		

		if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser')){
			$criteria->condition=' id_customer = '.Yii::app()->user->idCustomer.' ';
			$criteria->condition=' flag_deleted IS NULL or flag_deleted <>:deleted';
			$criteria->params=array(':deleted'=>1);	
		}
		else{
			$criteria->condition=' flag_deleted IS NULL or flag_deleted <>:deleted';
			$criteria->params=array(':deleted'=>1);	
		}
		
		
		$criteria->compare('id',$this->id);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('ani',$this->ani,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('media_type',$this->media_type);
		$criteria->compare('cp_name',$this->cp_name,true);
		$criteria->compare('cp_password',$this->cp_password,true);
		$criteria->compare('cp_sid',$this->cp_sid,true);
		$criteria->compare('cp_password_trx',$this->cp_password_trx,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}