<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Axis Telekom - Indonesia',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.models.*',
        'application.modules.user.components.*',
		'application.modules.rights.models.*',
        'application.modules.rights.components.*',
		'application.widgets.bootstrap.*',
		'application.extensions.CsvExportNew',
		'application.extensions.CsvExport',
	),

	'modules'=>array(
		'rights'=>array(
			'install'=>false,
			'superuserName'=>'Admin',
			'authenticatedName'=>'Authenticated',
			'userClass'=>'Users',//class tablenya
			'userIdColumn'=>'id',
			'userNameColumn'=>'username',
		),
		'user',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'superdodo',
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'mainprog',
	),

	// application components
	'components'=>array(
		'user'=>array(
			'class'=>'RWebUser',					
			'allowAutoLogin'=>true,
			'loginUrl' => array('/site/index'),
			'authExpires' =>1800,
		),
		'config' => array(
			'class' => 'application.extensions.EConfig',
		),
		'authManager'=>array(
			'class'=>'RDbAuthManager',
			'connectionID'=>'db',
			'defaultRoles'=>array('Guest'),
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		
		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		
		'db'=>array(
			'connectionString' => 'mysql:host=10.10.227.172;dbname=int_axis',
			'emulatePrepare' => true,
			'username' => 'mysqlxsms',
			'password' => 'xsmsmysql',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'sessionTimeoutSeconds'=>1800,
		'content_expired'=>86400,
		'path'=>'/projectcbc/program/web/cbc/file_batch_RunStop/',
		'paging'=>30,
		'status_batch'=>array(0=>"Waiting approval",1=>"Approved",2=>"Progress",3=>"Resuming",4=>"Pausing",5=>"Paused",6=>"Stopping",7=>"Stopped",9=>"Finish",11=>"Scheduled"),
		'type_invoice'=>array(0=>"Per Batch",1=>"Summarized Periode"),
		'invoice'=>array(0=>"Not Created",1=>"Created"),
		'date_campaign'=>array(0=>"Start Periode",1=>"Stop Periode"),
		'message_type'=>array(1=>"Normal",2=>"Interactive"),
		'groupBy'=>array(""=>"None","masking_customer"=>"Shortcode","id_customer"=>"Customer","media_type"=>"Channel Category","id_batch"=>"Campaign","error_code"=>"Error Code","id_group"=>'Location'),
		'groupBySumMt'=>array(""=>"None","sms_connection"=>"Sms Connection","masking_customer"=>"Shortcode","id_customer"=>"Customer","media_type"=>"Channel Category","id_batch"=>"Campaign","dr_state"=>"Error Code","id_group"=>'Location'),
		'groupByLocation'=>array(""=>"None","masking_customer"=>"Shortcode","id_customer"=>"Customer","id_group"=>"Location"),
		'groupByResponse'=>array(""=>"None","id_batch"=>"Campaign"),
		'widthTableMenu'=>"1000",
		'month'=>array(1=>"January",2=>"February",3=>"March",4=>"April",5=>"May",6=>"June",7=>"July",8=>"August",9=>"September",10=>"October",11=>"November",12=>"December"),
		'year'=>array("2012"=>"2012","2013"=>"2013","2014"=>"2014","2015"=>"2015","2016"=>"2016","2017"=>"2017","2018"=>"2018"),
		
		'tipeNpi' =>array(0=>"Unknown(0)",1=>"ISDN(1)",3=>"Data(3)",4=>"Telex(4)",6=>"Land Mobile(6)",8=>"National(8)",9=>"Private(9)",10=>"ERMES(10)",14=>"Internet(14)",18=>"WAP Client Id(18)"),							
		'tipeTon' => array(0=>"Unknown(0)",1=>"International(1)",2=>"National(2)",3=>"Network Specific(3)",4=>"Subscriber Number(4)",5=>"Alpha Numeric(5)",6=>"Abbreviated(6)"),
		'smpp_type' => array(0 => "SMPP Receiver", 1=>"SMP Transmitter", 2=>"SMPP Transceiver"),
		'state_connection' => array(0 => "Disconnected", 1=>"Binding", 2=>"Connected", 3 => "Disconnected", 4 => "Disconnected"),
		
		'type_campaign' => array(1=>"Msisdn",2=>"Imei",3=>"Tac"),
		'des_param_barring' => array(
			'T11:1' => 'Barring Telephony' ,
			'T11:0' => 'Debarring Telephony' ,
			'T21:1' => 'Barring SMSMT'  ,
			'T21:0' => 'Debarring SMSMT' ,
			'T22:1' => 'Barring SMSMO' ,
			'T22:0' => 'Debarring SMSMO' ,
			'T61:1' => 'Barring Fax and Data' ,
			'T61:0' => 'Debarring Fax and Data' ,
			'T62:1' => 'Barring Fax Only' ,
			'T62:0' => 'Debarring Fax Only' ,
			'B30:1' => 'Barring Video Telephony/Bearer Service Data Circuit Duplex General Synchronous' ,
			'B30:0' => 'Debarring Video Telephony/Bearer Service Data Circuit Duplex General Synchronous' ,
			'B21:1' => 'Barring Bearer Service Data Circuit Duplex Asynchronous 300 b/s' ,
			'B21:0' => 'Debarring Bearer Service Data Circuit Duplex Asynchronous 300 b/s' ,
			'B22:1' => 'Barring Bearer Service Data Circuit Duplex Asynchronous 1200 b/s' ,
			'B22:0' => 'Debarring Bearer Service Data Circuit Duplex Asynchronous 1200 b/s' ,
			'B24:1' => 'Barring Bearer Service Data Circuit Duplex Asynchronous 2400 b/s' ,
			'B24:0' => 'Debarring Bearer Service Data Circuit Duplex Asynchronous 2400 b/s' ,
			'B25:1' => 'Barring Bearer Service Data Circuit Duplex Asynchronous 4800 b/s' ,
			'B25:0' => 'Debarring Bearer Service Data Circuit Duplex Asynchronous 4800 b/s' ,
			'B26:1' => 'Barring Bearer Service Data Circuit Duplex Asynchronous 9600 b/s' ,
			'B26:0' => 'Debarring Bearer Service Data Circuit Duplex Asynchronous 9600 b/s' ,
			'B31:1' => 'Barring Bearer Service Data Circuit Duplex Synchronous 1200 b/s' ,
			'B31:0' => 'Debarring Bearer Service Data Circuit Duplex Synchronous 1200 b/s' ,
			'B32:1' => 'Barring Bearer Service Data Circuit Duplex Synchronous 2400 b/s' ,
			'B32:0' => 'Debarring Bearer Service Data Circuit Duplex Synchronous 2400 b/s' ,
			'B33:1' => 'Barring Bearer Service Data Circuit Duplex Synchronous 4800 b/s' ,
			'B33:0' => 'Debarring Bearer Service Data Circuit Duplex Synchronous 4800 b/s' ,
			'B34:1' => 'Barring Bearer Service Data Circuit Duplex Synchronous 9600 b/s' ,
			'B34:0' => 'Debarring Bearer Service Data Circuit Duplex Synchronous 9600 b/s' ,

			'BAIC:0' => 'Barring of all incoming calls.' ,
			'BAIC:1' => 'Debarring of all incoming calls.' ,
			'BAOC:0' => 'Barring of outgoing international calls.' ,
			'BAOC:1' => 'Debarring of outgoing international calls.' ,
			'BOIC:0' => 'Barring of outgoing international calls.' ,
			'BOIC:1' => 'Debarring of outgoing international calls.' ,
			'BICRO:0' => 'Barring  of incoming calls when roaming.' ,
			'BICRO:1' => 'Debarring  of incoming calls when roaming.' ,
			'BOIEXH:0' => 'Barring of outgoing international calls except those directed to the home PLMN country.' ,
			'BOIEXH:1' => 'Debarring of outgoing international calls except those directed to the home PLMN country.' ,
			'OBI:0' => 'Operator Determined barring of all incoming calls.' ,
			'OBI:1' => 'Operator Determined debarring of all incoming calls..' ,
			'OBO:0' => 'Operator Determined barring of all outgoing calls.' ,
			'OBO:1' => 'Operator Determined debarring of all outgoing calls.' ,
			'OBZO:0' => 'Operator Determined barring of outgoing international calls.' ,
			'OBZO:1' => 'Operator Determined debarring of outgoing international calls.' ,
			'OBICRO:0' => 'Operator Determined barring of incoming calls when roaming.' ,
			'OBICRO:1' => 'Operator Determined debarring of incoming calls when roaming.' ,
			'OBOIEXH:0' => 'Operator Determined barring of outgoing international calls when roaming.' ,
			'OBOIEXH:1' => 'Operator Determined debarring of outgoing international calls when roaming.' ,
			'OBR:0' => 'Operator Determined barring of roaming.' ,
			'OBR:1' => 'Operator Determined debarring of roaming.' ,
			'OBGPRS:0' => 'Operator Determined barring of packet oriented service.' ,
			'OBGPRS:1' => 'Operator Determined debarring of packet oriented service.'
		),
		'segmen_customer'=>array(0=>"Internal",1=>"External"),
		'connection'=>array(0=>"Failed",1=>"Success"),
		'default_rule_template'=>array('normal'=>'08:00-22:00','morning'=>'08:00-12:00','day'=>'12:00-16:00','afternoon'=>'16:00-19:00','evening'=>'19:00-22:00','office hour'=>'08:00-17:30','non office hour'=>'17:30-22:00','lunch'=>'11:30-13:30'),
		'cErrorSmpp'=>$cErrorSmpp = array('0'=>'Scheduled','1'=>'Pending','2'=>'Success','3'=>'Expired','4'=>'Deleted',
		'5'=>'Udeliverable','6'=>'Accepted','7'=>'Unknown','8'=>'Rejected',
		'19'=>'Timeout','3073'=>'Message Length is invalid','3074'=>'Command Length is invalid',
		'3075'=>'Invalid Command ID','3076'=>'Incorrect BIND Status for given command','3077'=>'ESME Already in Bound State','3078'=>'Invalid Priority Flag',
		'3079'=>'Invalid Registered Delivery Flag','3080'=>'System Error','3082'=>'Invalid Source Address','3083'=>'Invalid Dest Addr',
		'3084'=>'Message ID is invalid','3085'=>'Bind Failed','3086'=>'Invalid Password','3087'=>'Invalid System ID','3089'=>'Cancel SM Failed',
		'3091'=>'Replace SM Failed','3092'=>'Message Queue Full','3093'=>'Invalid Service Type','3123'=>'Invalid number of destinations',
		'3124'=>'Invalid Distribution List name','3136'=>'Destination flag is invalid (submit_multi)','3138'=>'Invalid �submit with replace� request (i.e. submit_sm with replace_if_present_flag set)',
		'3139'=>'Invalid esm_class field data','3140'=>'Cannot Submit to Distribution List','3141'=>'submit_sm or submit_multi failed',
		'3144'=>'Invalid Source address TON','3145'=>'Invalid Source address NPI','3152'=>'Invalid Destination address TON','3153'=>'Invalid Destination address NPI',
		'3155'=>'Invalid system_type field','3156'=>'Invalid replace_if_present flag','3157'=>'Invalid number of messages','3160'=>'Throttling error (ESME has exceeded allowed message limits)',
		'3169'=>'Invalid Scheduled Delivery Time','3170'=>'Invalid message validity period (Expiry time)','3171'=>'Predefined Message Invalid or Not Found',
		'3172'=>'ESME Receiver Temporary App Error Code','3173'=>'ESME Receiver Permanent App Error Code','3174'=>'ESME Receiver Reject Message Error Code',
		'3175'=>'query_sm request failed','3264'=>'Error in the optional part of the PDU Body','3265'=>'Optional Parameter not allowed',
		'3266'=>'Invalid Parameter Length','3267'=>'Expected Optional Parameter missing','3268'=>'Invalid Optional Parameter Value',
		'3326'=>'Delivery Failure (used for data_sm_resp)','3327'=>'Unknown Error'),
		'tipe_customer'   => array(0 => 'Prepaid', 1=> 'Postpaid'),
		'bypass_approval' => array(0 => 'No Bypass', 1 => 'Yes Bypass'),
		'blocked'         => array(0 => 'Not Blocked', 1 => 'Blocked'),
		'approved'        => array(-1 => 'Not Set',0 => 'New', 1 => 'Approved', 2 => 'Rejected'),
		'balance_approved' => array(0 => 'New', 1 => 'Approved From Technical', 2 => 'Approved From Accounting',3=>'Rejected'),
		'dataType'        => array(1 => 'Daily', 0 => 'Hourly'),
		'mediaType'       => array('All'=>'All',0=>"SMS",5=>"MMS",6=>"STATISTIC",7=>"Url Interface"),
		'media_type'      => array(0 => 'New SMS', 5 => 'New MMS',6=>"Statistic",7=>"Url Interface"),
		'grouping'        => array(0 => 'Time', 1 => 'Error Code' , 2 =>'Masking Customer'),
		'dataView'        => array(0 => 'Table', 1 => 'Graphic'),
		'dataHour'        => array("00"=>"00","01" =>"01","02" =>"02","03" =>"03","04" =>"04","05" =>"05","06" =>"06","07"=>"07","08" =>"08","09" =>"09","10" =>"10","11" =>"11","12" =>"12","13" =>"13","14" =>"14","15" =>"15","16" =>"16","17" =>"17","18" =>"18","19" =>"19","20" =>"20","21" =>"21","22" =>"22","23" =>"23"),
		'profile'         => array(1 => 'penduduk', 2 => 'pekerja', 3 => 'last_week', 4 => 'pernah_4_minggu', 5 => 'pernah_3_bulan',6 => 'mingguan', 7 => 'bulanan', 8=> 'full day settler', 9 => "last_day_visitor", 10 => "Sering",12 =>"Last 2 Week"),
		'adssendingtype'  => array(0=>"On Event"),
		'priority'		  => array(0=>"Normal",1=>"Medium",2=>"High"),
		'profile'		  => array(0=>'Normal'),
		'camstatus'		  => array(0=>'Idle',1=>'Approved',2=>'Progress',6=>'Stopping',7=>'Stopped / Rejected',9=>'Finish',11=>'Scheduled'),
		'channelcatagory' => array(0=>"SMS",5=>"MMS",8=>"USSD","65280"=>"HTTP Event"),
		'http_intercept_ads_type' => array(0=>"view",1=>"click"),
		'imagesize' => array(0 => "960 x 135", 1 => "700 x 105",2 => "300 x 45",3=>"635 x 270",4=>"377 x 270",5=>"320 x 80",6=>"-100 x 80",7=>"100% x 100%",8 =>"100% x 80",9 =>"-80 x 80",10=>"-100 x -100"),
		'tipe_paket' => array(0=>'View',1=>'Click'),
		'uploadpath' => "/pub/upload/data/banner/", ///nfs_data/projectmobads/program/web_admin/pub/upload/data/banner
		'uploadpathMMS' => "/pub/mms/", ///nfs_data/projectmobads/program/web_admin/pub/upload/data/banner
		'uploadpathBroadcast' => "/pub/broadcast/", ///nfs_data/projectmobads/program/web_admin/pub/upload/data/banner
		
		'imagetype' => array('image/gif', 'image/jpeg', 'image/jpg', 'image/png'),
		'filebroadcasttype' => array('text/plain'),
		'subscriber' => array(0=>'Not Unique', 3=>'Unique'),
		'expandtype' => array(0=>"Normal Banner",1=>"Premium Content",2=>"Premium Banner"),
		'expandvalue' => array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,256=>0,257=>0,769=>0,513=>1,513=>2),
		'http_intercept_ads_value' => array(
								
								//"0"		=>	"Right Bottom"
								//,"1"	=>	"Right Top"
								//,"2"	=>	"Left Top"
								//,"3"	=>	"Left Bottom"
								//,"4"	=>	"Top"
								//,"5"	=>	"Bottom"
								//,"6"	=>	"Central"
								
								/*
								,"7"	=>	"Customization JavaScript"
								,"8"	=>	"Customization JavaScript"
								,"9"	=>	"Customization JavaScript"
								,"A"	=>	"Customization JavaScript"
								,"B"	=>	"Customization JavaScript"
								,"C"	=>	"Customization JavaScript"
								,"D"	=>	"Customization JavaScript"
								,"E"	=>	"Customization JavaScript"
								,"F"	=>	"Customization JavaScript"
								,"10"	=>	"Customization JavaScript"
								,"11"	=>	"Customization JavaScript"
								,"12"	=>	"Customization JavaScript"
								,"13"	=>	"Customization JavaScript"
								,"14"	=>	"Customization JavaScript"
								,"15"	=>	"Customization JavaScript"
								,"16"	=>	"Customization JavaScript"
								,"17"	=>	"Customization JavaScript"
								,"18"	=>	"Customization JavaScript"
								,"19"	=>	"Customization JavaScript"
								,"1A"	=>	"Customization JavaScript"
								,"1B"	=>	"Customization JavaScript"
								,"1C"	=>	"Customization JavaScript"
								,"1D"	=>	"Customization JavaScript"
								,"1E"	=>	"Customization JavaScript"
								,"1F"	=>	"Customization JavaScript"
								,"20"	=>	"Customization JavaScript"
								,"21"	=>	"Customization JavaScript"
								,"22"	=>	"Customization JavaScript"
								,"23"	=>	"Customization JavaScript"
								,"24"	=>	"Customization JavaScript"
								,"25"	=>	"Customization JavaScript"
								,"26"	=>	"Customization JavaScript"
								,"27"	=>	"Customization JavaScript"
								,"28"	=>	"Customization JavaScript"
								,"29"	=>	"Customization JavaScript"
								,"2A"	=>	"Customization JavaScript"
								,"2B"	=>	"Customization JavaScript"
								,"2C"	=>	"Customization JavaScript"
								,"2D"	=>	"Customization JavaScript"
								,"2E"	=>	"Customization JavaScript"
								,"2F"	=>	"Customization JavaScript"
								,"30"	=>	"Customization JavaScript"
								,"31"	=>	"Customization JavaScript"
								,"32"	=>	"Customization JavaScript"
								,"33"	=>	"Customization JavaScript"
								,"34"	=>	"Customization JavaScript"
								,"35"	=>	"Customization JavaScript"
								,"36"	=>	"Customization JavaScript"
								,"37"	=>	"Customization JavaScript"
								,"38"	=>	"Customization JavaScript"
								,"39"	=>	"Customization JavaScript"
								,"3A"	=>	"Customization JavaScript"
								,"3B"	=>	"Customization JavaScript"
								,"3C"	=>	"Customization JavaScript"
								,"3D"	=>	"Customization JavaScript"
								,"3E"	=>	"Customization JavaScript"
								,"3F"	=>	"Customization JavaScript"
								,"40"	=>	"Customization JavaScript"
								,"41"	=>	"Customization JavaScript"
								,"42"	=>	"Customization JavaScript"
								,"43"	=>	"Customization JavaScript"
								,"44"	=>	"Customization JavaScript"
								,"45"	=>	"Customization JavaScript"
								,"46"	=>	"Customization JavaScript"
								,"47"	=>	"Customization JavaScript"
								,"48"	=>	"Customization JavaScript"
								,"49"	=>	"Customization JavaScript"
								,"4A"	=>	"Customization JavaScript"
								,"4B"	=>	"Customization JavaScript"
								,"4C"	=>	"Customization JavaScript"
								,"4D"	=>	"Customization JavaScript"
								,"4E"	=>	"Customization JavaScript"
								,"4F"	=>	"Customization JavaScript"
								,"50"	=>	"Customization JavaScript"
								,"51"	=>	"Customization JavaScript"
								,"52"	=>	"Customization JavaScript"
								,"53"	=>	"Customization JavaScript"
								,"54"	=>	"Customization JavaScript"
								,"55"	=>	"Customization JavaScript"
								,"56"	=>	"Customization JavaScript"
								,"57"	=>	"Customization JavaScript"
								,"58"	=>	"Customization JavaScript"
								,"59"	=>	"Customization JavaScript"
								,"5A"	=>	"Customization JavaScript"
								,"5B"	=>	"Customization JavaScript"
								,"5C"	=>	"Customization JavaScript"
								,"5D"	=>	"Customization JavaScript"
								,"5E"	=>	"Customization JavaScript"
								,"5F"	=>	"Customization JavaScript"
								,"60"	=>	"Customization JavaScript"
								,"61"	=>	"Customization JavaScript"
								,"62"	=>	"Customization JavaScript"
								,"63"	=>	"Customization JavaScript"
								,"64"	=>	"Customization JavaScript"
								,"65"	=>	"Customization JavaScript"
								,"66"	=>	"Customization JavaScript"
								,"67"	=>	"Customization JavaScript"
								,"68"	=>	"Customization JavaScript"
								,"69"	=>	"Customization JavaScript"
								,"6A"	=>	"Customization JavaScript"
								,"6B"	=>	"Customization JavaScript"
								,"6C"	=>	"Customization JavaScript"
								,"6D"	=>	"Customization JavaScript"
								,"6E"	=>	"Customization JavaScript"
								,"6F"	=>	"Customization JavaScript"
								,"70"	=>	"Customization JavaScript"
								,"71"	=>	"Customization JavaScript"
								,"72"	=>	"Customization JavaScript"
								,"73"	=>	"Customization JavaScript"
								,"74"	=>	"Customization JavaScript"
								,"75"	=>	"Customization JavaScript"
								,"76"	=>	"Customization JavaScript"
								,"77"	=>	"Customization JavaScript"
								,"78"	=>	"Customization JavaScript"
								,"79"	=>	"Customization JavaScript"
								,"7A"	=>	"Customization JavaScript"
								,"7B"	=>	"Customization JavaScript"
								,"7C"	=>	"Customization JavaScript"
								,"7D"	=>	"Customization JavaScript"
								,"7E"	=>	"Customization JavaScript"
								,"7F"	=>	"Customization JavaScript"
								,"80"	=>	"Customization JavaScript"
								,"81"	=>	"Customization JavaScript"
								,"82"	=>	"Customization JavaScript"
								,"83"	=>	"Customization JavaScript"
								,"84"	=>	"Customization JavaScript"
								,"85"	=>	"Customization JavaScript"
								,"86"	=>	"Customization JavaScript"
								,"87"	=>	"Customization JavaScript"
								,"88"	=>	"Customization JavaScript"
								,"89"	=>	"Customization JavaScript"
								,"8A"	=>	"Customization JavaScript"
								,"8B"	=>	"Customization JavaScript"
								,"8C"	=>	"Customization JavaScript"
								,"8D"	=>	"Customization JavaScript"
								,"8E"	=>	"Customization JavaScript"
								,"8F"	=>	"Customization JavaScript"
								,"90"	=>	"Customization JavaScript"
								,"91"	=>	"Customization JavaScript"
								,"92"	=>	"Customization JavaScript"
								,"93"	=>	"Customization JavaScript"
								,"94"	=>	"Customization JavaScript"
								,"95"	=>	"Customization JavaScript"
								,"96"	=>	"Customization JavaScript"
								,"97"	=>	"Customization JavaScript"
								,"98"	=>	"Customization JavaScript"
								,"99"	=>	"Customization JavaScript"
								,"9A"	=>	"Customization JavaScript"
								,"9B"	=>	"Customization JavaScript"
								,"9C"	=>	"Customization JavaScript"
								,"9D"	=>	"Customization JavaScript"
								,"9E"	=>	"Customization JavaScript"
								,"9F"	=>	"Customization JavaScript"
								,"A0"	=>	"Customization JavaScript"
								,"A1"	=>	"Customization JavaScript"
								,"A2"	=>	"Customization JavaScript"
								,"A3"	=>	"Customization JavaScript"
								,"A4"	=>	"Customization JavaScript"
								,"A5"	=>	"Customization JavaScript"
								,"A6"	=>	"Customization JavaScript"
								,"A7"	=>	"Customization JavaScript"
								,"A8"	=>	"Customization JavaScript"
								,"A9"	=>	"Customization JavaScript"
								,"AA"	=>	"Customization JavaScript"
								,"AB"	=>	"Customization JavaScript"
								,"AC"	=>	"Customization JavaScript"
								,"AD"	=>	"Customization JavaScript"
								,"AE"	=>	"Customization JavaScript"
								,"AF"	=>	"Customization JavaScript"
								,"B0"	=>	"Customization JavaScript"
								,"B1"	=>	"Customization JavaScript"
								,"B2"	=>	"Customization JavaScript"
								,"B3"	=>	"Customization JavaScript"
								,"B4"	=>	"Customization JavaScript"
								,"B5"	=>	"Customization JavaScript"
								,"B6"	=>	"Customization JavaScript"
								,"B7"	=>	"Customization JavaScript"
								,"B8"	=>	"Customization JavaScript"
								,"B9"	=>	"Customization JavaScript"
								,"BA"	=>	"Customization JavaScript"
								,"BB"	=>	"Customization JavaScript"
								,"BC"	=>	"Customization JavaScript"
								,"BD"	=>	"Customization JavaScript"
								,"BE"	=>	"Customization JavaScript"
								,"BF"	=>	"Customization JavaScript"
								,"C0"	=>	"Customization JavaScript"
								,"C1"	=>	"Customization JavaScript"
								,"C2"	=>	"Customization JavaScript"
								,"C3"	=>	"Customization JavaScript"
								,"C4"	=>	"Customization JavaScript"
								,"C5"	=>	"Customization JavaScript"
								,"C6"	=>	"Customization JavaScript"
								,"C7"	=>	"Customization JavaScript"
								,"C8"	=>	"Customization JavaScript"
								,"C9"	=>	"Customization JavaScript"
								,"CA"	=>	"Customization JavaScript"
								,"CB"	=>	"Customization JavaScript"
								,"CC"	=>	"Customization JavaScript"
								,"CD"	=>	"Customization JavaScript"
								,"CE"	=>	"Customization JavaScript"
								,"CF"	=>	"Customization JavaScript"
								,"D0"	=>	"Customization JavaScript"
								,"D1"	=>	"Customization JavaScript"
								,"D2"	=>	"Customization JavaScript"
								,"D3"	=>	"Customization JavaScript"
								,"D4"	=>	"Customization JavaScript"
								,"D5"	=>	"Customization JavaScript"
								,"D6"	=>	"Customization JavaScript"
								,"D7"	=>	"Customization JavaScript"
								,"D8"	=>	"Customization JavaScript"
								,"D9"	=>	"Customization JavaScript"
								,"DA"	=>	"Customization JavaScript"
								,"DB"	=>	"Customization JavaScript"
								,"DC"	=>	"Customization JavaScript"
								,"DD"	=>	"Customization JavaScript"
								,"DE"	=>	"Customization JavaScript"
								,"DF"	=>	"Customization JavaScript"
								,"E0"	=>	"Customization JavaScript"
								,"E1"	=>	"Customization JavaScript"
								,"E2"	=>	"Customization JavaScript"
								,"E3"	=>	"Customization JavaScript"
								,"E4"	=>	"Customization JavaScript"
								,"E5"	=>	"Customization JavaScript"
								,"E6"	=>	"Customization JavaScript"
								,"E7"	=>	"Customization JavaScript"
								,"E8"	=>	"Customization JavaScript"
								,"E9"	=>	"Customization JavaScript"
								,"EA"	=>	"Customization JavaScript"
								,"EB"	=>	"Customization JavaScript"
								,"EC"	=>	"Customization JavaScript"
								,"ED"	=>	"Customization JavaScript"
								,"EE"	=>	"Customization JavaScript"
								,"EF"	=>	"Customization JavaScript"
								,"F0"	=>	"Customization JavaScript"
								,"F1"	=>	"Customization JavaScript"
								,"F2"	=>	"Customization JavaScript"
								,"F3"	=>	"Customization JavaScript"
								,"F4"	=>	"Customization JavaScript"
								,"F5"	=>	"Customization JavaScript"
								,"F6"	=>	"Customization JavaScript"
								,"F7"	=>	"Customization JavaScript"
								,"F8"	=>	"Customization JavaScript"
								,"F9"	=>	"Customization JavaScript"
								,"FA"	=>	"Customization JavaScript"
								,"FB"	=>	"Customization JavaScript"
								,"FC"	=>	"Customization JavaScript"
								,"FD"	=>	"Customization JavaScript"
								,"FE"	=>	"Customization JavaScript"
								,"FF"	=>	"Customization JavaScript"
								*/
								
								"256"	=>	"Bottom Pannel"
								,"257"	=>	"Top Pannel"
								,"513"	=>	"Content Expandable"
								,"769"  =>  "Expandable"
								,"0"	=>	"Right Bottom"
								,"1"	=>	"Right Top"
								,"2"	=>	"Left Top"
								,"3"	=>	"Left Bottom"
								,"4"	=>	"Top"
								,"5"	=>	"Bottom"
								,"6"	=>	"Central"
								
								/*
								,"17"	=>	"Customization JavaScript"
								,"18"	=>	"Customization JavaScript"
								,"19"	=>	"Customization JavaScript"
								,"1A"	=>	"Customization JavaScript"
								,"1B"	=>	"Customization JavaScript"
								,"1C"	=>	"Customization JavaScript"
								,"1D"	=>	"Customization JavaScript"
								,"1E"	=>	"Customization JavaScript"
								,"1F"	=>	"Customization JavaScript"
								,"110"	=>	"Customization JavaScript"
								,"111"	=>	"Customization JavaScript"
								,"112"	=>	"Customization JavaScript"
								,"113"	=>	"Customization JavaScript"
								,"114"	=>	"Customization JavaScript"
								,"115"	=>	"Customization JavaScript"
								,"116"	=>	"Customization JavaScript"
								,"117"	=>	"Customization JavaScript"
								,"118"	=>	"Customization JavaScript"
								,"119"	=>	"Customization JavaScript"
								,"11A"	=>	"Customization JavaScript"
								,"11B"	=>	"Customization JavaScript"
								,"11C"	=>	"Customization JavaScript"
								,"11D"	=>	"Customization JavaScript"
								,"11E"	=>	"Customization JavaScript"
								,"11F"	=>	"Customization JavaScript"
								,"120"	=>	"Customization JavaScript"
								,"121"	=>	"Customization JavaScript"
								,"122"	=>	"Customization JavaScript"
								,"123"	=>	"Customization JavaScript"
								,"124"	=>	"Customization JavaScript"
								,"125"	=>	"Customization JavaScript"
								,"126"	=>	"Customization JavaScript"
								,"127"	=>	"Customization JavaScript"
								,"128"	=>	"Customization JavaScript"
								,"129"	=>	"Customization JavaScript"
								,"12A"	=>	"Customization JavaScript"
								,"12B"	=>	"Customization JavaScript"
								,"12C"	=>	"Customization JavaScript"
								,"12D"	=>	"Customization JavaScript"
								,"12E"	=>	"Customization JavaScript"
								,"12F"	=>	"Customization JavaScript"
								,"130"	=>	"Customization JavaScript"
								,"131"	=>	"Customization JavaScript"
								,"132"	=>	"Customization JavaScript"
								,"133"	=>	"Customization JavaScript"
								,"134"	=>	"Customization JavaScript"
								,"135"	=>	"Customization JavaScript"
								,"136"	=>	"Customization JavaScript"
								,"137"	=>	"Customization JavaScript"
								,"138"	=>	"Customization JavaScript"
								,"139"	=>	"Customization JavaScript"
								,"13A"	=>	"Customization JavaScript"
								,"13B"	=>	"Customization JavaScript"
								,"13C"	=>	"Customization JavaScript"
								,"13D"	=>	"Customization JavaScript"
								,"13E"	=>	"Customization JavaScript"
								,"13F"	=>	"Customization JavaScript"
								,"140"	=>	"Customization JavaScript"
								,"141"	=>	"Customization JavaScript"
								,"142"	=>	"Customization JavaScript"
								,"143"	=>	"Customization JavaScript"
								,"144"	=>	"Customization JavaScript"
								,"145"	=>	"Customization JavaScript"
								,"146"	=>	"Customization JavaScript"
								,"147"	=>	"Customization JavaScript"
								,"148"	=>	"Customization JavaScript"
								,"149"	=>	"Customization JavaScript"
								,"14A"	=>	"Customization JavaScript"
								,"14B"	=>	"Customization JavaScript"
								,"14C"	=>	"Customization JavaScript"
								,"14D"	=>	"Customization JavaScript"
								,"14E"	=>	"Customization JavaScript"
								,"14F"	=>	"Customization JavaScript"
								,"150"	=>	"Customization JavaScript"
								,"151"	=>	"Customization JavaScript"
								,"152"	=>	"Customization JavaScript"
								,"153"	=>	"Customization JavaScript"
								,"154"	=>	"Customization JavaScript"
								,"155"	=>	"Customization JavaScript"
								,"156"	=>	"Customization JavaScript"
								,"157"	=>	"Customization JavaScript"
								,"158"	=>	"Customization JavaScript"
								,"159"	=>	"Customization JavaScript"
								,"15A"	=>	"Customization JavaScript"
								,"15B"	=>	"Customization JavaScript"
								,"15C"	=>	"Customization JavaScript"
								,"15D"	=>	"Customization JavaScript"
								,"15E"	=>	"Customization JavaScript"
								,"15F"	=>	"Customization JavaScript"
								,"160"	=>	"Customization JavaScript"
								,"161"	=>	"Customization JavaScript"
								,"162"	=>	"Customization JavaScript"
								,"163"	=>	"Customization JavaScript"
								,"164"	=>	"Customization JavaScript"
								,"165"	=>	"Customization JavaScript"
								,"166"	=>	"Customization JavaScript"
								,"167"	=>	"Customization JavaScript"
								,"168"	=>	"Customization JavaScript"
								,"169"	=>	"Customization JavaScript"
								,"16A"	=>	"Customization JavaScript"
								,"16B"	=>	"Customization JavaScript"
								,"16C"	=>	"Customization JavaScript"
								,"16D"	=>	"Customization JavaScript"
								,"16E"	=>	"Customization JavaScript"
								,"16F"	=>	"Customization JavaScript"
								,"170"	=>	"Customization JavaScript"
								,"171"	=>	"Customization JavaScript"
								,"172"	=>	"Customization JavaScript"
								,"173"	=>	"Customization JavaScript"
								,"174"	=>	"Customization JavaScript"
								,"175"	=>	"Customization JavaScript"
								,"176"	=>	"Customization JavaScript"
								,"177"	=>	"Customization JavaScript"
								,"178"	=>	"Customization JavaScript"
								,"179"	=>	"Customization JavaScript"
								,"17A"	=>	"Customization JavaScript"
								,"17B"	=>	"Customization JavaScript"
								,"17C"	=>	"Customization JavaScript"
								,"17D"	=>	"Customization JavaScript"
								,"17E"	=>	"Customization JavaScript"
								,"17F"	=>	"Customization JavaScript"
								,"180"	=>	"Customization JavaScript"
								,"181"	=>	"Customization JavaScript"
								,"182"	=>	"Customization JavaScript"
								,"183"	=>	"Customization JavaScript"
								,"184"	=>	"Customization JavaScript"
								,"185"	=>	"Customization JavaScript"
								,"186"	=>	"Customization JavaScript"
								,"187"	=>	"Customization JavaScript"
								,"188"	=>	"Customization JavaScript"
								,"189"	=>	"Customization JavaScript"
								,"18A"	=>	"Customization JavaScript"
								,"18B"	=>	"Customization JavaScript"
								,"18C"	=>	"Customization JavaScript"
								,"18D"	=>	"Customization JavaScript"
								,"18E"	=>	"Customization JavaScript"
								,"18F"	=>	"Customization JavaScript"
								,"190"	=>	"Customization JavaScript"
								,"191"	=>	"Customization JavaScript"
								,"192"	=>	"Customization JavaScript"
								,"193"	=>	"Customization JavaScript"
								,"194"	=>	"Customization JavaScript"
								,"195"	=>	"Customization JavaScript"
								,"196"	=>	"Customization JavaScript"
								,"197"	=>	"Customization JavaScript"
								,"198"	=>	"Customization JavaScript"
								,"199"	=>	"Customization JavaScript"
								,"19A"	=>	"Customization JavaScript"
								,"19B"	=>	"Customization JavaScript"
								,"19C"	=>	"Customization JavaScript"
								,"19D"	=>	"Customization JavaScript"
								,"19E"	=>	"Customization JavaScript"
								,"19F"	=>	"Customization JavaScript"
								,"1A0"	=>	"Customization JavaScript"
								,"1A1"	=>	"Customization JavaScript"
								,"1A2"	=>	"Customization JavaScript"
								,"1A3"	=>	"Customization JavaScript"
								,"1A4"	=>	"Customization JavaScript"
								,"1A5"	=>	"Customization JavaScript"
								,"1A6"	=>	"Customization JavaScript"
								,"1A7"	=>	"Customization JavaScript"
								,"1A8"	=>	"Customization JavaScript"
								,"1A9"	=>	"Customization JavaScript"
								,"1AA"	=>	"Customization JavaScript"
								,"1AB"	=>	"Customization JavaScript"
								,"1AC"	=>	"Customization JavaScript"
								,"1AD"	=>	"Customization JavaScript"
								,"1AE"	=>	"Customization JavaScript"
								,"1AF"	=>	"Customization JavaScript"
								,"1B0"	=>	"Customization JavaScript"
								,"1B1"	=>	"Customization JavaScript"
								,"1B2"	=>	"Customization JavaScript"
								,"1B3"	=>	"Customization JavaScript"
								,"1B4"	=>	"Customization JavaScript"
								,"1B5"	=>	"Customization JavaScript"
								,"1B6"	=>	"Customization JavaScript"
								,"1B7"	=>	"Customization JavaScript"
								,"1B8"	=>	"Customization JavaScript"
								,"1B9"	=>	"Customization JavaScript"
								,"1BA"	=>	"Customization JavaScript"
								,"1BB"	=>	"Customization JavaScript"
								,"1BC"	=>	"Customization JavaScript"
								,"1BD"	=>	"Customization JavaScript"
								,"1BE"	=>	"Customization JavaScript"
								,"1BF"	=>	"Customization JavaScript"
								,"1C0"	=>	"Customization JavaScript"
								,"1C1"	=>	"Customization JavaScript"
								,"1C2"	=>	"Customization JavaScript"
								,"1C3"	=>	"Customization JavaScript"
								,"1C4"	=>	"Customization JavaScript"
								,"1C5"	=>	"Customization JavaScript"
								,"1C6"	=>	"Customization JavaScript"
								,"1C7"	=>	"Customization JavaScript"
								,"1C8"	=>	"Customization JavaScript"
								,"1C9"	=>	"Customization JavaScript"
								,"1CA"	=>	"Customization JavaScript"
								,"1CB"	=>	"Customization JavaScript"
								,"1CC"	=>	"Customization JavaScript"
								,"1CD"	=>	"Customization JavaScript"
								,"1CE"	=>	"Customization JavaScript"
								,"1CF"	=>	"Customization JavaScript"
								,"1D0"	=>	"Customization JavaScript"
								,"1D1"	=>	"Customization JavaScript"
								,"1D2"	=>	"Customization JavaScript"
								,"1D3"	=>	"Customization JavaScript"
								,"1D4"	=>	"Customization JavaScript"
								,"1D5"	=>	"Customization JavaScript"
								,"1D6"	=>	"Customization JavaScript"
								,"1D7"	=>	"Customization JavaScript"
								,"1D8"	=>	"Customization JavaScript"
								,"1D9"	=>	"Customization JavaScript"
								,"1DA"	=>	"Customization JavaScript"
								,"1DB"	=>	"Customization JavaScript"
								,"1DC"	=>	"Customization JavaScript"
								,"1DD"	=>	"Customization JavaScript"
								,"1DE"	=>	"Customization JavaScript"
								,"1DF"	=>	"Customization JavaScript"
								,"1E0"	=>	"Customization JavaScript"
								,"1E1"	=>	"Customization JavaScript"
								,"1E2"	=>	"Customization JavaScript"
								,"1E3"	=>	"Customization JavaScript"
								,"1E4"	=>	"Customization JavaScript"
								,"1E5"	=>	"Customization JavaScript"
								,"1E6"	=>	"Customization JavaScript"
								,"1E7"	=>	"Customization JavaScript"
								,"1E8"	=>	"Customization JavaScript"
								,"1E9"	=>	"Customization JavaScript"
								,"1EA"	=>	"Customization JavaScript"
								,"1EB"	=>	"Customization JavaScript"
								,"1EC"	=>	"Customization JavaScript"
								,"1ED"	=>	"Customization JavaScript"
								,"1EE"	=>	"Customization JavaScript"
								,"1EF"	=>	"Customization JavaScript"
								,"1F0"	=>	"Customization JavaScript"
								,"1F1"	=>	"Customization JavaScript"
								,"1F2"	=>	"Customization JavaScript"
								,"1F3"	=>	"Customization JavaScript"
								,"1F4"	=>	"Customization JavaScript"
								,"1F5"	=>	"Customization JavaScript"
								,"1F6"	=>	"Customization JavaScript"
								,"1F7"	=>	"Customization JavaScript"
								,"1F8"	=>	"Customization JavaScript"
								,"1F9"	=>	"Customization JavaScript"
								,"1FA"	=>	"Customization JavaScript"
								,"1FB"	=>	"Customization JavaScript"
								,"1FC"	=>	"Customization JavaScript"
								,"1FD"	=>	"Customization JavaScript"
								,"1FE"	=>	"Customization JavaScript"
								,"1FF"	=>	"Customization JavaScript",
								*/
		),
	),
);
