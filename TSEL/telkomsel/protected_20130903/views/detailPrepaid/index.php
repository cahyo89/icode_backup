<?php
$this->breadcrumbs=array(
	'Prepaid Approval    '=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('setting-balance-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>">
<h1> Balance Management</h1>
</table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'setting-balance-grid',
	'dataProvider'=>$model,
	'columns'=>array(
		array(
			'name'=>'Transaction Date',
			'type'=>'raw',
			'value'=>'$data["first_update"] == "" ? "No Data":$data["first_update"]',
		),
		array(
			'name'=>'PO Detail',
			'type'=>'raw',
			'value'=>'@Controller::setDetailPO("PO")',
		),
		array(
			'name'=>'Net Amount',
			'type'=>'raw',
			'value'=>'$data["first_update"] == "" ? "No Data":"Rp. ".number_format(($data["bayar"]*50)/100)',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',
		    'buttons'=>array
		    (
		        'update' => array
		        (
		            'label'=>'Approve',
		            'imageUrl'=>Yii::app()->request->baseUrl.'/images/update.png',
		            'url'=>'Yii::app()->createUrl("detailPrepaid/view", array("id"=>Controller::setDetailPO(),"dateStart"=>$_GET[\'dateStart\'],"dateEnd"=>$_GET[\'dateEnd\'],"customer"=>$_GET[\'customer\']))',
					'visible'=>'$data["first_update"] == "" ? false : true',
		        ),
		    ),
			'deleteConfirmation'=>"js:'Are you sure you want to Reset this Balance Value??\\n\\nCustomer Name :' +
	        $(this).parents('tr').children(':eq(0)').text() +
	        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to Proceed, or \"Cancel\" to Abort this action.\\n\\n'",
		),
	),
)); ?>
