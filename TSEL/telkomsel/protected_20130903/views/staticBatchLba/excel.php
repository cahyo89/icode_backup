<?php 
if($model !==null){
	$dataType = $_GET['dataType'];
	$dateStart = $_GET['dateStart'];
	$dateEnd = $_GET['dateEnd'];
	$hourStart = $_GET['hourStart'];
	$hourEnd = $_GET['hourEnd'];
	$customer = $_GET['customer'];
	$campaign = $_GET['campaign'];
	$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
	$modelCampaign = CHtml::listData(LbaBatch::model()->findAll(),'id_batch','jobs_id');
	
	$success =0;
	$failed = 0;
	$queue = 0;
	$total = 0;
	$qr = 0;
	$sr = 0;
	$fr = 0;
	$model = $model->getData();
	if($dataType == 1)
		$sqlError = " and  tanggal >= DATE_FORMAT('".$dateStart." 00:00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateEnd." 23:59:59','%Y-%m-%d %H:%i:%s') ";
	else
		$sqlError = " and tanggal >= DATE_FORMAT('".$dateStart." ".$hourStart.":00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateStart." ".$hourEnd.":00:00','%Y-%m-%d %H:%i:%s') ";
			
	foreach($model as $dataM)
	{
		$tblJobs[$dataM['jobs_id']] = $dataM['jobs_id'];
		$success += $dataM['success'];
		$failed += $dataM['failed'];
		//$queue += $dataM->queue;
		$total += $dataM['totalSend'];
		//$qr += $dataM->qr;
		//$sr += $dataM->sr;
		//$fr += $dataM->fr;
	}
		if($total != 0 ){
		//$qr = round(($queue/$total)*100,2);    
		$sr = round(($success/$total)*100,2);  
		$fr = round(($failed/$total)*100,2);  
		}
		else{
			$qr = 0;    
			$sr = 0;  
			$fr = 0;  
		}
			
		$arrayError = array();
	if(isset($tblJobs)){
		foreach($tblJobs as $id=>$value)
		{
			$connection=Yii::app()->db;  
			$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = "lba_xl" AND table_name = "'.$value.'";')->queryScalar();
			if($count != 0){
				$sql = "select count(id) as total,error_code from `".$value."` where error_code is not null  ".$sqlError."  group by error_code ";
				$command=$connection->createCommand($sql)->queryAll();
				foreach($command as $row)
				{
					if($row['error_code'] != "")
						$arrayError[$row['error_code']] += $row['total'];
				} 
			}
		}
	}
			
	$x=0; 
	echo "<br>";
?>
	<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

	<h4>Customer : <?php echo $customer == "" ?  'All' :  $modeCustomer[$customer] ;  ?></h4>

	<h4>Campaign : <?php echo $campaign == "" ?  'All' :  $modelCampaign[$campaign] ;  ?></h4>
	<table border='1'>
		<tr>
			<td>Date</td>
			<td>Jobs ID</td>
			<td>Customer</td>
			<td>Location</td>
			<td>Topic</td>
			<td>Sms Text</td>
			<td>Total</td>
			<td>Success</td>
			<td>Failed</td>
			<td>Total Send</td>
			<td>Success Rate Send</td>
			<td>Failed Rate Send</td>
		</tr>
	<?php foreach($model as $error){  ?>
		<tr <?php echo ($x++)%2==0?"style='background-color:#CCC'" : "" ; ?> >
			<td><?php echo $error['sent_time'] ; ?></td>
			<td><?php echo $error['jobs_id'] ; ?></td>
			<td><?php echo $modeCustomer[$error['id_customer']]; ?></td>
			<td><?php echo Controller::getLocation($error["cell"]); ?></td>
			<td><?php echo $error['topik'] ; ?></td>
			<td><?php echo $error['sms_text'] ; ?></td>
			<td><?php echo $error['sms_send_limit'] ; ?></td>
			<td><?php echo $error['success'] ; ?></td>
			<td><?php echo $error['failed'] ; ?></td>
			<td><?php echo $error['totalSend'] ; ?></td>
			<td><?php echo $error['sr'] ; ?></td>
			<td><?php echo $error['fr'] ; ?></td>
		</tr>
		<?php } ?>
		

		<table width="100%">
			<tr>
				<td>
				<b>Error Code<b>
				</td>
				<td>
				<b>Description<b>
				</td>
				<td>
				<b>Total<b>
				</td>
			</tr>
			<?php foreach($arrayError as $id=>$value){ ?>
				<tr>
					<td>
						<?php echo $id; ?>
					</td>
					<td>
						<?php echo Yii::app()->params['cErrorSmpp'][$id]; ?>
					</td>
					<td>
						<?php echo $value; ?>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td>
				
				</td>
				<td>
				
				</td>
				<td>
					<?php echo $failed; ?>
				</td>
			</tr>
		</table>
		
	<table>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Grand Total</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Total</td>
			<td><?php echo $total; ?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Success</td>
			<td><?php echo $success; ?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Failed</td>
			<td><?php echo $failed; ?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Success Rate</td>
			<td><?php echo $sr; ?></td>
		</tr>
		<tr>
			
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Failed Rate</td>
			<td><?php echo $fr; ?></td>
		</tr>
	<table>
	
<?php } ?>
