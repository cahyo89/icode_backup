<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List PaketCampaign', 'url'=>array('index')),
	array('label'=>'Create PaketCampaign', 'url'=>array('create')),
	array('label'=>'View PaketCampaign', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PaketCampaign', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update PaketCampaign #<?php echo $model->id; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'index','img'=>'/images/list.png','id'=>'','conf'=>''),
				   array('label'=>'Create','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
				   array('label'=>'View','link'=>'view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
					));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>