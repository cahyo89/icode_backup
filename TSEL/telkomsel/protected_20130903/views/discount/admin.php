<?php
/* @var $this DiscountController */
/* @var $model Discount */

$this->breadcrumbs=array(
	'Discounts'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List Discount', 'url'=>array('index')),
	array('label'=>'Create Discount', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#discount-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Discounts</h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
					)); ?></div></td>
					</tr></table>
					
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'discount-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'range',
		'discount',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
