<?php
/* @var $this DiscountController */
/* @var $model Discount */

$this->breadcrumbs=array(
	'Discounts'=>array('index'),
	$model->id,
);

/*$this->menu=array(
	array('label'=>'List Discount', 'url'=>array('index')),
	array('label'=>'Create Discount', 'url'=>array('create')),
	array('label'=>'Update Discount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Discount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Discount', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>View Discounts #<?php echo $model->id; ?></h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'List','link'=>'admin','img'=>'/images/list.png','id'=>'','conf'=>''),
				   array('label'=>'Create','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
				   array('label'=>'Update','link'=>'update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
				   array('label'=>'delete','link'=>'delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->id)
					)); ?></div></td>
					</tr></table>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'range',
		'discount',
	),
)); 

?>
