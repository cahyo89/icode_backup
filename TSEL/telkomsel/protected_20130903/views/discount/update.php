<?php
/* @var $this DiscountController */
/* @var $model Discount */

$this->breadcrumbs=array(
	'Discounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List Discount', 'url'=>array('index')),
	array('label'=>'Create Discount', 'url'=>array('create')),
	array('label'=>'View Discount', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Discount', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Discount <?php echo $model->id; ?></h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'List','link'=>'admin','img'=>'/images/list.png','id'=>'','conf'=>''),
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
				   array('label'=>'View','link'=>'view','img'=>'/images/view.png','id'=>$model->id,'conf'=>'')
					)); ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>