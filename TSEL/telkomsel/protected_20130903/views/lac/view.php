<?php
$this->breadcrumbs=array(
	'Lacs'=>array('index'),
	$model->lac_name,
);

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Lac #<?php echo $model->lac_name; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'lac/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'Delete','link'=>'lac/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->lac_name),
	   array('label'=>'New','link'=>'lac/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'lac_name',
		'description',
		'lac_id',
		'host',
		'port',
		array(
		'label'=>'Probing',
		'name'=>'Probing.probing_name',
		),
	),
)); 
Controller::createInfo($model);
?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'lac/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'lac/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	    array('label'=>'Delete','link'=>'lac/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->lac_name)
	   
		));  ?>
</div>