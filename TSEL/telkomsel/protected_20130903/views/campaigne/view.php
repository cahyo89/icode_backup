<?php
$this->breadcrumbs=array(
	'Campaignes'=>array('index'),
	$model->jobs_id,
);

?>

<?php 
//  yang bisa diupdate
//  start periode
//  content expired
//  isi sms text
//  topic
//  token
//  gambar
//  url click

	function getDetailPacketCampaign($model){
	
		$arrayUrl = array(0=>"disabled",1=>"enable");
		$arrayLocation = array(0=>"disabled",1=>"enable");
		$arrayUserAgent = array(0=>"disabled",1=>"enable");
		$arrayKeyword = array(0=>"disabled",1=>"enable");
		
		$sql = "SELECT * FROM tbl_paket_campaign where id =".$model->id_paket_campaign." ";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count 		= count($row);
		$x = 0;
		while($x < $count)
		{
			
			$packetName 				= $row[$x]['nama'];
			$packetTotalPerToken		= $row[$x]['harga'];
			$packetPriority				= Yii::app()->params['priority'][$row[$x]['priority']];
			$packetChannelCategory		= Yii::app()->params['channelcatagory'][$row[$x]['channel_category']];
			
			$advertisingType 			= Yii::app()->params['http_intercept_ads_type'][$row[$x]['http_intercept_ads_type']];
			$delayAdvertisingStart		= $row[$x]['ads_start_time'];
			$delayAdvertisingTimeOut	= $row[$x]['ads_time_out'];
			
			$site						= $arrayUrl[$row[$x]['site']];
			$location					= $arrayLocation[$row[$x]['location']];
			$userAgent					= $arrayUserAgent[$row[$x]['user_agent']];
			$keyword					= $arrayKeyword[$row[$x]['keyword']];
			
			$size1						= $row[$x]['ukuran_gambar1'];
			$size2						= $row[$x]['ukuran_gambar2'];
			$size3						= $row[$x]['ukuran_gambar3'];
			$x++;
		}
		
		if($model->channel_category == 65280){
			$result = "Packet Name : ".$packetName."| ";
			$result .= "Packet type : ".$packetChannelCategory."| ";
			$result .= "total / token : ".$packetTotalPerToken."| ";
			$result .= "priority : ".$packetPriority."| ";	
			$result .= "advertising type : ".$delayAdvertisingStart."| ";
			$result .= "delay start : ".$advertisingType."| ";
			$result .= "delay time out : ".$delayAdvertisingTimeOut."| ";
			$result .= "site : ".$site."| ";
			$result .= "location : ".$location."| ";
			$result .= "userAgent : ".$userAgent."| ";
			$result .= "keyword : ".$keyword."| ";
			$result .= "size1 : ".$size1."| ";
			$result .= "size2 : ".$size2."| ";
			$result .= "size3 : ".$size3."   ";
			return $result;
		}
		else{
			$result = "Packet Name : ".$packetName."| ";
			$result .= "Packet type : ".$packetChannelCategory."| ";
			$result .= "total / token : ".$packetTotalPerToken."| ";
			$result .= "priority : ".$packetPriority." ";	
			return $result;
			
		}
	}

	function getTotalByIdBatch($model){
		$rate = 0;
		$sql  = "SELECT harga FROM tbl_paket_campaign where id = ".$model->id_paket_campaign." ";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count 		= count($row);
		$x = 0;
		while($x < $count)
		{
			$rate = $row[$x]['harga'];
			$x++;
		}
		return $model->total_number/$rate;
	}
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>View Campaign #<?php echo $model->jobs_id; ?></h1></td>
<td><div class="operatorRight"><?php  
	   if($model->status_batch == 0){	
		   Controller::createMenu(array(
		   array('label'=>'Update','link'=>'Campaigne/update','img'=>'/images/update.png','id'=>$model->id_batch,'conf'=>''),
		   array('label'=>'Delete','link'=>'Campaigne/delete','img'=>'/images/delete.png','id'=>$model->id_batch,'conf'=>$model->jobs_id),
		   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
			)); 
	   }
	   else{
		   Controller::createMenu(array(
		   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
			));	
	   }	   
		
		?></div></td>
					</tr></table>

<?php 					
$columns[] = array(
				'label'=>'Campaign Id',
				'name'=>'jobs_id'
				);	
$columns[] = array(
				'label'=>'Topic',
				'name'=>'topik'
				);
$columns[] = array(
				'label'=>'Start Periode',
				'name'=>'start_periode'
				);
$columns[] = array(
				'label'=>'Content Expired',
				'name'=>'content_expired'
				);
$columns[] = array(
				'label'=>'Status Batch',
				'value'=>Yii::app()->params['status_batch'][$model->status_batch]
				);
$columns[] = array(
				'label'=>'Paket Campaign',
				'value'=>''.getDetailPacketCampaign($model).''
				);				
				
$columns[] = array(
				'label'=>'Total Requested',
				'name'=>'total_number'
				);
$columns[] = array(
				'label'=>'Campaign Type',
				'value'=>'Normal'
				);
					
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$columns,
	
)); 
Controller::createInfo($model);
?>
<div class="operatorLeft">
<?php  
		// cek menu
		if($model->status_batch == 0){
			Controller::createMenu(array(
			array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>''),
			array('label'=>'Update','link'=>'Campaigne/update','img'=>'/images/update.png','id'=>$model->id_batch,'conf'=>''),
		    array('label'=>'Delete','link'=>'Campaigne/delete','img'=>'/images/delete.png','id'=>$model->id_batch,'conf'=>$model->jobs_id)
		 	));
		}
		else{
			Controller::createMenu(array(
			array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		 	));
		}
		?>
</div>
