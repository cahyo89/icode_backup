<div class="form well">
	
	<?php 
		Yii::app()->clientScript->registerScript("test","
			$(document).ready(function() {
			  // Handler for .ready() called.	  
			  // $('body').removeAttr('id').attr('onload', 'haha();');
			});
			
			$('#generalSetting').click( function()
			   {
				 $('#campaigne-form').formwizard('show','Step1');
				 $('#headtitle').text('Create Campaign : General Setting');
			   }
			);
			
			$('#creative').click( function()
			   {
				 $('#campaigne-form').formwizard('show','Step2');
				 $('#headtitle').text('Create Campaign : Creative');
			   }
			);
			
			$('#targeting').click( function()
			   {
				 $('#campaigne-form').formwizard('show','Step3');
				 $('#headtitle').text('Create Campaign');
			   }
			);
			
		");
	?>
	
	<?php			
		$this->widget('ext.sformwizard.SFormWizard',array(
			'selector'=>"#campaigne-form",
			'disableUIStyles' => "true",
			'validationEnabled' => "true",
		));
	?>
	
	 <?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'campaigne-form',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="step" id="Step1">
		<h2 id="headtitle">Create Campaign : General Setting</h2>
		<div class="well-small">
		 <p class="note">Fields with <span class="required">*</span> are required.</p>
			<?php echo $form->labelEx($model,'Customer Name')."<b><font color='red'>*</font></b>" ?>
			<?php
				if(Yii::app()->user->getUserMode()==0)
				{
					$customer = Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
				}
				else if(Yii::app()->user->getUserMode() == 1)
				{
					$customer = Customer::model()->findAll('id_user = '.Yii::app()->user->id.' and approved = 1 and flag_deleted is null or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
				}
				else
				{
					$custHead = Customer::model()->findAll('id_customer = '.Yii::app()->user->getIdCustomer());
					if($custHead)
					$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					else
					{	
						$user = User::model()->findByPk(Yii::app()->user->id);
						$custHead = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and id_customer = '.$user->id_customer);
						$customer = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					}
				}
			?>
			<?php echo $form->dropDownList($model,'id_customer',CHtml::listData($customer, 'id_customer', 'nama'),array('class'=>'required','onchange'=>'{onChangeCustomer();}','empty'=>'--Please Select One--'));?>
			<?php echo $form->error($model,'id_customer'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model,'Campaign Id'); ?>
			<div class="read">
				<?php echo Chtml::activeTextField($model,'jobs_id',array('class' => 'input-xlarge' ,'size'=>300,'maxlength'=>2000,'readonly'=>true)); ?>
			</div>
			<?php echo $form->error($model,'jobs_id'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'Channel Category')."<b><font color='red'>*</font></b>"; ?>
			<?php 
				echo $form->dropDownList($model,'channel_category',array(""=>"-- Please Select One --"),array('class' => 'required' ,'onchange'=>'{onChangeChannelCategory();}')); 
			?>
			<?php echo $form->error($model,'channel_category'); ?>
		</div>
		
		<div id="customer_ani">
			<div class="well-small" id="customer_ani">
				<?php echo $form->labelEx($model,'Customer Masking')."<b><font color='red'>*</font></b>"; ?>
				<?php 
					echo $form->dropDownList($model,'customer_ani',array(""=>"-- Please Select One --"),array('class' => 'required')); 
				?>
				<?php echo $form->error($model,'customer_ani'); ?>
			</div>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'Topic')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textField($model,'topik',array('class' => 'required' ,'size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'topik'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->label($model,'Start Periode')."<b><font color='red'>*</font></b>"; ?>
			<?php $this->widget('application.extensions.timepicker.timepicker', array(
						'model'=>$model,
						'name'=>'start_periode',
					));
			?>
		</div>
		<div class="well-small">
			<?php echo $form->label($model,'Content Expired')."<b><font color='red'>*</font></b>"; ?>
			<?php $this->widget('application.extensions.timepicker.timepicker', array(
						'model'=>$model,
						'name'=>'content_expired',
					));
			?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model,'Subscribers'); ?>
			<?php echo $form->dropDownList($model,'unique_subscriber_per_batch',Yii::app()->params['subscriber'],array('class' => 'required' ,'onchange'=>'{onChangeSubscriber();}')); ?>
			<?php echo $form->error($model,'unique_subscriber_per_batch'); ?>
		</div>

		<div id="timesubscribe" class="well-small hidden">
			<?php echo $form->label($model,'Subscriber Time Out (In Milisecond) </br> * 0 = subscriber will get ads only once per campaign during process'); ?>
			<?php echo Chtml::activeTextField($model,'time_out_unique_subscriber_per_batch',array('class' => 'input-mini')); ?>
			<?php echo $form->error($model,'time_out_unique_subscriber_per_batch'); ?>
		</div>

		<div class="well-small hidden">
			<?php echo $form->labelEx($model,'Advertising Sending Type'); ?>
			<?php echo $form->dropDownList($model,'ast',Yii::app()->params['adssendingtype'],array('class' => 'required' ,'onchange'=>'{onChangeAdvertisingSendingType();}')); ?>
			<?php echo $form->error($model,'ast'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->label($model,'Campaign Type'); ?>
			<?php echo $form->dropDownList($model,'batch_type',array('0'=>'Normal','1'=>'Black List','2'=>'White List'),
			array('onchange'=>'{onChangeCampaigneType();}','class' => 'required')
			); ?>
			<?php echo $form->error($model,'batch_type'); ?>
		</div>
		<div id="filelist" class="well-small hidden">
			<?php echo $form->labelEx($model, 'File Black / White List'); ?>
			<?php echo $form->fileField($model, 'blackWhiteListFile',array('disabled'=>true)); ?>
			<?php echo $form->error($model, 'blackWhiteListFile'); ?>
		</div>
		
		<div class="well-small hidden">
			<?php echo $form->label($model,'Profile'); ?>
			<?php echo $form->dropDownList($model,'profile',Yii::app()->params['profile']);?>
			<?php echo $form->error($model,'profile'); ?>
		</div>
		<input name="Campaigne[message_type]" value="1" id="Campaigne_message_type" style="display:none" type="text">
		<input name="Campaigne[message_bit]" value="7" id="Campaigne_message_bit" style="display:none" type="text">
		<input name="Campaigne[binary_data]" value="0" id="Campaigne_binary_data" style="display:none" type="text">
		<input name="Campaigne[promotion_media]" value="0" id="Campaigne_promotion_media" style="display:none" type="text">
		<input name="Campaigne[sex_category]" value="0" id="Campaigne_sex_category" style="display:none" type="text">
		<input name="Campaigne[age_category]" value="0" id="Campaigne_age_category" style="display:none" type="text">
		<input name="Campaigne[status_category]" value="0" id="Campaigne_status_category" style="display:none" type="text">
		<input name="Campaigne[filter_prefix]" value="0" id="Campaigne_filter_prefix" style="display:none" type="text">
		<input name="Campaigne[education_category]" value="0" id="Campaigne_education_category" style="display:none" type="text">
		<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
		<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
		<input name="Campaigne[quota]" value="0" id="Campaigne_quota" style="display:none" type="text">
		<input name="Campaigne[vip]" value="1" id="Campaigne_vip" style="display:none" type="text">
		<input name="Campaigne[increase_counter]" value="1" id="Campaigne_increase_counter" style="display:none" type="text">
		<input name="Campaigne[flag_hour]" value="0" id="Campaigne_flag_hour" style="display:none" type="text">
		<input name="Campaigne[multimedia_type]" value="0" id="Campaigne_multimedia_type" style="display:none" type="text">
		<input name="Campaigne[flag_file]" value="0" id="Campaigne_flag_file" style="display:none" type="text">
	</div>
	<div class="step" id="Step2">
		<h2 id="headtitle">Create Campaign : Creative</h2>
		<div class="well-small">
			<?php echo $form->labelEx($model,'Packet')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'id_paket_campaign',array(""=>"--Please Choose One--"),array('class'=>'required','onchange'=>'{onChangePaket();}'));?>
			<?php echo $form->error($model,'id_paket_campaign'); ?>
		</div>
		<div class="well-small" id="paket"></div>
		
		<div id="mms_file">
		<div class="well-small">
			<?php echo $form->labelEx($model, 'MMS File')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameuploadMMS[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameuploadMMS'); ?>
		</div>
		</div>
		
		<div id="mobads">
		<div class="well-small">
			<?php echo $form->labelEx($model,'Redirect Link')."<b><font color='red'>*</font></b>"; ?>
			<?php echo Chtml::activeTextField($model,'flying_click_link',array('maxlength'=>200)); ?>
			<?php echo $form->error($model,'flying_click_link'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 1')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
		<div class="well-small hidden">
			<?php echo $form->labelEx($model,'Inner HTML 1')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textArea($model,'inner_html_1',array('class' => 'input-xxlarge','cols'=>50,'rows'=>10)); ?>
			<?php echo $form->error($model,'inner_html_1'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 2')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
		<div class="well-small hidden">
			<?php echo $form->labelEx($model,'Inner HTML 2')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textArea($model,'inner_html_2',array('class' => 'input-xxlarge','cols'=>50,'rows'=>10)); ?>
			<?php echo $form->error($model,'inner_html_2'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 3')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
		<div class="well-small hidden">
			<?php echo $form->labelEx($model,'Inner HTML 3')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textArea($model,'inner_html_3',array('class' => 'input-xxlarge','cols'=>50,'rows'=>10)); ?>
			<?php echo $form->error($model,'inner_html_3'); ?>
		</div>
		</div>
		
		<div id="time_window">
			<div class="well-small">
				<?php echo $form->labelEx($model,'Use Auto Pause Resume'); ?>
				<?php 
					echo $form->dropDownList($model,'flagAutoPauseResume',array(0=>'No',1=>'Yes'),array('onchange'=>'{onChangeAutoPauseResume();}')); 
				?>
				<?php echo $form->error($model,'flagAutoPauseResume'); ?>
			</div>
			<div class="well-small">
				<?php echo $form->labelEx($model,'Time Window')."<b><font color='red'>*</font></b>"; ?>
				<?php 
					echo $form->dropDownList($model,'time_gate',array(''=>'--Normal--'),array('multiple'=>'true')).""; 
				?>
				<?php echo $form->error($model,'time_gate'); ?>
			</div>
		</div>
		
		<div class="well-small" id="sms_text">
			<?php echo $form->labelEx($model,'Campaign SMS Text')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textArea($model,'sms_text',array('cols'=>50,'rows'=>10,'onkeyup'=>'{onEventSMSText();}')); ?>
			<?php echo $form->error($model,'sms_text'); ?>
		</div>
		<div class="well-small" id="length_text">
			Length SMS <input name="Campaigne[length_text]" value="0" size="3" id="Campaigne_length_text" readonly="true"  type="text">
		</div>
		
		
	</div>
	<div class="step" id="Step3">
		<h2 id="headtitle">Create Campaign : Targeting</h2>
		<div class="well-small" id="url">
			<p class="note">Fields with <span class="required">*</span> are required.</p>
			<?php echo $form->labelEx($model,'URL Filter')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'id_filter_url_intercept',CHtml::listData(FilterUrlIntercept::model()->findAll(), 'id', 'url_name'),array('onchange'=>'{onChangeUrlFilter();}'));?>			
			<?php echo $form->error($model,'id_filter_url_intercept'); ?>
		</div>
		<div class="well-small" id="filter"></div>
		<div class="well-small" id="user_agent">
			<?php echo $form->labelEx($model,'User Agent')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'id_user_agent',CHtml::listData(ParamUserAgent::model()->findAll(), 'id', 'name'));?>			
			<?php echo $form->error($model,'id_user_agent'); ?>
		</div>
		
		<div class="well-small" id="keyword">
			<?php echo $form->labelEx($model,'Keyword')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'id_keyword',CHtml::listData(ParamKeyword::model()->findAll(), 'id', 'name'));?>			
			<?php echo $form->error($model,'id_keyword'); ?>
		</div>
		
		<div id="file_broadcast">
			<div class="well-small">
				<?php echo $form->labelEx($model, 'Broadcast File')."<b><font color='red'>*</font></b>"; ?>
				<?php echo $form->fileField($model, 'filenameuploadBroadcast[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
				<?php echo $form->error($model, 'filenameuploadBroadcast'); ?>
			</div>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'total_number')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->textField($model,'total_number',array('class' => 'required' ,'size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'total_number'); ?>
		</div>
		
		<div class="well-small" id="cell">
			<?php echo $form->labelEx($model,'cell')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'cell',CHtml::listData(BtsGroup::model()->findAll("type = 1"), 'id', 'bts_name'));?>			
			<?php echo $form->error($model,'cell'); ?>
		</div>
		
		<!--
		<div class="well-small" id="loc">
			<input type="hidden" value="0" id="theValue" />
			<p><a href="javascript:;" onclick="addElement();">Add Area Group <b><font color='red'>*</font></b></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="removeAllElement();">Remove All Area Group</a></p>
			<div id="myDiv"> </div>
		</div>
		-->
		
	</div>
	<div id="data"></div>
	<div class="well-small">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-dangger')); ?>
		<?php echo CHtml::resetButton('Back',array('class'=>'btn btn-dangger')); ?>
	</div>
	<?php $this->endWidget(); ?>
	
</div><!-- form -->

<script type="text/javascript">
	
	
	var loc = 0;
	onChangeCustomer();
	onChangeChannelCategory();
	onEventSMSText();
	
	
	function onUploadBrowse(form, action_url, div_id)
	{
		return false;
		var iframe = document.createElement("iframe");
		iframe.setAttribute("id", "upload_iframe");
		iframe.setAttribute("name", "upload_iframe");
		iframe.setAttribute("width", "0");
		iframe.setAttribute("height", "0");
		iframe.setAttribute("border", "0");
		iframe.setAttribute("style", "width: 0; height: 0; border: none;");
		// Add to document...
		form.parentNode.appendChild(iframe);
		window.frames['upload_iframe'].name = "upload_iframe";
	 
		iframeId = document.getElementById("upload_iframe");
		
		// Add event...
		var eventHandler = function () {
	 
				if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
				else iframeId.removeEventListener("load", eventHandler, false);
				
				// Message from server...
				if (iframeId.contentDocument) {
					content = iframeId.contentDocument.body.innerHTML;
				} else if (iframeId.contentWindow) {
					content = iframeId.contentWindow.document.body.innerHTML;
				} else if (iframeId.document) {
					content = iframeId.document.body.innerHTML;
				}
	 
				document.getElementById(div_id).innerHTML = content;
	 				
			}
	 
		if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
		if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
	 
		// Set properties of form...
		form.setAttribute("target", "upload_iframe");
		form.setAttribute("action", action_url);
		form.setAttribute("method", "post");
		form.setAttribute("enctype", "multipart/form-data");
		form.setAttribute("encoding", "multipart/form-data");
	 
		// Submit the form...
		form.submit();
	 
		document.getElementById(div_id).innerHTML = "Uploading...";
		return false;
	}
	
	function onChangePaket()
	{
		
		if(document.getElementById('Campaign_id_paket_campaign') != "" ){
		var ukuran = Array('<?php echo join("','",Yii::app()->params['imagesize']); /*print("Array(\"".Yii::app()->params['imagesize'][0]."\",\"".Yii::app()->params['imagesize'][1]."\",\"".Yii::app()->params['imagesize'][2]."\");");*/ ?>');
		var adspos = Array('<?php echo join("','",Yii::app()->params['expandtype']);?>');
		<?php 
			echo CHtml::ajax(array(
			// the controller/function to call
			'url'=>CController::createUrl('onChangePaket'),

			// Data to be passed to the ajax function
			// Note that the ' should be escaped with \
			// The field id should be prefixed with the model name eg Vehicle_field_name
		   // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
			'data'=>array('http_paket'=>'js:$(\'#Campaigne_id_paket_campaign\').val()'),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>'function(data)
			{
				if(data.pakethttp[\'channel_category\'] ==65280){
						$("#paket").html("<table border=\'1\'>"
						+"<tr>"
						+"	<th bgcolor=\'skyblue\'>Packet Name</th>"
						+"	<th bgcolor=\'skyblue\'>Tipe</th>"
						+"	<th bgcolor=\'skyblue\'>Picture Size 1</th>"
						+"	<th bgcolor=\'skyblue\'>Picture Size 2</th>"
						+"	<th bgcolor=\'skyblue\'>Picture Size 3</th>"
						+"	<th bgcolor=\'skyblue\'>Total per Token</th>"
						+"	<th bgcolor=\'skyblue\'>Priority Campaign</th>"
						+"	<th bgcolor=\'skyblue\'>Ads Position</th>"
						+"</tr>"
						+"<tr>"
						+"	<td align=\'center\'>"+data.pakethttp[\'nama\']+"</td>"
						+"	<td align=\'center\'>"+data.tipepaket+"</td>"
						+"	<td align=\'center\' id=\'gambar1\'>"+data.pakethttp[\'ukuran_gambar1\']+"</td>"
						+"	<td align=\'center\' id=\'gambar2\'>"+data.pakethttp[\'ukuran_gambar2\']+"</td>"
						+"	<td align=\'center\' id=\'gambar3\'>"+data.pakethttp[\'ukuran_gambar3\']+"</td>"
						+"	<td align=\'center\'>"+data.pakethttp[\'harga\']+"</td>"
						+"	<td align=\'center\'>"+data.priority+"</td>"
						+"	<td align=\'center\'>"+adspos[data.pakethttp[\'http_intercept_ads_type\']]+"</td>"
						+"</tr>"
						+"</table>");
				}
				else{
					$("#paket").html("<table border=\'1\'>"
						+"<tr>"
						+"	<th bgcolor=\'skyblue\'>Packet Name</th>"
						+"	<th bgcolor=\'skyblue\'>Total per Token</th>"
						+"	<th bgcolor=\'skyblue\'>Priority Campaign</th>"
						+"</tr>"
						+"<tr>"
						+"	<td align=\'center\'>"+data.pakethttp[\'nama\']+"</td>"
						+"	<td align=\'center\'>"+data.pakethttp[\'harga\']+"</td>"
						+"	<td align=\'center\'>"+data.priority+"</td>"
						+"</tr>"
						+"</table>");
				}
				if(data.pakethttp[\'site\']==1)
				{
					document.getElementById(\'Campaigne_id_filter_url_intercept\').disabled = false;
					$(\'#Campaigne_id_filter_url_intercept\').removeAttr(\'disabled\');
					$(\'#url\').removeClass(\'hidden\');
				}
				else
				{
					document.getElementById(\'Campaigne_id_filter_url_intercept\').disabled = true;
					$(\'#Campaigne_id_filter_url_intercept\').attr(\'hidden\');
					$(\'#url\').addClass(\'hidden\');
				}
				
				if(data.pakethttp[\'location\'] == 1){
					document.getElementById(\'Campaigne_cell\').disabled = false;
					$(\'#Campaigne_cell\').removeAttr(\'disabled\');
					$(\'#cell\').removeClass(\'hidden\');
				
				}
				else{
					document.getElementById(\'Campaigne_cell\').disabled = true;
					$(\'#Campaigne_cell\').attr(\'hidden\');
					$(\'#cell\').addClass(\'hidden\');
				}
				
				if(data.pakethttp[\'keyword\'] == 1){
					document.getElementById(\'Campaigne_id_keyword\').disabled = false;
					$(\'#Campaigne_id_keyword\').removeAttr(\'disabled\');
					$(\'#keyword\').removeClass(\'hidden\');
				}
				else{
					document.getElementById(\'Campaigne_id_keyword\').disabled = true;
					$(\'#Campaigne_id_keyword\').attr(\'hidden\');
					$(\'#keyword\').addClass(\'hidden\');
				}
				
				if(data.pakethttp[\'user_agent\'] == 1){
					document.getElementById(\'Campaigne_id_user_agent\').disabled = false;
					$(\'#Campaigne_id_user_agent\').removeAttr(\'disabled\');
					$(\'#user_agent\').removeClass(\'hidden\');
				
				}
				else{
					document.getElementById(\'Campaigne_id_user_agent\').disabled = true;
					$(\'#Campaigne_id_user_agent\').attr(\'hidden\');
					$(\'#user_agent\').addClass(\'hidden\');
				}
			}
			',
		))?>;
				
		}
		return false;
	}
	
	function onChangeUrlFilter()
	{
		<?php 
			/*
			echo CHtml::ajax(array(
			'url'=>CController::createUrl('onChangeUrlFilter'), 
			'data'=>array('url_filter'=>'js:$(\'#Campaigne_id_filter_url_intercept\').val()'),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>'function(data)
			{
				$("#filter").html("<b>URL Filter : "+data.filter+"</b>");
			}
			',
		))
		*/?>
				
		return false;
	}
	function onChangeSubscriber()
	{
		if(document.getElementById('Campaigne_unique_subscriber_per_batch').value == 3){
			document.getElementById('Campaigne_time_out_unique_subscriber_per_batch').disabled = false;
			$('#Campaigne_time_out_unique_subscriber_per_batch').removeAttr("disabled");
			$('#timesubscribe').removeClass("hidden");
		}
		else{
			document.getElementById('Campaigne_time_out_unique_subscriber_per_batch').disabled = true;
			$('#Campaigne_time_out_unique_subscriber_per_batch').attr("hidden");
			$('#timesubscribe').addClass("hidden");
		}
					
		    return false;

	}

	function onChangeGlobalWhiteList(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeGlobalWhiteList'),
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('globalWhiteList'=>'js:$(\'#Campaigne_globalWhiteList\').val()',//-->is this correct??
					// To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_batch_type").html(data.batch_type);
					}
	                 ',
	                ))?>;
					
	        return false;
	}
	
	
	function onChangeAdvertisingSendingType(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeAdvertisingSendingType'),
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('ast'=>'js:$(\'#Campaigne_ast\').val()',//-->is this correct??
					// To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_profile").html(data.profile);
						$("#Campaigne_batch_type").html(data.batch_type);
						$("#Campaigne_globalWhiteList").html(data.globalWhiteList);
	                 }
	                 ',
	                ))?>;
					
	        return false;
	}
	
	function onChangeCampaigneType(){
		if(document.getElementById('Campaigne_batch_type').value == 1 || document.getElementById('Campaigne_batch_type').value == 2){
						document.getElementById('Campaigne_blackWhiteListFile').disabled = false;
						$('#Campaigne_blackWhiteListFile').removeAttr("disabled");
						$('#Campaigne_listEventCapture').removeAttr("disabled");
						$('#filelist').removeClass("hidden");
					}
					else{
						document.getElementById('Campaigne_blackWhiteListFile').disabled = true;
						$('#Campaigne_blackWhiteListFile').attr("hidden");
						$('#Campaigne_listEventCapture').attr("hidden");
						$('#filelist').addClass("hidden");
					}
					
		    return false;
	}
	
	
	function onEventSMSText(){
		var smsText =  document.getElementById('Campaigne_sms_text').value;
		document.getElementById('Campaigne_length_text').value = smsText.length;
	}
	
	
	function onChangeChannelCategory(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeChannelCategory'),
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_customer_ani").html(data.customer_ani);
						$("#Campaigne_id_paket_campaign").html(data.id_paket_campaign);
	                 }
	                 ',
	                ))?>;
					if(document.getElementById('Campaigne_channel_category').value != "" && document.getElementById('Campaigne_channel_category').value != "65280"){
						document.getElementById('sms_text').style.display="block";
						document.getElementById('length_text').style.display="block";
						document.getElementById('mobads').style.display="none";
						document.getElementById('file_broadcast').style.display="block";
						document.getElementById('time_window').style.display="none";
						document.getElementById('customer_ani').style.display = "block";
					}
					else{
						document.getElementById('sms_text').style.display="none";
						document.getElementById('length_text').style.display="none";	
						document.getElementById('mobads').style.display="block";
						document.getElementById('file_broadcast').style.display="none";
						document.getElementById('time_window').style.display="block";
						document.getElementById('customer_ani').style.display = "none";
					}
						
						if(document.getElementById('Campaigne_channel_category').value == 5){
							document.getElementById('mms_file').style.display="block";
						}
						else{
							document.getElementById('mms_file').style.display="none";
						}
	        return false;
	}
	
	
	function onChangeCustomer(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeCustomer'),
	 
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
					'flagAutoPauseResume'=>'js:$(\'#Campaigne_flagAutoPauseResume\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_jobs_id").val(data.jobs_id);
						$("#Campaigne_customer_ani").html(data.customer_ani);
						$("#Campaigne_id_ruletemplate").html(data.id_ruletemplate);
						$("#Campaigne_campaigneReference").html(data.campaigneReference);
						$("#Campaigne_channel_category").html(data.channel_category);
						$("#Campaigne_id_paket_campaign").html(data._id_paket_campaign);
					 }
	                 ',
	                ))?>;
					
	        return false;
	}
	
	function onChangeAutoPauseResume(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeAutoPauseResume'),
	 
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
				    'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',
					'flagAutoPauseResume'=>'js:$(\'#Campaigne_flagAutoPauseResume\').val()',//-->is this correct??
					// To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_time_gate").html(data.id_ruletemplate);
	                 }
	                 ',
	                ))?>;
					
	        return false;
	}
	
	function addElement() {
		  var ni = document.getElementById('myDiv');
		  var numi = document.getElementById('theValue');
		  var num = (document.getElementById('theValue').value -1)+ 2;
		  //numi.value = num;
		  numi.value = 100;
		  var newdiv = document.createElement('div');
		  //var divIdName = 'my'+num+'Div';
		  var divIdName = '100';
		  newdiv.setAttribute('id',divIdName);
		  //newdiv.innerHTML = 'Element Number '+num+' has been added! <a href=\'#\' onclick=\'removeElement('+divIdName+')\'>Remove the div "'+divIdName+'"</a>';

		  var data = "Cell Group Area ";
		  var data1 = "";
		  data =  data+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name="Campaigne[cell][]" id="Campaigne_cell">';

		  <?php 
			$sql = "SELECT * FROM tbl_bts where type = 1 AND (flag_deleted is null or flag_deleted not in (1)) order by bts_name asc";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			$count = count($row);
		    $x = 0;
			while($x < $count){
		  ?>
			data1 =  data1+'<option value="<?php echo $row[$x]['id']; ?>"><?php  echo $row[$x]['bts_name'];?></option>';
		  <?php
				$x++;
			}
		  ?>
		  
		  if(loc == 1)
		  data =  data+data1;
		  else
		  data = data+'<option value="-1">All</option>';
		  data = data+'</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Total Number :<input name="Campaigne[total_number][]" id="Campaigne_total_number" value="100" type="text" onkeyup="checkTotalNumberValue(this);" /><a href=\"#\" onclick=\'removeElement('+divIdName+')\'>Remove</a>';
		  
		  newdiv.innerHTML = data;
		  
		  ni.appendChild(newdiv);
	}

	function removeElement(divNum) {
		var d = document.getElementById('myDiv');
		var olddiv = document.getElementById('100');
		d.removeChild(olddiv);
	}
	
	function removeAllElement(divNum) {
		for(var x = 0 ; x < 1000 ;x++){
		var d = document.getElementById('myDiv');
		var olddiv = document.getElementById('100');
			d.removeChild(olddiv);
		}
	}
	
	function onClickEventCapture(){
		var i = document.getElementById('Campaigne_flagEventCapture').checked;
		
		if(i == true){
		   document.getElementById('Campaigne_eventCaptureName').disabled = false;
		}		
		else{
		   document.getElementById('Campaigne_eventCaptureName').disabled = true;
		}
		
	}
	
	function checkTotalNumberValue(field){
		
			var re = /^[0-9-'.'-',']*$/;
			if (!re.test(field.value)) {
				field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
			}
		
	}
	//automatic paket detail pertama
	onChangePaket();
	onChangeUrlFilter();
</script>


<?php  
  $baseUrl = Yii::app()->baseUrl; 
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile($baseUrl.'/assets/js/bootstrap-datetimepicker.js');
  $cs->registerCssFile($baseUrl.'/assets/css/datetimepicker.css');
?>

