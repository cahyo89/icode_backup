<?php
/* @var $this ParamKeywordController */
/* @var $data ParamKeyword */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('param_name')); ?>:</b>
	<?php echo CHtml::encode($data->param_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	*/ ?>

</div>