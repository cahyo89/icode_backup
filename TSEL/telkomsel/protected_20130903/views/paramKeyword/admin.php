<?php
/* @var $this ParamKeywordController */
/* @var $model ParamKeyword */

$this->breadcrumbs=array(
	'Param Keywords'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Param Keyword', 'url'=>array('index')),
	array('label'=>'Create Param Keyword', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#param-keyword-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Param Keywords</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'param-keyword-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'param_name',
		'first_update',
		'first_ip',
		'first_user',
		'last_update',
		'last_ip',
		'last_user',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
