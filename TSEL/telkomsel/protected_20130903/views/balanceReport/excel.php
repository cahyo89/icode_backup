<?php
if($model !==null){
	$model1 = $model->getData();
	echo "<br>";
	$x = 0;
	$dataNow = 0
?>
<h4>Report Balance </h4>
	<table border='1'>
		<tr>
			<td>POID</td>
			<td>BulkPOID</td>
			<td>PO Created Date</td>
			<td>PO Approve Date</td>
			<td>PO Approve By</td>
			<td>PO Release Date</td>
			<td>PO Release By</td>
			<td>Media Seller</td>
			<td>Client</td>
			<td>Token</td>
			<td>Price per Token</td>
			<td>Price</td>
			<td>Discount</td>
			<td>Net Price</td>
			<td>Sharing Rev Axis (50%)</td>
			<td>Sharing Rev ICode (50%)</td>
			<td>Token Status</td>
		</tr>
	<?php foreach($model1 as $data){  ?>
		<tr <?php echo ($x++)%2==0?"style='background-color:#CCC'" : "" ; ?> >
			<td><?php echo $data["idpo"]; ?></td>
			<td><?php echo $data["id_bulk"]; ?></td>
			<td><?php echo $data["create_date"]; ?></td>
			<td><?php echo $data["first_update"]; ?></td>
			<td><?php echo $data["first_user"]; ?></td>
			<td><?php echo $data["last_update"]; ?></td>
			<td><?php echo $data["last_user"]; ?> </td>
			<td><?php echo @(empty($data["parent_id"])) ? Controller::getCustomer($data["id_customer"]) : Controller::getCustomer($data["parent_id"]); ?></td>
			<td><?php echo @(empty($data["parent_id"])) ? "" : Controller::getCustomer($data["parent_id"]);?></td>
			<td><?php echo $data["prepaid_value"]; ?></td>
			<td><?php echo @"Rp. ".number_format($data["perharga"]); ?></td>
			<td><?php echo @"Rp. ".number_format($data["harga"]); ?></td>
			<td><?php echo @$data["disc"]; ?></td>
			<td><?php echo @"Rp. ".number_format($data["bayar"]); ?></td>
			<td><?php echo @"Rp. ".number_format($data["RevAxis"]); ?></td>
			<td><?php echo @"Rp. ".number_format($data["RevICode"]); ?></td>
			<td><?php echo @Controller::getTokenStatus($data["user"]); ?></td>
		</tr>
		<?php } ?>
	</table>
	
<?php } ?>