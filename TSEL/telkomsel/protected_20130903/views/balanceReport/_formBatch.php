<div class="well form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'GET',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<?php echo $form->errorSummary($model); ?>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'dateStart'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateStart', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			//'yearRange'=>'1920:2012',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'dateEnd'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateEnd', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			//'yearRange'=>'1920:2012',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>
	
	<div class="well-small submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->