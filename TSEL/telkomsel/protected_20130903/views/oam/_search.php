<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	
	<div class="row">
		<?php echo $form->label($model,'start_periode'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'start_periode',
        'name'=>$model->start_periode,    // This is how it works for me.
        'value'=>$model->start_periode,
        'options'=>array('dateFormat'=>'yy-mm-dd', 
                        'altFormat'=>'yy-mm-dd', 
                        'changeMonth'=>'true', 
                        'changeYear'=>'true', 
                        'yearRange'=>'1920:2012', 
                        'showOn'=>'both',
                       // 'buttonText'=>'...',
						'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
						'buttonImageOnly' => true,
						),
        'htmlOptions'=>array('size'=>'15')
   )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content_expired'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'content_expired',
        'name'=>$model->content_expired,    // This is how it works for me.
        'value'=>$model->content_expired,
        'options'=>array('dateFormat'=>'yy-mm-dd', 
                        'altFormat'=>'yy-mm-dd', 
                        'changeMonth'=>'true', 
                        'changeYear'=>'true', 
                        'yearRange'=>'1920:2012', 
                        'showOn'=>'both',
                       // 'buttonText'=>'...',
						'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
						'buttonImageOnly' => true,
						),
        'htmlOptions'=>array('size'=>'15')
   )); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'id_customer'); ?>
		<?php echo $form->dropDownList($model,'id_customer',CHtml::listData(Customer::model()->findAll('',array()), 'id_customer', 'nama')
		); ?>
	</div>
    <div class="row">
		<?php echo $form->label($model,'customer_ani'); ?>
		<?php echo $form->textField($model,'customer_ani',array('size'=>20,'maxlength'=>20)); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'jobs_id'); ?>
		<?php echo $form->textField($model,'jobs_id',array('size'=>60,'maxlength'=>200)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'topik'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status_batch'); ?>
		<?php echo $form->dropDownList($model,'status_batch',Controller::getStatusComboCampaigne()); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'priority_category'); ?>
		<?php echo $form->dropDownList($model,'priority_category',Controller::getPriorityComboCampaigne()); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ast'); ?>
		<?php echo $form->dropDownList($model,'ast',Controller::getAdvertisingSendingTypeComboCampaigne()); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cell'); ?>
		<?php echo $form->dropDownList($model,'cell',CHtml::listData(BtsGroup::model()->findAll('type = 1',array()), 'id', 'bts_name')
		); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'batch_type'); ?>
		<?php echo $form->dropDownList($model,'batch_type',Controller::getCampaigneTypeCombo()); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->