<?php 
if($model !==null){
	$dataType = $_GET['dataType'];
	$dateStart = $_GET['dateStart'];
	$dateEnd = $_GET['dateEnd'];
	$hourStart = $_GET['hourStart'];
	$hourEnd = $_GET['hourEnd'];
	$customer = $_GET['customer'];
	$campaign = $_GET['campaign'];
	$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
	$modelCampaign = CHtml::listData(LbaBatch::model()->findAll(),'id_batch','jobs_id');

				
	$x=0; 
	echo "<br>";
?>
	<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

	<h4>Customer : <?php echo $customer == "" ?  'All' :  $modeCustomer[$customer] ;  ?></h4>

	<h4>Campaign : <?php echo $campaign == "" ?  'All' :  $modelCampaign[$campaign] ;  ?></h4>
	<table border='1'>
		<tr>
			<td>Campaign</td>
			<td>Start Period</td>
			<td>Stop Period</td>
			<td>Topik</td>
			<td>Customer</td>
			<td>Channel Category</td>
			<td>Cell</td>
			<td>Total</td>
			<td>Success</td>
			<td>Failed</td>
			<td>Queue</td>
			<td>Queue Rate</td>
			<td>Success Rate</td>
			<td>Failed Rate</td>
		</tr>
	<?php foreach($model as $error){  ?>
		<tr <?php echo ($x++)%2==0?"style='background-color:#CCC'" : "" ; ?> >
			<td><?php echo $error->jobs_id ; ?></td>
			<td><?php echo $error->start_periode ; ?></td>
			<td><?php echo $error->stop_periode ; ?></td>
			<td><?php echo $error->topik ; ?></td>
			<td><?php echo @$error->Customer->nama; ?></td>
			<td><?php echo @$error->ChannelCategory->name; ?></td>
			<td><?php echo @$error->Bts->bts_name; ?></td>
			<td><?php echo $error->sms_send_limit ; ?></td>
			<td><?php echo $error->sms_success ; ?></td>
			<td><?php echo $error->sms_failed ; ?></td>
			<td><?php echo $error->queue ; ?></td>
			<td><?php echo $error->qr ; ?></td>
			<td><?php echo $error->sr ; ?></td>
			<td><?php echo $error->fr ; ?></td>
		</tr>
		<?php } ?>
	</table>
<?php } ?>
