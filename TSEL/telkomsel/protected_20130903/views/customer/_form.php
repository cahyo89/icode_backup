<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well-small">
		<?php echo $form->labelEx($model,'nama')."<b><font color='red'>*</font></b>"; ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'segmen_customer')."<b><font color='red'>*</font></b>"; ?>
		<?php 
			if(Yii::app()->user->getUserMode() == 1)
			{
				$model->segmen_customer = 1;
				echo $form->hiddenField($model,'segmen_customer');
				echo '<input type="text" name="segmen_customer" id="segmen_customer" value="external" readonly="readonly"/>';
			}
			else
			{
				echo $form->dropDownList($model,'segmen_customer',Yii::app()->params['segmen_customer']); 
			}
		?>
		<?php echo $form->error($model,'segmen_customer'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'tipe_customer')."<b><font color='red'>*</font></b>"; ?>
		<?php echo $form->dropDownList($model,'tipe_customer',@Yii::app()->params["tipe_customer"],array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'tipe_customer'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'category_customer')."<b><font color='red'>*</font></b>"; ?>
		<?php echo $form->dropDownList($model,'category_customer',CHtml::listData(TreeId::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'tree_id', 'nama_perangkat'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'category_customer'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'bypass_approval')."<b><font color='red'>*</font></b>"; ?>
		<?php echo $form->dropDownList($model,'bypass_approval',@Yii::app()->params["bypass_approval"],array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'bypass_approval'); ?>
	</div>
	
		<?php   
			
			if(Yii::app()->user->checkAccess('Admin'))
			{
				echo '<div class="well-small">';
				echo $form->labelEx($model,'mseller')."<b><font color='red'>*</font></b>"; 
				
				$slr = @CHtml::listData(Users::model()->findAll('status = 1 and usermode = 1',array('order'=>'username asc')),'id','username');
				if(!$model->isNewRecord)
				{
					$model->mseller = $model->id_user;
				}
		
				echo $form->dropDownList($model,'mseller',$slr,array('onchange'=>'{onChangeMseller();}'),$readonly); 
				echo $form->error($model,'mseller'); 
				echo '</div>';
			}
		?>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'parent_id')."<b><font color='red'>*</font></b>"; ?>
		<?php
				$parent = @CHtml::listData(Customer::model()->findAll('id_user = :user and approved = 1 and blocked = 0 and (hierarchy_type = 0 or hierarchy_type = 1 or hierarchy_type is null )',array(':user'=>Yii::app()->user->id)), 'id_customer', 'nama');
		?>
		<?php	
			/*if($model->isNewRecord)
			{*/
				echo $form->dropDownList($model,'parent_id',$parent,array('empty'=>'Parent','onchange'=>'{onChangeParent();}')); 
			/*}
			else
			{
				echo $form->dropDownList($model,'parent_id',$parent,array('onchange'=>'{onChangeParent();}'),array('readonly'=>'readonly'));
			}*/			
		?>
		<?php	echo $form->error($model,'parent_id'); ?>
	</div>
	
		
	<div class="well-small">
		<?php echo $form->labelEx($model,'hierarchy_type')."<b><font color='red'>*</font></b>"; ?>
		<?php 
			$type = @Yii::app()->params["hierarchy_type"];
			
			if(!Yii::app()->user->checkAccess('Admin'))
			{
				unset($type[0]);
			}
		?>
		<?php echo $form->dropDownList($model,'hierarchy_type',$type ); ?>
		<?php echo $form->error($model,'hierarchy_type'); ?>
	</div>

	<div class="well-small buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
	$(document).ready(function(){
		<?php
			if(Yii::app()->user->checkAccess('Admin'))
			{
				echo 'onChangeMseller();';
			}
		?>
		
		<?php 
			if(!$model->isNewRecord)
			{
				echo 'document.getElementById("Customer_parent_id").disabled = true;';
				echo 'document.getElementById("Customer_hierarchy_type").disabled = true;';
				
				if(Yii::app()->user->checkAccess('Admin'))
				{
					echo 'document.getElementById("Customer_mseller").disabled = true;';
				}
			}
		?>
	});	

	function onChangeMseller()
	{
		<?php 
			echo CHtml::ajax(array(
				'url'=>CController::createUrl('onChangeMseller'),
				'data'=>array('mseller_id'=>'js:$(\'#Customer_mseller\').val()'),
				'type'=>'post',
				'dataType'=>'json',
				'success'=>
					'function(data)
					{
						$("#Customer_parent_id").html(data.parent);
					}',
			));
		?>
		
		return false;
	}
	
	function onChangeParent()
	{	
		<?php 
			echo CHtml::ajax(array(
			// the controller/function to call
			'url'=>CController::createUrl('onChangeParent'),

			// Data to be passed to the ajax function
			// Note that the ' should be escaped with \
			// The field id should be prefixed with the model name eg Vehicle_field_name
		   // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
			'data'=>array('parent_id'=>'js:$(\'#Customer_parent_id\').val()'),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>'function(data)
			{
				$("#Customer_hierarchy_type").html(data.type);
			}
			',
		))?>;
				
		return false;
	}
</script>