<?php
$this->breadcrumbs=array(
	'Prepaid Packet'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('paket-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php if(Yii::app()->user->hasFlash('error')): ?>
 
<div class="flash-success">
    <font color="#000000" size="+1" style="background-color: #D2FFFE;"><?php echo Yii::app()->user->getFlash('error'); ?></font>
</div>
 
<?php endif; ?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Standar Packet Price</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   //array('label'=>'New','link'=>'paket/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'paket-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'jenis_paket',
			'type'=>'raw',
			'value'=>'CHtml::link($data->jenis_paket,array("paket/view","id"=>$data->id_paket))',
		),
		'harga_paket',
		'sms_paket',
		'expired_paket',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
		    'buttons'=>array
		    (
		        
		        'delete' => array
		        (
		        	'visible'=>true,
					
		        ),
		    ),
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>
<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   //array('label'=>'New','link'=>'paket/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>

