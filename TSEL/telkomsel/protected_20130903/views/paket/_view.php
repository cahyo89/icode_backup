<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_paket')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_paket), array('view', 'id'=>$data->id_paket)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_paket')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga_paket')); ?>:</b>
	<?php echo CHtml::encode($data->harga_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_paket')); ?>:</b>
	<?php echo CHtml::encode($data->sms_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expired_paket')); ?>:</b>
	<?php echo CHtml::encode($data->expired_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->flag_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	*/ ?>

</div>