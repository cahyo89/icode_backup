<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	

	<div class="row">
		<?php echo $form->label($model,'jenis_paket'); ?>
		<?php echo $form->textField($model,'jenis_paket',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'harga_paket'); ?>
		<?php echo $form->textField($model,'harga_paket'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sms_paket'); ?>
		<?php echo $form->textField($model,'sms_paket'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expired_paket'); ?>
		<?php echo $form->textField($model,'expired_paket'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->