<?php
/* @var $this ParamUserAgentController */
/* @var $model ParamUserAgent */

$this->breadcrumbs=array(
	'Param User Agents'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List ParamUserAgent', 'url'=>array('index')),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Create Param User Agent</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'list','link'=>'admin','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>