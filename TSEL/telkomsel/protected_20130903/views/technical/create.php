<?php
$this->breadcrumbs=array(
	'Balance'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SettingBalance', 'url'=>array('index')),
	array('label'=>'Manage SettingBalance', 'url'=>array('admin')),
);
?>

<h1>Create Balance</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>