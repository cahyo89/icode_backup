<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List FilterUrlIntercept', 'url'=>array('index')),
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
	array('label'=>'View FilterUrlIntercept', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FilterUrlIntercept', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Filter Url Intercepts #<?php echo $model->id; ?></h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
				   array('label'=>'List','link'=>'index','img'=>'/images/list.png','id'=>'','conf'=>''),
				   array('label'=>'View','link'=>'view','img'=>'/images/view.png','id'=>$model->id,'conf'=>'')
					)); ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>