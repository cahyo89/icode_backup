<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'get',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Report Transaction  </h1>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'id_customer'); ?>
		<?php echo $form->dropDownList($model,'id_customer',CHtml::listData(Customer::model()->findAll(' (flag_deleted IS NULL or flag_deleted <> :deleted)',array('deleted'=>1)),'id_customer','nama')); ?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->