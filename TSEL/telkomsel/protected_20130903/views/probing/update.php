<?php
$this->breadcrumbs=array(
	'Probe'=>array('index'),
	$model->probing_name=>array('view','id'=>$model->id_probing),
	'Update',
);


?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Probe #<?php echo $model->probing_name; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'probing/view','img'=>'/images/view.png','id'=>$model->id_probing,'conf'=>''),
	   array('label'=>'List','link'=>'probing/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'probing/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'probing/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'probing/view','img'=>'/images/view.png','id'=>$model->id_probing,'conf'=>''),
	   array('label'=>'List','link'=>'probing/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>