<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users1-form',
	'enableAjaxValidation'=>false,
)); ?>

	<h1>Your password has expired, Please change your password</h1>
<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well-small" style="display:none">
	
	<?php echo $form->labelEx($model,'id'); ?>
	<?php echo $form->passwordField($model,'id'); ?>
	<?php echo $form->error($model,'id'); ?>
	</div>
	
	<div class="well-small">
	
	<?php echo $form->labelEx($model,'oldpassword'); ?>
	<?php echo $form->passwordField($model,'oldpassword'); ?>
	<?php echo $form->error($model,'oldpassword'); ?>
	</div>
	
	<div class="well-small">
	<?php echo $form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
	<p class="hint">

	</p>
	</div>
	
	<div class="well-small">
	<?php echo $form->labelEx($model,'verifyPassword'); ?>
	<?php echo $form->passwordField($model,'verifyPassword'); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	</div>

	

	<div class="well-small buttons">
		<?php  echo CHtml::submitButton('Change'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->