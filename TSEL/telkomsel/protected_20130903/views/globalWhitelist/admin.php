<?php
$this->breadcrumbs=array(
	'Global Whitelists'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('global-whitelist-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Message Global Whitelist Subscriber</h1></td>
<td><div class="operatorRight"><table>
									<tr>
										<td><?php echo CHtml::link(Yii::app()->params['excel'],array('globalWhitelist/export')); ?></td>
										<td><?php  Controller::createMenu(array(
											   array('label'=>'New','link'=>'globalWhitelist/create','img'=>'/images/new.png','id'=>'','conf'=>'')
												));  ?></td>
									</tr>
								</table></div></td>
					</tr></table>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php
 
	 ?>

<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'excel-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
	<div class="row">
                <b>File Excel(.xls):</b>
		<?php echo $form->fileField($model,'filee',array('size'=>40,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'filee'); ?>
		<?php echo CHtml::submitButton('Upload'); ?>
		
	</div>
        
<?php $this->endWidget(); ?>
</div>
<b>
Sample File Uploads:<br>
Subscriber Number (No Header)</b>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'global-whitelist-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'subscriber_number',
			'type'=>'raw',
			'value'=>'CHtml::link($data->subscriber_number,array("globalWhitelist/view","id"=>$data->id))',
		),
		/*
		'last_ip',
		'last_update',
		*/
	array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{delete}',
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nSubscriber Number:' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'globalWhitelist/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>
