<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'channel-category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>20,'maxlength'=>20,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'push_price'); ?>
		<?php echo $form->textField($model,'push_price',array('size'=>20,'maxlength'=>20,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'push_price'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'response_price'); ?>
		<?php echo $form->textField($model,'response_price',array('size'=>20,'maxlength'=>20,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'response_price'); ?>
	</div>
	
	<br>
	<div class="row">
		<?php echo "Customer"; //echo $form->labelEx($model,'Bts '); ?>
		<?php 
			$where = "";
			if(Yii::app()->user->getUserMode() == 1)
			{
				$where = ' and id_user = '.Yii::app()->user->id;
			}
		
			if($model->isNewRecord){
				$this->widget(
				'application.widgets.emultiselect.EMultiSelect',
				array('sortable'=>true, 'searchable'=>true)
					);
				echo CHtml::listBox('slot','',CHtml::listData(Customer::model()->findAll('(approved = 1 and flag_deleted is null or flag_deleted <> 1) '), 'id_customer', 'nama'),
				array(
				'multiple'=>'multiple',
				'key'=>'id', 
				'class'=>'multiselect',
				));
				}
			else {
				$pp=Controller::selectedItem('tbl_customer','id_customer','tbl_channel_category_detail','id_channel',$model->id);
				$this->widget(
				'application.widgets.emultiselect.EMultiSelect',
				array('sortable'=>true, 'searchable'=>true)
					);
				echo CHtml::listBox('slot','',CHtml::listData(Customer::model()->findAll('(approved = 1 and flag_deleted is null or flag_deleted <> 1) '), 'id_customer', 'nama'),
				array(
				'multiple'=>'multiple',
				//'key'=>'id', 
				'class'=>'multiselect',
				'options'=>$pp,
				));
			
			}
		?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->