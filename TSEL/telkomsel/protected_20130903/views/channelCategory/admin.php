<?php
$this->breadcrumbs=array(
	'Basic Channel Rate'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('channel-category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>">
<h1>Basic Channel Rate</h1>
</table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'channel-category-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'name',
			'type'=>'raw',
			'value'=>'CHtml::link($data->name,array("channelCategory/view","id"=>$data->id))',
		),
		'push_price',
		//'response_price',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>
