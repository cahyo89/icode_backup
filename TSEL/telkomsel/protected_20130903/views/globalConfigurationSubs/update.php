<?php
$this->breadcrumbs=array(
	'Global Configuration ',
);
Yii::app()->clientScript->registerCss(uniqid(),"
div.form label {
    display: block;
    font-size: 1em;
    font-weight: bold;
    width: 270px;
}
");
?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Global Configuration Opt-in#<?php echo $model->shortcode; ?></h1></td>
<td><div class="operatorRight"></div></td>
					</tr></table>
<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

