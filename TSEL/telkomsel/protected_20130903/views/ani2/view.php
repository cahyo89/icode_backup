<?php
$this->breadcrumbs=array(
	'Customer Shortcode'=>array('index'),
	$model->ani,
);


?>
<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Customer Shortcode #<?php echo $model->ani; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'ani2/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'Delete','link'=>'ani2/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->ani),
	   array('label'=>'New','link'=>'ani2/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'label'=>'Customer',
			'value'=>@$model->Customer->nama == "" ? "All":$model->Customer->nama,
		),
		array(
			'label'=>'SDC',
			'value'=>$model->ani,
			//'visible'=>false,
		),
		array(
			'label'=>'Vas ID',
			'value'=>$model->vas_id,
			'visible'=>$model->media_type == 5 ? true :false,
		),
		array(
			'label'=>'Svc Code',
			'value'=>@$model->svc_code,
			'visible'=>$model->media_type == 5 ? true :false,
		),
		array(
			'name'=>'media_type',
			'type'=>'raw',
			'value'=>$model->ChannelCategory->name,
			
		),
		
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>Controller::getConstanta("approved",$model->status),
		),
		'reason',
	),
)); 
if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('superUser')){
	if($model->status != 2)
		echo CHtml::button('Reject', array('submit' => array('reject','id'=>$model->id),'confirm'=>'Are you sure want to REJECT this Customer?')); 
	if($model->status != 1)
		echo CHtml::button('Approve', array('submit' => array('app','id'=>$model->id),'confirm'=>'Are you sure want to APPROVE this Customer?')); 
	echo "<BR>";
}
Controller::createInfo($model);
?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'ani2/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'ani2/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	    array('label'=>'Delete','link'=>'ani2/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->ani)
	   
		));  ?>
</div>