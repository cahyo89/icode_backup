<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ani-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_customer'); ?>
		<?php echo $form->dropDownList($model,'id_customer',CHtml::listData(Customer::model()->findAll(array('condition'=>'flag_deleted IS NULL or flag_deleted <> 1','order'=>'nama')), 'id_customer', 'nama'),array('empty'=>"All")); ?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'media_type'); ?>
		<?php echo $form->dropDownList($model,'media_type',CHtml::listData(ChannelCategory::model()->findAll(), 'id', 'name'),
				array('ajax'=>array(
						'type'=>'POST',
						'url'=>CController::createUrl('ani2/cekDataType'),
						'data'=>array('dataType' => 'js:this.value'),
						'success' => 'function(retval){$("#sdc").attr("style",retval);$("#vas").attr("style",retval == "display:block" ? "display:none" : "display:block" );}',
						'error'=> 'function(){alert(\'Bad AJAX\');}',
						'update'=>'#insider_div',
				))
		); ?>
		<?php echo $form->error($model,'media_type'); ?>
	</div>

	<div id="sdc" class="row">
		<?php echo $form->labelEx($model,'ani'); ?>
		<?php echo $form->textField($model,'ani',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'ani'); ?>
	</div>
	
	<div id="vas" style="display:none">
		<div class="row">
			<?php echo $form->labelEx($model,'vas_id'); ?>
			<?php echo $form->textField($model,'vas_id',array('size'=>35,'maxlength'=>99)); ?>
			<?php echo $form->error($model,'vas_id'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'svc_code'); ?>
			<?php echo $form->textField($model,'svc_code',array('size'=>35,'maxlength'=>99)); ?>
			<?php echo $form->error($model,'svc_code'); ?>
		</div>
	</div>
	




	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

 <script type="text/javascript">
	    // The bits above should be familiar to you
	    // This function is called by the "HPI check" button above
	   function onChangeMedia()
	    {
	        var media = document.getElementById('Ani2_media_type').value;
			if(media == 5 ){
				document.getElementById('sdc').style.display = 'none';
				document.getElementById('vas').style.display = 'block';
			}
			else{
				document.getElementById('sdc').style.display = 'block';
				document.getElementById('vas').style.display = 'none';
			}
	        return false;  
	    } 
</script>