<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		/*
		//if(!Yii::app()->user->isGuest)
		{
			$roles = Rights::getAssignedRoles(Yii::app()->user->Id);
			$rolein = "(";
			foreach($roles as $role)
			{
				$rolein .= "'".$role->name."',";
			}
			$rolein = rtrim($rolein, ",");
			$rolein .= ")";
			$sql = "SELECT * FROM `menu` m join `authitemchild` aic on substring(replace(link,'/','.'),2,LENGTH(link)-1) = lower(aic.child) where aic.parent in ".$rolein." GROUP BY link";
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql);
			$rows=$command->queryAll();
			foreach($rows as $row)
			{
				echo $row['link'];
			}
			
		}
		$test = Yii::app()->controller->display_menu();
		echo "<pre>";
		print_r($test);
		echo "</pre>";
		*/
		
		if(!Yii::app()->user->isGuest)
		$this->render('dashboard');
		else
		$this->actionLogin();
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];//beresin bug doble login
				$user = User::model()->findByAttributes(array('username'=>$model->username));
				if(isset($user)){
					$jam=date("Y-m-d H:i:s",$user->lastvisit);
					if((strtotime(date("Y-m-d H:i:s"))-strtotime($jam) ) >= Yii::app()->params['sessionTimeoutSeconds'])
					{
						$connection=Yii::app()->db;
						$sql= "update tbl_users set ip = 0,flag = 0 where id = $user->id";
						$connection->createCommand($sql)->execute();
					}
				}
			//sampe sini
			// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$current= Users::model()->findByPk(Yii::app()->user->id);
					$id = Yii::app()->user->id;
					$hari = strtotime($current->change_pass_date);
					if($current->change_pass_date == NULL  ||  (($hari+($current->passage1*86400)) < mktime())){
						//Yii::app()->user->logout();
						//$this->redirect(array('/change/index','id'=>$id));
					}
					if(($hari+($current->passage2*86400)) < mktime()){
						$connection=Yii::app()->db;
						$sql= "update tbl_users set status = -1 where id = $id";
						$connection->createCommand($sql)->execute();
						Yii::app()->user->logout();
					}
					
					//$this->lastViset();
					if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
						$this->redirect(Yii::app()->user->returnUrl);
					else
						$this->redirect(Yii::app()->controller->module->returnUrl);
				}
			}
			// display the login form
			$this->RenderPartial('login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->user->returnUrl);
			
		/*
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		*/
	}
	
	private function lastViset() {
	
		$id = Yii::app()->user->id;
		$connection=Yii::app()->db;
		$sql= "update tbl_users set lastvisit = '".time()."' where id = $id";
		$connection->createCommand($sql)->execute();
		
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}