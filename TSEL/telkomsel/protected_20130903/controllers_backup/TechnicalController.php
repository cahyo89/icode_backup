<?php

class TechnicalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$model = Yii::app()->db->createCommand("select tbl_detail_prepaid.*,tbl_customer.nama,tbl_customer.tipe_paket,tbl_customer.tipe_customer from tbl_detail_prepaid left join tbl_customer on tbl_detail_prepaid.id_customer = tbl_customer.id_customer where id = ".$id)->queryRow();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SettingBalance;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SettingBalance']))
		{
			$model->attributes=$_POST['SettingBalance'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_customer));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$temp = $model->temp_prepaid;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SettingBalance']))
		{
			$model->attributes=$_POST['SettingBalance'];
			$modelPaketExpired = CHtml::listData(Paket::model()->findAll(),'id_paket','expired_paket');			
			$modelPaketHarga = CHtml::listData(Paket::model()->findAll(),'id_paket','harga_paket');
			$model->approved_prepaid = 0;
			if($model->tipe_customer == 0){
				$berita = "Add Balance : Name : $model->nama , Paket : $model->tipe_paket";
				$model->temp_prepaid = $model->temp_prepaid+$_POST['token'];//$modelPaketHarga[$model->tipe_paket];
				
				$dateNow = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " +".$modelPaketExpired[$model->tipe_paket]." day");
				$dateExp = strtotime($model->expired_date);
				
				if($dateNow > $dateExp)
					$model->expired_date = date('Y-m-d',$dateNow);
			}
			else
			{	
				$berita = "Add Balance : Name : $model->nama , Balance : $model->temp_prepaid";
				$model->temp_prepaid = $model->temp_prepaid+$temp;
				
			}
			
			if($model->save()){
				
				//Controller::afterUpdate("SettingBalance",$model->id_customer);
				Controller::addberita($berita);
				$this->redirect(array('index'));
				//$this->redirect(array('view','id'=>$model->id_customer));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionApp($id)
	{
		$current= SettingBalance::model()->findByPk($id);
		$modelPaketExpired = CHtml::listData(Paket::model()->findAll(),'id_paket','expired_paket');			
		$modelPaketHarga = CHtml::listData(Paket::model()->findAll(),'id_paket','harga_paket');
		$modelPaketToken = CHtml::listData(Paket::model()->findAll(),'id_paket','sms_paket');
		$discount = Discount::model()->findAll();
		$current->approved_prepaid = 1;
		$current->reason_prepaid = NULL;
		//$current->prepaid_value  = $current->prepaid_value+$current->temp_prepaid;
		$token = $current->temp_prepaid;
		$berita = "Approved Setting Balance untuk Customer = $current->nama ";
		$current->temp_prepaid = 0;
		//$current->last_ip = CHttpRequest::getUserHostAddress();
		//$current->last_user = Yii::app()->user->first_name;
		$current->update();
		
		$currentVR= new VoucherOrder;
		
		$currentVR->id_customer = $id;
		$currentVR->voucher_type = $current->tipe_paket;
		$currentVR->total = $current->prepaid_value; 
		$currentVR->remain = $current->prepaid_value;
		$currentVR->start = date('Y-m-d'); 
		
		if($current->tipe_customer ==0)
			$currentVR->end = $current->expired_date;
			
		$currentVR->first_user = Yii::app()->user->first_name;
		$currentVR->first_ip = CHttpRequest::getUserHostAddress();
		$currentVR->first_update = date('Y-m-d h:i:s');
		$currentVR->last_user = Yii::app()->user->first_name;
		$currentVR->last_ip = CHttpRequest::getUserHostAddress();
		$currentVR->last_update =  date('Y-m-d h:i:s');
		$currentVR->save();
		
		$discAmount = 0;
		
		foreach($discount as $disc)
		{
			//echo $disc->range."".$modelPaketHarga[$current->tipe_paket];
			if($token >= $disc->range)
			{
				$discAmount = $disc->discount;
			}
		}
		
		$idPaket = $current->tipe_paket;
		$paketModel = Paket::model()->findByPk($idPaket);
		if($paketModel->flag == 1){
			$sql = "INSERT INTO tbl_detail_prepaid values (NULL,NULL, ".$id.",".($modelPaketHarga[$current->tipe_paket]*$token).",0,".(($modelPaketHarga[$current->tipe_paket]*$token))." ,".$token.", 1,'".date("Y-m-d H:i:s")."',null, '".Yii::app()->user->first_name."', "." '".CHttpRequest::getUserHostAddress()."', "." '".date("Y-m-d H:i:s")."', '".Yii::app()->user->first_name."', "." '".CHttpRequest::getUserHostAddress()."', "." '".date("Y-m-d H:i:s")."');";
		}
		else{
			$sql = "INSERT INTO tbl_detail_prepaid values (NULL,NULL, ".$id.",".($modelPaketHarga[$current->tipe_paket]*$token).",".$discAmount.",".(($modelPaketHarga[$current->tipe_paket]*$token)-((($modelPaketHarga[$current->tipe_paket]*$token) * $discAmount) / 100))." ,".$token.", 1,'".date("Y-m-d H:i:s")."',null, '".Yii::app()->user->first_name."', "." '".CHttpRequest::getUserHostAddress()."', "." '".date("Y-m-d H:i:s")."', '".Yii::app()->user->first_name."', "." '".CHttpRequest::getUserHostAddress()."', "." '".date("Y-m-d H:i:s")."');";
		}
		
		$test = Yii::app()->db->createCommand($sql)->execute();
		Controller::addberita($berita);
		$this->redirect(array('index'));
	}
	
	public function actionReject($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['SettingBalance']))
		{
			$model->attributes=$_POST['SettingBalance'];
			if($model->save()){
				$current= Customer::model()->findByPk($id);
				$current->approved_prepaid = 2;
				$berita = "Reject Setting Balance untuk Customer = $current->nama ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}

		$this->render('reject',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			$model->prepaid_value = 0;
			//$model->approved_prepaid = -1;
			$model->expired_date = date('Y-m-d');
			$model->save();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new SettingBalance('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SettingBalance']))
			$model->attributes=$_GET['SettingBalance'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SettingBalance::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='setting-balance-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
