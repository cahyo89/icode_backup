<?php

class CustomerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights'
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Customer;

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			
			
			
			$modelCeks = CHtml::listData(Customer::model()->findAllByAttributes(array('nama'=>$model->nama,'flag_deleted'=>1)),'nama','id_customer');
			//print "<pre>";
			//print_r($_POST);
			//print_r($modelCeks);
			//print "</pre>";
			//exit;
			
			if(!isset($modelCeks[$model->nama])){
				//echo "b bb";
				$model->id_user = Yii::app()->user->id;
				//$model->segmen_customer = 0;
				if($model->save()){
					Controller::afterCreate("Customer",$model->id_customer);
					Controller::addberita("Create Customer : Name : $model->nama , Segmen Customer : ".Yii::app()->params['segmen_customer'][$model->segmen_customer]." ");
					$this->redirect(array('view','id'=>$model->id_customer));
				}
			}
			else
			{
				$modelU=$this->loadModel($modelCeks[$model->nama]);
				$modelU->attributes=$_POST['Customer'];
				$modelU->flag_deleted = 0;
				
				//echo "aaa";
				//print "<pre>";
				//print_r($modelU);
				//print "</pre>";
				//exit;
				if($modelU->save()){
					Controller::afterCreate("Customer",$modelU->id_customer);
					Controller::addberita("Create Customer : Name : $model->nama , Segmen Customer : ".Yii::app()->params['segmen_customer'][$model->segmen_customer]." ");
					$this->redirect(array('view','id'=>$modelU->id_customer));
				}
			
			}
		}
		else{
			//echo "xxxx";
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		$tempTipe = $model->tipe_customer;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			if($tempTipe == 1 && $model->tipe_customer != 1 ){
				$model->expired_date = date('Y-m-d');
				$model->temp_prepaid = $model->prepaid_value;
				$model->prepaid_value = 0 ; 
				$model->approved_prepaid = 0;
				}
			if($model->save()){
				Controller::afterUpdate("Customer",$model->id_customer);
				Controller::addberita("Update Customer : Name : $modelOld->nama -> $model->nama , Segmen Customer : ".Yii::app()->params['segmen_customer'][$modelOld->segmen_customer]." -> ".Yii::app()->params['segmen_customer'][$model->segmen_customer]." ");
				$this->redirect(array('view','id'=>$model->id_customer));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			//if(Controller::cekExist($id,"id_customer","Ani") && Controller::cekExist($id,"id_customer","RuleTemplate")){
			if(Controller::cekExist($id,"id_customer","Campaigne"))
			{
				$model=$this->loadModel($id);
				Controller::beforeDelete('Customer',$id);
				Controller::deleted('Customer',$id);
				Controller::addberita("Delete Customer : Name : $model->nama , Segmen Customer : ".Yii::app()->params['segmen_customer'][$model->segmen_customer]." ");
				
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	
	public function actionReject($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			if($model->save()){
				$current= Customer::model()->findByPk($id);
				$current->approved = 2;
				$berita = "Reject Customer dengan Jobs ID = $current->nama ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}

		$this->render('reject',array(
			'model'=>$model,
		));
	}
	public function actionBlock($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Customer']))
		{
			$model->attributes=$_POST['Customer'];
			if($model->save()){
				$current= Customer::model()->findByPk($id);
				$current->blocked = 1;
				$berita = "Blocked Customer dengan Jobs ID = $current->nama ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}

		$this->render('block',array(
			'model'=>$model,
		));
	}
	
	public function actionApp($id)
	{
		$current= Customer::model()->findByPk($id);
		$current->approved = 1;
		$berita = "Approved Customer dengan Jobs ID = $current->nama ";
		$current->last_ip = CHttpRequest::getUserHostAddress();
		$current->last_user = Yii::app()->user->first_name;
		$current->update();
		Controller::addberita($berita);
		$this->redirect(array('index'));
	}
	
	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$dataprovider=new CActiveDataProvider('Customer', array());
			
		if(Yii::app()->user->getUserMode() == 1)
		{
			$custHead = Customer::model()->findAll("id_customer = '".Yii::app()->user->getIdCustomer()."'");
			
			//$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
			$dataprovider=new CActiveDataProvider('Customer', array(
					'criteria'=>array(
							'condition'=>"approved = 1 and ( parent_id = '".$custHead[0]->id_customer."' or id_customer = '".$custHead[0]->id_customer."' )",
					),
			));
		}
		//$model=new Customer('search');
		//$model->unsetAttributes();  // clear any default values
		
		//if(isset($_GET['Customer']))
		//	$model->attributes=$_GET['Customer'];

		$this->render('admin',array(
			'model'=>$dataprovider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Customer::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	
	//PARENT CUSTOMER
	public function actionOnChangeParent()
	{
		$idParent = Yii::app()->request->getParam('parent_id');
		$cust = Customer::model()->findByPk((int)$idParent);
		$type = "";
		if ($cust->parent_id == null || $cust->parent_id == 0 && $cust->hierarchy_type == 0)
		{
			$type .= CHtml::tag('option', array('value'=>1),CHtml::encode('Media Seller'),true);
			$type .= CHtml::tag('option', array('value'=>2),CHtml::encode('Advertiser'),true);
		}
		else if($cust->hierarchy_type == 1)
		{
			$type .= CHtml::tag('option', array('value'=>2),CHtml::encode('Advertiser'),true);
		}
		
		echo CJSON::encode(array(
			'type'=>$type,			
		)); 
		
		Yii::app()->end();
	}
	
	//Get Info Customer
	public function actionOnGetInfo()
	{
		$idParent = Yii::app()->request->getParam('parent_id');
		$cust = Customer::model()->findByPk((int)$idParent);
		$downlines = Customer::model()->findAll('parent_id = '.(int)$idParent.' and approved = 1 and blocked = 0');
		$type = "";
		foreach($downlines as $key => $down )
		{
			$type .= CHtml::tag('option', array('value'=>$down->id_customer),CHtml::encode($down->nama),true);
		}
		echo CJSON::encode(array(
			'type'=>$type,		
			'balance'=>$cust->prepaid_value,
		)); 
		
		Yii::app()->end();
	}
	
	//Transfer CheckExpVoucer
	public function checkExpiredVoucher($id_customer){
		$customer = Customer::model()->findByPk($id_customer);
		if($customer->tipe_customer == 0){
			if(mktime() >= (strtotime($customer->expired_date)) ){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	
	}
	
	//Transfer Balance
	public function actionTransferBalance()
	{
		$model=new Customer;

		if(isset($_POST['Customer']))
		{
			$booleanGue = true;
			$customerparent = Customer::model()->findByPk($_POST['Customer']['parent_id']);
			$customerchild = Customer::model()->findByPk($_POST['Customer']['downline']);
			
			//CEK CUSTOMER Parent Type
			if($customerparent->tipe_customer == 0)
			{
				//CEK EXPIRED VOCER
				if($this->checkExpiredVoucher($customerparent->id_customer)){
					$booleanGue = false;
					$model->addError('error','Customer Parent Voucher is expired. Please contact your Administrator');
				}				
			}
			
			//CEK CUSTOMER child Type
			if($customerchild->tipe_customer == 0)
			{
				//CEK EXPIRED VOCER
				if($this->checkExpiredVoucher($customerchild->id_customer)){
					$booleanGue = false;
					$model->addError('error','Customer Child Voucher is expired. Please contact your Administrator');
				}
			}
			
			//CEK BALANCE
			if($customerparent->prepaid_value < $_POST['Customer']['amount']){
				$booleanGue = false;
				$model->addError('error','Not enough balance for transfer balance');
			}
			
			
			//CEK CUSTOMER Valid
			$valid = Customer::model()->findAll('id_customer = '.$customerchild->id_customer.' and parent_id = '.$customerparent->id_customer.' and approved = 1 and blocked = 0');
			if(!$valid)
			{
				$booleanGue = false;
				$model->addError('error','Customer is not valid');
			}
			
			if($booleanGue)
			{
				//TRANSFER BALANCE
				
				$customerparent->prepaid_value = ($customerparent->prepaid_value-$_POST['Customer']['amount']);
				$customerchild->prepaid_value = ($customerchild->prepaid_value+$_POST['Customer']['amount']);
				$customerparent->save();
				$customerchild->save();

				//INSERT HISTORY.
				$parentHist= new VoucherOrder;
				$parentHist->id_customer = $customerparent->id_customer;
				$parentHist->voucher_type = -1;
				$parentHist->total = $_POST['Customer']['amount']; 
				$parentHist->remain = ($customerparent->prepaid_value+$_POST['Customer']['amount']);
				$parentHist->start = date('Y-m-d'); 
				
				if($customerparent->tipe_customer ==0)
					$parentHist->end = $customerparent->expired_date;
					
				$parentHist->first_user = Yii::app()->user->first_name;
				$parentHist->first_ip = CHttpRequest::getUserHostAddress();
				$parentHist->first_update = date('Y-m-d h:i:s');
				$parentHist->last_user = Yii::app()->user->first_name;
				$parentHist->last_ip = CHttpRequest::getUserHostAddress();
				$parentHist->last_update =  date('Y-m-d h:i:s');
				$parentHist->id_customer_dest = $customerchild->id_customer;
				$parentHist->save();
				
				$childHist= new VoucherOrder;
				$childHist->id_customer = $customerchild->id_customer;
				$childHist->voucher_type = -1;
				$childHist->total = $_POST['Customer']['amount']; 
				$childHist->remain = ($customerchild->prepaid_value-$_POST['Customer']['amount']);
				$childHist->start = date('Y-m-d'); 
				
				if($current->tipe_customer ==0)
					$childHist->end = $customerchild->expired_date;
					
				$childHist->first_user = Yii::app()->user->first_name;
				$childHist->first_ip = CHttpRequest::getUserHostAddress();
				$childHist->first_update = date('Y-m-d h:i:s');
				$childHist->last_user = Yii::app()->user->first_name;
				$childHist->last_ip = CHttpRequest::getUserHostAddress();
				$childHist->last_update =  date('Y-m-d h:i:s');
				$childHist->save();
				
				Controller::addberita($berita);
				$this->redirect(array('index'));
				
				
			}
		}

		$this->render('transfer',array(
			'model'=>$model,
		));
	}
}
