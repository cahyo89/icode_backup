<?php

class TreeIdController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TreeId;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TreeId']))
		{
			$model->attributes=$_POST['TreeId'];
			$modelCeks = CHtml::listData(TreeId::model()->findAllByAttributes(array('nama_perangkat'=>$model->nama_perangkat,'flag_deleted'=>1)),'nama_perangkat','tree_id');
			
			if(!isset($modelCeks[$model->nama_perangkat])){
				$model->usermode = 2;
				if($model->save()){
					Controller::afterCreate("TreeId",$model->tree_id);
					Controller::addberita("Create Customer Category : Name : $model->nama_perangkat ");
					$this->redirect(array('view','id'=>$model->tree_id));
				}
			}
			else
			{
				$modelU=$this->loadModel($modelCeks[$model->nama_perangkat]);
				$modelU->attributes=$_POST['TreeId'];
				$modelU->flag_deleted = 0; 
				if($modelU->save()){
					Controller::afterCreate("TreeId",$modelU->tree_id);
					Controller::addberita("Create Customer Category : Name : $model->nama_perangkat ");
					$this->redirect(array('view','id'=>$modelU->tree_id));
				}
			
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TreeId']))
		{
			$model->attributes=$_POST['TreeId'];
			if($model->save()){
				Controller::afterUpdate("TreeId",$model->tree_id);
				Controller::addberita("Update Customer Category : Name : $modelOld->nama_perangkat -> $model->nama_perangkat ");
				$this->redirect(array('view','id'=>$model->tree_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if(Controller::cekExist($id,"category_customer","Customer")){
				$model=$this->loadModel($id);
				Controller::beforeDelete('TreeId',$id);
				Controller::deleted('TreeId',$id);
				Controller::addberita("Delete Customer Category : Name : $model->nama_perangkat ");
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new TreeId('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TreeId']))
			$model->attributes=$_GET['TreeId'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TreeId::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tree-id-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
