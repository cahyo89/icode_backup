<?php

class ApplyParamController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	


	

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new ApplyParam;
		ini_set("error_reporting","E_ALL");
		ini_set("memory_limit","500M");
		
		$pathLst = "/nfs_data/projectmobads/config_data/config/";
			
			if(isset($_GET['ApplyParam'])){
				
				
		
				
				$modelCentral = Lac::model()->findAll(array('condition'=>'flag_deleted <> 1 or flag_deleted is null','order' => 'id ASC'));
				$handle = fopen($pathLst."lacs.lst","w");
			
			
				foreach($modelCentral as $dataM)
				{
					fwrite($handle,$dataM->lac_id.",".$dataM->host.",".$dataM->port."\r\n");
				}
				fclose($handle);
				@chmod($pathLst."lacs.lst", 0777);
				
				//$pathCommand = "/nfs_data/projectlbaxl/command/init.cmd";
				$pathCommand = "/nfs_data/projectmobads/commandDesta/init.cmd";
				$handle = fopen($pathCommand,"w");
				fwrite($handle,"reload");
				fclose($handle);
				@chmod($pathCommand, 0777);

				//$pathCommand = "/nfs_data/projectlbaxl/commandDesta/init.cmd";
				$pathCommand = "/nfs_data/projectmobads/commandlba/init.cmd";	
				$handle = fopen($pathCommand,"w");
				fwrite($handle,"reload");
				fclose($handle);
				@chmod($pathCommand, 0777);
				
				Yii::app()->user->setFlash('error', "Success Apply Parameter.");
				Controller::addberita("Success Apply Parameter [Lac]");
				$this->redirect(array('index'));
				
			}
		$this->render('index',array(
			'model'=>$model,
		));
	}
	
}
