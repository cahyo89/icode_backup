<?php

class BtsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Bts;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bts']))
		{
			$model->attributes=$_POST['Bts'];
			
			$modelTampung = Bts::model()->findAllByAttributes(array('cellname'=>$model->cellname,'site_id'=>$model->site_id,'bts_id'=>$model->bts_id,'lac'=>$model->lac,'flag_deleted'=>1,'type'=>0));
			foreach ($modelTampung as $modelTT){
				$modelCeks[$modelTT->lac][$modelTT->bts_id][$modelTT->cellname][$modelTT->site_id] = $modelTT->id;
			}
			//$modelLac = CHtml::listData(Lac::model()->findAll(),'id','lac_id');
			//$model->lac = $modelLac[$model->lac];
			if(!isset($modelCeks[$model->lac][$model->bts_id][$model->cellname][$model->site_id])){
				$model->type = 0;
				if($model->save()){
					Controller::afterCreate("Bts",$model->id);
					Controller::addberita("Create CI : Name : $model->bts_name , Bts Id : $model->bts_id");
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			else
			{
				$modelU=$this->loadModelFlag($modelCeks[$model->lac][$model->bts_id][$model->cellname][$model->site_id]);
				$modelU->attributes=$_POST['Bts'];
				$modelU->flag_deleted = 0; 
				if($modelU->save()){
					Controller::afterCreate("Bts",$modelU->id);
					Controller::addberita("Create CI : Name : $model->bts_name , Bts Id : $model->bts_id");
					$this->redirect(array('view','id'=>$modelU->id));
				}
			
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld = $this->loadModel($id);
		//$modelLac = CHtml::listData(Lac::model()->findAll(),'lac_id','id');
		//$model->lac = $modelLac[$model->lac];
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Bts']))
		{
			$model->attributes=$_POST['Bts'];
			//$modelLac = CHtml::listData(Lac::model()->findAll(),'id','lac_id');
			//$model->lac = $modelLac[$model->lac];
			if($model->save()){
				Controller::afterUpdate("Bts",$model->id);
				Controller::addberita("Update CI : shortcode : $modelOld->bts_name -> $model->bts_name , Lac Id : $modelOld->bts_id -> $model->bts_id");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if(Controller::cekExist($id,"id_bts","BtsGroupDetail")){
				$model=$this->loadModel($id);
				Controller::beforeDelete('Bts',$id);
				Controller::deleted('Bts',$id);
				Controller::addberita("Delete CI : Name : $model->bts_name , Lac Id : $model->bts_id");
				
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Bts('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bts']))
			$model->attributes=$_GET['Bts'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Bts::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelFlag($lac,$bts_id,$cellname,$site_id)
	{
			
		$model = Bts::model()->findAllByAttributes(array('cellname'=>$cellname,'site_id'=>$site_id,'bts_id'=>$bts_id,'lac'=>$lac,'type'=>0));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bts-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
