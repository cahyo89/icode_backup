<?php

class BtsGroupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$dataProvider=new CActiveDataProvider('Bts',array(
		'criteria'=>array(
		'condition'=>'id in(select id_bts from tbl_btsgroup_detail where id_group = :id) and (flag_deleted is null or flag_deleted <>1)',
		 'params'=>array(':id'=>$id),
			),
			));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BtsGroup;
		$pp = array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BtsGroup']))
		{
			$model->attributes=$_POST['BtsGroup'];
			$modelLocationStat = CHtml::listData(LocationStat::model()->findAllByAttributes(array('id_loc_stat'=>$model->id_loc_stat)),'id_loc_stat','ip');
			$modelCeks = CHtml::listData(BtsGroup::model()->findAllByAttributes(array('bts_name'=>$model->bts_name,'flag_deleted'=>1,'type'=>1)),'bts_name','id');
			
			if(!isset($modelCeks[$model->bts_name])){
				$model->type = 1;
				if($model->id_loc_stat != ""){
					$model->loc_stat_ip = $modelLocationStat[$model->id_loc_stat];
					$model->flag_profiling = 1;
				}
				else{
					$model->flag_profiling = 0;
				}
				if($model->save()){
					if(isset($_POST['BtsGroup']['slot'])){
						Controller::insertSlot('tbl_btsgroup_detail','id_group','id_bts','BtsGroupDetail',$_POST['BtsGroup']['slot'],$model->id);
						}
					Controller::afterCreate("Bts",$model->id);
					Controller::addberita("Create Group Area : Name : $model->bts_name , Location Statistic : $model->id_loc_stat");
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			else
			{
				$modelU=$this->loadModel($modelCeks[$model->bts_name]);
				$modelU->attributes=$_POST['BtsGroup'];
				$modelU->flag_deleted = 0; 
				$modelU->loc_stat_ip = $modelLocationStat[$model->id_loc_stat];
				if($modelU->save()){
					if(isset($_POST['slot']))
					{
						Controller::insertSlot('tbl_btsgroup_detail','id_group','id_bts','BtsGroupDetail',$_POST['slot'],$modelU->id,1);
					}
					Controller::afterCreate("Bts",$modelU->id);
					Controller::addberita("Create Group Area : Name : $model->bts_name , Location Statistic : $model->id_loc_stat");
					$this->redirect(array('view','id'=>$modelU->id));
				}
			
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'pp'=>$pp,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld = $this->loadModel($id);
		$pp=Controller::getSelectedItem('tbl_bts','id_bts','tbl_btsgroup_detail','id_group',$id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BtsGroup']))
		{
			$model->attributes=$_POST['BtsGroup'];
			$modelLocationStat = CHtml::listData(LocationStat::model()->findAllByAttributes(array('id_loc_stat'=>$model->id_loc_stat)),'id_loc_stat','ip');
			
			if($model->id_loc_stat != ""){
				$model->loc_stat_ip = $modelLocationStat[$model->id_loc_stat];
				$model->flag_profiling = 1;
			}
			else{
				$model->flag_profiling = 0;
			}
			if($model->save()){
				if(isset($_POST['BtsGroup']['slot'])){
					Controller::insertSlot('tbl_btsgroup_detail','id_group','id_bts','BtsGroupDetail',$_POST['BtsGroup']['slot'],$model->id,1);
				}
				Controller::afterUpdate("Bts",$model->id);
				Controller::addberita("Update Group Area : Name : $modelOld->bts_name -> $model->bts_name , Location Statistic : $modelOld->id_loc_stat -> $model->id_loc_stat");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'pp'=>$pp,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
			Controller::beforeDelete('Bts',$id);
			Controller::deleted('Bts',$id);
			Controller::addberita("Create Group Area : Name : $model->bts_name , Location Statistic : $model->id_loc_stat");
			
			//$connection = Yii::app()->db;
			BtsGroupDetail::model()->deleteAll("id_group = '$id'");
			//$sql = "delete from tbl";
			//$command = $connection->createCommand($sql)->execute();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new BtsGroup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BtsGroup']))
			$model->attributes=$_GET['BtsGroup'];
			
		if(isset($_GET['excel'])){
			$model=BtsGroup::model()->findAll("(flag_deleted <> 1 or flag_deleted is null) and type = 1 ");
			
			Yii::app()->request->sendFile('GroupArea.xls',
				$this->renderPartial('excel',array(
						'model'=>$model,
					),true)
			);
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionSearchT()
	{
	
		ini_set('memory_limit',-1);
		$arrayBts = array();
		
		$dataLac = Yii::app()->request->getParam('dataL');
		$dataCI = Yii::app()->request->getParam('dataC');
		$dataName = Yii::app()->request->getParam('dataN');
		
		if($dataLac != "" || $dataName != "" || $dataCI !="" ){
			$bts = Bts::model()->findAll('(flag_deleted is null or flag_deleted <> 1) and type = 0 and lac like "'.$dataLac.'%" and bts_id like "'.$dataCI.'%" and bts_name like "'.$dataName.'%" order by bts_name asc');
			foreach($bts as $dataB){
				$arrayBts[$dataB->id] = "[".$dataB->lac."|".$dataB->bts_id."] ".$dataB->bts_name." ";
			}
			
			$dropDownA = '';
			foreach($arrayBts as $value=>$name){
			   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
			}
			echo CJSON::encode(array(
					'dataa'=>$dropDownA,
			)); 
			Yii::app()->end();
		}
		else{
			$dropDownA = '';
			echo CJSON::encode(array(
					'dataa'=>$dropDownA,
			)); 
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BtsGroup::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bts-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
