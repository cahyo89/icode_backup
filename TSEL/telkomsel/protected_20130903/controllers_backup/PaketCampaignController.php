<?php

class PaketCampaignController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PaketCampaign;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PaketCampaign']))
		{
			$model->attributes=$_POST['PaketCampaign'];
			$model->ukuran_gambar1 = Yii::app()->params['imagesize'][$model->ukuran_gambar1];
			$model->ukuran_gambar2 = Yii::app()->params['imagesize'][$model->ukuran_gambar2];
			$model->ukuran_gambar3 = Yii::app()->params['imagesize'][$model->ukuran_gambar3];
			$model->first_user = Yii::app()->user->first_name;
			$model->first_update = date("Y-m-d H:i:s");
			$model->first_ip = CHttpRequest::getUserHostAddress();
			$model->last_user = Yii::app()->user->first_name;
			$model->last_update = date("Y-m-d H:i:s");
			$model->last_ip = CHttpRequest::getUserHostAddress();
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PaketCampaign']))
		{
			$model->attributes=$_POST['PaketCampaign'];
			$model->ukuran_gambar1 = Yii::app()->params['imagesize'][$model->ukuran_gambar1];
			$model->ukuran_gambar2 = Yii::app()->params['imagesize'][$model->ukuran_gambar2];
			$model->ukuran_gambar3 = Yii::app()->params['imagesize'][$model->ukuran_gambar3];
			$model->last_user = Yii::app()->user->first_name;
			$model->last_update = date("Y-m-d H:i:s");
			$model->last_ip = CHttpRequest::getUserHostAddress();
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$sql = "select status_batch from tbl_lba_batch where id_paket_campaign = '".$id."' and (status_batch = 7 or status_batch = 9)";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
		
		if($command > 0)
		{
			$model=$this->loadModel($id);
			Controller::beforeDelete('PaketCampaign',$id);
			Controller::deleted('PaketCampaign',$id);
			Controller::addberita("Delete Paket Campaign : Packet : $model->nama , Price : $model->harga ");

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PaketCampaign');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PaketCampaign('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PaketCampaign']))
			$model->attributes=$_GET['PaketCampaign'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PaketCampaign the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PaketCampaign::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PaketCampaign $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='paket-campaign-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
