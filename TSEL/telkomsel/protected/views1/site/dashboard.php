<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
	var data = google.visualization.arrayToDataTable([
	  ['', ''],
	  ['',  0],
	  ['',  0],
	]);

	var options = {
	  title:"Impressions : 0"
	};

	var chart = new google.visualization.LineChart(document.getElementById('impressions'));
	chart.draw(data, options);
	var chart2 = new google.visualization.LineChart(document.getElementById('CTR'));
	chart2.draw(data, options);
	var chart3 = new google.visualization.LineChart(document.getElementById('Bookings'));
	chart3.draw(data, options);
	var chart4 = new google.visualization.LineChart(document.getElementById('Earnings'));
	chart4.draw(data, options);
	var chart5 = new google.visualization.LineChart(document.getElementById('EstProfit'));
	chart5.draw(data, options);
	var chart6 = new google.visualization.LineChart(document.getElementById('FillRate'));
	chart6.draw(data, options);
  }
</script>

<div class="page-header">
	<h3>Dashboard</h3>
</div>

<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#account">Account</a></li>
  <li><a href="#campaigns">Campaigns In Danger</a></li>
  <li><a href="#channels">Channels</a></li>
</ul>
 
<div class="tab-content">
  <div class="tab-pane active" id="account">
	<div class="well-small">
		<h5>Account - Last 30 Days</h5>
		<hr/>
		<div class="row">
			<div id="impressions" class="span3" style="width: 150px; height: 100px;"></div>
			<div id="CTR" class="span3" style="width: 150px; height: 100px;"></div>
			<div id="Bookings" class="span3" style="width: 150px; height: 100px;"></div>
			<div id="Earnings" class="span3" style="width: 150px; height: 100px;"></div>
			<div id="EstProfit" class="span3" style="width: 150px; height: 100px;"></div>
			<div id="FillRate" class="span3" style="width: 150px; height: 100px;"></div>
		</div>
		<div class="row" style="margin-top:25px;">
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>Impressions</h4>
				  </li>
				  <li>
					0 in last 30 days <span class="percentage positive">(+8%)</span>
				  </li>
				  <li>
					0 in last 60 days <span class="percentage positive">(+8%)</span>
				  </li>
				</ul>
			</div>
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>CTR</h4>
				  </li>
				  <li>
					0.000% in last 30 days <span class="percentage positive">(+8%)</span>
				  </li>
				  <li>
					0.000% in last 60 days <span class="percentage positive">(+8%)</span>
				  </li>
				</ul>
			</div>
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>Bookings</h4>
				  </li>
				  <li>
					$20.00 in last 30 days <span class="percentage ">(0.0%)</span>
				  </li>
				  <li>
					$20.00 in last 60 days <span class="percentage ">(0.0%)</span>
				  </li>
				</ul>
			</div>
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>Earnings</h4>
				  </li>
				  <li>
					$0.00 in last 30 days <span class="percentage positive">(+8%)</span>
				  </li>
				  <li>
					$0.00 in last 60 days <span class="percentage positive">(+8%)</span>
				  </li>
				</ul>
			</div>	
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>Profits</h4>
				  </li>
				  <li>
					$0.00 in last 30 days <span class="percentage positive">(+8%)</span>
				  </li>
				  <li>
					$0.00 in last 60 days <span class="percentage positive">(+8%)</span>
				  </li>
				</ul>
			</div>
			<div class="span3">
				<ul class="unstyled">
				  <li>
					<h4>Fill Rate</h4>
				  </li>
				  <li>
					00.0% in last 30 days <span class="percentage">
					  <span class="percentage ">(0.0%)</span>
					</span>
				  </li>
				  <li>
					00.0% in last 60 days <span class="percentage">
					  <span class="percentage ">(0.0%)</span>
					</span>
				  </li>
				</ul>
			</div>
		</div>
	</div>
  </div>
  <div class="tab-pane" id="campaigns">
	<div class="well-small">
		No Campaigns in Danger
	</div>
  </div>
  <div class="tab-pane" id="channels">
	<table class="table table-striped">
      <colgroup><col>
      <col>
      <col>
      <col>
      <col>
      <col>
      </colgroup><thead>
        <tr class="altrow">
          <th class="firstth">Name</th>
          <th>Imp.</th>
          <th>Fill</th>
          <th>Bookings</th>
          <th>Earnings</th>
          <th class="lastth">Profit</th>
        </tr>
      </thead>
      <tbody>
        <tr class="subrow firstrow lastsubrow">
          <td class="firsttd lasttdleft">All Sites</td>
          <td>
            0<br><span class="percentage positive">(+8%)</span>
          </td>
          <!--
          <td>
            ${roomStat.UniqueVisitors.ToFormatedWithCommasStringNoDec()}<br /><span class="percentage ${roomStat.UniqueVisitorsPct.NegPos()">(${roomStat.UniqueVisitorsPct.PercentAsHtml()})</span>
          </td>-->
          <td>
            00.0%<br><span class="percentage ">(0.0%)</span>
          </td>
          <td>
            $20.00<br><span class="percentage ">(0.0%)</span>
          </td>
          <td>
            $0.00<br><span class="percentage positive">(+8%)</span>
          </td>
          <td class="lasttdright">
            $0.00<br><span class="percentage positive">(+8%)</span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
 
<script>
  $('#myTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	})
</script>