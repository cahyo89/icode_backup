<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <title>AXIS TELEKOM - INDONESIA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="assets/css/default.css" rel="stylesheet">
    <link href="assets/css/DT_bootstrap.css" rel="stylesheet">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/jquery.dataTables.js"></script>
	<script src="assets/js/DT_bootstrap.js"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    
    <!-- Le fav and touch icons 
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    -->
  </head>
  
  <body>
	
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="span5 logo">
					<div>
						<img src="assets/img/adOS-logo.png"/>
					</div>
					<div class="label label-inverse">
						Axis Telekom - Indonesia
					</div>
				</div>
				
				<div class="span7">
					<?php 
						$this->widget('zii.widgets.CMenu',array(
						'activeCssClass'=>'',
						'activateParents'=>true,
						'submenuHtmlOptions'=>array('class'=>'dropdown-menu',),
						'items'=>array(
							array(
								'label'=>'Settings<b class="caret"></b>', 
								'url'=>array('#'),
								'visible'=>!Yii::app()->user->isGuest,
								'itemOptions'=>array('class'=>'dropdown'),
								'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>'dropdown'),
								'items'=>array(
									array('label'=>'Password/User Login', 'url'=>array('/user/profile'),'itemOptions'=>array('id'=>'itemCompany'),),
									array('label'=>'Account Name/Domain', 'url'=>array('/company/index'),'itemOptions'=>array('id'=>'itemCompany'),),
									array('label'=>'Agreements', 'url'=>array('/company/index'),'itemOptions'=>array('id'=>'itemCompany'),),
									array('label'=>'Channels', 'url'=>array('/company/index'),'itemOptions'=>array('id'=>'itemCompany'),),
									array('label'=>'Billing Information', 'url'=>array('/company/index'),'itemOptions'=>array('id'=>'itemCompany'),),
									array('label'=>'API Keys', 'url'=>array('/company/index'),'itemOptions'=>array('id'=>'itemCompany'),),
								  ),
							),
							array('label'=>'Help', 'url'=>array('/site/login'), 'visible'=>!Yii::app()->user->isGuest),
							array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
						),
						'htmlOptions'=>array('class'=>'nav nav-pills pull-right top-nav'),
						'encodeLabel'=>false,
					)); 
					?>
				</div>
			</div>
		</div>
    </div>
	
	
	<?php
	//User
	$roles = Rights::getAssignedRoles(Yii::app()->user->Id);
	$rolein = "(";
	foreach($roles as $role)
	{
		$rolein .= "'".$role->name."',";
	}
	$rolein = rtrim($rolein, ",");
	$rolein .= ")";
	$test = Yii::app()->controller->display_menu($rolein);
	$test = Yii::app()->controller->display_menu_admin();
	
	$role = Yii::app()->authManager->getRoles(Yii::app()->user->id);
	$this->widget('EBootstrapNavigation',array(
					'items'=>$test,
					'htmlOptions' => array(
						'class' => 'navbar-static-top',
					),
					'responsive' => true,
					'encodeLabel' => false,
					'dark' => true,
	));
	?>
	
	<div class="container well">
	
	<?php echo $content; ?>
	
	</div>
	
  </body>
</html>