<?php
$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Create',
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Create Customer</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'customer/index','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<div class="operatorLeft"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'customer/index','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div>