<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'segmen_customer'); ?>
		<?php echo $form->dropDownList($model,'segmen_customer',Yii::app()->params['segmen_customer']); ?>
		<?php echo $form->error($model,'segmen_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipe_customer'); ?>
		<?php echo $form->dropDownList($model,'tipe_customer',Controller::getConstanta('tipe_customer'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'tipe_customer'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'category_customer'); ?>
		<?php echo $form->dropDownList($model,'category_customer',CHtml::listData(TreeId::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'tree_id', 'nama_perangkat'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'category_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bypass_approval'); ?>
		<?php echo $form->dropDownList($model,'bypass_approval',Controller::getConstanta('bypass_approval'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'bypass_approval'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->