<?php

class CsvExportNew {
	
	public static $rows;
	
	public static function addRow($row)
    {
		self::$rows = $row;
	}
	
	//$opt = array("sum"=>array("Anka","Umur"));
    public static function export($data,$opt=array())
    {
        $endLine = "\r\n";
		header("Content-Type: application/force-download");
		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=test.csv");
		
		$head = 0;
		
		foreach($data as $row)
		{
			if($head == 0)
			{
				foreach($row as $key => $line)
				{
					echo $key.",";
				}
					echo $endLine;
			}
			$head = 1;
			foreach($row as $key => $line)
			{
				echo $line.",";
			}
				echo $endLine;
		}

		foreach($opt as $key => $option)
		{
			if($key == "sum")
			{
				$sum = array();
				foreach($option as $field)
				{
					$tmp = 0;
					foreach($data as $row)
					{
						$tmp += $row[$field];
					}
					$sum[$field] = $tmp;
				}
				
				foreach($sum as $key => $row)
				{
					echo $key.",".$row.",".$endLine;
				}
			}
		}
		
		return;
    }
	
}