<?php

/**
 * This is the model class for table "{{lac_profile}}".
 *
 * The followings are the available columns in table '{{lac_profile}}':
 * @property integer $id
 * @property string $profile_name
 * @property string $description
 * @property string $first_ip
 * @property string $first_user
 * @property string $first_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 */
class LacProfile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return LacProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{lac_profile}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profile_name', 'required'),
			array('profile_name', 'unique'),
			array('profile_name', 'length', 'max'=>200),
			array('description', 'length', 'max'=>500),
			array('first_ip, last_ip', 'length', 'max'=>25),
			array('first_user, last_user', 'length', 'max'=>50),
			array('first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, profile_name, description, first_ip, first_user, first_update, last_ip, last_user, last_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'profile_name' => 'Profile Name',
			'description' => 'Description',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('profile_name',$this->profile_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}