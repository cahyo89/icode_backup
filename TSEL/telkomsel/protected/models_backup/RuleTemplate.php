<?php

/**
 * This is the model class for table "{{rule_template}}".
 *
 * The followings are the available columns in table '{{rule_template}}':
 * @property integer $id
 * @property integer $id_customer
 * @property string $pause_time
 * @property string $resume_time
 * @property integer $quota_value
 * @property string $rule_name
 * @property string $description
 * @property string $first_ip
 * @property string $first_user
 * @property string $first_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $reason_reject
 * @property integer $flag
 */
class RuleTemplate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return RuleTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rule_template}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer, quota_value, flag', 'numerical', 'integerOnly'=>true),
			array('rule_name', 'length', 'max'=>100),
			//array('rule_name', 'unique'),
			array('rule_name', 'required'),
			array('first_ip, last_ip', 'length', 'max'=>25),
			array('first_user, last_user', 'length', 'max'=>50),
			array('pause_time, resume_time, description, first_update, last_update, reason_reject', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_customer, pause_time, resume_time, quota_value, rule_name, description, first_ip, first_user, first_update, last_ip, last_user, last_update, reason_reject, flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Customer'=>array(self::BELONGS_TO, 'Customer', 'id_customer')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_customer' => 'Customer',
			'pause_time' => 'Pause Time',
			'resume_time' => 'Resume Time',
			'quota_value' => 'Quota Value',
			'rule_name' => 'Rule Name',
			'description' => 'Description',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'reason_reject' => 'Reason Reject',
			'flag' => 'Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser')){
			$criteria->condition=' id_customer = '.Yii::app()->user->idCustomer.' ';
			$criteria->condition=' id_customer <> -1 ';
		}
		else{
			$criteria->condition=' id_customer <> -1 ';
		}
		
		$criteria->compare('id',$this->id);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('pause_time',$this->pause_time,true);
		$criteria->compare('resume_time',$this->resume_time,true);
		$criteria->compare('quota_value',$this->quota_value);
		$criteria->compare('rule_name',$this->rule_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('reason_reject',$this->reason_reject,true);
		$criteria->compare('flag',$this->flag);
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}