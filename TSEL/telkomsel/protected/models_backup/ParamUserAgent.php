<?php

/**
 * This is the model class for table "{{param_user_agent}}".
 *
 * The followings are the available columns in table '{{param_user_agent}}':
 * @property integer $id
 * @property string $param_name
 * @property string $first_update
 * @property string $first_ip
 * @property string $first_user
 * @property string $last_update
 * @property string $last_ip
 * @property string $last_user
 */
class ParamUserAgent extends CActiveRecord
{
	public $name;
	public $flag_blacklist;
	public $script_name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParamUserAgent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{param_user_agent}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flag_blacklist, name,param_name, first_update, first_ip, first_user, last_update, last_ip, last_user', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('first_ip, last_ip', 'length', 'max'=>25),
			array('name,first_user, last_user', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, param_name, first_update, first_ip, first_user, last_update, last_ip, last_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Title',
			'param_name' => 'User Agent Name',
			'first_update' => 'First Update',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'last_update' => 'Last Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'flag_blacklist'=> 'Flag Blacklist',
			'script_name'=>'Script Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('param_name',$this->param_name,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('flag_blacklist',$this->flag_blacklist);
		$criteria->compare('script_name',$this->script_name);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}