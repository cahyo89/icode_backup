<?php
/* @var $this ParamUserAgentController */
/* @var $model ParamUserAgent */

$this->breadcrumbs=array(
	'Param User Agents'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ParamUserAgent', 'url'=>array('index')),
	array('label'=>'Create ParamUserAgent', 'url'=>array('create')),
	array('label'=>'Update ParamUserAgent', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ParamUserAgent', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);
?>

<h1>View ParamUserAgent #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'param_name',
		'first_update',
		'first_ip',
		'first_user',
		'last_update',
		'last_ip',
		'last_user',
	),
)); ?>
