<?php
$this->breadcrumbs=array(
	'Basic Channel Rate'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChannelCategory', 'url'=>array('index')),
	array('label'=>'Manage ChannelCategory', 'url'=>array('admin')),
);
?>

<h1>Create Basic Channel Rate</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>