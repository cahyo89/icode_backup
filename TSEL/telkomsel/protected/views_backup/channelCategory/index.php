<?php
$this->breadcrumbs=array(
	'Channel Categories',
);

$this->menu=array(
	array('label'=>'Create ChannelCategory', 'url'=>array('create')),
	array('label'=>'Manage ChannelCategory', 'url'=>array('admin')),
);
?>

<h1>Channel Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
