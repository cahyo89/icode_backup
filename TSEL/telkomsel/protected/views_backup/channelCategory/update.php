<?php

$this->breadcrumbs=array(
	'Basic Channel Rate'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>">
<h1>Update Basic Channel Rate<?php echo $model->name; ?></h1>
</table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>