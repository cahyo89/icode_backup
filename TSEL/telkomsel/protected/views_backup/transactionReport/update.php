<?php
$this->breadcrumbs=array(
	'Transaction Reports'=>array('index'),
	$model->id_batch=>array('view','id'=>$model->id_batch),
	'Update',
);

$this->menu=array(
	array('label'=>'List TransactionReport', 'url'=>array('index')),
	array('label'=>'Create TransactionReport', 'url'=>array('create')),
	array('label'=>'View TransactionReport', 'url'=>array('view', 'id'=>$model->id_batch)),
	array('label'=>'Manage TransactionReport', 'url'=>array('admin')),
);
?>

<h1>Update TransactionReport <?php echo $model->id_batch; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>