<?php
$this->breadcrumbs=array(
	'History Globalblackwhitelists'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List HistoryGlobalblackwhitelist', 'url'=>array('index')),
	array('label'=>'Create HistoryGlobalblackwhitelist', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('history-globalblackwhitelist-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage History Globalblackwhitelists</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'history-globalblackwhitelist-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'msisdn',
		'shortcode',
		'sms_text',
		'action',
		'first_update',
		/*
		'first_user',
		'first_ip',
		'last_update',
		'last_user',
		'last_ip',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
