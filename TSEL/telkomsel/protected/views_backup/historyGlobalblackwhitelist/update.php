<?php
$this->breadcrumbs=array(
	'History Globalblackwhitelists'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List HistoryGlobalblackwhitelist', 'url'=>array('index')),
	array('label'=>'Create HistoryGlobalblackwhitelist', 'url'=>array('create')),
	array('label'=>'View HistoryGlobalblackwhitelist', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage HistoryGlobalblackwhitelist', 'url'=>array('admin')),
);
?>

<h1>Update HistoryGlobalblackwhitelist <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>