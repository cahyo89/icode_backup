<?php
$this->breadcrumbs=array(
	'Report Summary Campaign Filter'=>array('index'),
	);
	
$dataType = $_POST['SummaryTransactionHourly']['dataType'];
$dateStart = $_POST['dateStart'];
$dateEnd = $_POST['dateEnd'];
$hourStart = $_POST['SummaryTransactionHourly']['hourStart'];
$hourEnd = $_POST['SummaryTransactionHourly']['hourEnd'];
$customer = $_POST['SummaryTransactionHourly']['customer'];
$campaign = $_POST['SummaryTransactionHourly']['campaign'];
$tipe = $_POST['SummaryTransactionHourly']['tipe'];

//echo $sqlExcel;

$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
$modelCampaign = CHtml::listData(Campaigne::model()->findAll(),'id_batch','jobs_id');
$modelChannel = CHtml::listData(ChannelCategory::model()->findAll(),'id','name');
//$modelLocation = CHtml::listData(Bts::model()->findAll(),'id','bts_name');
?>
<script type="text/javascript">
 
OFC = {};
 
OFC.jquery = {
    name: "jQuery",
    version: function(src) { return $('#'+ src)[0].get_version() },
    rasterize: function (src, dst) { $('#'+ dst).replaceWith(OFC.jquery.image(src)) },
    image: function(src) { return "<img src='data:image/png;base64," + $('#'+src)[0].get_img_binary() + "' />"},
    popup: function(src) {
        var img_win = window.open('', 'Charts: Export as Image')
        with(img_win.document) {
            write('<html><head><title>Charts: Export as Image<\/title><\/head><body>' + OFC.jquery.image(src) + '<\/body><\/html>') }
		// stop the 'loading...' message
		img_win.document.close();
     }
}
 
// Using an object as namespaces is JS Best Practice. I like the Control.XXX style.
//if (!Control) {var Control = {}}
//if (typeof(Control == "undefined")) {var Control = {}}
if (typeof(Control == "undefined")) {var Control = {OFC: OFC.jquery}}
 
 
// By default, right-clicking on OFC and choosing "save image locally" calls this function.
// You are free to change the code in OFC and call my wrapper (Control.OFC.your_favorite_save_method)
// function save_image() { alert(1); Control.OFC.popup('my_chart') }
function save_image() { alert(1); OFC.jquery.popup('my_chart') }
function moo() { alert(99); };


// This is the part where I set up the three adapters.
// Please choose the one you need and discard others.
// I did this because I observed that in some frameworks (especially ExtJS),
// using the standard DOM modifiers breaks up the framework's inner workings.
 
OFC = {}
 
OFC.prototype = {
    name: "Prototype",
    version: function(src) { return $(src).get_version() },
    rasterize: function (src, dst) { $(dst).replace(new Element("img", {src: Control.OFC.image(src)})) },
    image: function(src) {return "data:image/png;base64," + $(src).get_img_binary()},
    popup: function(src) {
        var img_win = window.open('', 'Charts: Export as Image')
        with(img_win.document) {
            write("<html><head><title>Charts: Export as Image<\/title><\/head><body><img src='" + Control.OFC.image(src) + "' /><\/body><\/html>") }
     }
}
 
OFC.jquery = {
    name: "jQuery",
    version: function(src) { return $('#'+ src)[0].get_version() },
    rasterize: function (src, dst) { $('#'+ dst).replaceWith(Control.OFC.image(src)) },
    image: function(src) { return "<img src='data:image/png;base64," + $('#'+src)[0].get_img_binary() + "' />"},
    popup: function(src) {
        var img_win = window.open('', 'Charts: Export as Image')
        with(img_win.document) {
            write('<html><head><title>Charts: Export as Image<\/title><\/head><body>' + Control.OFC.image(src) + '<\/body><\/html>') }
     }
}
 
OFC.none = {
    name: "pure DOM",
    version: function(src) { return document.getElementById(src).get_version() },
    rasterize: function (src, dst) {
      var _dst = document.getElementById(dst)
      e = document.createElement("div")
      e.innerHTML = Control.OFC.image(src)
      _dst.parentNode.replaceChild(e, _dst);
    },
    image: function(src) {return "<img src='data:image/png;base64," + document.getElementById(src).get_img_binary() + "' />"},
    popup: function(src) {
        var img_win = window.open('', 'Charts: Export as Image')
        with(img_win.document) {
            write("<html><head><title>Charts: Export as Image<\/title><\/head><body>" + Control.OFC.image(src) + "<\/body><\/html>") }
     }
}
 
// Using an object as namespaces is JS Best Practice. I like the Control.XXX style.
if (!Control) {var Control = {}}
 
// By default, right-clicking on OFC and choosing "save image locally" calls this function.
// You are free to change the code in OFC and call my wrapper (Control.OFC.your_favorite_save_method)
function save_image() { Control.OFC.popup('img_chart_1') }

//OFC.jquery.rasterize('openFlashChart2_1', 'openFlashChart2_1');
var countImage;
var batas = 0;
function convertImages()
{
	while(batas < countImage){
		var name = 'openFlashChart2_'+batas;
		OFC.jquery.rasterize(name, name);
		batas = batas+1;
	}
	document.getElementById('now').style.display = 'block';
	document.getElementById('mode').style.display = 'none';	
	document.getElementById('view').style.display = 'block';	
	//alert(batas);
	//alert(countImage);
	
	//printImages();
	
}

	function printImages()
	{
		var printContents = document.getElementById('printMe').innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
		
		location.reload();

	}
</script>
<?php if(isset($chart)){?>

	<div class="operatorRight" id="mode" > 
		<input type="BUTTON"  value="Print Mode." onclick="convertImages()">
	</div>
	
	<div class="operatorRight" id="now" style="display:none" > 
		<input type="BUTTON"  value="Print Now." onclick="printImages()">
	</div>
	
	<div class="operatorRight" id="view" style="display:none" > 
		<input type="BUTTON" onclick="location.reload();" value="View Mode.">
	</div>
	
<?php } ?>

<div id="printMe">
<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Report Summary Campaign</h1></td>
<td><div class="operatorRight"><?php echo CHtml::button('Export Data', array('submit' => array('SummaryTransactionHourly/excel'))); ?> <?php  if($dataView == 0 ){  echo CHtml::link(Yii::app()->params['excel'],array('summaryHourlyHistory/view',
										'excel'=>'1',
										'dataView' =>$dataView,
										'dataType'=>$dataType,
										'dateStart'=>$dateStart,
										'dateEnd'=>$dateEnd,
										'hourStart'=>$hourStart,
										'hourEnd'=>$hourEnd,
										'customer'=>$customer,
										'campaign'=>$campaign,
										'shortcode' => $shortcode,
										'channel' => $channel,
										'location' => $location,
										'groupBy' =>$groupBy
										)); } ?></div></td>
					</tr></table>
		
<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

<h4>Customer : <?php echo $customer == "" ?  'All' :  $modeCustomer[$customer] ;  ?></h4>

<h4>Campaign : <?php echo $campaign == "" ?  'All' : $modelCampaign[$campaign] ;  ?></h4>

<h4>Tipe : <?php echo $tipe == "" ?  'All' : Yii::app()->params['tipe_paket'][$tipe] ;  ?></h4>


<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>

<?php
if(isset($chart)){
	$a = 0;
	foreach($chart as $id=>$value){
		$a++;
		$this->widget(
		   'application.extensions.OpenFlashChart2Widget.OpenFlashChart2Widget',
		   array(
			 'chart' => $value,
			 'width' => '100%',
			 'height' => '200'
		   )
		 );
	 }
 
?>
<script type="text/javascript">
	countImage = <?php echo $a ?>
</script>
<?php
 }
?>
<?php 
if(isset($dataProvider)){
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'summary-hourly-history-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(            
            'name'=>'tanggal',
            'value'=>'@$data->tanggal',
        ),
		array(            
            'name'=>'Media Seller',
            'value'=>'@$data->Customer->nama',
        ),
		array(            
            'name'=>'Advertiser',
            'value'=>'@Controller::getCustomer($data->Customer->parent_id)',
        ),
		array(            
            'name'=>'Campaign',
            'value'=>'@$data->Campaigne->jobs_id',
        ),
		array(            
            'name'=>'Type',
            'value'=>'@Yii::app()->params["tipe_paket"][@$data->tipe]',
        ),
		array(            
            'name'=>'Total',
            'value'=>'@$data->total',
        ),
		
		/*
		array(            
            'name'=>'jumlah',
			'type'=>'raw',
            'value'=>'CHtml::link($data->jumlah,array("summaryHourlyHistory/excel","tanggal"=>$data->tanggal,"id_customer"=>$data->id_customer,"id_batch"=>$data->id_batch,"masking_customer"=>$data->masking_customer,"media_type"=>$data->media_type,"error_code"=>$data->error_code,"groupBy"=>"'.$groupBy.'","flag"=>"total","sqlExcel"=>"'.$sqlExcel.'"))',
			'visible'=>$groupBy == "id_batch"? true:false,
        ),
		array(            
            'name'=>'error_code',
            'value'=>'@$data->error_code',
			'visible'=>$groupBy == "error_code"? true:false,
        ),
		array(            
            'name'=>'Description',
            'value'=>'@Yii::app()->params["cErrorSmpp"][@$data->error_code]',
			'visible'=>$groupBy == "error_code"? true:false,
        ),
		
		'shortcode',
		'id_batch',
		'jumlah',
		'result',
		'error_code',
		*/
	),
));
}
 ?>
 </div>