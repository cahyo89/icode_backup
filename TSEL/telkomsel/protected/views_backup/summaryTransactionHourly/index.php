<div class="well form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'POST',
	'enableAjaxValidation'=>false,
)); ?>

<h1>Report Summary Campaign</h1>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<b>*Value for today's report has not been completed. to see the report completely see today -1.</b><br />
	<b>*For graphic view of data grouped by campaign will take more longer.</b>
	<br />
	<?php echo $form->errorSummary($model); ?>
	<br />
	<div class="well-small">
		<?php echo $form->labelEx($model,'dataType'); ?>
		<?php echo $form->dropDownList($model,'dataType',Yii::app()->params['dataType'],
				array('ajax'=>array(
						'type'=>'POST',
						'url'=>CController::createUrl('SummaryTransactionHourly/cekDataType'),
						'data'=>array('dataType' => 'js:this.value'),
						'success' => 'function(retval){$("#insider_div").attr("style",retval);}',
						'error'=> 'function(){alert(\'Bad AJAX\');}',
						'update'=>'#insider_div',
				))
		); ?>
		<?php echo $form->error($model,'dataType'); ?>
	</div>

	
		<div class="well-small">
		<?php echo $form->labelEx($model,'dateStart'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateStart', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			'yearRange'=>'1920:2012',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	<div id="insider_div1">  
	
		<div class="well-small" >
			<?php echo $form->labelEx($model,'dateEnd'); ?>
			<?php
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
				//'model'=>$model,
				'attribute'=>'start',
				'name'=>'dateEnd', // This is how it works for me.
				'value'=>date('Y-m-d'),
				'options'=>array('dateFormat'=>'yy-mm-dd',
				'altFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				'yearRange'=>'1920:2012',
				'showOn'=>'both',
				// 'buttonText'=>'...',
				'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				'buttonImageOnly' => true,
				),
				'htmlOptions'=>array('size'=>'15')
				));
			?>
			<?php echo $form->error($model,'dateEnd'); ?>
		</div>
	
	</div>
	
	<div id="insider_div" style="display:none">  
	
		<div class="well-small">
			<?php echo $form->labelEx($model,'hourStart'); ?>
			<?php echo $form->dropDownList($model,'hourStart',Yii::app()->params['dataHour']); ?>
			<?php echo $form->error($model,'hourStart'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'hourEnd'); ?>
			<?php echo $form->dropDownList($model,'hourEnd',Yii::app()->params['dataHour']); ?>
			<?php echo $form->error($model,'hourEnd'); ?>
		</div>
	
	</div>
			
	<div class="well-small">
		<?php echo $form->labelEx($model,'Group'); ?>
		<?php //echo $form->dropDownList($model,'id_group',CHtml::listData(Bts::model()->findAll(array('condition'=>'type = 1 and (flag_deleted IS NULL or flag_deleted <> 1)', 'order' => 'bts_name ASC')),'id','bts_name'),array('empty'=>'All')); ?>
		<?php echo $form->error($model,'id_group'); ?>
	</div>
		
	<div class="well-small">
		<?php echo $form->labelEx($model,'tipe'); ?>
		<?php echo $form->dropDownList($model,'tipe',Yii::app()->params['tipe_paket'],array('empty'=>'All')); ?>
		<?php echo $form->error($model,'tipe'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'customer'); ?>
		<?php 
			if(Yii::app()->user->checkAccess('Admin'))
			{
				$customer = Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
			}
			else
			{
				$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
				$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
			}
			if(Yii::app()->user->getUserMode()==0){
				echo $form->dropDownList($model,'customer',CHtml::listData($customer,'id_customer','nama'),
						array('empty'=>'All',
								'ajax'=>array(
												'type'=>'post',
												'dataType'=>'json',
												'url'=>CController::createUrl('cekCampaign'),
												'data'=>array(	'dataCus' => 'js:this.value',
																'dataType' => 'js:$(\'#SummaryTransactionHourly_dataType\').val()',
																'dataDateStart' => 'js:$(\'#dateStart\').val()',
																'dataDateEnd' => 'js:$(\'#dateEnd\').val()',
																'id_group' => 'js:$(\'#SummaryTransactionHourly_id_group\').val()',
																'dataHourStart' => 'js:$(\'#SummaryTransactionHourly_hourStart\').val()',
																'dateHourEnd' => 'js:$(\'#SummaryTransactionHourly_hourEnd\').val()'
																),
												'success' => 'function(data)
												{
													$("#SummaryTransactionHourly_campaign").html(data.campaignD);
												}',
												
												'error'=> 'function(){alert(\'Bad AJAX\');}',
										)	
							)	
		
		
			);}
				else{
					echo $form->dropDownList($model,'customer',CHtml::listData(Customer::model()->findAll(array('condition'=>'(id_customer ='.Yii::app()->user->id.' ) and flag_deleted IS NULL or flag_deleted <> 1','order'=>'nama')),'id_customer','nama'),
						array('empty'=>'All',
								'ajax'=>array(
												'type'=>'post',
												'dataType'=>'json',
												'url'=>CController::createUrl('cekCampaign'),
												'data'=>array(	'dataCus' => 'js:this.value',
																'dataType' => 'js:$(\'#SummaryTransactionHourly_dataType\').val()',
																'dataDateStart' => 'js:$(\'#SummaryTransactionHourly_dateStart\').val()',
																'dataDateEnd' => 'js:$(\'#SummaryTransactionHourly_dateEnd\').val()',
																'id_group' => 'js:$(\'#SummaryTransactionHourly_id_group\').val()',
																'dataHourStart' => 'js:$(\'#SummaryTransactionHourly_hourStart\').val()',
																'dateHourEnd' => 'js:$(\'#SummaryTransactionHourly_hourEnd\').val()'
																),
												'success' => 'function(data)
												{
													$("#SummaryTransactionHourly_campaign").html(data.campaignD);
												}',
												
												'error'=> 'function(){alert(\'Bad AJAX\');}',
										)	
							)	
		
		
						);
		}
		?>
		<?php echo $form->error($model,'customer'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'campaign'); ?>
		<?php echo $form->dropDownList($model,'campaign',array(''=>'All')); ?>
		<?php echo $form->error($model,'campaign'); ?>
	</div>
			
	<div class="well-small submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->
