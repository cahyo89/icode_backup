<?php
$this->breadcrumbs=array(
	'Lac Profiles',
);

$this->menu=array(
	array('label'=>'Create LacProfile', 'url'=>array('create')),
	array('label'=>'Manage LacProfile', 'url'=>array('admin')),
);
?>

<h1>Lac Profiles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
