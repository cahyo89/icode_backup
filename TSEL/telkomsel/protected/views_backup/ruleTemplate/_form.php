<div class="well form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rule-template-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well-small">
		<?php echo $form->labelEx($model,'rule_name'); ?>
		<?php echo $form->textField($model,'rule_name',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'rule_name'); ?>
	</div>
	
<div class="well-small">
		<?php echo $form->labelEx($model,'id_customer'); ?>
		<?php 
				if(Yii::app()->user->checkAccess('Admin'))
				{
					$customer = Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
				}
				else
				{
					$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
					if($custHead)
					$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					else
					{	
						$user = User::model()->findByPk(Yii::app()->user->id);
						$custHead = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and id_customer = '.$user->id_customer);
						$customer = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					}
				}
		?>
		<?php echo $form->dropDownList($model,'id_customer',CHtml::listData($customer, 'id_customer', 'nama'),array('class'=>'required','onchange'=>'{onChangeCustomer();}','empty'=>'--Please Select One--'));?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'pause_time'); ?>
		<?php                    
		$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
		        'language'=>'',
		        'model'=>$model,                                // Model object
		        'attribute'=>'pause_time', // Attribute name//'minDate'=>date('Y-m-d'),  'hourMin' => (int)date('h')
		        'mode'=>'time',                     // Use "time","date" or "datetime" (default)
		        'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'changeMonth'=>'true',
					'changeYear'=>'true',
					'hourGrid'=>5,
					'minuteGrid'=>10,
					'yearRange'=>'1920:2013',
					'showOn'=>'both',
					'buttonImageOnly'=>'true',
					'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				),                       
		        'htmlOptions'=>array('readonly'=>true), // HTML options
		));                             
		?>
		<?php echo $form->error($model,'pause_time'); ?>
	</div>

	<div class="well-small" >
		<?php echo $form->labelEx($model,'resume_time'); ?>
		<?php 
			$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
		        'language'=>'',
		        'model'=>$model,                                // Model object
		        'attribute'=>'resume_time', // Attribute name//'minDate'=>date('Y-m-d'),  'hourMin' => (int)date('h')
		        'mode'=>'time',                     // Use "time","date" or "datetime" (default)
		        'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'changeMonth'=>'true',
					'changeYear'=>'true',
					'hourGrid'=>5,
					'minuteGrid'=>10,
					'yearRange'=>'1920:2013',
					'showOn'=>'both',
					'buttonImageOnly'=>'true',
					'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				),                       
		        'htmlOptions'=>array('readonly'=>true), // HTML options
		));          
		?>
		<?php echo $form->error($model,'resume_time'); ?>
	</div>
	


	<div class="well-small buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->