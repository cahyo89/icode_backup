<?php
$this->breadcrumbs=array(
	'Rule Templates',
);

$this->menu=array(
	array('label'=>'Create RuleTemplate', 'url'=>array('create')),
	array('label'=>'Manage RuleTemplate', 'url'=>array('admin')),
);
?>

<h1>Rule Templates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
