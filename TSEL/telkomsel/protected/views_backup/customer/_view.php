<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_customer')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_customer), array('view', 'id'=>$data->id_customer)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_customer')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prepaid_value')); ?>:</b>
	<?php echo CHtml::encode($data->prepaid_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_ani')); ?>:</b>
	<?php echo CHtml::encode($data->customer_ani); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->flag_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usage')); ?>:</b>
	<?php echo CHtml::encode($data->usage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('booking')); ?>:</b>
	<?php echo CHtml::encode($data->booking); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_customer')); ?>:</b>
	<?php echo CHtml::encode($data->category_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_paket')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expired_date')); ?>:</b>
	<?php echo CHtml::encode($data->expired_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved')); ?>:</b>
	<?php echo CHtml::encode($data->approved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
	<?php echo CHtml::encode($data->reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_prepaid')); ?>:</b>
	<?php echo CHtml::encode($data->temp_prepaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason_prepaid')); ?>:</b>
	<?php echo CHtml::encode($data->reason_prepaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_prepaid')); ?>:</b>
	<?php echo CHtml::encode($data->approved_prepaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('segmen_customer')); ?>:</b>
	<?php echo CHtml::encode($data->segmen_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bypass_approval')); ?>:</b>
	<?php echo CHtml::encode($data->bypass_approval); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quota_periode')); ?>:</b>
	<?php echo CHtml::encode($data->quota_periode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quota_value')); ?>:</b>
	<?php echo CHtml::encode($data->quota_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('blocked')); ?>:</b>
	<?php echo CHtml::encode($data->blocked); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('blocked_reason')); ?>:</b>
	<?php echo CHtml::encode($data->blocked_reason); ?>
	<br />

	*/ ?>

</div>