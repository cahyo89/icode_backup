<?php
$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('customer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Customer Management</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>


<?php if(Yii::app()->user->hasFlash('error')): ?>
	<br></br>
	<div class="flash-info flash flash-block">
	    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
	</div>

	<?php endif; ?>

<?php 
	
	
	
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-grid',
	'dataProvider'=>$model,
	'columns'=>array(
		array(
			'name'=>'nama',
			'type'=>'raw',
			'value'=>'CHtml::link($data->nama,array("customer/view","id"=>$data->id_customer))',
		),
		array(
			'name'=>'segmen_customer',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["segmen_customer"][$data->segmen_customer]',
		),
		array(
			'name'=>'tipe_customer',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["tipe_customer"][$data->tipe_customer]',
		),
		array(
			'name'=>'category_customer',
			'value'=>'@$data->TreeId->nama_perangkat',
		),
		array(
			'name'=>'tipe_paket',
			'value'=>'@$data->Paket->jenis_paket',
		),
		array(
			'name'=>'bypass_approval',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["bypass_approval"][$data->bypass_approval]',
		),
		'prepaid_value',
		array(
			'name'=>'approved',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["approved"][$data->approved]',
		),
		array(
			'name'=>'blocked',
			'type'=>'raw',
			'value'=>'@Yii::app()->params["blocked"][$data->blocked]',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
		    'buttons'=>array
		    (
		        
		        'delete' => array
		        (
		        	'visible'=>true,
					
		        ),
		    ),
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'customer/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>