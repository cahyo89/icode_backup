<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	
	<div class="row">
		<?php 
					echo $form->labelEx($model,'Customer Name');
					echo $form->dropDownList($model,'id_customer',CHtml::listData(Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted ORDER by nama asc',array(':flag_deleted'=>1)), 'id_customer', 'nama'),
					array('empty'=>'--Please Select One--'));
		?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Campaign Id'); ?>
		<?php echo $form->textField($model,'jobs_id',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_periode'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'start_periode',
        'name'=>$model->start_periode,    // This is how it works for me.
        'value'=>$model->start_periode,
        'options'=>array('dateFormat'=>'yy-mm-dd', 
                        'altFormat'=>'yy-mm-dd', 
                        'changeMonth'=>'true', 
                        'changeYear'=>'true', 
                        'yearRange'=>'1920:2012', 
                        'showOn'=>'both',
                       // 'buttonText'=>'...',
						'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
						'buttonImageOnly' => true,
						),
        'htmlOptions'=>array('size'=>'15')
   )); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'content_expired'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'content_expired',
        'name'=>$model->content_expired,    // This is how it works for me.
        'value'=>$model->content_expired,
        'options'=>array('dateFormat'=>'yy-mm-dd', 
                        'altFormat'=>'yy-mm-dd', 
                        'changeMonth'=>'true', 
                        'changeYear'=>'true', 
                        'yearRange'=>'1920:2012', 
                        'showOn'=>'both',
                       // 'buttonText'=>'...',
						'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
						'buttonImageOnly' => true,
						),
        'htmlOptions'=>array('size'=>'15')
   )); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'Status Campaign'); ?>
		<?php 
			echo $form->dropDownList($model,'status_batch',Yii::app()->params['camstatus'],
			array('empty'=>'--Please Select One--','onchange'=>'{onChangeChannelCategory();}')); 
		?>
		<?php echo $form->error($model,'status_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Topic'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Customer Masking'); ?>
		<?php 
			//echo $form->dropDownList($model,'customer_ani',CHtml::listData(Ani::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND status = 1 and flag_deleted IS NULL or flag_deleted <> 1 order by ani asc'), 'ani', 'ani'),array('empty'=>'--Please Select One--'));
		?>
		<?php echo $form->error($model,'customer_ani'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Channel Category'); ?>
		<?php 
			echo $form->dropDownList($model,'channel_category',Yii::app()->params['channelcatagory'],
			array('empty'=>'--Please Select One--','onchange'=>'{onChangeChannelCategory();}')); 
		?>
		<?php echo $form->error($model,'channel_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Priority Campaign'); ?>
		<?php echo $form->dropDownList($model,'priority_category',Yii::app()->params['priority'],array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'priority_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Advertising Sending Type'); ?>
		<?php echo $form->dropDownList($model,'ast',Yii::app()->params['adssendingtype']); ?>
		<?php echo $form->error($model,'ast'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Cell Group Area'); ?>
		<?php //echo $form->dropDownList($model,'cell',CHtml::listData(BtsGroup::model()->findAll('type = 1 order by bts_name asc',array()), 'id', 'bts_name'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'cell'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->