<?php
$this->breadcrumbs=array(
	'Group Area'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bts-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Group Area</h1></td><td><div class="operatorRight"><?php 
									echo CHtml::link('Export Report to CSV',array('btsGroup/index',
																			'excel'=>'1',
																			));
									?></div>
									</td>
									<td><div class="operatorRight"><?php
									
									Controller::createMenu(array(
										array('label'=>'New','link'=>'btsGroup/create','img'=>'/images/new.png','id'=>'','conf'=>'')
									));  
								?></div></td>
					</tr></table>
					
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bts-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'bts_name',
			'type'=>'raw',
			'value'=>'CHtml::link($data->bts_name,array("btsGroup/view","id"=>$data->id))',
		),
		'description',
		array(
		'name'=>'id_loc_stat',
		'value'=>'@$data->LocationStat->location_stat_name',
		),
		/*
		'latitude',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'lac',
		'show_flag',
		'loc_stat_ip',
		'flag_deleted',
		'id_loc_stat',
		'gis_id',
		'gis_name',
		'gis_date',
		'sector_type',
		*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'btsGroup/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>