<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="row">
		<?php echo $form->label($model,'bts_name'); ?>
		<?php echo $form->textField($model,'bts_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>100)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'id_loc_stat'); ?>
		<?php echo $form->dropDownList($model,'id_loc_stat',CHtml::listData(LocationStat::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_loc_stat', 'location_stat_name'),array('empty'=>'--Please Select One--')); ?>
		</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->