<script>
$(function(){
$("#buttonAdd").click(function(){

$("#BtsGroup_temp option:selected").each(function(){
$(this).remove().appendTo("#BtsGroup_slot");
});
});
$("#buttonRemove").click(function(){
$("#BtsGroup_slot option:selected").each(function(){
var flag = 0;
var data=$("#bts-form").serialize();
var temp = $("#BtsGroup_slot option:selected").val();
/*
$.ajax({
type: 'POST',
url: '<?php echo Yii::app()->createAbsoluteUrl("rights/authItem/updatechild"); ?>',
data:data,
error: function(data) { // if error occured
alert("Error occured.please try again");
flag = 1;
},
dataType:'html'
});
*/
if(flag == 0)
{
$(this).remove().appendTo("#BtsGroup_temp");
}
else
{
$("#BtsGroup_slot option:selected").appendTo(temp);
}
});
});
});
</script>
<div class="form well">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bts-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="well-small">
		<?php echo $form->labelEx($model,'bts_name'); ?>
		<?php echo $form->textField($model,'bts_name',array('size'=>75,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'bts_name'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'id_loc_stat'); ?>
		<?php echo $form->dropDownList($model,'id_loc_stat',CHtml::listData(LocationStat::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_loc_stat', 'location_stat_name'),array('empty'=>'--No Profile--')); ?>
		<?php echo $form->error($model,'id_loc_stat'); ?>
	</div>
	<br />
	<table>
	<tr>
		<td>
			Lac: 
		</td>
		<td>
			<?php echo CHtml::activeTextField($model,'searchLac',array('size'=>16,'maxlength'=>30)); ?>
		</td>
	</tr>
	<tr>
		<td>
			CI: 
		</td>
		<td>
			<?php echo CHtml::activeTextField($model,'searchCi',array('size'=>16,'maxlength'=>30)); ?>
		</td>
	</tr>
	<tr>
		<td>
			Site Name: 
		</td>
		<td>
			<?php echo CHtml::activeTextField($model,'searchName',array('size'=>16,'maxlength'=>30)); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::button('Search', array('onclick'=>'onBlurSearch();')); ?>	
		</td>
		<td>
			<div id="loading" class='loading1'></div>
		</td>
	</tr>
	</table>
	<div class="well-small">
			<?php echo $form->labelEx($model,'flag_blacklist'); ?>
			<?php echo $form->dropDownList($model,'flag_blacklist',array(1=>"Normal",0=>"Blacklist"),array('class' => 'required')); ?>
			<?php echo $form->error($model,'flag_blacklist'); ?>
		</div>
	<div class="well-small">
		
		<?php echo $form->dropDownList($model, 'temp', array(),array('multiple'=>true ,'style'=>'width:400px;','size'=>'18')); ?>
		<?php echo $form->dropDownList($model, 'slot', $pp ,array('multiple'=>true ,'style'=>'width:400px;','size'=>'18')); ?>
		<?php echo $form->error($model, 'port'); ?>	
		</div>
		 
		<div class="well-small buttons">
		<input id="buttonAdd" type="button" value="Add" />
		<input id="buttonRemove" type="button" value="Remove" />
	
	</div>
	

	<div class="well-small buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('onclick'=>'$("#BtsGroup_slot option").attr("selected","selected");')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
	function onBlurSearch(){
		if(document.getElementById('BtsGroup_searchLac').value != '' || document.getElementById('BtsGroup_searchCi').value != '' || document.getElementById('BtsGroup_searchName').value != ''){
			
			if(document.getElementById('BtsGroup_searchLac').value != ''){
				if(document.getElementById('BtsGroup_searchLac').value.length < 3 ){
					alert("Minimum length for Lac is 3");
					return false;
				}
			}
			if(document.getElementById('BtsGroup_searchCi').value != ''){
				if(document.getElementById('BtsGroup_searchCi').value.length < 3){
					alert("Minimum length for CI is 3");
					return false;
				}
			}
		
			<?php echo CHtml::ajax(array(
						'url'=>CController::createUrl('searchT'),
						'data'=>array('dataL'=>'js:$(\'#BtsGroup_searchLac\').val()',
										'dataC'=>'js:$(\'#BtsGroup_searchCi\').val()',
										'dataN'=>'js:$(\'#BtsGroup_searchName\').val()'),
						'type'=>'POST',
						'dataType'=>'json',
						'beforeSend' => 'function(){
							$("#loading").addClass("loading");}',
						'success'=>'function(data)
						{
							$("#BtsGroup_temp").html(data.dataa);
						 }
						 ',
						'complete' => 'function(){
							$("#loading").removeClass("loading");}',
						 'error'=> 'function (xhr, ajaxOptions, thrownError) {
									alert(xhr.status);
									alert(thrownError);}'
						))?>;
				
			return false;
		}
		else{
			return false;
		}
	}
</script>