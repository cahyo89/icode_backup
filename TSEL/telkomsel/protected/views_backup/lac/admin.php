<?php
$this->breadcrumbs=array(
	'Lacs'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('lac-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Lac Management</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'lac/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if(Yii::app()->user->hasFlash('error')): ?>
	<br></br>
	<div class="flash-info flash flash-block">
	    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
	</div>

	<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'lac-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		
		array(
			'name'=>'lac_name',
			'type'=>'raw',
			'value'=>'CHtml::link($data->lac_name,array("lac/view","id"=>$data->id))',
		),
		'description',
		'lac_id',
		'host',
		'port',
		array(
		'name'=>'id_probing',
		'value'=>'@$data->Probing->probing_name',
		),
		/*
		'first_ip',
		'first_update',
		'last_user',
		'last_ip',
		'last_update',
		
		'port',
		
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
		    'buttons'=>array
		    (
		        
		        'delete' => array
		        (
		        	'visible'=>'Controller::cekExist($data->lac_id,"lac","Bts") ? true : false',
					
		        ),
		    ),
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>
<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'lac/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>