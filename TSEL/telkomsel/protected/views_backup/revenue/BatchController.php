<?php

class BatchController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('dodo','dynamiccities,getStatusBatch,media,bts,group'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$dataProvider=new CActiveDataProvider('BatchSlotTime',array(
		'criteria'=>array(
		'condition'=>'id in(select slot_id from tbl_batch_slot where batch_id = :id)',
		 'params'=>array(':id'=>$id),
			),
			));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Batch;

		// Uncomment the following line if AJAX validation is needed
		 //$this->performAjaxValidation($model);
		
		if(isset($_POST['Batch']))
		{
			$model->attributes=$_POST['Batch'];
			
			if(isset($_POST['slot'])){
				
					if(Controller::cekSlot($_POST['Batch'],$_POST['slot'])){
						if(Controller::cekPrice($_POST['Batch'],$_POST['slot'])){
							$model->price = $model->price * count($_POST['slot']);
							if($model->save())
								{	
									$last = Yii::app()->db->getLastInsertID();
									Controller::aftersave("Batch",8,$last);
									Controller::cekSlot($_POST['Batch'],$_POST['slot'],$last,false);
									Controller::minBalanceValue($last);
									$this->redirect(array('view','id'=>$last));
								}
						}else
						$model->addError('error','Price > Balance Value');
					}else
					$model->addError('error','Slot already exist');
			}else
			$model->addError('error','Please Choose Slot');
			}
		$model->message_type = "";
		$model->broadcast_type = "";
		$model->group_id = "";
		$model->price = "";
		$this->render('create',array(
			'model'=>$model,
			'stat'=>0,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$message_type = $model->message_type;
		$broadcast_type = $model->broadcast_type;
		if($model->status_batch == 0 || $model->status_batch == NULL){
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if(isset($_POST['Batch']))
			{	
	
					
						if($model->status_batch == 0 || $model->status_batch == NULL){
							$model->attributes=$_POST['Batch'];
							$model->message_type = $message_type;
							$model->broadcast_type = $broadcast_type;
							$data = Controller::beforeupdate("Batch",$id,8);
							if($model->save()){
								Controller::afterupdate("Batch",$id,$data,8);
								$this->redirect(array('view','id'=>$model->id));
								}
						}
						else{
							Yii::app()->user->setFlash('error', 'Unable to update a batch whose status is other than IDLE');
							$this->redirect(array('index'));
							}

				
			}
			$this->render('update',array(
				'model'=>$model,
				'stat'=>1,
			));
		}
		else{
			Yii::app()->user->setFlash('error', 'Unable to update a batch whose status is other than IDLE');
			$this->redirect(array('index'));
			}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=$this->loadModel($id);
		if($model->status_batch == 0 || $model->status_batch == NULL)
				{
					if(Yii::app()->request->isPostRequest)		
						{
				
					// we only allow deletion via POST request
						$data = Controller::beforeupdate("Batch",$id,8);
						Controller::afterdelete($data,8);
						Controller::plusBalanceValue($id);
						Controller::deleted("Batch",$id);
						
					// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
						if(!isset($_GET['ajax']))
							$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
					
					}
					else{
					
						Yii::app()->user->setFlash('error', 'Unable to delete a batch whose status is other than IDLE');
						$this->redirect(array('index'));
					}
				}
				else
				{
				Yii::app()->user->setFlash('error', 'Unable to delete a batch whose status is other than IDLE');
				$this->redirect(array('index'));
				}
				//throw new CHttpException('Tidak dapat men-Delete batch yang statusnya selain IDLE');
	}

	/**
	 * Lists all models.
	 */
	

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Batch('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Batch']))
			$model->attributes=$_GET['Batch'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Batch::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='batch-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionDynamiccities()
	{
	$data = ProductCategory::model()->findAll('id in (select id_product_category from tbl_advertiser_category where id_advertiser = 2)',
	array(':a'=>(int) $_GET['id_advertiser'])
	);
    $data=CHtml::listData($data,'id','name');
    foreach($data as $value=>$id)
    {
        echo CHtml::tag('option',
                   array('value'=>$value),CHtml::encode($name),true);
    }
	}	
public function actionDodo()
{

	$id_advertiser = Yii::app()->request->getParam('id_advertiser');
	
	$name = Advertiser::model()->findByPk($id_advertiser);
	$advertisername = $name->name.Controller::genRandomString();
	$data = ProductCategory::model()->findAll('id in (select id_product_category from tbl_advertiser_category where id_advertiser = :a)',
	array(':a'=>$id_advertiser)
	);
	$datab = Ani::model()->findAll('id_advertiser = :a and status = 1 and (deleted is NULL or deleted<> 1)',
	array(':a'=>$id_advertiser)
	);
	$datab = CHtml::listData($datab,'ani','ani');
	
    $data=CHtml::listData($data,'id','name');
	$dropDownA = "";
	$dropDownB = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($data as $value=>$name)
		   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
		   
		 $dropDownB .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($datab as $value=>$ani)
		  $dropDownB .= CHtml::tag('option', array('value'=>$ani),CHtml::encode($ani),true);
		 

		 
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'dataa'=>$dropDownA,
				'datab'=>$dropDownB,
				'id_advertiser'=>$advertisername,
			)); 
			  Yii::app()->end();
	
	
}
public function actionBts()
{
	
	$group_id = Yii::app()->request->getParam('group_id');
	$message_type = Yii::app()->request->getParam('message_type');
	
	if($group_id != -1){
	$location = BtsGroup::model()->findByPk($group_id);
	$location_price = LocationCategory::model()->findByPk($location->location_category);
	$price = $location_price->category_price;
	}
	else
	{
		if($message_type == 1){
		$param = GlobalParam::model()->find('id = 1');
		$price = $param->value;
		}
		else{
		$param = GlobalParam::model()->find('id = 2');
		$price = $param->value;
		}
	}
	
	
	
	
	//$price = 1500000;
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'price'=>$price,
			)); 
			  Yii::app()->end();
	
	
}


public function actionGrand()
{
	
	$price = Yii::app()->request->getParam('price');
	$slot = Yii::app()->request->getParam('slot');
	if(count($slot) == 0)
	$price = 0;
	else
	$price = $price * count($slot);
	
	//$price = 1500000;
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'price'=>$price,
			)); 
			  Yii::app()->end();
	
	
}
	
public function actionMedia()
{

	$id_media_seller = Yii::app()->request->getParam('id_media_seller');
	
	$data = Advertiser::model()->findAll('id_media_seller =  :a',
	array(':a'=>$id_media_seller)
	);
    $data=CHtml::listData($data,'id','name');
	$dropDownA = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($data as $value=>$name)
		   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
		 
//			foreach($dataB as $value=>$name)
	//	   $dropDownB .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
		 
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'dataa'=>$dropDownA,
			)); 
			  Yii::app()->end();
	
	
}	

public function actionMessage()
{

	
		 $dropDownA = "";
		 $dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
		 $dropDownA .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('National Wide'),true);
		 $dropDownA .= CHtml::tag('option',array('value'=>'1'),CHtml::encode('Specific Area'),true);
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'dataa'=>$dropDownA,
			)); 
			  Yii::app()->end();
	
	
}	

public function actionDialog()
{

	$price = Yii::app()->request->getParam('price');
	$slot = Yii::app()->request->getParam('slot');
	//$slot = null;
	$nameSlot= "";
	if($slot != 'null' ){
	$x = 0;
	$a = "";
	while($x < count($slot))
	{
	$a .= $slot[$x].',';
	$x++;
	}
	$a = substr($a,0,-1);
	$sql = "select * from tbl_batch_slot_time where id in ($a)";
	$connection = Yii::app()->db;
	$row = $connection->createCommand($sql)->queryAll();
	$z = 0;
	$nameSlot = "";
	while($z < count($row))
	{
	$nameSlot .= $row[$z]['name'].", ";
	$z++;
	}
	$nameSlot = substr($nameSlot,0,-2);
	}
	
		$final="Price: ".$price * count($slot)."\nSlot: ".$nameSlot."\n\n Are Your Sure ??";
			/*
	$final="Price: ".$price * count($slot)."\nSlot: ".$slot."\n\n Are Your Sure ??";
	if($slot != 'null' ){
	$final = "\nSlot: ".$slot."\n\n Are Your Sure ??";
	}*/
			echo $final; 
			  Yii::app()->end();
	
	
}	



public function actionTes()
{

	$id_media_seller = Yii::app()->request->getParam('id_media_seller');
	
	$data = Advertiser::model()->findAll('id_media_seller =  :a',
	array(':a'=>$id_media_seller)
	);
    $data=CHtml::listData($data,'id','name');
	$dropDownA = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($data as $value=>$name)
		   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
		 
//			foreach($dataB as $value=>$name)
	//	   $dropDownB .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
		 
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'dataa'=>$dropDownA,
			)); 
			  Yii::app()->end();
	
	
}	


public function actionGroup()
{
	$price = '';
	$broadcast_type = Yii::app()->request->getParam('broadcast_type');
	$message_type = Yii::app()->request->getParam('message_type');
	if($broadcast_type == 1){
	$dataslot = BatchSlotTime::model()->findAll('broadcast_type = :type',array(':type'=>1));
	$data = BtsGroup::model()->findAll('id != -1 and (deleted IS NULL or deleted <> :deleted) ',array(':deleted'=>1));
	$data=CHtml::listData($data,'id','name');
	$dropDownA = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($data as $value=>$name)
		   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
	}
	else{
	$dataslot = BatchSlotTime::model()->findAll('broadcast_type = 0');
	$data = BtsGroup::model()->findAll('id = -1');
	$data=CHtml::listData($data,'id','name');
	$dropDownA = "";
			//$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
			foreach($data as $value=>$name)
		   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
	if($message_type == 1){
		$param = GlobalParam::model()->find('id = 1');
		$price = $param->value;
		}
		else{
		$param = GlobalParam::model()->find('id = 2');
		$price = $param->value;
		}
		   
		   
	}
  
	$dataslot = CHtml::listData($dataslot,'id','name');
	$slot = "";
			
			foreach($dataslot as $value=>$name)
		   $slot .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
				
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'dataa'=>$dropDownA,
				'slot'=>$slot,
				'price'=>$price,
			)); 
			  Yii::app()->end();
	
	
}	

public function getStatusBatch($id)
{
	if($id == 0 || $id == NULL)
		$status = 'Idle';
		else if($id == 1)
		$status = 'Approved';
		else if($id == 2)
		$status = 'Progres';
		else if($id == 7)
		$status = 'Stoped / Rejected';
		else if($id == 9)
		$status = 'Finish';
		else if($id == 11)
		$status = 'Scheduled';
	//return $status;

}

public function actionTable()
	{
	
	$option_count = Yii::app()->request->getParam('option_count');
	$menu = "<table width='200' border='1'>
  <tr>
    <td>asdads</td>
  </tr>
</table>";
			// return data (JSON formatted)
			/*echo CJSON::encode(array(
				'dataa'=>$menu,
			)); */
			echo $menu;
			  Yii::app()->end();
	
	
		}	


}
