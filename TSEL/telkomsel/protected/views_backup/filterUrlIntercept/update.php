<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FilterUrlIntercept', 'url'=>array('index')),
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
	array('label'=>'View FilterUrlIntercept', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FilterUrlIntercept', 'url'=>array('admin')),
);
?>

<h1>Update FilterUrlIntercept <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>