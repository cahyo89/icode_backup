<?php
$this->breadcrumbs=array(
	'Customer Category'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tree-id-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if(Yii::app()->user->hasFlash('error')): ?>
 
<div class="flash-success">
    <font color="#000000" size="+1" style="background-color: #D2FFFE;"><?php echo Yii::app()->user->getFlash('error'); ?></font>
</div>
 
<?php endif; ?>


<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Customer Category</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'treeId/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tree-id-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'nama_perangkat',
			'type'=>'raw',
			'value'=>'CHtml::link($data->nama_perangkat,array("treeId/view","id"=>$data->tree_id))',
		),
		/*
		'first_update',
		'last_user',
		'last_ip',
		'last_update',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
		    'buttons'=>array
		    (
		        
		        'delete' => array
		        (
		        	'visible'=>'Controller::cekExist($data->tree_id,"category_customer","Customer") ? true : false',
					
		        ),
		    ),
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'treeId/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>