<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('tree_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->tree_id), array('view', 'id'=>$data->tree_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usermode')); ?>:</b>
	<?php echo CHtml::encode($data->usermode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_perangkat')); ?>:</b>
	<?php echo CHtml::encode($data->nama_perangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag_deleted')); ?>:</b>
	<?php echo CHtml::encode($data->flag_deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	*/ ?>

</div>