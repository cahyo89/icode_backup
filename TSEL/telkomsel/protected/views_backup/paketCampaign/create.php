<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Paket Campaign', 'url'=>array('index')),
	array('label'=>'Manage Paket Campaign', 'url'=>array('admin')),
);
?>

<h1>Create Paket Campaign</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>