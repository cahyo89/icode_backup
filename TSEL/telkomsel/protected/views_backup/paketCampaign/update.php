<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PaketCampaign', 'url'=>array('index')),
	array('label'=>'Create PaketCampaign', 'url'=>array('create')),
	array('label'=>'View PaketCampaign', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PaketCampaign', 'url'=>array('admin')),
);
?>

<h1>Update PaketCampaign <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>