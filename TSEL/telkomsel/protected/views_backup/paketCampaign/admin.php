<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Paket Campaign', 'url'=>array('index')),
	array('label'=>'Create Paket Campaign', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#paket-campaign-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Paket Campaigns</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'paket-campaign-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'harga',
		array(
		'name'=>'tipe',
		'value'=>'Yii::app()->params[\'tipe_paket\'][$data->tipe]',
		),
		'total',
		array(
		'name'=>'posisi',
		'value'=>'Yii::app()->params[\'http_intercept_ads_value\'][$data->posisi]',
		),
		/*
		'ads_start_time',
		'ads_time_out',
		'ukuran_gambar1',
		'ukuran_gambar2',
		'ukuran_gambar3',
		'priority',
		'first_user',
		'first_update',
		'first_ip',
		'last_user',
		'last_update',
		'last_ip',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
