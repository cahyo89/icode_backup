<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */
/* @var $form CActiveForm */
?>
<?php			
	$this->widget('ext.sformwizard.SFormWizard',array(
		'selector'=>"#paket-campaign-form",
		'disableUIStyles' => "true",
		'validationEnabled' => "true",
	));
?>
<div class="form well">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paket-campaign-form',
	'enableAjaxValidation'=>false,
)); ?>
	<div class="step" id="Step1">
		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?php echo $form->errorSummary($model); ?>

		<div class="well-small">
			<?php echo $form->labelEx($model,'nama'); ?>
			<?php echo $form->textField($model,'nama',array('class'=>'required input-xlarge','size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'nama'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'harga'); ?>
			<?php echo $form->textField($model,'harga',array('class'=>'required digits')); ?>
			<?php echo $form->error($model,'harga'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'tipe'); ?>
			<?php echo $form->dropDownList($model,'tipe',Yii::app()->params['tipe_paket'],array('class' => 'required')); ?>
			<?php echo $form->error($model,'tipe'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'total'); ?>
			<?php echo $form->textField($model,'total',array('class'=>'required digits')); ?>
			<?php echo $form->error($model,'total'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'posisi'); ?>
			<?php echo $form->dropDownList($model,'posisi',Yii::app()->params['http_intercept_ads_value'],array('class'=>'required digits')); ?>
			<?php echo $form->error($model,'posisi'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'Expandable Type')."<b><font color='red'>*</font></b>" ?>
			<?php echo $form->dropDownList($model,'http_intercept_ads_type',Yii::app()->params['expandtype'],array('class'=>'required','onchange'=>'{onChangePaket();}'));?>
			<?php echo $form->error($model,'http_intercept_ads_type'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'Ads Start Time')."<b><font color='red'>*</font></b>"; ?>
			<?php echo Chtml::activeTextField($model,'ads_start_time',array('class' => 'input-mini required digits' ,'maxlength'=>200))." (in milisecond)"; ?>
			<?php echo $form->error($model,'ads_start_time'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'Ads Time Out')."<b><font color='red'>*</font></b>"; ?>
			<?php echo Chtml::activeTextField($model,'ads_time_out',array('class' => 'input-mini required digits' ,'maxlength'=>200))." (in milisecond)"; ?>
			<?php echo $form->error($model,'ads_time_out'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'ukuran_gambar1'); ?>
			<?php echo $form->dropDownList($model,'ukuran_gambar1',Yii::app()->params['imagesize'],array('class' => 'required')); ?>
			<?php echo $form->error($model,'ukuran_gambar1'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'ukuran_gambar2'); ?>
			<?php echo $form->dropDownList($model,'ukuran_gambar2',Yii::app()->params['imagesize'],array('class' => 'required')); ?>
			<?php echo $form->error($model,'ukuran_gambar2'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'ukuran_gambar3'); ?>
			<?php echo $form->dropDownList($model,'ukuran_gambar3',Yii::app()->params['imagesize'],array('class' => 'required')); ?>
			<?php echo $form->error($model,'ukuran_gambar3'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'priority'); ?>
			<?php echo $form->dropDownList($model,'priority',Yii::app()->params['priority'],array('class' => 'required')); ?>
			<?php echo $form->error($model,'priority'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'location'); ?>
			<?php echo $form->dropDownList($model,'location',array(0=>"Unavailable",1=>"Available"),array('class' => 'required')); ?>
			<?php echo $form->error($model,'location'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'site'); ?>
			<?php echo $form->dropDownList($model,'site',array(0=>"Unavailable",1=>"Available"),array('class' => 'required')); ?>
			<?php echo $form->error($model,'site'); ?>
		</div>

		<div class="well-small buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->