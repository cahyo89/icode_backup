<?php
/* @var $this ParamKeywordController */
/* @var $model ParamKeyword */

$this->breadcrumbs=array(
	'Param Keywords'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Param Keyword', 'url'=>array('index')),
	array('label'=>'Create Param Keyword', 'url'=>array('create')),
	array('label'=>'View Param Keyword', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Param Keyword', 'url'=>array('admin')),
);
?>

<h1>Update ParamKeyword <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>