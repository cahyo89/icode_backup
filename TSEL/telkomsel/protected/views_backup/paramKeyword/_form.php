<?php
/* @var $this ParamKeywordController */
/* @var $model ParamKeyword */
/* @var $form CActiveForm */
?>

<div class="form well">

<?php			
		$this->widget('ext.sformwizard.SFormWizard',array(
			'selector'=>"#filter-url-intercept-form",
			'disableUIStyles' => "true",
			'validationEnabled' => "true",
		));
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'param-keyword-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="step" id="Step1">
		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?php echo $form->errorSummary($model); ?>
		<div class="well-small">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('class' => 'required','size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		<div class="well-small">
		<?php echo $form->labelEx($model,'param_name'); ?>
		<?php echo $form->textField($model,'param_name',array('class' => 'input-xxlarge required')); ?>
		<?php echo $form->error($model,'param_name'); ?>
		</div>
		<div class="well-small">
			<?php echo $form->labelEx($model,'flag_blacklist'); ?>
			<?php echo $form->dropDownList($model,'flag_blacklist',array(1=>"Normal",0=>"Blacklist"),array('class' => 'required')); ?>
			<?php echo $form->error($model,'flag_blacklist'); ?>
		</div>
		<div class="well-small buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->