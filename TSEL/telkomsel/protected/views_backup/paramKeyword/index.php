<?php
/* @var $this ParamKeywordController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Param Keywords',
);

$this->menu=array(
	array('label'=>'Create Param Keyword', 'url'=>array('create')),
	array('label'=>'Manage Param Keyword', 'url'=>array('admin')),
);
?>

<h1>Param Keywords</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
