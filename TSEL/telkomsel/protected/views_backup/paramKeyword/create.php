<?php
/* @var $this ParamKeywordController */
/* @var $model ParamKeyword */

$this->breadcrumbs=array(
	'Param Keywords'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Param Keyword', 'url'=>array('index')),
	array('label'=>'Manage Param Keyword', 'url'=>array('admin')),
	array('label'=>'Manage Param Keyword', 'url'=>array('admin')),
);
?>

<h1>Create ParamKeyword</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>