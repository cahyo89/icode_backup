<script>
	function reject()
	{
		var reason = $("#reason").val();
		var currentid = '<?php echo $poid; ?>';
		if(reason == "")
		{
			alert("Reason must be filled");
		}
		else
		{
			window.location = document.URL.substring(0,document.URL.lastIndexOf("view"))+"reject&id="+currentid+"&reason="+reason;
		}
	}
	
	function approve()
	{
		var reason = $("#reason").val();
		var currentid = '<?php echo $poid; ?>';
		if(reason == "")
		{
			alert("Reason must be filled");
		}
		else
		{
			window.location = document.URL.substring(0,document.URL.lastIndexOf("view"))+"app&id="+currentid+"&reason="+reason;
		}
	}
</script>

<?php
$this->breadcrumbs=array(
	'Balance'=>array('index'),
	'PO '.$model->id,
);


?>

<h1>View Balance #<?php echo 'PO '.$poid;  ?></h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'setting-balance-grid',
	'dataProvider'=>$model,
	'columns'=>array(
		array(
			'name'=>'Transaction Date',
			'type'=>'raw',
			'value'=>'$data["first_update"]',
		),
		array(
			'name'=>'PO#',
			'type'=>'raw',
			'value'=>'"PO".$data["id"]',
		),
		array(
			'name'=>'Token',
			'type'=>'raw',
			'value'=>'$data["prepaid_value"]',
		),
		array(
			'name'=>'Price',
			'type'=>'raw',
			'value'=>'"Rp. ".number_format($data["harga"])',
		),
		array(
			'name'=>'Dicount',
			'type'=>'raw',
			'value'=>'$data["discount"]',
		),
		array(
			'name'=>'Grand Total',
			'type'=>'raw',
			'value'=>'"Rp. ".number_format($data["bayar"])',
		),
		array(
			'name'=>'Net Amount',
			'type'=>'raw',
			'value'=>'"Rp. ".number_format(($data["bayar"]*50)/100)',
		),
	),
)); ?>
<h3>Summary</h3>
<?php
 $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model1,
	'attributes'=>array(
		array(
			'label'=>'Token',
			'name'=>'prepaid_value',
		),
		array(
			'label'=>'price',
			'name'=>'harga',
			'value'=>"Rp.".number_format($model1["harga"]),
		),
		array(
			'label'=>'Grand Total',
			'name'=>'bayar',
			'value'=>"Rp.".number_format($model1['bayar']),
		),
		array(
			'name'=>'Net Amount',
			'type'=>'raw',
			'value'=>"Rp.".number_format(($model1['bayar']*50)/100),
		),
		array(
			'name'=>'Last User Update',
			'type'=>'raw',
			'value'=>$model1["last_user"]." at ".$model1["last_update"],
		),
		
	),
)); 
?>

	<table style="margin-bottom:15px; margin-top:15px;">		
		<tr>
			<td style="margin-top:25px;">Reason :</td>
			<td style="margin-top:25px;"><textarea class="input-xlarge" id="reason"></textarea></td> 
		</tr>
		<tr>
			<td width="500px">Less than the carrying value of PO</td>
			<td>: Rejected</td>
		</tr>
		<tr>
			<td>More than PO value up to Rp. 49.999,-</td>
			<td>: Approved</td>
		</tr>
		<tr>
			<td>More than PO value starting from Rp. 50.000,-</td>
			<td>: Rejected</td>
		</tr>
	</table>
	

<?php
//Controller::createInfo($model1);
if(Yii::app()->user->checkAccess('Admin') || Yii::app()->user->checkAccess('superUser')){
	//if($model->status == 1){
		echo CHtml::button('Reject', array('onclick' => 'js:reject();','confirm'=>'Are you sure want to REJECT this Setting Balance?','style'=>'margin-right:200px;')); 
		echo CHtml::button('Approve', array('onclick' => 'js:approve();','confirm'=>'Are you sure want to APPROVE this Setting Balance?','style'=>'margin-right:200px;')); 
		//echo CHtml::button('Reject', array('submit' => array('reject','id'=>$model->id),'confirm'=>'Are you sure want to REJECT this Setting Balance?','style'=>'margin-right:200px;')); 
		//echo CHtml::button('Approve', array('submit' => array('app','id'=>$model->id),'confirm'=>'Are you sure want to APPROVE this Setting Balance?')); 
	//}
}
?>
