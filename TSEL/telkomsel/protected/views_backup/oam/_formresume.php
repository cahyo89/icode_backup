<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'oam',
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'jobs_id'); ?>
		<?php echo $form->textField($model,'jobs_id',array('size'=>50,'maxlength'=>50,'readonly'=>true)); ?>
		<?php echo $form->error($model,'jobs_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textField($model,'comment',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->