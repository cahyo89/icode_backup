<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
//beresin bug doble login
				$user = User::model()->findByAttributes(array('username'=>$model->username));
				if(isset($user)){
				$jam=date("Y-m-d H:i:s",$user->lastvisit);
				if((strtotime(date("Y-m-d H:i:s"))-strtotime($jam) ) >= Yii::app()->params['sessionTimeoutSeconds'])
				{
					$connection=Yii::app()->db;
				$sql= "update tbl_users set ip = 0,flag = 0 where id = $user->id";
				$connection->createCommand($sql)->execute();
				}
			}
				//sampe sini
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
						$current= Users::model()->findByPk(Yii::app()->user->id);
						$id = Yii::app()->user->id;
						$hari = strtotime($current->change_pass_date);
						if($current->change_pass_date == NULL  ||  (($hari+($current->passage1*86400)) < mktime())){
						Yii::app()->user->logout();
						$this->redirect(array('/change/index','id'=>$id));
						}
						if(($hari+($current->passage2*86400)) < mktime()){
						$connection=Yii::app()->db;
						$sql= "update tbl_users set status = -1 where id = $id";
						$connection->createCommand($sql)->execute();
						Yii::app()->user->logout();
						}
					$this->lastViset();
					if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$id = Yii::app()->user->id;
		$connection=Yii::app()->db;
		$sql= "update tbl_users set lastvisit = '".time()."' where id = $id";
		$connection->createCommand($sql)->execute();
		
	}

}