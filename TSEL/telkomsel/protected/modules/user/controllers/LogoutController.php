<?php

class LogoutController extends Controller
{
	public $defaultAction = 'logout';

	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout()
	{	
		$connection=Yii::app()->db;
		$berita = "LOGOUT";
		$sql= "update tbl_users set flag = 0,ip = 0 where id = '".Yii::app()->user->id."'";
		$command=$connection->createCommand($sql)->execute();
		$sql= "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
		$command=$connection->createCommand($sql)->execute();
		Yii::app()->user->logout();
		
		$this->redirect(Yii::app()->controller->module->returnLogoutUrl);
	}

}