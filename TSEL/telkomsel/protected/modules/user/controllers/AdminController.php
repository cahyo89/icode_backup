<?php

class AdminController extends Controller
{
	public $defaultAction = 'admin';
	
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return CMap::mergeArray(parent::filters(),array(
			'rights', // perform access control for CRUD operations
		));
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','view','dodo','unBlock'),
				'users'=>UserModule::getAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$criteria = new CDbCriteria;
		
		if(!Yii::app()->user->checkAccess('Admin') &&  !Yii::app()->user->checkAccess('Media_Seller')){
			$criteria->addCondition("id_customer = ".Yii::app()->user->idCustomer." ");
		}
		
		if(Yii::app()->user->checkAccess('Media_Seller') && !Yii::app()->user->checkAccess('Admin'))
		{
			$criteria->addCondition("id_media_seller = '".Yii::app()->user->id."'");
		}
		
		if(Yii::app()->user->checkAccess('Customer') && !Yii::app()->user->checkAccess('Media_Seller') && !Yii::app()->user->checkAccess('Admin'))
		{
			$criteria->addCondition("id = '".Yii::app()->user->id."'");
		}
		
		if(Yii::app()->user->checkAccess('Media_Seller') && !Yii::app()->user->checkAccess('Admin'))
		{
			$criteria->addCondition("id = '".Yii::app()->user->id."'",'OR');
		}
		
		if(!Yii::app()->user->checkAccess('Admin'))
		{
			$criteria->addCondition("id not in (".Controller::getNotAdmin().") ");
		}
		
		
		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>30,//Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;
		$profile=new Profile;
		
		if(Yii::app()->user->checkAccess('Admin'))
			$AuthItem = CHtml::listData(AuthItem::model()->findAll('type = 2'),'name','name');
		else if(Yii::app()->user->checkAccess('Media_Seller'))
			$AuthItem = CHtml::listData(AuthItem::model()->findAll('type = 2 and name !="Admin" '),'name','name');
			
		if(Yii::app()->user->checkAccess('Admin'))
		{
			$customer = CHtml::listData( Customer::model()->findAll('approved = 1 and blocked = 0'),'id_customer','nama');
		}
		else
		{
			$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
			$customer = CHtml::listData( Customer::model()->findAll('approved = 1 and blocked = 0 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )'),'id_customer','nama');
		}
				

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
			$model->createtime=time();
			$model->lastvisit=time();
			$model->username = $_POST['User']['username'];
			$model->email = $_POST['User']['email'];
			$model->flag_multiple = $_POST['User']['flag_multiple'];
			$model->status = $_POST['User']['status'];
			
			//$profile->attributes=$_POST['Profile'];
			//$profile->user_id=0;
			if(empty($model->id_customer))
			{
				$model->id_customer = 0;
			}
			$model->rtries_count_use = $model->rtries_count;
			if($model->validate()) {//&&$profile->validate()) {
				$model->password=Yii::app()->controller->module->encrypting($model->password);
				
				if($_POST['User']['role'] != "Treasury" || $_POST['User']['role'] != "Technical" || $_POST['User']['role'] != "Admin" && $_POST['User']['role'] != "Media_Seller") {
					$model->superuser = 0;
				}
				else{
					$model->superuser = 1;
				}
				
				if($_POST['User']['role'] == "Admin")
				{
					$model->usermode = 0;
				}
				else if($_POST['User']['role'] == "Media_Seller")
				{
					$model->usermode = 1;
				}
				else
				{
					$model->usermode = 2;
				}
				
				if(Yii::app()->user->getUserMode() == 1)
				{
					$model->id_media_seller = Yii::app()->user->id;
				}
				
				if($model->save()) {
						$last = Yii::app()->db->getLastInsertID();
						$current = User::model()->findByPk($last);
						$connection = Yii::app()->db;
						$berita = "Create User dengan name: $current->username,Id Customer : $current->id_customer";
						$sql = "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
						$command = $connection->createCommand($sql)->execute();	
						$sqlRights = "insert into AuthAssignment values('".$_POST['User']['role']."',".$model->id.",NULL,'N;')";
						$commandRights = $connection->createCommand($sqlRights)->execute();		
						
					//$profile->user_id=$model->id;
					//$profile->save();
				}
				$this->redirect(array('view','id'=>$model->id));
			} //else $profile->validate();
		}
		unset($AuthItem['Guest']);
		if(Yii::app()->user->getUserMode()==2 || Yii::app()->user->getUserMode()==1)
		{
			unset($AuthItem['Admin']);
			unset($AuthItem['Media_Seller']);
			unset($AuthItem['Technical']);
			unset($AuthItem['Treasury']);
		}
		
		$this->render('create',array(
			'model'=>$model,
			'profile'=>$profile,
			'AuthItem' =>$AuthItem,
			'customer'=>$customer,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$AuthAssignment = AuthAssignment::model()->findByAttributes(array('userid'=> $model->id));
		if($AuthAssignment !== null)
			$model->role = $AuthAssignment->itemname;
		
		if(Yii::app()->user->checkAccess('Admin'))
			$AuthItem = CHtml::listData(AuthItem::model()->findAll('type = 2'),'name','name');
		else if(Yii::app()->user->checkAccess('Media_Seller'))
		{
			$AuthItem = CHtml::listData(AuthItem::model()->findAll('type = 2 and name !="Admin" '),'name','name');
			unset($AuthItem['Media_Seller']);
		}
		unset($AuthItem['Guest']);
			
		$profile=$model->profile;
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			//$profile->attributes=$_POST['Profile'];
			
			if($model->validate()) {//&&$profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password!=$model->password) {
					$model->password=Yii::app()->controller->module->encrypting($model->password);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
					$model->change_pass_date = NULL;
				}
				$model->flag_multiple = $_POST['User']['flag_multiple'];
				
				$current = User::model()->findByPk($model->id);
				$connection = Yii::app()->db;
				$berita = "Update User dengan name: $current->username --> $model->username ,Id Customer : $current->id_customer --> $model->id_customer";
				$sql = "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
				$command = $connection->createCommand($sql)->execute();	
				$sqlRights = "update AuthAssignment set itemname = '".(($model->id==Yii::app()->user->id && Yii::app()->user->getUserMode() == 1) ? "Media_Seller" : $_POST['User']['role'])."' where userid = ".$model->id." ";
				$commandRights = $connection->createCommand($sqlRights)->execute();	
				if($_POST['User']['role'] != "Admin" && $_POST['User']['role'] != "Media_Seller") {
					$model->superuser = 0;
				}
				else{
					$model->id_customer = NULL;
					$model->superuser = 1;
				}
				if($_POST['User']['role'] == "Admin"){
					$model->usermode = 0;
				}
				else if($_POST['User']['role'] == "Media_Seller" || $model->usermode == 1){
					$model->usermode = 1;
				}
				else{
					$model->usermode = 2;
				}
				$model->save();
				//$profile->save();
				$this->redirect(array('view','id'=>$model->id));
			} //else $profile->validate();
		}
		if(Yii::app()->user->checkAccess('Admin'))
		{
			$customer = CHtml::listData( Customer::model()->findAll('approved = 1 and blocked = 0'),'id_customer','nama');
		}
		else
		{
			$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
			$customer = CHtml::listData( Customer::model()->findAll('approved = 1 and blocked = 0 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )'),'id_customer','nama');
		}
		$this->render('update',array(
			'model'=>$model,
			'profile'=>$profile,
			'AuthItem' =>$AuthItem,
			'customer' => $customer,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel();
			
			if(Yii::app()->user->id == $model->id)
			{
				if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
				
				return false;
			}
			if(Yii::app()->user->getUserMode() == 1)
			{
				$sqlCheck = "select * from tbl_users where id_media_seller = ".Yii::app()->user->id." and id = '".$model->id."'";
				$connection=Yii::app()->db;
				$command=$connection->createCommand($sqlCheck);
				$rows=$command->queryRow();
				if(!$rows)
				{
					return false;
				}
			}
			//$profile = Profile::model()->findByPk($model->id);
			$current = User::model()->findByPk($model->id);
			$connection = Yii::app()->db;
			$berita = "Delete User dengan name: $current->username,Id Customer : $current->id_customer ";
			$sql = "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
			$command = $connection->createCommand($sql)->execute();		
			$sqlRights = "delete from AuthAssignment where userid = ".$model->id." ";
			$commandRights = $connection->createCommand($sqlRights)->execute();
			
			//$profile->delete();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionDodo()
	{

		$id_media_seller = Yii::app()->request->getParam('id_media_seller');
		
		$data = Advertiser::model()->findAll('id_media_seller =  :a',
		array(':a'=>$id_media_seller)
		);
		$data=CHtml::listData($data,'id','name');
		$dropDownA = "";
		$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('--None--'),true);
		foreach($data as $value=>$name)
	   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
	 
//			foreach($dataB as $value=>$name)
//	   $dropDownB .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
	 
		// return data (JSON formatted)
		echo CJSON::encode(array(
			'dataa'=>$dropDownA,
		)); 
		  Yii::app()->end();
	}	
	
	public function actionUnBlock(){
			
			$model = User::model()->findByAttributes(array( 'username' => $_POST['User']['username']));
			$connection = Yii::app()->db;
			$sql = "update tbl_users set rtries_count_use =  ".$model->rtries_count." where id = ".$model->id." ";
			$command = $connection->createCommand($sql)->execute();	
			$this->redirect(array('view','id'=>$model->id));
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			{
				if(Yii::app()->user->getUserMode() == 0)
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
				else{
					$sqlCheck = "select * from tbl_users where id_media_seller = ".Yii::app()->user->id." and id = '".$_GET['id']."'";
					$connection=Yii::app()->db;
					$command=$connection->createCommand($sqlCheck);
					$rows=$command->queryRow();
					if(!$rows)
					{
						$this->_model=User::model()->notsafe()->findbyPk(Yii::app()->user->id);
					}
					else
					{
						$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
					}
				}
			}
			if($this->_model===null)

				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
}