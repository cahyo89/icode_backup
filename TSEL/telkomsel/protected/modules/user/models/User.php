<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANED=-1;
	
	public $role;
	public $usermode;
	public $id_customer;
	public $flag_free;
	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $status
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		
		return ((Yii::app()->getModule('user')->isAdmin())?array(
			#array('username, password, email', 'required'),
			array('flag_free, change_pass_date,id_customer,id_media_seller','safe'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('id_customer', 'cekCustomer'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANED)),
			array('superuser', 'in', 'range'=>array(0,1)),
			array('username, email, createtime, lastvisit, superuser, status,active_date,passage1,passage2,rtries_count,rtries_count_use,role', 'required'),
			array('createtime, lastvisit, superuser, status,rtries_count,passage1,passage2,', 'numerical', 'integerOnly'=>true),
		):((Yii::app()->user->id==$this->id)?array(
			array('flag_free, change_pass_date,id_customer,id_media_seller,role,usermode,flag_multiple','safe'),
			array('username, email', 'required'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
		):array()));
	}
	public function cekCustomer($attribute,$params)
	{
		if($this->role != "Admin" && $this->role != "Media_Seller" && $this->role != "Customer")
		{
			//if($this->id_customer == "")
			//	$this->addError('id_customer',"$attribute $this->role cannot be blank..");
		}
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		$relations = array(
			'profile'=>array(self::HAS_ONE, 'Profile', 'user_id'),
			//'Customer'=>array(self::BELONGS_TO ,'Customer','id_customer'),
			//'MediaSeller'=>array(self::BELONGS_TO ,'MediaSeller','id_media_seller'),
		);
		if (isset(Yii::app()->getModule('user')->relations)) $relations = array_merge($relations,Yii::app()->getModule('user')->relations);
		return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>UserModule::t("username"),
			'password'=>UserModule::t("password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'id' => UserModule::t("Id"),
			'activkey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'lastvisit' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Allow Create User"),
			'status' => UserModule::t("Status"),
			'flag' => UserModule::t("Status Login"),
			'id_customer'=>UserModule::t("Customer"),
			'id_media_seller'=>UserModule::t("Media Seller"),
			'active_date' => UserModule::t("Active Date"),
			'rtries_count' => UserModule::t("Retries Count"),
			'change_pass_date' => UserModule::t("Change Password Date"),
			'passage1' => UserModule::t("Password Expired 1"),
			'passage2' => UserModule::t("Password Expired 2"),
			'Advertiser.name'=>UserModule::t("Advertiser"),
			'MediaSeller.name'=>UserModule::t("Media Seller"),
			'flag_multiple'=>UserModule::t("Double Login"),
			'flag_free'=>UserModule::t("Token Status"),
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactvie'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANED,
            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, createtime, lastvisit, superuser, status,id_customer,id_media_seller,passage1,passage2,rtries_count,active_date,change_pass_date,rtries_count_use,usermode',
            ),
        );
    }
	
	public function defaultScope()
    {
        return array(
            'select' => 'id, username, password, email, createtime, lastvisit, superuser, status,flag,id_customer,id_media_seller,passage1,passage2,rtries_count,active_date,change_pass_date,rtries_count_use,flag_multiple,usermode',
        );
    }
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
			'FlagMultiple' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
			'LoginStatus' => array(
				'0' => UserModule::t('Not Login'),
				'1' => UserModule::t('Login'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : $_items[$type]['0'];
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
}