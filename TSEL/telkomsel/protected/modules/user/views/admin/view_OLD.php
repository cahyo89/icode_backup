<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	$model->username,
);
?>
<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1><?php echo UserModule::t('View User').' "'.$model->username.'"'; ?></h1></td>
	<td><div class="operatorRight"><?php Controller::createMenu(array(
	array('label'=>'update','link'=>'update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	array('label'=>'delete','link'=>'delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->username),
	array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
	)); ?></div></td>
	</tr></table>


<?php echo "</br>"; ?>

<?php /*echo $this->renderPartial('_menu', array(
		'list'=> array(
			CHtml::link(UserModule::t('Create User'),array('create')),
			CHtml::link(UserModule::t('Update User'),array('update','id'=>$model->id)),
			CHtml::linkButton(UserModule::t('Delete User'),array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?'))),
		),
	)); */


	$attributes = array(
		'id',
		'username',
'Advertiser.name',
		'MediaSeller.name',
	);
	
	$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'type'=>'raw',
					'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
				));
		}
	}
	
	array_push($attributes,
		'password',
		//'email',
		//'activkey',
		array(
			'name' => 'createtime',
			'value' => date("d.m.Y H:i:s",$model->createtime),
		),
		array(
			'name' => 'lastvisit',
			'value' => (($model->lastvisit)?date("d.m.Y H:i:s",$model->lastvisit):UserModule::t("Not visited")),
		),
		array(
			'name' => 'superuser',
			'value' => User::itemAlias("AdminStatus",$model->superuser),
		),
		array(
			'name' => 'status',
			'value' => User::itemAlias("UserStatus",$model->status),
		)
	);
	
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));
	

?>

<table width ="845"><tr>
	<td><div class="operatorLeft"><?php Controller::createMenu(array(
	array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
	array('label'=>'update','link'=>'update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	array('label'=>'delete','link'=>'delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->username)
	)); ?></div></td>
	</tr></table>