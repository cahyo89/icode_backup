<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	UserModule::t('Manage'),
);
?>

<?php /* echo $this->renderPartial('_menu', array(
		'list'=> array(
			CHtml::link(UserModule::t('Create User'),array('create')),
		),
	));*/
	
	$dodo = "asd";
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>">
<tr>
<td>
<h1>Manage Users</h1>
</td>
</tr>
<tr>
<td>
<div class="operatorRight">
<?php Controller::createMenu(array(
array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
)); ?></div>
</td>
</tr>
	</table>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/view","id"=>$data->id))',
		),
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->email), "mailto:".$data->email)',
		),
		array(
			'name' => 'createtime',
			'value' => 'date("d.m.Y H:i:s",$data->createtime)',
		),
		array(
			'name' => 'lastvisit',
			'value' => '(($data->lastvisit)?date("d.m.Y H:i:s",$data->lastvisit):UserModule::t("Not visited"))',
		),
		array(
			'name'=>'status',
			'value'=>'User::itemAlias("UserStatus",$data->status)',
		),
		array(
			'name'=>'flag',
			'value'=>'User::itemAlias("LoginStatus",$data->flag)',
		),
		array(
			'name'=>'superuser',
			'value'=>'User::itemAlias("AdminStatus",$data->superuser)',
		),
		array(
			'name'=>'Role',
			'value'=>'Controller::getRoleUser($data->id)',
		),
		
		array(
			'class'=>'CButtonColumn',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nName :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>
