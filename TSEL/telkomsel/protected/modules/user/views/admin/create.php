<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	UserModule::t('Create'),
);
?>
<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Create User</h1></td></tr>
	<tr><td><div class="operatorRight"><?php Controller::createMenu(array(
	array('label'=>'List','link'=>'admin','img'=>'icon-th-list','id'=>'','conf'=>'')
	)); ?></div></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	</table>
	
<?php 
	/*echo $this->renderPartial('_menu',array(
		'list'=> array(),
	));*/
	
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile,'AuthItem'=>$AuthItem,'customer'=>$customer));
?>
