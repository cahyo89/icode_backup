<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1><?php echo  UserModule::t('Update User')." ".$model->username; ?></h1></td>
	<td><div class="operatorRight"><?php Controller::createMenu(array(
	array('label'=>'view','link'=>'view','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
	array('label'=>'List','link'=>'admin','img'=>'icon-th-list','id'=>'','conf'=>'')
	)); ?></div></td>
	</tr></table>

<?php echo "</br>"; ?>

<?php /*echo $this->renderPartial('_menu', array(
		'list'=> array(
			CHtml::link(UserModule::t('Create User'),array('create')),
			CHtml::link(UserModule::t('View User'),array('view','id'=>$model->id)),
		),
	)); */

	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile,'AuthItem' =>$AuthItem,'customer'=>$customer)); ?>
