<div class="form">

<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo CHtml::errorSummary(array($model)); ?>
<div class="row">	
	<div class="span3">
	
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo CHtml::error($model,'username'); ?>
	</div>

	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'password'); ?>
		<?php echo CHtml::activePasswordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo CHtml::error($model,'password'); ?>
	</div>

	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'email'); ?>
		<?php echo CHtml::activeTextField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo CHtml::error($model,'email'); ?>
	</div>

	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'status'); ?>
		<?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus')); ?>
		<?php echo CHtml::error($model,'status'); ?>
	</div>
</div>
<div class="row">	
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'flag_multiple'); ?>
		<?php echo CHtml::activeDropDownList($model,'flag_multiple',User::itemAlias('FlagMultiple')); ?>
		<?php echo CHtml::error($model,'flag_multiple'); ?>
	</div>
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'role'); ?>
		<?php 
		if(isset($model->id)&&$model->id==Yii::app()->user->id && Yii::app()->user->getUserMode() == 1)
		{
			echo CHtml::activeTextField($model,'role',array('value'=>'Media Seller','readonly'=>'readonly','size'=>11,'maxlength'=>10));
		}
		else
		{
			echo CHtml::activeDropDownList($model,'role',$AuthItem,array('onchange'=>'{onChangeRole();}'));
		}
		?>
		<?php echo CHtml::error($model,'role'); ?>
	</div>

	<!--
	<div class="row">
		<?php /* echo CHtml::activeLabelEx($model,'id_media_seller'); ?>
		<?php echo CHtml::activedropDownList($model,'id_media_seller',CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_customer', 'nama'),array('onchange'=>'{tes();}','empty'=>'--None--')); ?>
		<?php echo CHtml::error($model,'id_media_seller');*/ ?>
	</div>
	-->
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'id_customer'); ?>
		
		<?php 
		if(($model->usermode == 0 || $model->usermode == null))
		{
			echo CHtml::activeDropDownList($model,'id_customer',$customer,array('onchange'=>'{onChangeRole();}','disabled'=>'disabled'));
		}
		else
		{
			echo CHtml::activeDropDownList($model,'id_customer',$customer,array('onchange'=>'{onChangeRole();}')); 
		}
		?>
		<?php echo CHtml::error($model,'id_customer'); ?>
	</div>
	
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'rtries_count'); ?>
		<?php echo CHtml::activeTextField($model,'rtries_count',array('size'=>11,'maxlength'=>10)); ?>
		<?php echo CHtml::error($model,'rtries_count'); ?>
	</div>
</div>
<div class="row">

	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'active_date'); ?>
		<?php 
               $this->widget('zii.widgets.jui.CJuiDatePicker', 
                        array(
                                'model' => $model,
                                'attribute' => 'active_date',
								'language'=>'en-AU',
                                'options' => array(
								
                                        'showAnim' => 'fold',
                                        'dateFormat' => 'yy-mm-dd', 
                                        'defaultDate' => $model->active_date,
                                        'changeYear' => true,
                                        'changeMonth' => true,
										'showOn'=>'both',
										'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
										'buttonImageOnly' => true,
                                ),
                ));
        ?><?php echo CHtml::error($model,'active_date'); ?>
	</div>
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'passage1'); ?>
		<?php echo CHtml::activeTextField($model,'passage1',array('size'=>11,'maxlength'=>10)); ?>(Days)
		<?php echo CHtml::error($model,'passage1'); ?>
	</div>
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'passage2'); ?>
		<?php echo CHtml::activeTextField($model,'passage2',array('size'=>11,'maxlength'=>10)); ?>(Days)
		<?php echo CHtml::error($model,'passage2'); ?>
	</div>
	
	<div class="span3">
		<?php echo CHtml::activeLabelEx($model,'flag_free'); ?>
		<?php echo CHtml::activeDropDownList($model,'flag_free',array(0=>"Paid",1=>"Free"),array('class' => 'required')); ?>
		<?php echo CHtml::error($model,'flag_free'); ?>
	</div>
</div>	
	
	
	
	
	
	
<?php 
	/*	$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="row">
		<?php echo CHtml::activeLabelEx($profile,$field->varname); ?>
		<?php 
		if ($field->widgetEdit($profile)) {
			echo $field->widgetEdit($profile);
		} elseif ($field->range) {
			echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo CHtml::activeTextField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
		}
		 ?>
		<?php echo CHtml::error($profile,$field->varname); ?>
	</div>	
			<?php
			}
		}*/
?>

<div class="well-small buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');?>
	<?php echo CHtml::endForm();?>

<?php //echo CHtml::submitButton('Cancel',array('submit'=>Yii::app()->request->urlReferrer,'style'=>'font-size: 14px;font-weight: bold;')); 

echo CHtml::button('Cancel',array('onclick'=>'js:history.go(-1);returnFalse;','style'=>'font-size: 14px;font-weight: bold;')); 
if($model->rtries_count_use <=0 )
echo CHtml::button('UnBlock User', array('submit' => array('unblock')));
?>

</div></div><!-- form -->

 <script type="text/javascript">
	    // The bits above should be familiar to you
	    // This function is called by the "HPI check" button above
	   function onChangeRole()
	    {
	        	var role = document.getElementById('User_role').value;
			if(role == "Admin" || role == "Media_Seller" || role == "Technical" || role == "Treasury"||role == "OAM_AXIS"){
				document.getElementById('User_id_customer').selectedIndex = "";
				document.getElementById('User_id_customer').disabled = true;
			}
			else{
				document.getElementById('User_id_customer').disabled = false;
			}
	        return false;  
	    } 
</script>

