<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
/*$this->breadcrumbs=array(
	UserModule::t("Login"),
);*/
?>
<div class="loginForm">
<h1><?php //echo UserModule::t("Login"); ?></h1>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; 
?>

<div class="form">
<?php echo CHtml::beginForm(); ?>

	<p class="note"><?php //echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php
	echo CHtml::errorSummary($model,'ERROR'); ?>
	<div  class="loginFormLeft">
		<table>
				<tr>
					<td><?php echo CHtml::activeLabelEx($model,'username'); ?></td>
				</tr>
				<tr>
					<td><?php echo CHtml::activeTextField($model,'username') ?></td>
				</tr>
				<tr>
					<td><?php echo CHtml::activeLabelEx($model,'password'); ?></td>
				<tr>
				</tr>
					<td><?php echo CHtml::activePasswordField($model,'password') ?></td>
				</tr>
				<tr>
					<td><?php echo CHtml::submitButton(UserModule::t("Login")); ?></td>
				</tr>
		</table>
	</div>
<?php echo CHtml::endForm(); ?>
</div><!-- form -->

<script type="text/javascript">
if ( document.getElementById("UserLogin_username").value == '' ) {
    document.getElementById("UserLogin_username").focus();
}else
 document.getElementById("UserLogin_password").focus();

 document.getElementById("UserLogin_password").value = "";
</script> 
<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>
</div>