<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('index'),
	$model->username,
);
?>
<h1><?php echo UserModule::t('View User').' "'.$model->username.'"'; ?></h1>

<html>
<head>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<th style="background-color:#00CC66; color:white">Operations</th>
</tr> 
<tr>
<td style="background-color: #CCCCCC">&nbsp;&nbsp;&nbsp;<?php echo CHtml::link(UserModule::t('List User'),array('index'));
?></td>
</tr>
</table>
</body>
</html>

<?php echo "</br>"; ?>

<ul class="actions">
	<li><?php //echo CHtml::link(UserModule::t('List User'),array('index')); ?></li>
</ul><!-- actions -->

<?php 

// For all users
	$attributes = array(
			'username',
	);
	
	$profileFields=ProfileField::model()->forAll()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'value' => $model->profile->getAttribute($field->varname),
				));
		}
	}
	array_push($attributes,
		array(
			'name' => 'createtime',
			'value' => date("d.m.Y H:i:s",$model->createtime),
		),
		array(
			'name' => 'lastvisit',
			'value' => (($model->lastvisit)?date("d.m.Y H:i:s",$model->lastvisit):UserModule::t('Not visited')),
		)
	);
			
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));

?>
