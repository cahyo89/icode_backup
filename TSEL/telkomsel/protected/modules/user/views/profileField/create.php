<?php
$this->breadcrumbs=array(
	UserModule::t('Profile Fields')=>array('admin'),
	UserModule::t('Create'),
);
?>
<h1><?php echo UserModule::t('Create Profile Field'); ?></h1>

<?php /*echo $this->renderPartial('_menu',array(
		'list'=> array(),
	)); */?>

<html>
<head>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<th style="background-color:#00CC66; color:white">Operations</th>
</tr> 
<tr>
<td style="background-color: #CCCCCC">&nbsp;&nbsp;&nbsp;<?php echo CHtml::link(UserModule::t('Manage User'),array('/user/admin'));
?></td>
</tr>
<tr>
<td style="background-color: #CCCCCC">&nbsp;&nbsp;&nbsp;<?php echo CHtml::link(UserModule::t('Manage Profile Field'),array('admin'));
?></td>
</tr>
</table>
</body>
</html>

<?php echo "</br>"; ?>
	
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>