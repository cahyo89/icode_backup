<?php
$this->breadcrumbs=array(
	UserModule::t('Profile Fields')=>array('admin'),
	UserModule::t('Manage'),
);
?>
<h1><?php echo UserModule::t('Manage Profile Fields'); ?></h1>

<?php /*echo $this->renderPartial('_menu', array(
		'list'=> array(
			CHtml::link(UserModule::t('Create Profile Field'),array('create')),
		),
	));*/
?>

<html>
<head>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<th style="background-color:#00CC66; color:white">Operations</th>
</tr> 
<tr>
<td style="background-color: #CCCCCC">&nbsp;&nbsp;&nbsp;<?php echo CHtml::link(UserModule::t('Manage User'),array('/user/admin'));
?></td>
</tr>
<tr>
<td style="background-color: #CCCCCC">&nbsp;&nbsp;&nbsp;<?php echo CHtml::link(UserModule::t('Create Profile Field'),array('create'));
?></td>
</tr>
</table>
</body>
</html>

<?php echo "</br>"; ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		'varname',
		array(
			'name'=>'title',
			'value'=>'UserModule::t($data->title)',
		),
		'field_type',
		'field_size',
		//'field_size_min',
		array(
			'name'=>'required',
			'value'=>'ProfileField::itemAlias("required",$data->required)',
		),
		//'match',
		//'range',
		//'error_message',
		//'other_validator',
		//'default',
		'position',
		array(
			'name'=>'visible',
			'value'=>'ProfileField::itemAlias("visible",$data->visible)',
		),
		//*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
