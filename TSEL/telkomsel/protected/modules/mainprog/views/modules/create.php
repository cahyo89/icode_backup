<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Modules'=>array('index'),
	'Create',
);

?>
<div class="row">
<div class="span8">
<h1>Create Modules</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label' => 'List Modules', 'url'=>array('index'),'icon' => 'th-list'),
				array('label' => 'Manage Modules', 'url' => array('admin'),'icon' => 'cog'),
			),
		),
	),
));
?>
</div>