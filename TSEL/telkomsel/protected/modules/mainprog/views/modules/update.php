<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Modules'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>
<div class="row">
	<div class="span8">
	<h1>Update Modules <?php echo $model->name; ?></h1>

	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	<div class="span3">
	<?php 
	$this->widget('EBootstrapSidebar', array(
		'items'=> array(
			array(
				'label' => 'Side Menu', 'items' => array(
					array('label'=>'List Modules', 'url'=>array('index')),
					array('label'=>'Create Modules', 'url'=>array('create')),
					array('label'=>'View Modules', 'url'=>array('view', 'id'=>$model->id)),
					array('label'=>'Manage Modules', 'url'=>array('admin')),
				),
			),
		),
	));
	?>
	</div>
</div>