<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Create',
);

?>
<div class="row">
<div class="span8">
<h1>Create Menus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'controllerlink'=>$controllerlink,'parent'=>$parent)); ?>
</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label' => 'List Menu', 'url'=>array('index'),'icon' => 'th-list'),
				array('label' => 'Manage Menu', 'url' => array('admin'),'icon' => 'cog'),
			),
		),
	),
));
?>
</div>