<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php 
			//SELECT ai.name as id , m.name as name from modules m join authitem ai on m.name = SUBSTRING_INDEX( ai.name,  '.' , 2 )
			//CHtml::listData(Modules::model()->findAllBySql($sql)
			//$sql = "SELECT LOWER(CONCAT('/',REPLACE(ai.name,'.','/'))) as id , ai.name as name from modules m join authitem ai on m.name = SUBSTRING_INDEX( ai.name,  '.' , 2 ) WHERE INSTR( ai.name, '*' ) = 0";
			echo $form->dropDownList($model,'link', CHtml::listData($controllerlink, 'id', 'name'), array('empty'=>'Parent Menu')); 
		?>
		<?php echo $form->error($model,'link'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'enable'); ?>
		<?php echo $form->dropDownList($model,'enable', array('1'=>'Enable','2'=>'Disable')); ?>
		<?php echo $form->error($model,'enable'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php
			echo $form->dropDownList($model,'parent_id', CHtml::listData($parent, 'id', 'title'), array('empty'=>'Parent Menu')); 
		?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->