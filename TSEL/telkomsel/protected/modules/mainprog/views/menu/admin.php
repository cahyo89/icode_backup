<?php
/* @var $this ModulesController */
/* @var $model Modules */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	
);
?>


<div class="row">
<div class="span8">
	<?php
		Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$('#modules-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
		");
	?>
	<h1>Manage Menus</h1>

	<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
	</p>

	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
	<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->

	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'menu-grid',
		'dataProvider'=>$model->search(),
		'columns'=>array(
			'id',
			'link',
			'title',
			'enable',
			'parent_id',
			array(
				'class'=>'CButtonColumn',
			),
		),
	)); ?>

</div>
<div class="span3">
<?php 
$this->widget('EBootstrapSidebar', array(
	'items'=> array(
		array(
			'label' => 'Side Menu', 'items' => array(
				array('label'=>'List Menu', 'url'=>array('index'),'icon' => 'th-list'),
				array('label'=>'Create Menu', 'url'=>array('create'),'icon' => 'plus'),
			),
		),
	),
));
?>
</div>
</div>