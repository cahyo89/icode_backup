<?php

/**
 * This is the model class for table "{{blacklist_channel_category}}".
 *
 * The followings are the available columns in table '{{blacklist_channel_category}}':
 * @property integer $id
 * @property string $msisdn
 * @property integer $channel_category
 * @property string $first_ip
 * @property string $first_user
 * @property string $first_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $name
 * @property string $address
 * @property string $request
 * @property string $description
 */
class BlacklistChannelCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blacklist_channel_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('msisdn, channel_category', 'required'),
			array('channel_category', 'numerical', 'integerOnly'=>true),
			array('msisdn, first_user, last_user, name, address, request, description', 'length', 'max'=>100),
			array('first_ip, last_ip', 'length', 'max'=>50),
			array('first_update, last_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, msisdn, channel_category, first_ip, first_user, first_update, last_ip, last_user, last_update, name, address, request, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'msisdn' => 'Msisdn',
			'channel_category' => 'Channel Category',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'name' => 'Name',
			'address' => 'Address',
			'request' => 'Request',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('channel_category',$this->channel_category);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('request',$this->request,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlacklistChannelCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
