<?php

/**
 * This is the model class for table "{{bts}}".
 *
 * The followings are the available columns in table '{{bts}}':
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $description
 * @property string $bts_id
 * @property string $longitude
 * @property string $latitude
 * @property string $first_ip
 * @property string $first_user
 * @property string $first_update
 * @property string $last_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $lac
 * @property integer $show_flag
 * @property string $loc_stat_ip
 * @property integer $flag_deleted
 * @property integer $id_loc_stat
 * @property string $gis_id
 * @property string $gis_name
 * @property string $gis_date
 * @property integer $sector_type
 */
class Bts extends CActiveRecord
{
	//public $cellname;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Bts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('bts_name', 'cekUnique'),
			array('cellname', 'cekUnique'),
			
			array('bts_name, bts_id, lac,cellname,site_id,longitude,latitude', 'required'),
			
			array('show_flag, flag_deleted, id_loc_stat, sector_type', 'numerical', 'integerOnly'=>true),
			array('bts_name, first_user, last_user', 'length', 'max'=>100),
			array('description', 'length', 'max'=>100),
			array('bts_id', 'length', 'max'=>10),
			array('first_ip, last_ip, lac', 'length', 'max'=>20),
			array('loc_stat_ip, gis_id, gis_name', 'length', 'max'=>30),
			array('longitude, latitude, first_update, last_update, gis_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, bts_name,cellname,site_id,site_type, description, bts_id, longitude, latitude, first_ip, first_user, first_update, last_ip, last_user, last_update, lac, show_flag, loc_stat_ip, flag_deleted, id_loc_stat, gis_id, gis_name, gis_date, sector_type', 'safe', 'on'=>'search'),
		);
	}

		
	public function cekUnique($attribute,$params)
	{
		$current = Bts::model()->findByAttributes(array('cellname'=>$this->cellname,'site_id'=>$this->site_id,'bts_id'=>$this->bts_id,'lac'=>$this->lac,'type'=>0));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id != $this->id)
			$this->addError('cellname'," '$this->cellname' has already been taken.");
			$this->addError('site_id'," '$this->site_id' has already been taken.");
			$this->addError('bts_id'," '$this->bts_id' has already been taken.");
			$this->addError('lac'," '$this->lac' has already been taken.");
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Lac'=>array(self::BELONGS_TO, 'Lac', 'lac')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'bts_name' => 'Name',
			'cellname' => 'Cell Name',
			'description' => 'Description',
			'bts_id' => 'Bts',
			'longitude' => 'Longitude',
			'latitude' => 'Latitude',
			'first_ip' => 'First Ip',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'last_ip' => 'Last Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'lac' => 'Lac',
			'show_flag' => 'Show Flag',
			'loc_stat_ip' => 'Loc Stat Ip',
			'flag_deleted' => 'Flag Deleted',
			'id_loc_stat' => 'Id Loc Stat',
			'gis_id' => 'Gis',
			'gis_name' => 'Gis Name',
			'gis_date' => 'Gis Date',
			'sector_type' => 'Sector Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition=' type = 0 and (flag_deleted IS NULL or flag_deleted <>:deleted)';
		$criteria->params=array(':deleted'=>1);	
		
		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type);
		$criteria->compare('bts_name',$this->bts_name,true);
		$criteria->compare('cellname',$this->cellname,true);
		$criteria->compare('site_id',$this->site_id,true);
		$criteria->compare('site_type',$this->site_type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('bts_id',$this->bts_id,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('lac',$this->lac,true);
		$criteria->compare('show_flag',$this->show_flag);
		$criteria->compare('loc_stat_ip',$this->loc_stat_ip,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('id_loc_stat',$this->id_loc_stat);
		$criteria->compare('gis_id',$this->gis_id,true);
		$criteria->compare('gis_name',$this->gis_name,true);
		$criteria->compare('gis_date',$this->gis_date,true);
		$criteria->compare('sector_type',$this->sector_type);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}
