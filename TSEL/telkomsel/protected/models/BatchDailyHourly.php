<?php
class BatchDailyHourly extends CFormModel
{
    public $jobs_id;
	public $waktu;
	public $success;
	public $failed;
	
	public function rules()
    {
        return array(
            array('jobs_id', 'required'),
			array('jobs_id', 'CekExistTable'),
			array('jobs_id', 'CekIsset'),
        );
    }
	public function CekExistTable($attribute,$params)
	{
		$table= Yii::app()->db->createCommand('SHOW tables like "'.$this->jobs_id.'" ')->queryScalar();
		if($table == ""){
			$this->addError('jobs_id',"$attribute '$this->jobs_id' Doesn't Exist.");
		}
	}
	
	public function CekIsset($attribute,$params)
	{
		$table= Yii::app()->db->createCommand('select count(column_name) from information_schema.columns where  table_schema="lba_xl" and table_name="'.$this->jobs_id.'"and (column_name ="success" or column_name ="failed" or column_name ="message_id")')->queryScalar();
		if($table != "3"){
			$this->addError('jobs_id'," Data Doesn't Exist.");
		}
	}
	public function attributeLabels()
	{
		return array(
			'jobs_id' => 'Jobs ID',
			'waktu' => 'Date',
			'success' => 'Success',
			'failed' => 'Failed',
		);
	}
}
