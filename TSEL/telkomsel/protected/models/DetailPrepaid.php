<?php

/**
 * This is the model class for table "{{detail_prepaid}}".
 *
 * The followings are the available columns in table '{{detail_prepaid}}':
 * @property integer $id
 * @property integer $id_customer
 * @property integer $prepaid_value
 * @property integer $status
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class DetailPrepaid extends CActiveRecord
{
	public $dateStart;
	public $dateEnd;
	public $id_bulk;
	public $reason;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetailPrepaid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{detail_prepaid}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer, prepaid_value, status', 'required'),
			array('id_customer, prepaid_value, status', 'numerical', 'integerOnly'=>true),
			array('first_user, first_update, last_user, last_update', 'length', 'max'=>50),
			array('first_ip, last_ip', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,id_bulk,reason id_customer, prepaid_value, status, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Customer'=>array(self::BELONGS_TO, 'Customer', 'id_customer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_bulk' => 'Id Bulk',
			'reason' => 'Reason',
			'id_customer' => 'Id Customer',
			'prepaid_value' => 'Prepaid Value',
			'status' => 'Status',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('prepaid_value',$this->prepaid_value);
		$criteria->compare('status',$this->status);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('id_bulk',$this->id_bulk,true);
		$criteria->compare('reason',$this->reason,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}