<?php

/**
 * This is the model class for table "{{paket_campaign}}".
 *
 * The followings are the available columns in table '{{paket_campaign}}':
 * @property integer $id
 * @property string $nama
 * @property integer $harga
 * @property integer $tipe
 * @property integer $total
 * @property string $posisi
 * @property integer $ads_start_time
 * @property integer $ads_time_out
 * @property string $ukuran_gambar1
 * @property string $ukuran_gambar2
 * @property string $ukuran_gambar3
 * @property integer $priority
 * @property string $first_user
 * @property string $first_update
 * @property string $first_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $last_ip
 */
 
class BucketSingleCommand extends CActiveRecord
{
	
	public $idPacketCampaign;
	public $totalBuyToken;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaketCampaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bucket_single_command}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPacketCampaign,totalBuyToken,id_customer,channel_category,total,first_update,last_update,first_ip,last_ip,first_user,last_user', 'required'),
			array('idPacketCampaign,totalBuyToken,id_customer,channel_category,total', 'numerical', 'integerOnly'=>true),
			array('first_user, last_user', 'length', 'max'=>100),
			array('first_ip, last_ip', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPacketCampaign,totalBuyToken,id_customer,channel_category,total,first_update,last_update,first_ip,last_ip,first_user,last_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	 
	public function attributeLabels()
	{
		return array(
			'channel_category' 	=> " Channel Type",
			'id_customer' 	   	=> " Customer Name",
			'total'			   	=> " Total",
			'idPacketCampaign'	=> " Packet Campaign",
			'totalBuyToken'		=> " Total Buy Token"
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('channel_category',$this->channel_category);	
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('total',$this->total);
		
		$criteria->compare('idPacketCampaign',$this->idPacketCampaign);
		$criteria->compare('totalBuyToken',$this->totalBuyToken);
		
		
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}