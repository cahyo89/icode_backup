<?php

/**
 * This is the model class for table "{{voucher_order}}".
 *
 * The followings are the available columns in table '{{voucher_order}}':
 * @property integer $id
 * @property integer $id_customer
 * @property integer $voucher_type
 * @property integer $harga
 * @property integer $total
 * @property integer $remain
 * @property string $start
 * @property string $end
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class VoucherOrder extends CActiveRecord
{
	public $id_customer_dest;
	/**
	 * Returns the static model of the specified AR class.
	 * @return VoucherOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{voucher_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_customer, voucher_type, harga, total, remain', 'numerical', 'integerOnly'=>true),
			array('first_user, last_user', 'length', 'max'=>50),
			array('first_ip, last_ip', 'length', 'max'=>20),
			array('start, end, first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_customer, voucher_type, harga, total, remain, start, end, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_customer' => 'Id Customer',
			'voucher_type' => 'Voucher Type',
			'harga' => 'Harga',
			'total' => 'Total',
			'remain' => 'Remain',
			'start' => 'Start',
			'end' => 'End',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('voucher_type',$this->voucher_type);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('total',$this->total);
		$criteria->compare('remain',$this->remain);
		$criteria->compare('start',$this->start,true);
		$criteria->compare('end',$this->end,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}