<?php
class ApplyParam extends CFormModel
{
	public $jobs_id;
	public $lac_profile;
	public $waktu;
	public $success;
	public $failed;
	

	
	public function attributeLabels()
	{
		return array(
			'jobs_id' => 'Jobs ID',
			'waktu' => 'Date',
			'success' => 'Success',
			'failed' => 'Failed',
			'lac_profile' => 'Lac Profile',
		);
	}
}