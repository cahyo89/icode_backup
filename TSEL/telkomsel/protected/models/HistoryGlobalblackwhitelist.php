<?php

/**
 * This is the model class for table "{{history_globalblackwhitelist}}".
 *
 * The followings are the available columns in table '{{history_globalblackwhitelist}}':
 * @property integer $id
 * @property string $msisdn
 * @property string $shortcode
 * @property string $sms_text
 * @property integer $action
 * @property string $first_update
 * @property string $first_user
 * @property string $first_ip
 * @property string $last_update
 * @property string $last_user
 * @property string $last_ip
 */
class HistoryGlobalblackwhitelist extends CActiveRecord
{
	public $dateStart;
	public $dateEnd;
	public $jumlahF;
	public $jumlahSub;
	public $jumlahUnSub;
	public $jumlahBlock;
	public $jumlahUnBlock;
	public $jumlahNumber;
	/**
	 * Returns the static model of the specified AR class.
	 * @return HistoryGlobalblackwhitelist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{history_globalblackwhitelist}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('action', 'numerical', 'integerOnly'=>true),
			array('msisdn, shortcode, first_user, first_ip, last_user', 'length', 'max'=>50),
			array('last_ip', 'length', 'max'=>25),
			array('sms_text, first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,dateEnd,dateStart, msisdn, shortcode, sms_text, action, first_update, first_user, first_ip, last_update, last_user, last_ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'msisdn' => 'Msisdn',
			'shortcode' => 'Shortcode',
			'sms_text' => 'Sms Text',
			'action' => 'Action',
			'first_update' => 'First Update',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'last_update' => 'Date',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'jumlahF'=>'Invalid Subscriber',
			'jumlahSub'=>'Subscriber',
			'jumlahUnSub'=>'Unsubscriber',
			'jumlahBlock'=>'Block',
			'jumlahUnBlock'=>'Unblock',
			'jumlahNumber'=>'Net Subscriber',
			
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('shortcode',$this->shortcode,true);
		$criteria->compare('sms_text',$this->sms_text,true);
		$criteria->compare('action',$this->action);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}