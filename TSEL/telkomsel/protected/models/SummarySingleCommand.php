<?php

/**
 * This is the model class for table "{{summary_single_command}}".
 *
 * The followings are the available columns in table '{{summary_single_command}}':
 * @property integer $id
 * @property string $trans_date
 * @property integer $id_customer
 * @property string $ani
 * @property integer $success
 * @property integer $failed
 * @property string $error_code
 */
class SummarySingleCommand extends CActiveRecord
{
	public $customer;
	public $resultRepotSumary;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{summary_single_command}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trans_date, id_customer, ani, success, failed, error_code', 'required'),
			array('id_customer, success, failed', 'numerical', 'integerOnly'=>true),
			array('ani, error_code', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, trans_date, id_customer, ani, success, failed, error_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'Customer'=>array(self::BELONGS_TO, 'Customer', 'id_customer'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trans_date' => 'Trans Date',
			'id_customer' => 'Customer',
			'ani' => 'Ani',
			'success' => 'Success',
			'failed' => 'Failed',
			'error_code' => 'Error Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trans_date',$this->trans_date,true);
		$criteria->compare('id_customer',$this->id_customer);
		$criteria->compare('ani',$this->ani,true);
		$criteria->compare('success',$this->success);
		$criteria->compare('failed',$this->failed);
		$criteria->compare('error_code',$this->error_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SummarySingleCommand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
