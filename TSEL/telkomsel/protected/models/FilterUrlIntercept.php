<?php

/**
 * This is the model class for table "{{filter_url_intercept}}".
 *
 * The followings are the available columns in table '{{filter_url_intercept}}':
 * @property integer $id
 * @property string $url_name
 * @property string $url_detail
 * @property string $first_user
 * @property string $first_update
 * @property string $first_ip
 * @property string $last_user
 * @property string $last_update
 * @property string $last_ip
 * @property integer $flag_deleted
 */
class FilterUrlIntercept extends CActiveRecord
{
	public $flag_blacklist;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FilterUrlIntercept the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{filter_url_intercept}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flag_blacklist, url_name, url_detail, first_update, first_ip, last_update, last_ip, flag_deleted', 'required'),
			array('flag_deleted', 'numerical', 'integerOnly'=>true),
			array('url_name, first_user, last_user', 'length', 'max'=>100),
			array('first_ip, last_ip', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, url_name, url_detail, first_user, first_update, first_ip, last_user, last_update, last_ip, flag_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url_name' => 'Url Name',
			'url_detail' => 'Url Detail',
			'first_user' => 'First User',
			'first_update' => 'First Update',
			'first_ip' => 'First Ip',
			'last_user' => 'Last User',
			'last_update' => 'Last Update',
			'last_ip' => 'Last Ip',
			'flag_deleted' => 'Flag Deleted',
			'flag_blacklist'=> 'Flag Blacklist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url_name',$this->url_name,true);
		$criteria->compare('url_detail',$this->url_detail,true);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('flag_blacklist',$this->flag_blacklist);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}