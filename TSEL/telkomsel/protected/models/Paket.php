<?php

/**
 * This is the model class for table "{{paket}}".
 *
 * The followings are the available columns in table '{{paket}}':
 * @property integer $id_paket
 * @property string $jenis_paket
 * @property integer $harga_paket
 * @property integer $sms_paket
 * @property integer $expired_paket
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $first_ip
 * @property string $first_update
 * @property string $last_user
 * @property string $last_ip
 * @property string $last_update
 */
class Paket extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Paket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{paket}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenis_paket,harga_paket,sms_paket,expired_paket', 'required'),
			array('jenis_paket', 'cekUnique'),
			
			array('harga_paket, sms_paket, expired_paket, flag_deleted', 'numerical', 'integerOnly'=>true),
			array('jenis_paket, first_user, last_user', 'length', 'max'=>50),
			array('first_ip, last_ip', 'length', 'max'=>20),
			array('flag,first_update, last_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('flag,id_paket, jenis_paket, harga_paket, sms_paket, expired_paket, flag_deleted, first_user, first_ip, first_update, last_user, last_ip, last_update', 'safe', 'on'=>'search'),
		);
	}

	public function cekUnique($attribute,$params)
	{
		$current = Paket::model()->findByAttributes(array('jenis_paket'=>$this->jenis_paket));
		if($current != ""){
			if($current->flag_deleted != 1 && $current->id_paket != $this->id_paket)
			$this->addError('jenis_paket',"$attribute '$this->jenis_paket' has already been taken.");
		}
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paket' => 'Id Paket',
			'jenis_paket' => 'Packet Types',
			'harga_paket' => 'Token Packet Price',
			'sms_paket' => 'Token Packet',
			'expired_paket' => 'Expired Token Packet',
			'flag_deleted' => 'Flag Deleted',
			'first_user' => 'First User',
			'first_ip' => 'First Ip',
			'first_update' => 'First Update',
			'last_user' => 'Last User',
			'last_ip' => 'Last Ip',
			'last_update' => 'Last Update',
			'flag'=>'promo packet type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition=' flag_deleted IS NULL or flag_deleted <>:deleted';
		$criteria->params=array(':deleted'=>1);	
		
		
		$criteria->compare('flag',$this->flag);
		$criteria->compare('id_paket',$this->id_paket);
		$criteria->compare('jenis_paket',$this->jenis_paket,true);
		$criteria->compare('harga_paket',$this->harga_paket);
		$criteria->compare('sms_paket',$this->sms_paket);
		$criteria->compare('expired_paket',$this->expired_paket);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_update',$this->last_update,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>Yii::app()->params['paging'],
			),
		));
	}
}
