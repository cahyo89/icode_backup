<?php

/**
 * This is the model class for table "{{message_filter}}".
 *
 * The followings are the available columns in table '{{message_filter}}':
 * @property integer $id
 * @property string $banned_text
 * @property integer $flag_deleted
 * @property string $first_user
 * @property string $last_user
 * @property string $first_update
 * @property string $last_update
 * @property string $first_ip
 * @property string $last_ip
 */
class MessageFilter extends CActiveRecord
{

	public $ip;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{message_filter}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('flag_deleted', 'numerical', 'integerOnly'=>true),
			array('first_user, last_user, first_ip, last_ip', 'length', 'max'=>50),
			array('banned_text, first_update, last_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, banned_text, flag_deleted, first_user, last_user, first_update, last_update, first_ip, last_ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'banned_text' => 'Banned Text',
			'flag_deleted' => 'Status',
			'first_user' => 'First User',
			'last_user' => 'Last User',
			'first_update' => 'First Update',
			'last_update' => 'Last Update',
			'first_ip' => 'First Ip',
			'last_ip' => 'Last Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('banned_text',$this->banned_text,true);
		$criteria->compare('flag_deleted',$this->flag_deleted);
		$criteria->compare('first_user',$this->first_user,true);
		$criteria->compare('last_user',$this->last_user,true);
		$criteria->compare('first_update',$this->first_update,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('first_ip',$this->first_ip,true);
		$criteria->compare('last_ip',$this->last_ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MessageFilter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
