<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends RController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	
	public function beforeAction(){
	   if ( !Yii::app()->user->isGuest)  {
			$session=new CHttpSession;
			$session->open();
			$connection=Yii::app()->db;
			$sql = "select flag from tbl_users where id = '".$session['id']."' and ip = '".CHttpRequest::getUserHostAddress()."'";
			$sql1 = "select change_pass_date from tbl_users where id = '".$session['id']."'";
			$sql2 = "select status from tbl_users where id = '".$session['id']."'";
			
			$row1=$connection->createCommand($sql1)->queryScalar();
			$row=$connection->createCommand($sql)->queryScalar();
			$row2 = $connection->createCommand($sql2)->queryScalar();
			//$this->redirect(array('/user/profile/changepassword'));
			if($row2 != 1)
			{
				Yii::app()->user->logout();
				$this->redirect(array('/site/index'));
			}
			if($row == 1){
			if ( yii::app()->user->getState('userSessionTimeout') < time() ) {
					// timeout
					Yii::app()->user->logout();
					$this->redirect(array('/site/SessionTimeout'));  //
				} else {
					Yii::app()->user->setLastAction();
					yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
					$param= true; 
				}
			}
			else{
				Yii::app()->user->logout();
				$this->redirect(array('/site/index'));  //
				}
			return $param;	
			
		}
		else {
		return true;
		}
	}
	
	function createMenu($param)
	{
		/*
		cara pake:
		Controller::createMenu(array(
		array('label'=>'xxx','link'=>'xxx/xxx'),
		array('label'=>'xxx','link'=>'xxx/xxx')
		));
		*/
		if(count($param) == 1)
		$width = 150;
		else if(count($param) == 2)
		$width = 150;
		else
		$width = 210;
		$menuO = "<table width=$width border='0' cellpadding='0' cellspacing='0'>
		<tr>";
		$x=0;
		$menu = "";
		while($x < count($param)){
		
		$label = $param[$x]['label'];
		$link = $param[$x]['link'];
		
		if($param[$x]['id']==""){
		$final=CHtml::link("$label",array("$link"),array("class"=>"btn btn-info", "type"=>"button"));
		}
		else
		$final=CHtml::link("$label",array("$link",'id'=>$param[$x]['id']),array("class"=>"btn btn-info", "type"=>"button"));
		
		if($param[$x]['conf'] != "")
		$final=CHtml::link("$label","#", array("class"=>"btn btn-info", "type"=>"button","submit"=>array("delete", 'id'=>$param[$x]['id']), 'confirm' => "Are you sure want to delete '".$param[$x]['conf']."' ?"));
		if($param[$x] != null)
		{
			if($param[$x]['img'] == ""){
			$menu .="
			<td>$final</td>
			";
			}
			else{
			$menu .="
			<td>&nbsp;&nbsp;</td><td valign='bottom'>$final</td>
			";
			}
		}
		$x++;
		}
		$menu .="</tr></table>";
		echo $menuO.$menu;

	}
	
	function createInfo($model)
	{
		echo "<div class = 'info'>";
		echo "Create :".$model->first_user."(".$model->first_ip.")/ ".$model->first_update;echo "<br>";
		echo "Update :".$model->last_user."(".$model->last_ip.")/ ".$model->last_update;
		echo "</div>";
		echo "<br />";
		echo "<br />";
	}
	
	function cekRole()
	{
		$session=new CHttpSession;
			$session->open();
		$connection=Yii::app()->db;
			$sqlCel="select itemname from AuthAssignment where userid =  '".$session['id']."' and itemname = 'superUser'";
			$commandCel=$connection->createCommand($sqlCel)->queryAll();
			if($commandCel == NULL)
			return false;
			else 
			return true;
	}
	
	function cekMenu($param)
	{
		$session=new CHttpSession;
		$session->open();
		$connection=Yii::app()->db;
		$sqlCel="select * from AuthAssignment where userid = (select id from tbl_users where id = '".$session['id']."') and itemname = 'Admin'";
		$commandCel=$connection->createCommand($sqlCel)->queryAll();
		if($commandCel == NULL){
			$panjang = strlen($param)+1;
			$param = $param.".";
			$sql = "select child from AuthItemChild where child in(select name from AuthItem where (type = 0 or type = 1) )
					and parent in(select itemname from AuthAssignment where userid = '".$session['id']."')
					and substring(child,1,'".$panjang."') ='".$param."'";
			$command=$connection->createCommand($sql)->queryAll();
			if($command == NULL)
			return false;
			else 
			return true;
		}
		else
		return true;
	}
	
	function getRoleUser($id){
		$AuthAssignment = AuthAssignment::model()->findByAttributes(array('userid'=> $id));
		if($AuthAssignment !== null)
			$return = $AuthAssignment->itemname;
		else
			$return = "Not Set";
			
		return $return;
	}
	
	//get user roles for menu
	function getRoles()
	{
		$roles = Rights::getAssignedRoles(Yii::app()->user->Id);
		$rolein = "(";
		foreach($roles as $role)
		{
			$rolein .= "'".$role->name."',";
		}
		$rolein = rtrim($rolein, ",");
		$rolein .= ")";
		return $rolein;
	}
	
	function getNotAdmin(){
		$AuthAssignment = AuthAssignment::model()->findAll('itemname = "Admin"');
		$return = "";
		foreach($AuthAssignment as $dataM){
			$return .= $dataM->userid.",";
		}
		return substr($return,0,-1);
	}
		
	public function afterCreate($nama,$id)
	{
			$connection = Yii::app()->db;
			$tabel = array();
			$tabel = $this->tablename($nama);
			$mod = call_user_func(array($nama,'model'));
			$current = $mod->findByPk($id);
			$sql="";
			$sql="update ".$tabel[0]." set last_user = '".Yii::app()->user->first_name."', first_user = '".Yii::app()->user->first_name."',last_ip = '".CHttpRequest::getUserHostAddress()."',first_ip = '".CHttpRequest::getUserHostAddress()."',last_update = NOW(),first_update = NOW() where ".$tabel[1]." = $id";
			$command = $connection->createCommand($sql)->execute();
	}
	
	function tablename($flags)
	{
		if($flags == 1)
		{
			$tabel = "tbl_media_seller";
		}
		else if($flags == 2)
		{
			$tabel = "tbl_product_category";
		}
		else if($flags == 3)
		{
			$tabel = "tbl_advertiser";
		}
		else if($flags == 4)
		{
			$tabel = "tbl_bsc";
		}
		else if($flags == 5)
		{
			$tabel = "tbl_bsc_type";
		}
		else if($flags == "Bts")
		{
			$tabel[0] = "tbl_bts";
			$tabel[1] = "id";
		}
		else if($flags == 7)
		{
			$tabel = "tbl_bts_group";
		}
		else if($flags == 8)
		{
			$tabel = "tbl_batch";
		}
		else if($flags == "Ani")
		{
			$tabel[0] = "tbl_ani";
			$tabel[1] = "id";
		}
		else if($flags == 10)
		{
			$tabel = "tbl_location_category";
		}
		else if($flags == 11)
		{
			$tabel = "tbl_packet";
		}
		else if($flags == 12)
		{
			$tabel = "tbl_advertiser";
		}
		else if($flags == 13)
		{
			$tabel = "tbl_cbc";
		}
		else if($flags == "Banned")
		{
			$tabel[0] = "tbl_banned";
			$tabel[1] = "id";
		}
		else if($flags == "Probing")
		{
			$tabel[0] = "tbl_probing";
			$tabel[1] = "id_probing";
		}
		else if($flags == "Lac")
		{
			$tabel[0] = "tbl_lac";
			$tabel[1] = "id";
		}
		else if($flags == "LocationStat")
		{
			$tabel[0] = "tbl_location_stat";
			$tabel[1] = "id_loc_stat";
		}
		else if($flags == "GlobalBlacklistAsli")
		{
			$tabel[0] = "tbl_global_blacklist_asli";
			$tabel[1] = "id";
		}
		else if($flags == "TreeId")
		{
			$tabel[0] = "tbl_tree_id";
			$tabel[1] = "tree_id";
		}
		else if($flags == "Paket")
		{
			$tabel[0] = "tbl_paket";
			$tabel[1] = "id_paket";
		}
		else if($flags == "Customer")
		{
			$tabel[0] = "tbl_customer";
			$tabel[1] = "id_customer";
		}
		else if($flags == "ShortcodeConfiguration")
		{
			$tabel[0] = "tbl_shortcode_configuration";
			$tabel[1] = "id";
		}
		else if($flags == "RuleTemplate")
		{
			$tabel[0] = "tbl_rule_template";
			$tabel[1] = "id";
		}
		else if($flags == "InvoiceLba")
		{
			$tabel[0] = "tbl_invoice_lba";
			$tabel[1] = "id_invoice";
		}
		else if($flags == "UrlInterface")
		{
			$tabel[0] = "tbl_url_interface";
			$tabel[1] = "id";
		}
		else if($flags == "GlobalWhitelist")
		{
			$tabel[0] = "tbl_global_whitelist";
			$tabel[1] = "id";
		}
		else if($flags == "GlobalConfigurationSubs")
		{
			$tabel[0] = "tbl_global_configuration_subs";
			$tabel[1] = "shortcode";
		}
		else if($flags == "SmppIn")
		{
			$tabel[0] = "tbl_smpp_in";
			$tabel[1] = "id";
		}
		else if($flags == "SmppOut")
		{
			$tabel[0] = "tbl_smpp_out";
			$tabel[1] = "id";
		}
		else if($flags == "BtsTest")
		{
			$tabel[0] = "tbl_bts_test";
			$tabel[1] = "id";
		}
		else if($flags == "BtsTest")
		{
			$tabel[0] = "tbl_bts_test";
			$tabel[1] = "id";
		}
		else if($flags == "LacProfile")
		{
			$tabel[0] = "tbl_lac_profile";
			$tabel[1] = "id";
		}
		else if($flags == "LacProfileDetail")
		{
			$tabel[0] = "tbl_lac_profile_detail";
			$tabel[1] = "id";
		}
		else if($flags == "MessageFilter")
		{
			$tabel[0] = "tbl_message_filter";
			$tabel[1] = "id";
		}
		return $tabel;
	}
	
	/*
	function cekExist($id,$case){
		if($case == "Customer")
		{
			$exist = Campaigne::model()->findByPk(array('id_customer'=>$id));
			if(!empty($exist))
			{
				return false;
			}
		}
		else if($case == "Customer")
		{
			$exist = Campaigne::model()->findByPk(array('id_customer'=>$id));
			if(!empty($exist))
			{
				return false;
			}
		}
		return $return;
	}
	*/
	
	function cekExist($id,$field,$case){
		if($case == "Campaign")
		{
			$exist = Campaign::model()->findAll($field.' = '.$id);
			if(!empty($exist))
			{
				return false;
			}
			else
				return true;
		}
		else if($case == "Customer")
		{
			$exist = Customer::model()->findAll($field.' = '.$id);

			if(!empty($exist))
			{
				return false;
			}
			else
				return true;
		}
	}

	function getCI($id){
		$bts = Bts::model()->findByPk($id);
		$lac = CHtml::listData(Lac::model()->findAll('(flag_deleted is null or flag_deleted <> 1)'),'lac_id','description');
		$return = "[".$bts->bts_id."|".$bts->lac."] ".$bts->bts_name." (".$lac[$bts->lac].")";
		return $return;
	}
	
	public function getData($nama,$id)
	{
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		if($nama == 'Banned')
		{
			$data = $current->banned_text;
		}
		else if($nama == 'Bts')
		{
			$data = $current->bts_name.','.$current->type;
		}
		else if($nama == 'Probing')
		{
			$data = $current->probing_name;
		}
		else if($nama == 'Lac')
		{
			$data = $current->lac_name;
		}
		else if($nama == 'LocationStat')
		{
			$data = $current->location_stat_name;
		}
		else if($nama == 'GlobalBlacklistAsli')
		{
			$data = $current->name;
		}
		else if($nama == 'TreeId')
		{
			$data = $current->nama_perangkat;
		}
		else if($nama == 'Paket')
		{
			$data = $current->jenis_paket;
		}
		else if($nama == 'Ani')
		{
			$data = $current->ani;
		}
		else if($nama == 'Customer')
		{
			$data = $current->nama;
		}
		else if($nama == 'ShortcodeConfiguration')
		{
			$data = $current->shortcode;
		}
		else if($nama == 'RuleTemplate')
		{
			$data = $current->rule_name;
		}
		else if($nama == 'InvoiceLba')
		{
			$data = $current->invoice_no;
		}
		else if($nama == 'UrlInterface')
		{
			$data = $current->url_interface;
		}
		else if($nama == 'GlobalWhitelist')
		{
			$data = $current->subscriber_number;
		}
		else if($nama == 'GlobalConfigurationSubs')
		{
			$data = $current->shortcode;
		}
		else if($nama == 'SmppIn')
		{
			$data = $current->name;
		}
		else if($nama == 'SmppOut')
		{
			$data = $current->smpp_conn_id;
		}
		return $data;
	}
	
	public function deleted($nama,$id)
	{
		//$nama = $this->projectname($tanda);
		//$current = $nama::model()->findByPk($id);
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		$current->flag_deleted = 1;
		if($nama == "Ani")
		{
			$current->status = null;
		}
		else if($nama == "Packet")
		{
			$current->status = null;
		}
		else if($nama == "Advertiser")
		{
			$current->approved_packet = null;
		}
		else if($nama == "ProductCategory")
		{
			$current->channel_use = null;
		}
		$current->update();
	}
	
	public function beforeDelete($nama,$id)
	{
		$data = $this->getData($nama,$id);
		$data = explode(",",$data);
		$connection = Yii::app()->db;
		
		if($nama == 'Banned')
		{
			$berita = "Delete ".$nama." : Banned Text : $data[0]";
		}
		else if($nama == 'Bts')
		{
			$berita = "Delete ".$nama." : Name : $data[0], type : $data[1]";
		}
		else if($nama == 'Probing')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Lac')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'LocationStat')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'GlobalBlacklistAsli')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'TreeId')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Paket')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Ani')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'Customer')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'ShortcodeConfiguration')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'RuleTemplate')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'InvoiceLba')
		{
			$berita = "Delete ".$nama." : invoice_no : $data[0]";
		}
		else if($nama == 'UrlInterface')
		{
			$berita = "Delete ".$nama." : Url : $data[0]";
		}
		else if($nama == 'GlobalWhitelist')
		{
			$berita = "Delete ".$nama." : Number : $data[0]";
		}
		else if($nama == 'GlobalConfigurationSubs')
		{
			$berita = "Delete ".$nama." : Shortcode : $data[0]";
		}
		else if($nama == 'SmppIn')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		else if($nama == 'SmppOut')
		{
			$berita = "Delete ".$nama." : Name : $data[0]";
		}
		//$this->addberita($berita);
	}
	
	public function addberita($berita)
	{
		$connection=Yii::app()->db;
		$sql = "insert into tbl_activity_history(iduser,username,berita,date,ip) value ('".Yii::app()->user->id."','".Yii::app()->user->first_name."','".$berita."',NOW(),'".CHttpRequest::getUserHostAddress()."')";
		$command = $connection->createCommand($sql)->execute();		
	}
	
	function selectedItem($tbl,$select,$tbldetail,$idHeader,$id){
		$connection = Yii::app()->db;
		if($tbl =="tbl_customer")
			$idTbl = "id_customer";
		else
			$idTbl = "id";
		$sql = "select * from $tbl where $idTbl in(select $select from $tbldetail where $idHeader = $id)";
		$row = $connection->createCommand($sql)->queryAll();
		$x = 0;
		$pp = "";
		while($x < count($row))
		{
				 $pp[$row[$x][$idTbl]] = array('selected'=>'selected');
				 $x++;
		}
		return $pp;
	}
	
	function insertSlot($tbl,$idHeader,$idDetail,$model,$slot,$id,$status = false)//insertSlot($tbl,$idHeader,idDetail,$model)
	{	
		if($status){
		$connection = Yii::app()->db;
		$sql = "delete from $tbl where $idHeader = $id";
		$row = $connection->createCommand($sql)->execute();
		}
		$z= 0;
		while($z < count($slot))
		{
		$slotc = new $model;
		$slotc->$idHeader = $id;
		$slotc->$idDetail = $slot[$z];
		$slotc->first_user = Yii::app()->user->first_name;
		$slotc->first_ip = CHttpRequest::getUserHostAddress();
		$slotc->first_update = date('Y-m-d h:i:s');
		$slotc->last_user = Yii::app()->user->first_name;
		$slotc->last_ip = CHttpRequest::getUserHostAddress();
		$slotc->last_update =  date('Y-m-d h:i:s');
		$slotc->save();
		$z++;
		}
	}
	
	public function afterUpdate($nama,$id)
	{
		$connection = Yii::app()->db;
		$tabel = array();
		$tabel = $this->tablename($nama);
		$mod = call_user_func(array($nama,'model'));
		$current = $mod->findByPk($id);
		$sql = "";
		$berita ="";
		$sql = "update ".$tabel[0]." set last_user = '".Yii::app()->user->first_name."', last_ip = '".CHttpRequest::getUserHostAddress()."', last_update = NOW() where ".$tabel[1]." = $id";
		$command = $connection->createCommand($sql)->execute();
		$data = $this->getData($nama,$id);
	}
	
	function getIdCustomerByIdUserAndesta(){
		// cek kalau id usernya adalah admin berarti dia bisa semua
		if(Yii::app()->user->getUserMode() != 0){//Yii::app()->user->isAdmin()
			// kalau dia adalah operator / media seller atau     customer atau operator cek by usernya dia
			$sql = "select id_customer from tbl_customer where id_customer in ( select id_customer from tbl_users where id = ".Yii::app()->user->id." ) ";
			
		}
		else{
			$sql = "select id_customer from tbl_customer";
		}
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count = count($row);
		$x = 0;
		$listIdCustomer = "";
		while($x < $count)
		{
				$listIdCustomer .= $row[$x]['id_customer'].",";
				$x++;
		}
		if($listIdCustomer != "")
			return rtrim($listIdCustomer,",");	
		else
			return -1;
	}
	
	function getStatusCombo()
	{
		return array(0=>'Idle',1=>'Approved',2=>'Progress',6=>'Stopping',7=>'Stopped / Rejected',9=>'Finish',11=>'Scheduled');
	}
		
	function getSelectedItem($tbl,$select,$tbldetail,$idHeader,$id){
		$connection = Yii::app()->db;
		if($tbl =="tbl_customer")
			$idTbl = "id_customer";
		else
			$idTbl = "id";
		$sql = "select * from $tbl where $idTbl in(select $select from $tbldetail where $idHeader = $id)";
		$row = $connection->createCommand($sql)->queryAll();
		$x = 0;
		$pp = "";
		while($x < count($row))
		{
				 $pp[$row[$x][$idTbl]] = "[".$row[$x]['lac']."|".$row[$x]['bts_id']."] ".$row[$x]['bts_name'];
				 $x++;
		}
		return $pp;
	}	
		
	function getComboEventCaptureName(){
		$data = array();
		$sql 		= "select * from tbl_event_capture";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$x = 0;
		$count = count($row);
		while($x < $count){
			$data[$row[$x]['jobs_id']] = $row[$x]['profile_name'];
			$x++;
		}
		return $data;
	}
	
	function generateCampaigneId()
	{
		
		// generate  max id batch from tbl_lba_batch
		$sql 		= "select max(id_batch) as maxs from tbl_lba_batch";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count = count($row);
		if($row[0]['maxs'] == ""){
			$maxIdBatch = 1;
			$batchNo 	= 1;
		}	
		else{
			$maxIdBatch = $row[0]['maxs'];
			$sql="SELECT batch_no,left(right(jobs_id,23),2) as bln FROM tbl_lba_batch WHERE id_batch=".$maxIdBatch;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
					 if(	$row[0]['bln'] != date('m'))
					 {
						 $batchNo=1;
					 } 
					 else
					 {
						 $batchNo=intval($row[0]['batch_no']) + 1;
					 }  
		}
		
		$formatBatchNo = sprintf('%03d',$batchNo);
		$campaigneId = "_".$formatBatchNo."_".date('mYdhis')."_".date("Ymd");
		return $campaigneId;
	}
	
	//=======andesta
	function getChannelCategoryCampaigne($id){
	switch(intval($id)){
		CASE 0:
			return "SMS";
		break;
		CASE 5:
			return "MMS";
		break;
		CASE 6:
			return "STATISTIC";
		break;
		CASE 7:
			return "URL INTERFACE";
		break;
		CASE 8:
			return "USSD";
		break;
		
		CASE 9:
			return "WAP";
		break;
		
		CASE 10:
			return "USSD MENU";
		break;
		
		CASE 11:
			return "SMS FLASH";
		break;
		
		default:
				return "UNDEFINED";
			break;
	}
 }
 
 function getStatusCampaigne($id)
{

	switch(intval($id)){
		case 0:
			return "QUEUE";
		break;
		case 1:
			return "APPROVED";
		break;
		case 2:
			return "PROGRESS";
		break;
		case 3:
			return "RESUMING";
		break;
		case 4:
			return "PAUSING";
		break;
		case 5:
			return "PAUSED";
		break;
		case 6:
			return "STOPPING";
		break;
		case 7:
			return "STOPPED";
		break;
		case 8:
			return "RETRY";
		break;
		case 9:
			return "FINISH";
		break;
		case 11:
			return "SCHEDULED";
		break;
		DEFAULT :
			return "UNDEFINED STATUS";
		break;
	}
}	


function getAdvertisingSendingTypeCampaigne($id){
	switch(intval($id)){
		case 0:
			return "ON ENTRY";
		break;
		case 1:
			return "ON LOCATION";
		break;
		/*
		case 2:
			return "INSTANT PROFILE";
		break;
		case 3:
			return "INSTANT FILE";
		break;
		*/
		default:
			return "UNKNOWN";
		break;
	}
 }
 
 function getPriorityCampaigne($id){
	switch(intval($id)){
		CASE 0:
			return "LOW";
		break;
		CASE 1:
			return "MID";
		break;
		CASE 2:
			return "HIGH";
		break;
		DEFAULT:
			return "UNDEFINED";
		break;
	}
 }
 
 function getCampaigneType($id){
	switch(intval($id)){
		case 0:
			return "NORMAL";
		break;
		case 1:
			return "BLACK LIST FILE";
		break;
		case 2:
			return "WHITE LIST FILE";
		break;
		case 3:
			return "BLACK LIST PROFILE";
		break;
		case 4:
			return "WHITE LIST PROFILE";
		break;
		DEFAULT:
			return "UNDEFINE";
		break;
	}
 }
 
	function getSms($sms)
	{
		return nl2br($sms);
	}	

	function getConstanta($idx=null,$val=null){
		$array = array(
					'tipe_customer'   => array(0 => 'Prepaid', 1=> 'Postpaid'),
					'bypass_approval' => array(0 => 'No Bypass', 1 => 'Yes Bypass'),
					'blocked'         => array(0 => 'Not Blocked', 1 => 'Blocked'),
					'approved'        => array(-1 => 'Not Set',0 => 'New', 1 => 'Approved', 2 => 'Rejected'),
					'dataType'        => array(1 => 'Daily', 0 => 'Hourly'),
					'mediaType'       => array('All'=>'All',0=>"SMS",5=>"MMS",6=>"STATISTIC",7=>"Url Interface"),
					'media_type'       => array(0 => 'New SMS', 5 => 'New MMS',6=>"Statistic",7=>"Url Interface"),
					'grouping'        => array(0 => 'Time', 1 => 'Error Code' , 2 =>'Masking Customer'),
					'dataView'        => array(0 => 'Table', 1 => 'Graphic'),
					'dataHour'        => array("00"=>"00","01" =>"01","02" =>"02","03" =>"03","04" =>"04","05" =>"05","06" =>"06","07"=>"07","08" =>"08","09" =>"09","10" =>"10","11" =>"11","12" =>"12","13" =>"13","14" =>"14","15" =>"15","16" =>"16","17" =>"17","18" =>"18","19" =>"19","20" =>"20","21" =>"21","22" =>"22","23" =>"23"),
					'profile'         => array(1 => 'penduduk', 2 => 'pekerja', 3 => 'last_week', 4 => 'pernah_4_minggu', 5 => 'pernah_3_bulan',6 => 'mingguan', 7 => 'bulanan', 8=> 'full day settler', 9 => "last_day_visitor", 10 => "Sering",12 =>"Last 2 Week")
				);
		if(($idx!="")&&($val!="")){
			$x = $array[$idx][$val];
			$array = $x;
		}
		else if($idx!=""){
			$array = $array[$idx];
		}
		return $array;
	}	

	function getListTanggal($dateFrom,$dateTo,$type = null)
	{
		$dateList = array();
		$datefrom2 = $dateFrom;
		$dateto2 = $dateTo;
		while($datefrom2 <= $dateto2){
			//echo "<BR>".$datefrom2;
			$dateList[] = $datefrom2;
			if($type == 1){
				$dodo = explode (" ",$datefrom2);
				$date1 = explode("-",$dodo[0]);
				$date2 = explode(":",$dodo[1]);
			}
			else
				$date1 = explode("-",$datefrom2);
				
			if($type == 1){
				$datefrom2 = date("Y-m-d H:00", mktime(intval($date2[0])+1, 0, 0, $date1[1], $date1[2], $date1[0]));
			}
			else
				$datefrom2 = date("Y-m-d", mktime(0, 0, 0, $date1[1], intval($date1[2])+1, $date1[0]));
				
		}
		//print_r($dateList);
		//print "<pre>";print_r($dateList);
		return $dateList;
	}	

	function getCustomer($id){
		$model = Customer::model()->findByPk($id);
		return $model->nama;
	}
	
	function getTokenStatus($id)
	{
		$statusToken = array(0=>"Paid",null=>"Free",1=>"Free");
		$model = Users::model()->findByPk($id);
		return $statusToken[$model->flag_free];
	}
	
	function setDetailPO($limit="")
	{
		$dateStart = $_GET['dateStart'];
		$dateEnd = $_GET['dateEnd'];
		$customer = $_GET['customer'];
		$where="";
		if($dateStart != "")
		{
			$where .= "and first_update > '$dateStart 00:00:00'";
		}
		if($dateEnd != "")
		{
			$where .= "and first_update < '$dateEnd 23:59:59'";
		}
		if($customer != "")
		{
			$where .= "and id_customer = '$customer'";
		}
		$poDetail = Yii::app()->db->createCommand('select id from tbl_detail_prepaid where status = 1 '.$where)->queryAll();
		$poall = "";
		foreach($poDetail as $pa)
		{
			$poall .= $limit.$pa["id"].",";
		}
		if($poall == "")
		{
			return "No Data";
		}
		return substr($poall,0,-1);
	}
	
	function allDisc($limit="")
	{
		$poDetail = Yii::app()->db->createCommand('select discount from tbl_detail_prepaid where status = 1')->queryAll();
		$poall = "";
		foreach($poDetail as $pa)
		{
			$poall .= $pa["discount"].$limit.",";
		}
		return substr($poall,0,-1);
	}
	//==============
	
}