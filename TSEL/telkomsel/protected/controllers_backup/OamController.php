<?php

class OamController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','reject','app','pause','resume'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionExcel($jobs_id,$flag)
	{
		set_time_limit(0);
		//$jobs_id = "dodo_1";
		//$flag = "success";
		//$sql='SELECT number,tanggal as waktu FROM '.$jobs_id.' where '.$flag.' > 0 ';
		//$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM `'.$jobs_id.'`')->queryScalar();
		$sql='SELECT number,tanggal as waktu FROM `'.$jobs_id.'` where '.$flag.' > 0 ';
		/*$dataProvider=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'pagination' => array(
	   	             'pageSize' => 9999999,			
			),
		));*/
		
		
		
			//$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM `'.$jobs_id.'`')->queryScalar();
			$sqlError='Select error_code,count(error_code) as jumlah FROM `'.$jobs_id.'` where '.$flag.' > 0  group by error_code';
			/*$dataProviderError=new CSqlDataProvider($sql, array(
				'totalItemCount'=>$count,
				'pagination' => array(                    
                             		'pageSize' => 9999999,
                        	),
			));*/
		
		$header = "Date \t Number\n";
		$dataExcel = "";
		header("Content-type: application/text-plain");
		header("Content-Disposition: attachment; filename=DATA_[".$flag."_number]`".$jobs_id."`.txt");
		header("Pragma: no-cache");
		header("Expires: 0"); 
		
		echo $header;
		
		$dataReader=Yii::app()->db->createCommand($sql)->query();

		while (($row = $dataReader->read()) !== false)
		{
		   $dateX =date("Y-M-d H:i:s",strtotime($row["waktu"])); 
			if(Controller::userMode()){
				$numberX = trim(trim(substr_replace($row['number'], 'xxx',-3,3))) ;
			}
			else{
				$numberX =  "`".trim($row['number']) ;
			}
			
			echo $dateX."\t".$numberX."\n";
		}
		
		$dataReaderError=Yii::app()->db->createCommand($sqlError)->query();
		
		echo "\n\n Error Code \t Total\n";
		
		while (($rowError = $dataReaderError->read()) !== false)
		{
			$errorCodeX =$rowError["error_code"]; 
			$jumlahX =$rowError["jumlah"]; 
			
			
			echo $errorCodeX."\t".$jumlahX."\n";
		}
		
		exit;
		
		Yii::app()->request->sendFile('DATA_['.$flag.'_number]`'.$jobs_id.'`.xls',
			$this->renderPartial('excelNumber',array(
					'model'=>$dataProvider,
					'modelError'=>$dataProviderError,
				),true)
		);
	}
	
	
	public function actionPause($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Oam']))
		{
			$model->attributes=$_POST['Oam'];
			if($model->save()){
				$current= Oam::model()->findByPk($id);
				$current->status_batch = 4;
				$berita = "pause campaigne dengan Jobs ID = $current->jobs_id ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}
		$this->render('pause',array(
			'model'=>$model,
		));
	}
	
	public function actionApp($id)
	{
		$connection = Yii::app()->db;
		$current= Oam::model()->findByPk($id);
		$current->status_batch = 1;
		$berita = "Approve Campaigne dengan Campaigne ID = $current->jobs_id ";
		$current->approved_time = new CDbExpression('NOW()');
		$current->last_ip = CHttpRequest::getUserHostAddress();
		$current->last_user = Yii::app()->user->first_name;
		$current->update();
		Controller::addberita($berita);
		$this->redirect(array('index'));		
	}
	
	
	public function actionStop($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Oam']))
		{
			$model->attributes=$_POST['Oam'];
			if($model->save()){
				$current= Oam::model()->findByPk($id);
				$current->status_batch = 7;
				$berita = "Stop campaigne dengan Jobs ID = $current->jobs_id ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}
		$this->render('stop',array(
			'model'=>$model,
		));
	}
	
	public function actionResume($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Oam']))
		{
			$model->attributes=$_POST['Oam'];
			if($model->save()){
				$current= Oam::model()->findByPk($id);
				$current->status_batch = 3;
				$berita = "Resume campaigne dengan Jobs ID = $current->jobs_id ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}
		$this->render('resume',array(
			'model'=>$model,
		));
	}
	
	
	public function actionReject($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Oam']))
		{
			$model->attributes=$_POST['Oam'];
			if($model->save()){
				$current= Oam::model()->findByPk($id);
				$current->status_batch = 7;
				$berita = "Reject campaigne dengan Jobs ID = $current->jobs_id ";
				$current->last_ip = CHttpRequest::getUserHostAddress();
				$current->last_user = Yii::app()->user->first_name;
				$current->update();
				Controller::addberita($berita);
				$this->redirect(array('index'));
				}
		}

		$this->render('reject',array(
			'model'=>$model,
		));
	}
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionViewWaitingList($id)
	{
		$this->render('viewWaitingList',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionViewProgressList($id)
	{
		$this->render('viewProgressList',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionViewFinishList($id)
	{
		$this->render('viewFinishList',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Oam('searchWaitingList');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Oam']))
			$model->attributes=$_GET['Oam'];
			
		$model2=new Oam('searchProgressList');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['Oam']))
			$model2->attributes=$_GET['Oam'];

		$model3=new Oam('searchFinishList');
		$model3->unsetAttributes();  // clear any default values
		if(isset($_GET['Oam']))
			$model3->attributes=$_GET['Oam'];		
	
		$this->render('admin',array(
			'model'=>$model,
			'model2'=>$model2,
			'model3'=>$model3,
		));
		
	}

	/**
	 * Manages all models.
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Oam::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='oam-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
