<?php

class RuleTemplateController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new RuleTemplate;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['RuleTemplate']))
		{	
			$return = true;
			$model->attributes=$_POST['RuleTemplate'];
			$model->description = $model->rule_name."(".$model->pause_time." | ".$model->resume_time.")";
			if($model->id_customer == "")
				$modelCustomer = CHtml::listData(Customer::model()->findAll(),'id_customer','id_customer');
			else
				$modelCustomer = array(0=>$model->id_customer);
			foreach($modelCustomer as $id => $value){
				$modelCek = RuleTemplate::model()->findByAttributes(array('id_customer'=>$value,'pause_time'=>$model->pause_time,'resume_time'=>$model->resume_time));
				if($modelCek != "")
					$return = false;
			}
			if($model->validate()){
				if($return == true){
					foreach($modelCustomer as $id => $value)
					{
						$modelR=new RuleTemplate;
						$modelR->setIsNewRecord(TRUE);
						$modelR->id = NULL ;
						$modelR->attributes=$_POST['RuleTemplate'];
						$modelR->id_customer = $value;
						$modelR->description = $model->rule_name."(".$model->pause_time." | ".$model->resume_time.")";
						$modelR->save();
						Controller::addberita("Create Time Window Management : Name : $model->rule_name ");
						Controller::afterCreate("RuleTemplate",$modelR->id);
					}
					$this->redirect(array('view','id'=>$modelR->id));
				}
				else
				$model->addError('id_customer','Data Already Exsit.');
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RuleTemplate']))
		{
			$model->attributes=$_POST['RuleTemplate'];
			$model->description = $model->rule_name."(".$model->pause_time." | ".$model->resume_time.")";
			if($model->save()){
				Controller::afterUpdate("RuleTemplate",$model->id);
				Controller::addberita("Update Time Window Management : Name : $modelOld->rule_name -> $model->rule_name ");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
			Controller::beforeDelete('RuleTemplate',$id);
			Controller::addberita("Delete Time Window Management : Name : $model->rule_name ");
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new RuleTemplate('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RuleTemplate']))
			$model->attributes=$_GET['RuleTemplate'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RuleTemplate::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rule-template-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
