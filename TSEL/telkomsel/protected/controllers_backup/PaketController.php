<?php

class PaketController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','index','view'),
				'users'=>Yii::app()->user->checkAccess('Admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Paket;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paket']))
		{
			$model->attributes=$_POST['Paket'];
			$modelCeks = CHtml::listData(Paket::model()->findAllByAttributes(array('jenis_paket'=>$model->jenis_paket,'flag_deleted'=>1)),'jenis_paket','id_paket');
			
			if(!isset($modelCeks[$model->jenis_paket])){
				if($model->save()){
					Controller::afterCreate("Paket",$model->id_paket);
					Controller::addberita("Create Prepiad Packet : Packet : $model->jenis_paket , Price : $model->harga_paket ");
					$this->redirect(array('view','id'=>$model->id_paket));
				}
			}
			else
			{
				$modelU=$this->loadModel($modelCeks[$model->jenis_paket]);
				$modelU->attributes=$_POST['Paket'];
				$modelU->flag_deleted = 0; 
				if($modelU->save()){
					Controller::afterCreate("Paket",$modelU->id_paket);
					Controller::addberita("Create Prepiad Packet : Packet : $model->jenis_paket , Price : $model->harga_paket ");
					$this->redirect(array('view','id'=>$modelU->id_paket));
				}
			
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paket']))
		{
			$model->attributes=$_POST['Paket'];
			if($model->save()){
				Controller::afterUpdate("Paket",$model->id_paket);
				Controller::addberita("Update Prepiad Packet : Packet : $modelOld->jenis_paket -> $model->jenis_paket , Price : $modelOld->harga_paket -> $model->harga_paket");
				$this->redirect(array('view','id'=>$model->id_paket));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if(Controller::cekExist($id,"tipe_paket","Customer")){
				$model=$this->loadModel($id);
				Controller::beforeDelete('Paket',$id);
				Controller::deleted('Paket',$id);
				Controller::addberita("Delete Prepiad Packet : Packet : $model->jenis_paket , Price : $model->harga_paket ");
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Paket('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Paket']))
			$model->attributes=$_GET['Paket'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Paket::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='paket-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
