<?php

class ProbingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Probing;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Probing']))
		{
			$model->attributes=$_POST['Probing'];
			$modelCeks = CHtml::listData(Probing::model()->findAllByAttributes(array('probing_name'=>$model->probing_name,'flag_deleted'=>1)),'probing_name','id_probing');
			
			if(!isset($modelCeks[$model->probing_name])){
				if($model->save()){
					Controller::afterCreate("Probing",$model->id_probing);
					Controller::addberita("Create Probe : Name : $model->probing_name , IP : $model->ip , Port : $model->port");
					$this->redirect(array('view','id'=>$model->id_probing));
				}
			}
			else
			{
				$modelU=$this->loadModel($modelCeks[$model->probing_name]);
				$modelU->attributes=$_POST['Probing'];
				$modelU->flag_deleted = 0; 
				if($modelU->save()){
					Controller::afterCreate("Probing",$modelU->id_probing);
					Controller::addberita("Create Probe : Name : $model->probing_name , IP : $model->ip , Port : $model->port");
					$this->redirect(array('view','id'=>$modelU->id_probing));
				}
			
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Probing']))
		{
			$model->attributes=$_POST['Probing'];
			if($model->save()){
				Controller::afterUpdate("Probing",$model->id_probing);
				$sql = "update tbl_lac set host = '".$model->ip."' ,port = '".$model->port."' where id_probing = ".$id." ";
				Yii::app()->db->createCommand($sql)->execute();
				$sql = "update tbl_lac_profile_detail set ip = '".$model->ip."' ,port = '".$model->port."' where probe_id = ".$id." ";
				Yii::app()->db->createCommand($sql)->execute();
				Controller::addberita("Update Probe : Name : $modelOld->probing_name -> $model->probing_name , IP : $modelOld->ip -> $model->ip , Port : $modelOld->port -> $model->port");
				$this->redirect(array('view','id'=>$model->id_probing));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if(Controller::cekExist($id,"id_probing","Lac")){
				$model=$this->loadModel($id);
				Controller::beforeDelete('Probing',$id);
				Controller::deleted('Probing',$id);
				Controller::addberita("Delete Probe : Name : $model->probing_name , IP : $model->ip , Port : $model->port");
					
				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
			else
			{
				Yii::app()->user->setFlash('error', "Data in Use!");
				$this->redirect(array('index'));
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Probing('search');
		$model->unsetAttributes();  // clear any default values
		Controller::getIdCustomerByIdUserAndesta();
		if(isset($_GET['Probing']))
			$model->attributes=$_GET['Probing'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Probing::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='probing-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
