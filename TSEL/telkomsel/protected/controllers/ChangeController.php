<?php

class ChangeController extends Controller
{
	public function actionIndex($id)
	{
		$model=new Change;
		$model->unsetAttributes();  // clear any default values
		$model->id = $id;
			if(isset($_POST['ajax']) && $_POST['ajax']==='users1-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}
		
		
		if(isset($_POST['Change']))
		{
			$model->attributes=$_POST['Change'];
			if($model->validate()){
				$new_password = User::model()->findbyPk($id);
				$new_password->password = UserModule::encrypting($model->password);
				$new_password->activkey= UserModule::encrypting(microtime().$model->password);
				$new_password->change_pass_date = new CDbExpression('NOW()');
				$new_password->save();
				$this->redirect(array("/user/login"));
			}
		}
		$this->render('index',array(
			'model'=>$model,
			'id'=>$id,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}