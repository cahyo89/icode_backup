<?php

class HistoryGlobalblackwhitelistController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new HistoryGlobalblackwhitelist;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HistoryGlobalblackwhitelist']))
		{
			$model->attributes=$_POST['HistoryGlobalblackwhitelist'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['HistoryGlobalblackwhitelist']))
		{
			$model->attributes=$_POST['HistoryGlobalblackwhitelist'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new HistoryGlobalblackwhitelist('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HistoryGlobalblackwhitelist'])){
			$this->redirect(array('view','dateStart'=>$_GET['dateStart'],'dateEnd'=>$_GET['dateEnd']));
		}
		$this->render('index',array(
			'model'=>$model,
		));
	}

	var $_lastId = 0;
 
   //called on rendering a grid row
   //the first column
   //the params are $data (=the current rowdata) and $row (the row index)
    protected function gridNet($data,$row)
    {
	
		$return =  $data["jumlahNumber"]+$this->_lastId ;  
			
		$this->_lastId = $return;
		
		return $return;
    }
	
	public function actionView()
	{
		$dateStart = $_GET['dateStart'];
		$dateEnd = $_GET['dateEnd'];
		
		$criteria = new CDbCriteria;
		
		$criteria->select='	DATE_FORMAT(last_update,"%Y-%m-%d") as last_update,
							DATE_FORMAT(last_update,"%Y-%m-%d") as tanggal,
							shortcode,
							sms_text,
							sum(case when action = 0 then 1 else 0 end) as jumlahF,
							sum(case when action = 1 then 1 else 0 end) as jumlahSub,
							sum(case when action = 2 then 1 else 0 end) as jumlahUnSub,
							sum(case when action = 3 then 1 else 0 end) as jumlahBlock,
							sum(case when action = 4 then 1 else 0 end) as jumlahUnBlock,
							count(distinct(msisdn)) as jumlahNumber
							';
							
		$criteria->addCondition("last_update >= DATE_FORMAT('".$dateStart." 00:00:00','%Y-%m-%d %H:%i:%s') AND last_update <= DATE_FORMAT('".$dateEnd." 23:59:59','%Y-%m-%d %H:%i:%s')");
		
		$criteria->group='tanggal';
		$criteria->order='tanggal desc';
		
		$sql = 'select
				id,
				DATE_FORMAT(last_update,"%Y-%m-%d") as last_update,
				DATE_FORMAT(last_update,"%Y-%m-%d") as tanggal,
				shortcode,
				sms_text,
				sum(case when action = 0 then 1 else 0 end) as jumlahF,
				sum(case when action = 1 then 1 else 0 end) as jumlahSub,
				sum(case when action = 2 then 1 else 0 end) as jumlahUnSub,
				sum(case when action = 3 then 1 else 0 end) as jumlahBlock,
				sum(case when action = 4 then 1 else 0 end) as jumlahUnBlock,
				count(distinct(msisdn)) as jumlahNumber
				from
				(select distinct msisdn,last_update,action,shortcode,sms_text,id from tbl_history_globalblackwhitelist group by msisdn,action,DATE_FORMAT(last_update,"%Y-%m-%d")) tbl_
				where 
				last_update >= DATE_FORMAT("'.$dateStart.' 00:00:00","%Y-%m-%d %H:%i:%s") AND last_update <= DATE_FORMAT("'.$dateEnd.' 23:59:59","%Y-%m-%d %H:%i:%s")
				group by tanggal
				';
		$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM ('.$sql.') tbl')->queryScalar();
		
		if(isset($_GET['excel'])){
			//$model=HistoryGlobalblackwhitelist::model()->findAll($criteria);
			$dataProvider=new CSqlDataProvider($sql, array(
					'totalItemCount'=>$count,
					'pagination'=>array(
						'pageSize'=>999999,
					),
				));
			Yii::app()->request->sendFile('Opt-in.xls',
				$this->renderPartial('excel',array(
						'model'=>$dataProvider,
						'dateStart'=>$dateStart,
						'dateEnd'=>$dateEnd,
					),true)
			);
		}
		else{
			/*$dataProvider=new CActiveDataProvider('HistoryGlobalblackwhitelist',array(
				'criteria'=>$criteria,
				'pagination'=>array(
					'pageSize'=>Yii::app()->params['paging'],
				),
			));
			*/
			$dataProvider=new CSqlDataProvider($sql, array(
					'totalItemCount'=>$count,
					'pagination'=>array(
						'pageSize'=>999999,
					),
				));
			$this->render('view',array(
				'dataProvider'=>$dataProvider,
			));
		}
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new HistoryGlobalblackwhitelist('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['HistoryGlobalblackwhitelist']))
			$model->attributes=$_GET['HistoryGlobalblackwhitelist'];
				$this->redirect(array('view','dateStart'=>$_GET['dateStart'],'dateEnd'=>$_GET['dateEnd']));

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=HistoryGlobalblackwhitelist::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='history-globalblackwhitelist-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
