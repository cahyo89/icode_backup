<?php

class BucketSingleCommandController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function insertToken($idCustomer,$channelCategory,$total){
		$connection = Yii::app()->db;
		$query = "SELECT id FROM tbl_bucket_single_command where channel_category = ".$channelCategory." ";
		$query .= " AND ";
		$query .= " id_customer = ".$idCustomer." ";
		$rows = $connection->createCommand($query)->queryRow();
		if($rows == 0){
			$query = " INSERT INTO tbl_bucket_single_command ";
			$query .= " ( ";
			$query .= " id_customer, ";
			$query .= " channel_category, ";
			$query .= " total, ";
			$query .= " first_ip, ";
			$query .= " first_user, ";
			$query .= " first_update, ";
			$query .= " last_ip, ";
			$query .= " last_user, ";
			$query .= " last_update ";
			$query .= " ) ";
			$query .= " VALUES ";
			$query .= " ( ";
			$query .= " ".$idCustomer.", ";
			$query .= " ".$channelCategory.", ";
			$query .= " ".$total.", ";
			$query .= " '".CHttpRequest::getUserHostAddress()."', ";
			$query .= " '".Yii::app()->user->first_name."', ";
			$query .= " '".date("Y-m-d H:i:s")."', ";
			$query .= " '".CHttpRequest::getUserHostAddress()."', ";
			$query .= " '".Yii::app()->user->first_name."', ";
			$query .= " '".date("Y-m-d H:i:s")."' ";
			$query .= " ) ";
			$command = $connection->createCommand($query)->execute();
		}
		else{
			$query = " UPDATE tbl_bucket_single_command ";
			$query .= " SET ";
			$query .= " total = total + ".$total.", ";
			$query .= " last_ip = '".CHttpRequest::getUserHostAddress()."', ";
			$query .= " last_user = '".Yii::app()->user->first_name."', ";
			$query .= " last_update = '".date("Y-m-d H:i:s")."' ";
			$query .= " WHERE ";
			$query .= " id_customer = ".$idCustomer." ";
			$query .= " AND ";
			$query .= " channel_category = ".$channelCategory." ";
			$command = $connection->createCommand($query)->execute();
		}
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BucketSingleCommand;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BucketSingleCommand']))
		{
			$model->attributes=$_POST['BucketSingleCommand'];
			$booleanGue = true;
			// check customer
			if($model->id_customer == ""){
				$booleanGue = false;
				$model->addError('error','Please choose Customer');
			}
			
			$paketCampaign 	= PaketCampaign::model()->findByPk($model->idPacketCampaign);
			$customer 		= Customer::model()->findByPk($model->id_customer);
			
			// approved
			if($customer->approved != "1"){
				$booleanGue = false;
				$model->addError('error','Customer '.$customer->nama.' not approved by Admin');
			}
			// blocked
			if($customer->blocked == "1"){
				$booleanGue = false;
				$model->addError('error','Customer '.$customer-nama.' is blocked');
			}
			//balance
			if($customer->prepaid_value < $model->totalBuyToken){
				$booleanGue = false;
				$model->addError('error','Not enough balance token for Customer '.$customer->nama.'');
			}
			
			
			
			if($booleanGue == true){
				// total paket sms
			$total = $paketCampaign->harga*$model->totalBuyToken;
			$model->total = $total;
			$model->first_user = Yii::app()->user->first_name;
			$model->first_update = date("Y-m-d H:i:s");
			$model->first_ip = CHttpRequest::getUserHostAddress();
			$model->last_user = Yii::app()->user->first_name;
			$model->last_update = date("Y-m-d H:i:s");
			$model->last_ip = CHttpRequest::getUserHostAddress();
				$this->insertToken($model->id_customer,$model->channel_category,$model->total);
				$this->redirect(array('index','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BucketSingleCommand']))
		{
			$model->attributes=$_POST['BucketSingleCommand'];
			
			if($model->channel_category < 65280){
			
				$model->posisi				= NULL;
				$model->http_intercept_ads_type		= NULL;	
				$model->ads_start_time		= NULL;
				$model->ads_time_out		= NULL;
				$model->ukuran_gambar1		= NULL;
				$model->ukuran_gambar2		= NULL;
				$model->ukuran_gambar3	    = NULL;
				$model->tipe				= NULL;
			
			}
			
			$model->ukuran_gambar1 = Yii::app()->params['imagesize'][$model->ukuran_gambar1];
			$model->ukuran_gambar2 = Yii::app()->params['imagesize'][$model->ukuran_gambar2];
			$model->ukuran_gambar3 = Yii::app()->params['imagesize'][$model->ukuran_gambar3];
			$model->last_user = Yii::app()->user->first_name;
			$model->last_update = date("Y-m-d H:i:s");
			$model->last_ip = CHttpRequest::getUserHostAddress();
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$sql = "select status_batch from tbl_lba_batch where id_paket_campaign = '".$id."' and status_batch not in(7,9) ";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
		
		if($command == 0)
		{
			$model=$this->loadModel($id);
			Controller::beforeDelete('BucketSingleCommand',$id);
			Controller::deleted('BucketSingleCommand',$id);
			Controller::addberita("Delete Paket Campaign : Packet : $model->nama , Price : $model->harga ");

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		/*$criteria=new CDbCriteria(array(                    
                                'order'=>'nama desc',
                                'condition'=>' flag_deleted is null or flag_deleted =0 ',
		));

		
		$dataProvider=new CActiveDataProvider('PaketCampaign',array(
            'criteria'=>$criteria,
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		
			$model = new BucketSingleCommand('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BucketSingleCommand']))
			$model->attributes=$_GET['BucketSingleCommand'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	   
		$model = new bucketSingleCommand('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BucketSingleCommand']))
			$model->attributes=$_GET['BucketSingleCommand'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PaketCampaign the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = BucketSingleCommand::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PaketCampaign $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bucket-single-command-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
