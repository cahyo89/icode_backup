<?php

class CampaigneController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	
	public function actionOnChangeCampaigneType(){
		$idCampaigneType = Yii::app()->request->getParam('batch_type');
		
		$dropDownProfile = "";
		$dropDownProfile .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('--Normal--'),true);
		
		//if($idCampaigneType > 2){
			foreach(Controller::getProfileComboCampaigne() as $idx => $value){
			  $dropDownProfile .= CHtml::tag('option', array('value'=>$idx),CHtml::encode($value),true);
			}
		//}		
		
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'profile'=>$dropDownProfile,			
			)); 
			  Yii::app()->end();
	}
	
	
	public function actionOnChangeGlobalWhiteList(){
		$idGlobalWhiteList = Yii::app()->request->getParam('globalWhiteList');
		
		
		$dropDownCampaigneType = "";
		$dropDownCampaigneType .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('--Normal--'),true);

		$dropDownProfile = "";
		$dropDownProfile .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('--Normal--'),true);
		
		//if($idGlobalWhiteList == 1){
			//foreach(Controller::getCampaigneTypeCombo() as $idx => $value){
			  //if( $idx == 0 || $idx ==2)
				// $dropDownCampaigneType .= CHtml::tag('option', array('value'=>$idx),CHtml::encode($value),true);
		    //}
		//}
		//else{
			foreach(Controller::getCampaigneTypeCombo() as $idx => $value){
			  //if( $idx == 0 || $idx ==2)
				 $dropDownCampaigneType .= CHtml::tag('option', array('value'=>$idx),CHtml::encode($value),true);
		    }
		//}
		
		
		// return data (JSON formatted)
			// return data (JSON formatted)
			echo CJSON::encode(array(
				'batch_type'=>$dropDownCampaigneType,
			)); 
			  Yii::app()->end();
		
	}
	
	public function actionOnChangeAdvertisingSendingType(){
		$idAdvertisingSendingType = Yii::app()->request->getParam('ast');
		
		$dropDownProfile = "";
		$dropDownProfile .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('--Normal--'),true);
		
		$dropDownCampaigneType = "";
		$dropDownCampaigneType .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('--Normal--'),true);
		
		$dropDownOPT = "";
		
		if($idAdvertisingSendingType == 0){
			foreach(Controller::getProfileComboCampaigne() as $idx => $value){
			  $dropDownProfile .= CHtml::tag('option', array('value'=>$idx),CHtml::encode($value),true);
			}
			
			foreach(Controller::getCampaigneTypeCombo() as $idx => $value){
			  $dropDownCampaigneType .= CHtml::tag('option', array('value'=>$idx),CHtml::encode($value),true);
			}
			
			$dropDownOPT .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('Off'),true);
			$dropDownOPT .= CHtml::tag('option',array('value'=>'1'),CHtml::encode('On'),true);
		}
		else{
			$dropDownOPT .= CHtml::tag('option',array('value'=>'0'),CHtml::encode('Off'),true);
		}
		
	
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'profile'=>$dropDownProfile,
				'batch_type'=>$dropDownCampaigneType,
				'globalWhiteList'=>$dropDownOPT,
			
			)); 
			  Yii::app()->end();
	}
	
	
	public function actionOnChangeChannelCategory(){
		$idChannelCategory = Yii::app()->request->getParam('channel_category');
		$idCustomer = Yii::app()->request->getParam('id_customer');
			
		$dropDownCustomerMasking = "";
		
		if($idChannelCategory != "65280")
			$dropDownCustomerMasking .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
		else
			$dropDownCustomerMasking .= CHtml::tag('option',array('value'=>'MOBADS'),CHtml::encode('MOBADS'),true);
		
		if($idChannelCategory != "" && $idCustomer != ""){
			// customer masking by id customer
			$dataCustomerMasking = Ani::model()->findAll('id_customer = '.$idCustomer.' AND media_type = '.$idChannelCategory.'  AND status = 1 AND (flag_deleted is NULL or flag_deleted<> 1)',
				array(':id_customer'=>$idCustomer)
			);
			$dataCustomerMasking = CHtml::listData($dataCustomerMasking,'ani','ani');
			foreach($dataCustomerMasking as $value=> $ani)
			  $dropDownCustomerMasking .= CHtml::tag('option', array('value'=>$ani),CHtml::encode($ani),true);
		// akhir customer masking
		}
		
		$dropDownPaketCampaign = "";
		$dropDownPaketCampaign .= CHtml::tag('option',array("value"=>""),CHtml::encode("--Please Choose One --"),true);
		$sql = "SELECT * FROM tbl_paket_campaign where channel_category = ".$idChannelCategory."";
		$connection = Yii::app()->db;
				$dataReader = $connection->createCommand($sql);
				$row = $dataReader->queryAll();
				$total = count($row);
				$x = 0;
				while($x < $total){
					$dropDownPaketCampaign .= CHtml::tag('option', array('value'=>$row[$x]['id']),CHtml::encode($row[$x]['nama']),true);	
					$x++;
				}
		
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'customer_ani'=>$dropDownCustomerMasking,
				'id_paket_campaign'=>$dropDownPaketCampaign,
			)); 
			  Yii::app()->end();
	}

	
	public function loadDataRuleTemplateById(){
			$data = array();
			
			$sql 		= "select * from tbl_rule_template";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			$count = count($row);
		    $x = 0;
			while($x < $count){
				$data[$row[$x]['id']]['pause']  = $row[$x]['pause_time'];
				$data[$row[$x]['id']]['resume'] = $row[$x]['resume_time'];
				$x++;
			}	
		return $data;
	}
	
	
	public function checkRuleCampaigne($model){
		if($model->id_ruletemplate[0] == "" ){
			if($model->flagAutoPauseResume == 0)
				$flag = true;
			else
				$flag =  false;
		}
		else{
			$data = $this->loadDataRuleTemplateById();
			$flag = true;
			foreach($model->id_ruletemplate as $idx => $value){
				  
				  if( strtotime($this->getDateByStartPeriode($model->start_periode)." ".$data[$value]['pause']) < strtotime($model->start_periode) ){
					$flag = false;
					break;
				  }
				  
				  if(strtotime($this->getDateByStartPeriode($model->start_periode)." ".$data[$value]['pause']) > strtotime($model->content_expired)){
					$flag = false;
					break;
				  }
				  
				  if(strtotime($this->getDateByStartPeriode($model->start_periode)." ".$data[$value]['resume']) < strtotime($model->start_periode)){
					$flag = false;
					break;
				  }
				  
				  if(strtotime($this->getDateByStartPeriode($model->start_periode)." ".$data[$value]['resume']) > strtotime($model->content_expired)){
					$flag = false;
					break;
				  }
			}			
		}
		
		return $flag;
	}
	
	public function actionOnChangeAutoPauseResume(){
		$flag = Yii::app()->request->getParam('flagAutoPauseResume');
		$idCustomer = Yii::app()->request->getParam('id_customer');
		$dropDownRuleTemplate = "";

		if($flag == 1){
				if($idCustomer != ""){
					$sql 		= "select id,description from tbl_rule_template where id_customer in(".$idCustomer.",-1) order by pause_time asc,resume_time asc";
					$connection = Yii::app()->db;
					$dataReader = $connection->createCommand($sql);
					$row = $dataReader->queryAll();
					$total = count($row);

					$x = 0;
					if($total > 0){
						while($x < $total){
							$dropDownRuleTemplate .= CHtml::tag('option', array('value'=>$row[$x]['id']),CHtml::encode($row[$x]['description']),true);	
							$x++;
						}
					}
					else{
						$dropDownRuleTemplate .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Normal--'),true);
					}
				
				}
				else{
					$dropDownRuleTemplate .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Normal--'),true);
				}
		}
		else{
			$dropDownRuleTemplate .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Normal--'),true);
		}
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'id_ruletemplate'=>$dropDownRuleTemplate,
				
			)); 
			  Yii::app()->end();
		
	}
	
	public function actionOnChangeCustomer(){
		
		$idChannelCategory = Yii::app()->request->getParam('channel_category');
		$idCustomer = Yii::app()->request->getParam('id_customer');
		$flagAutoPauseResume = Yii::app()->request->getParam('flagAutoPauseResume');
		
		//$dropDownCustomerMasking = "";
		//$dropDownCustomerMasking .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Please Select One--'),true);
		
		//$dropDownRuleTemplate = "";
		//$dropDownRuleTemplate .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Normal--'),true);
		
		//$dropDownCampaigneReference = "";
		//$dropDownCampaigneReference .= CHtml::tag('option',array('value'=>''),CHtml::encode('--Normal--'),true);
		
			$dropDownChannelCategory = "";
			$dropDownChannelCategory .= CHtml::tag('option',array("value"=>""),CHtml::encode("--Please Choose One --"),true);
			
		if($idCustomer != "" ){
				
				$customer = Customer::model()->findByPk($idCustomer);
				$campaigneId = $customer->nama.Controller::generateCampaigneId();
				
				/*
				if($flagAutoPauseResume == 1){
					$sql 		= "select id,description from tbl_rule_template where id_customer in(".$idCustomer.",-1)";
					$connection = Yii::app()->db;
					$dataReader = $connection->createCommand($sql);
					$row = $dataReader->queryAll();
					$total = count($row);
					$x = 0;
					while($x < $total){
						$dropDownRuleTemplate .= CHtml::tag('option', array('value'=>$row[$x]['id']),CHtml::encode($row[$x]['description']),true);	
						$x++;
					}
				}
				*/
				
				$sql  = "SELECT id_channel FROM tbl_channel_category_detail where id_customer = ".$idCustomer." ";
				$connection = Yii::app()->db;
				$dataReader = $connection->createCommand($sql);
				$row = $dataReader->queryAll();
				$total = count($row);
				$x = 0;
				while($x < $total){
					$dropDownChannelCategory .= CHtml::tag('option', array('value'=>$row[$x]['id_channel']),Yii::app()->params['channelcatagory'][CHtml::encode($row[$x]['id_channel'])],true);	
					$x++;
				}
				
				$sql 		= "select id_batch,jobs_id from tbl_lba_batch where id_customer = ".$idCustomer." AND status_batch in (7,9) order by start_periode desc";
				$connection = Yii::app()->db;
				$dataReader = $connection->createCommand($sql);
				$row = $dataReader->queryAll();
				$total = count($row);
				$x = 0;
				while($x < $total){
					$dropDownCampaigneReference .= CHtml::tag('option', array('value'=>$row[$x]['id_batch']),CHtml::encode($row[$x]['jobs_id']),true);	
					$x++;
				}
		}
		
		
		$dropDownPaketCampaign = "";
		$dropDownPaketCampaign .= CHtml::tag('option',array("value"=>""),CHtml::encode("--Please Choose One --"),true);
		if($idChannelCategory != ""){
			$sql = "SELECT * FROM tbl_paket_campaign where channel_category = ".$idChannelCategory."";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
				$total = count($row);
				$x = 0;
				while($x < $total){
					$dropDownPaketCampaign .= CHtml::tag('option', array('value'=>$row[$x]['id']),CHtml::encode($row[$x]['nama']),true);	
					$x++;
				}
		}
		
		$dropDownCustomerMasking = "";
		$dropDownCustomerMasking .= CHtml::tag('option',array("value"=>""),CHtml::encode("--Please Choose One --"),true);
		if($idCustomer != "" && $idChannelCategory != ""){
				// customer masking by id customer
				$dataCustomerMasking = Ani::model()->findAll('id_customer in('.$idCustomer.',-1) AND media_type = '.$idChannelCategory.'  AND status = 1 AND (flag_deleted is NULL or flag_deleted<> 1)',
					array(':id_customer'=>$idCustomer)
				);
				  $dataCustomerMasking = CHtml::listData($dataCustomerMasking,'ani','ani');
				  foreach($dataCustomerMasking as $value=> $ani)
				  $dropDownCustomerMasking .= CHtml::tag('option', array('value'=>$ani),CHtml::encode($ani),true);
		}
		
		if($idChannelCategory == 8){
			$dropDownCustomerMasking .= CHtml::tag('option', array('value'=>"USSD"),CHtml::encode("USSD"),true);
		}
		
		
		// return data (JSON formatted)
			echo CJSON::encode(array(
				'jobs_id'=>$campaigneId,
				'customer_ani'=>$dropDownCustomerMasking,
				'id_ruletemplate'=>$dropDownRuleTemplate,
				'campaigneReference'=>$dropDownCampaigneReference,
				'channel_category'=>$dropDownChannelCategory,
				'id_paket_campaign'=>$dropDownPaketCampaign,
			)); 
			  Yii::app()->end();
	}
	
	
	public function actionOnChangePaket(){
		
		$paketID = Yii::app()->request->getParam('http_paket');
		
		if($paketID != "" ){
				
			$paket = PaketCampaign::model()->findByPk($paketID);
				
		}

		// return data (JSON formatted)
		echo CJSON::encode(array(
			'pakethttp'=>$paket,
			'priority'=> Yii::app()->params['priority'][$paket['priority']],
			'tipepaket'=> Yii::app()->params['tipe_paket'][$paket['tipe']],
		)); 
		
		Yii::app()->end();
	}
	
	
	public function actionOnChangeUrlFilter(){
		
		$paketFilter = Yii::app()->request->getParam('url_filter');
		
		if($paketFilter != "" ){
			$filter = FilterUrlIntercept::model()->findByPk($paketFilter);
		}
		// return data (JSON formatted)
		echo CJSON::encode(array(
			'filter'=>$filter['url_detail'],
		)); 
		Yii::app()->end();
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('onChangeCustomer','onChangeChannelCategory','onChangeChangeCustomer','onChangePaket'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function checkValidCustomer($model){
		if($model->id_customer == ""){
			return false;
		}
		else{
			return true;
		}
	}
	
	public function checkValidChannelCategory($model){
		if($model->channel_category == ""){
			return false;
		}
		else{
			return true;
		}
	}
	
	public function checkValidTopik($model){
		if($model->topik == ""){
			return false;
		}
		else{
			return true;
		}
	}
	
	public function checkValidCustomerMasking($model){
		if($model->customer_ani == ""){
			return false;
		}
		else{
			return true;
		}
	}
	
	public function checkValidTotalNumber($model){
		if(count($model->total_number) == 0){
			return false;
		}
		else{
			$flag = true;
			foreach($model->total_number as $idx => $value){
				if( $value == "" ){
					$flag = false;
					break;
				}
				
				if( ctype_digit($value) == false ){
					$flag = false;
					break;
				}
			}
			return $flag;
		}
	}
	
	public function replaceBannedText($text){
		$data = "";
		for($x = 0 ; $x < (strlen($text)) ;$x++){
			$data .= "*"; 
		}
		return $data;
	}
	
	public function checkSpamList($model){
		
		/*
		$sql 		= "select * from tbl_banned where deleted = 0 or deleted is null";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$total = count($row);
		$x = 0;
		while($x < $total){
			$model->sms_text = str_replace("".$row[$x]['banned_text']."",$this->replaceBannedText($row[$x]['banned_text']),$model->sms_text);
			$x++;
		}
		*/
		return $model->sms_text;
	}
	
	public function checkValidSmsText($model){
		
		if($model->channel_category != "5"){
			if($model->sms_text == ""){
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
		
	}
	
	
	public function checkValidStartPeriodeCampaigne($model){
		if($model->start_periode == ""){
			return false;
		}
		else{
			//$mkTimeStartPeriode = strtotime($model->start_periode);
			//if($mkTimeStartPeriode >= mktime()){
				return true;
			//}
			//else{
				//return false;
			//}
		}
	}
	
	public function checkValidContentExpiredCampaigne($model){
		if($model->content_expired == ""){
			return false;
		}
		else{
			$mkTimeContentExpired = strtotime($model->content_expired);
			
			if($mkTimeContentExpired >= mktime()){
				return true;
			}
			else{
				return false;
			}
		}
	}
	
	
	public function checkValidPeriodeCampaigne($model){
		$mkTimeStartPeriode   = strtotime($model->start_periode);
		$mkTimeContentExpired = strtotime($model->content_expired);
		if($mkTimeStartPeriode < $mkTimeContentExpired){
			return true;
		}		
		else{
			return false;
		}
	}
	
	
	
	function afterUpdate($model){
		// add berita
		Controller::addberita("Edit campaigne id :".$model->jobs_id." with status : ".Controller::getStatusCampaigne($model->status_batch)." ");
		
		// update deleted
		$connection = Yii::app()->db;
		$sql = "update tbl_lba_batch set last_user = '".Yii::app()->user->first_name."',last_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),flag_deleted = 0 where jobs_id = '".$model->jobs_id."'";
		$command = $connection->createCommand($sql)->execute();
	}
	
	
	public function afterDelete($idBatch){
		// add berita
		Controller::addberita("Delete campaigne id_batch :".$idBatch."  ");
		
		// update deleted
		$connection = Yii::app()->db;
		$sql = "update tbl_lba_batch set status_batch = 7,stop_periode = '".date("Y-m-d H:i:s")."',last_user = '".Yii::app()->user->first_name."',last_ip ='".CHttpRequest::getUserHostAddress()."',last_update = NOW(),flag_deleted = 1 where id_batch = ".$idBatch."";
		$command = $connection->createCommand($sql)->execute();
	}
	
	
	public function afterSave($model,$idBatch,$lastPrepaidValue,$fileNameMMS){
		$paket = PaketCampaign::model()->findByPk($model->id_paket_campaign);
		$connection = Yii::app()->db;
		
		//filename, booking
		$sql = "UPDATE tbl_lba_batch  SET filename = '".$model->filename."' ,booking = ".$paket['harga']." where id_customer=".$model->id_customer." AND jobs_id='".$model->jobs_id."'";
		$command = $connection->createCommand($sql)->execute();
		
		//insert into table history_invoice
		$sql = "insert into tbl_history_invoice_lba(id_batch,date_history,first_prepaid,id_customer) values ('$idBatch','".date("Y-m-d H:i:s")."','".$lastPrepaidValue."','".$model->id_customer."')";
		$command = $connection->createCommand($sql)->execute();
		
		
		$folderImageCustomer = "/projectlbaxl/program/lba_xl/temp/";
		if(file_exists($folderImageCustomer.$idBatch) == false){
			mkdir($folderImageCustomer.$idBatch);
		}
		
		// rule template
		$this->insertRuleCampaigne($model,$idBatch);
		
		// add berita
		Controller::addberita("create campaigne id :".$model->jobs_id." with status : ".Yii::app()->params['status_batch'][$model->status_batch]." ");
		
		$this->insertEventCapture($model);
		
		// shell exec
		shell_exec('/nfs_data/projectmobads/program/web_admin/sync.sh');
		
	}
	
	function insertRuleCampaigne($model,$lastIdBatch){
		if($model->id_ruletemplate[0] != ""){
			
			foreach($model->id_ruletemplate as $idx => $value){
					$sql 		= "select * from tbl_rule_template where id = ".$value."";
					$connection = Yii::app()->db;
					$dataReader = $connection->createCommand($sql);
					$row = $dataReader->queryAll();
					
					$query = "INSERT INTO tbl_rule_campaigne ";
					$query .= " ( ";
					$query .= " id_batch, ";
					$query .= " id_customer, ";
					$query .= " start_periode, ";
					$query .= " content_expired, ";
					$query .= " pause_time, ";
					$query .= " resume_time, ";
					$query .= " quota_value, ";
					$query .= " first_ip, ";
					$query .= " first_user, ";
					$query .= " first_update, ";
					$query .= " last_ip, ";
					$query .= " last_user, ";
					$query .= " last_update, ";
					$query .= " flag ";
					$query .= " ) ";
					$query .= " VALUES ";
					$query .= " ( ";
					$query .= " ".$lastIdBatch.", ";
					$query .= " ".$model->id_customer.", ";
					$query .= " '".$model->start_periode."', ";
					$query .= " '".$model->content_expired."', ";
					$query .= " '".$this->getDateByStartPeriode($model->start_periode)." ".$row[0]['pause_time']."', ";
					$query .= " '".$this->getDateByStartPeriode($model->start_periode)." ".$row[0]['resume_time']."', ";
					$query .= " 0, ";
					$query .= " '".CHttpRequest::getUserHostAddress()."', ";
					$query .= " '".Yii::app()->user->first_name."', ";
					$query .= " '".date("Y-m-d H:i:s")."', ";
					$query .= " '".CHttpRequest::getUserHostAddress()."', ";
					$query .= " '".Yii::app()->user->first_name."', ";
					$query .= " '".date("Y-m-d H:i:s")."', ";
					$query .= " 1 ";
					$query .= " ) ";
					$command = $connection->createCommand($query)->execute();
			}
		}
	}
	
	public function createTableCampaigneId($model,$idx){
		$connection = Yii::app()->db;
		
		//if($idx == "")
			$jobsId = $model->jobs_id;
		//else
			//$jobsId = ($idx+1).$model->jobs_id;
		
		$sql = "SHOW TABLES LIKE '".$jobsId."'";
		$rows = $connection->createCommand($sql)->queryRow();
		if($rows > 0){
			// do drop table
			$sql = "DROP TABLE `".$jobsId."`";
			$command = $connection->createCommand($sql)->execute();
			// create table
			$sql =		   " CREATE TABLE `".$jobsId."` (
							id int(11) AUTO_INCREMENT primary key,
							request_time datetime,
							response_time datetime,
							ip_request varchar(15),
							uid int,
							url_on_click varchar(100),
							url_requested varchar(100),
							lac int,
							ci int,
							msisdn varchar(30),
							tipe int(11'),
							channel_category int(11),
							result int(11'),
							dynamic_argument blob,
							INDEX (request_time),
							INDEX (response_time),
							INDEX (lac),
							INDEX (ci),
							INDEX (msisdn),
							INDEX (channel_category),
							INDEX (result)
						  ) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ";
			$command = $connection->createCommand($sql)->execute();
		}
		else{
			// create table
			$sql =		   " CREATE TABLE `".$jobsId."` (
							id int(11) AUTO_INCREMENT primary key,
							request_time datetime,
							response_time datetime,
							ip_request varchar(15),
							uid int,
							url_on_click varchar(100),
							url_requested varchar(100),
							lac int,
							ci int,
							msisdn varchar(30),
							tipe int(11),
							channel_category int(11),
							result int(11),
							dynamic_argument blob,
							INDEX (request_time),
							INDEX (response_time),
							INDEX (lac),
							INDEX (ci),
							INDEX (msisdn),
							INDEX (channel_category),
							INDEX (result)
						  ) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ";
			$command = $connection->createCommand($sql)->execute();
		}
	}
	
	public function minBalanceCustomer($model){
		$paket = PaketCampaign::model()->findByPk($model->id_paket_campaign);
		$sql = "UPDATE tbl_customer set prepaid_value = prepaid_value - ".$model->total_number." where id_customer = ".$model->id_customer."";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
	}
	
	public function checkExpiredVoucher($model){
		$customer = Customer::model()->findByPk($model->id_customer);
		if($customer->tipe_customer == 0){
			if(mktime() >= (strtotime($customer->expired_date)) ){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	
	}

	function checkAdvertisingSendingType($model){
		  if($model->ast == -1){
			return false;
		  }
		  else{
			return true;
		  }
	}	
	
	public function checkBlockedCustomer($model){
		$customer = Customer::model()->findByPk($model->id_customer);
		if($customer->blocked == 1){
			return true;
		}
		else{
			return false;
		}	
	}
	
	public function checkValidBalance($model){
		$customer = Customer::model()->findByPk($model->id_customer);
		$paket = PaketCampaign::model()->findByPk($model->id_paket_campaign);
		if($customer->prepaid_value >= $model->total_number ){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	function addOneDay($oldDate,$param){
			
			$date 		= explode(" ",$oldDate);
			$tanggal 	= explode("-",$date[0]);
			$waktu 		= explode(":",$date[1]);
			
			$y = $tanggal[0];
			$m = $tanggal[1];
			$d = $tanggal[2];
			
			$h = $waktu[0];
			$i = $waktu[1];
			$s = $waktu[2];
			
			// hour menit second month day year
			$newDate  =  date("Y-m-d H:i:s", mktime(intval($h),intval($i)+$param,intval($s),intval($tanggal[1]),intval($tanggal[2]), intval($tanggal[0])));
			return $newDate;
	}
	
	public function insertBalancingCampaigne($model,$lastIdBatch,$quota){
		$executeTime = $this->addOneDay($model->start_periode,15);
		$query = "INSERT INTO tbl_balancing_campaigne ";
		$query .= " ( ";
		$query .= " id_batch, ";
		$query .= " id_customer, ";
		$query .= " start_periode, ";
		$query .= " content_expired, ";
		$query .= " quota, ";
		$query .= " periode_time, ";
		$query .= " current_counter, ";
		$query .= " total_number, ";
		$query .= " execute_time, ";
		$query .= " first_update, ";
		$query .= " first_user, ";
		$query .= " first_ip, ";
		$query .= " last_update, ";
		$query .= " last_user, ";
		$query .= " last_ip, ";
		$query .= " flag ";
		$query .= " ) ";
		$query .= " VALUES ";
		$query .= " ( ";
		$query .= " ".$lastIdBatch.", ";
		$query .= " ".$model->id_customer.", ";
		$query .= " '".$model->start_periode."', ";
		$query .= " '".$model->content_expired."', ";
		$query .= " ".$quota.", ";
		$query .= " 15, ";
		$query .= " 0, ";
		$query .= " ".$model->total_number.", ";
		$query .= " '".$executeTime."', ";
		$query .= " '".date("Y-m-d H:i:s")."', ";
		$query .= " '".Yii::app()->user->first_name."', ";
		$query .= " '".CHttpRequest::getUserHostAddress()."', ";
		$query .= " '".date("Y-m-d H:i:s")."', ";
		$query .= " '".Yii::app()->user->first_name."', ";
		$query .= " '".CHttpRequest::getUserHostAddress()."', ";
		$query .= " 1 ";
		$query .= " ) ";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($query)->execute();
		
		
	}
	
	public function processQuota($startPeriode,$contentExpired,$totalNumber){
			$timeStartPeriode   = strtotime($startPeriode);
			$timeContentExpired = strtotime($contentExpired);
			
			$range = $timeContentExpired - $timeStartPeriode;
			if($range <= (15*60)){
				$quota = 1; 
			}
			else{
				$div = ceil($range/(15*60));
				$quota = ceil($totalNumber/$div);
			}
			
		return $quota;
	}
	
	public function sumSmsSendLimit($model){
		if($model->channel_category != ""){
			$sql 		= "select * from tbl_channel_category where id = ".$model->channel_category."";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
		}
		// set sms send limit
		$lengthSMS 			   = strlen($model->sms_text);
		$model->sms_send_limit = 0;
		$model->sms_send_limit = (ceil($lengthSMS/160)) * $model->total_number * ($row[0]['push_price']) ;
		
		return $model->sms_send_limit;
	}
	
	public function parseDefaultCreateDataCampaigne($model){
		// default batch no
		$customer = Customer::model()->findByPk($model->id_customer);
		
		$arrayData = explode("_",$model->jobs_id);
		$model->batch_no = $arrayData[1];
		
		// flag global white list
		$model->flag_global_whitelist = $model->globalWhiteList;
		
		//promotion media 
		if($model->channel_category == 5){
			$model->promotion_media = 2;
		}		
		else{
			$model->promotion_media = 1;
		}
		
		// parse batch reference
		if($model->campaigneReference > 0){
			$model->flag_multi_batch 	= 1;
			$model->id_batch_reference 	= $model->campaigneReference;
			$model->slot_multi_batch	= $this->getSlotReferenceByCampaigneId($model->campaigneReference);
		}
		else{
			$model->flag_multi_batch 	= 0;
			$model->id_batch_reference 	= NULL;
			$model->slot_multi_batch	= NULL;	
		}
		
		// cek jika jumlah group lebih dari 1
		if(count($model->cell) > 1){
			$model->flag_multi_batch   = 1; 
		}
		else{
			$model->flag_multi_batch   = 0;
		}
		
		
		$model->quota = 0;
		
		if($model->channel_category != ""){
			$sql 		= "select * from tbl_channel_category where id = ".$model->channel_category."";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
		}
		// set sms send limit
		$lengthSMS 			   = strlen($model->sms_text);
		$model->sms_send_limit = 0;
		if(count($model->total_number)>0){
			foreach($model->total_number  as $idx => $value){
				$model->sms_send_limit += (ceil($lengthSMS/160)) * $value * ($row[0]['push_price']) ;
			}
		}
		$model->sms_text_view  = "";
		if($customer->bypass_approval == 1){
			$model->status_batch  = 1;
			$model->approved_time = date("Y-m-d H:i:s");
			$model->last_user 		= Yii::app()->user->first_name;
			$model->last_ip 		= CHttpRequest::getUserHostAddress();
			$model->last_update 	= date("Y-m-d H:i:s");
			
		}		
		else{
			$model->status_batch = 0;
		}
		
		//define rule customer segmentation
		if($customer->segmen_customer == 1){
			$model->segmenCustomer = 1;
		}
		else{
			$model->segmenCustomer = 0;
		}
		$model->sms_text = $this->checkSpamList($model);
	}

	public function checkValidEventCaptureName($model){
		if($model->flagEventCapture == 1){
			if($model->eventCaptureName != ""){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return true;
		}
	}

	public function checkValidExistCaptureName($model){
		$sql 		= "select * from tbl_event_capture where profile_name = '".$model->eventCaptureName."'";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		if(count($row) > 0){
			return false;
		}
		else{
			return true;
		}
	}	
	
	public function checkValidBlackWhiteListFile($model){
		if($model->batch_type == 1 || $model->batch_type == 2){
			$model->blackWhiteListFile = CUploadedFile::getInstance($model,'blackWhiteListFile');
			$model->blackWhiteListFile->getSize();
			if($model->blackWhiteListFile == ""){
				return false;
			}
			
			if($model->blackWhiteListFile->getSize() <= 5120000){
				return true;
			}
			else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	public function checkValidMMSFile($model){
		if($model->channel_category == 5){
			$model->mmsFile = CUploadedFile::getInstance($model,'mmsFile');
			
			if($model->mmsFile == ""){
				return false;
			}
			
			if($model->mmsFile->getSize() > 0){
				return true;
			}
			else{
				return false;
			}
		}else{
			return true;
		}
		
	}
	
	
	function insertCampaigneListedFile($model,$lastIdBatch){
		
		// drop if exists
		$sql   = "DROP TABLE IF EXISTS `tbl_batch_listed_".$lastIdBatch."`";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
		
		$sql = " CREATE TABLE `tbl_batch_listed_".$lastIdBatch."` ( ";
		$sql .= " `mdn` varchar(50) DEFAULT NULL, ";
		$sql .= "  KEY `mdn` (`mdn`) ";
		$sql .= " ) ENGINE=MyISAM ";
		//$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
		
			if($model->batch_type < 3){
				if(file_exists($model->blackWhiteListFile->getTempName())){
					$handle = fopen($model->blackWhiteListFile->getTempName(),"r");
					if($handle){
						 while (!feof($handle)) {
							$buffer = fgets($handle, 4096);
							 if( trim($buffer)!="" ){
								if(ctype_digit(trim($buffer))){
									$sql = "INSERT INTO tbl_batch_listed_".$lastIdBatch." (mdn) VALUES ('".trim($buffer)."')";
									//echo $sql."</br>";
									//$connection = Yii::app()->db;
									$command = $connection->createCommand($sql)->execute();
								}
							 }
						 }
						 fclose($handle);
					 }	
				}
			}
			
			// selec insert dari table jobs id
			if($model->listEventCapture != ""){
				$sql = "INSERT INTO tbl_batch_listed_".$lastIdBatch." (mdn) SELECT number from ".$model->listEventCapture."";
				$command = $connection->createCommand($sql)->execute();
			}
			
	}
	
	function insertCampaigneListedOPT($model,$lastIdBatch){
		
	// drop if exists
	//$sql   = "DROP TABLE IF EXISTS `tbl_batch_listed_".$lastIdBatch."`";
	//$connection = Yii::app()->db;
	//$command = $connection->createCommand($sql)->execute();
	
	$connection = Yii::app()->db;
	
	if($model->batch_type == 0   ){
		$sql = " CREATE TABLE `tbl_batch_listed_".$lastIdBatch."` ( ";
		$sql .= " `mdn` varchar(50) DEFAULT NULL, ";
		$sql .= "  KEY `mdn` (`mdn`) ";
		$sql .= " ) ENGINE=MyISAM ";
		//$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
	}
	
	$sql = "INSERT INTO tbl_batch_listed_".$lastIdBatch." (mdn) SELECT subscriber_number FROM tbl_global_whitelist";
	$command = $connection->createCommand($sql)->execute();
		
	}
	
	
	function checkValidGroup($model){
		if(count($model->cell) == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	
	function insertTableLbaBatch($model,$idx,$totalCell){
		/*
		$sql = "INSERT INTO tbl_lba_batch ";
		$sql .= " ( ";
		$sql .= " batch_type, ";
		$sql .= " increase_counter, ";
		$sql .= " unique_subscriber_per_batch, ";
		$sql .= " quota, ";
		$sql .= " profile, ";
		$sql .= " vip, ";
		$sql .= " multimedia_type, ";
		$sql .= " flag_file, ";
		$sql .= " id_customer, ";
		$sql .= " jobs_id, ";
		$sql .= " start_periode, ";
		$sql .= " sms_send_limit, ";
		$sql .= " sms_text, ";
		$sql .= " message_type, ";
		$sql .= " message_bit, ";
		$sql .= " status_batch, ";
		$sql .= " filename, ";
		$sql .= " first_ip, ";
		$sql .= " last_ip, ";
		$sql .= " first_user, ";
		$sql .= " last_user, ";
		$sql .= " first_update, ";
		$sql .= " last_update, ";
		$sql .= " topik, ";
		$sql .= " schedule_delivery, ";
		$sql .= " batch_no, ";
		$sql .= " content_expired, ";
		$sql .= " filenameupload, ";
		//$sql .= " broadcast_type, ";
		$sql .= " total_number, ";
		$sql .= " binary_data, ";
		$sql .= " promotion_media, ";
		$sql .= " sex_category, ";
		$sql .= " age_category, ";
		$sql .= " status_category, ";
		$sql .= " filter_prefix, ";
		$sql .= " education_category, ";
		$sql .= " priority_category, ";
		$sql .= " channel_category, ";
		$sql .= " ast, ";
		$sql .= " cell, ";
		$sql .= " customer_ani ";
		$sql .= " ) ";
		$sql .= " Values ";
		$sql .= " ( ";
		$sql .= " ".$model->batch_type.", ";
		$sql .= " ".$model->increase_counter.", ";
		$sql .= " ".$model->unique_subscriber_per_batch.", ";
		$sql .= " ".$model->quota.", ";
		$sql .= " ".$model->profile.", ";
		$sql .= " ".$model->vip.", ";
		$sql .= " ".$model->multimedia_type.", ";
		$sql .= " ".$model->flag_file.", ";
		$sql .= " ".$model->id_customer.", ";
		$sql .= " '".$model->jobs_id."', ";
		$sql .= " '".$model->start_periode."', ";
		$sql .= " ".$model->sms_send_limit.", ";
		$sql .= " '".$model->sms_text."', ";
		$sql .= " ".$model->message_type.", ";
		$sql .= " ".$model->message_bit.", ";
		$sql .= " ".$model->status_batch.", ";
		$sql .= " '".$model->filename."', ";
		$sql .= " '".$mmodel->first_ip."', ";
		$sql .= " '".$model->last_ip."', ";
		$sql .= " '".$model->first_user."', ";
		$sql .= " '".$model->last_user."', ";
		$sql .= " '".$model->first_update."', ";
		$sql .= " '".$model->last_update."', ";
		$sql .= " '".$model->topik."', ";
		$sql .= " '".$model->schedule_delivery."', ";
		$sql .= " '".$model->batch_no."', ";
		$sql .= " '".$model->content_expired."', ";
		$sql .= " '".$model->filenameupload."', ";
		//$sql .= " ".$model->broadcast_type.", ";
		$sql .= " ".$model->total_number.", ";
		$sql .= " ".$model->binary_data.", ";
		$sql .= " ".$model->promotion_media.", ";
		$sql .= " ".$model->sex_category.", ";
		$sql .= " ".$model->age_category.", ";
		$sql .= " ".$model->status_category.", ";
		$sql .= " ".$model->filter_prefix.", ";
		$sql .= " ".$model->education_category.", ";
		$sql .= " ".$model->priority_category.", ";
		$sql .= " ".$model->channel_category.", ";
		$sql .= " ".$model->ast.", ";
		$sql .= " ".$model->cell.", ";
		$sql .= " '".$model->customer_ani."' ";
		$sql .= " )";
		*/
		
	$sql = " INSERT INTO tbl_lba_batch ";
	$sql .= " ( ";
	$sql .= " cs_description, ";
	$sql .= " topik_mms, ";
	$sql .= " url_wap, ";
	$sql .= " id_customer, ";                 
	$sql .= " jobs_id, ";                     
	$sql .= " start_periode, ";               
	$sql .= " stop_periode, ";                
	$sql .= " sms_send_limit, ";              
	$sql .= " sms_text, ";                    
	$sql .= " message_type, ";                
	$sql .= " message_bit, ";                 
	$sql .= " status_batch, ";                
	$sql .= " filename, ";                    
	$sql .= " first_ip, ";                
	$sql .= " first_user, ";                
	$sql .= " first_update, ";              
	$sql .= " last_ip, ";            
	$sql .= " last_user, ";                   
	$sql .= " last_update, ";                 
	$sql .= " topik, ";                       
	$sql .= " approved_time, ";               
	$sql .= " sms_success, ";                 
	$sql .= " sms_failed, ";                  
	$sql .= " batch_no, ";                    
	$sql .= " content_expired, ";             
	$sql .= " broadcast_type, ";              
	$sql .= " total_number, ";                
	$sql .= " customer_ani, ";                
	$sql .= " binary_data, ";                 
	$sql .= " prefix_detail, ";               
	$sql .= " sms_text_view, ";               
	$sql .= " instant_failed, ";              
	$sql .= " tipe_batch, ";                  
	$sql .= " promotion_media, ";             
	$sql .= " sex_category, ";                
	$sql .= " age_category, ";                
	$sql .= " status_category, ";             
	$sql .= " filter_prefix, ";               
	$sql .= " education_category, ";          
	$sql .= " channel_category, ";            
	$sql .= " priority_category, ";           
	$sql .= " flag_deleted, ";                
	$sql .= " ast, ";                         
	$sql .= " cell, ";                        
	$sql .= " flag_lba, ";                    
	$sql .= " flag_file, ";                   
	$sql .= " multimedia_type, ";             
	$sql .= " quota, ";                       
	$sql .= " profile, ";                     
	$sql .= " batch_type, ";                  
	$sql .= " vip, ";                         
	$sql .= " increase_counter, ";            
	$sql .= " unique_subscriber_per_batch, "; 
	$sql .= " flag_hour, ";                   
	$sql .= " flag_multi_batch, ";            
	$sql .= " slot_multi_batch, ";            
	$sql .= " id_batch_reference, ";          
	$sql .= " black_list_profile, ";          
	$sql .= " flag_template_message, ";       
	$sql .= " profile_gender, ";              
	$sql .= " profile_gprs, ";                
	$sql .= " profile_kesehatan, ";           
	$sql .= " profile_usia, ";                
	$sql .= " profile_lifestyle, ";           
	$sql .= " profile_sains, ";               
	$sql .= " profile_travel, ";              
	$sql .= " profile_arpu, ";                
	$sql .= " param_arpu, ";                  
	$sql .= " param_usia, ";                  
	$sql .= " profile_usia2, ";               
	$sql .= " profile_arpu2, ";               
	$sql .= " flag_master_batch, ";           
	$sql .= " interface_channel, ";           
	$sql .= " response_success, ";            
	$sql .= " response_failed, ";             
	$sql .= " profile_production, ";          
	$sql .= " flag_global_whitelist, ";       
	$sql .= " max_total_response, ";          
	$sql .= " list_id_batch_reference ";
	$sql .= " ) ";
	$sql .= " VALUES ";
	$sql .= " ( ";

	if($model->cs_description == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->cs_description."', ";

	if($model->topik_mms == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->topik_mms."', ";

	if($model->url_wap == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->url_wap."', ";
		
	if($model->id_customer == "")
		$sql .= " NULL, "; 
	else
		$sql .= " $model->id_customer, ";
		
	if($model->jobs_id == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->jobs_id."', ";

	if($model->start_periode == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->start_periode."', ";	

	if($model->stop_periode == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->stop_periode."', ";	

	if($model->sms_send_limit == "")
		$sql .= " NULL, "; 
	else
		$sql .= " ".$this->sumSmsSendLimit($model).", ";	
		
	if($model->sms_text == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->sms_text."', ";

	if($model->message_type == "")
		$sql .= " NULL, "; 
	else
		$sql .= " ".$model->message_type.", ";	

	if($model->message_bit == "")
		$sql .= " NULL, "; 
	else
		$sql .= " ".$model->message_bit.", ";	
		
	if($model->status_batch == "")
		$sql .= " 0, "; 
	else
		$sql .= " ".$model->status_batch.", ";
		
	if($model->filename == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->filename."', ";
		
	if($model->first_ip == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->first_ip."', ";
		
	if($model->first_user == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->first_user."', ";

	if($model->first_update == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->first_update."', ";	

	if($model->last_ip == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->last_ip."', ";	
		
	if($model->last_user == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->last_user."', ";
		
	if($model->last_update == "")
		$sql .= " '".date("Y-m-d H:i:s")."', "; 
	else
		$sql .= " '".$model->last_update."', ";
		
	if($model->topik == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->topik."', ";
		
	if($model->approved_time == "")
		$sql .= " NULL, "; 
	else
		$sql .= " '".$model->approved_time."', ";

	if($model->sms_success == "")	
		$sql .= " 0, ";                 
	else
		$sql .= " 0, ";

	if($model->sms_failed == "")	
		$sql .= " 0, ";                  
	else
		$sql .= " 0, ";

	if($model->batch_no == "")	
		$sql .= " NULL, ";                    
	else
		$sql .= " '".$model->batch_no."', ";
		
	if($model->content_expired == "")	
		$sql .= " NULL, ";  
	else
		$sql .= " '".$model->content_expired."', ";

	if($model->broadcast_type == "")           
		$sql .= " NULL, ";              
	else
		$sql .= " ".$model->broadcast_type.", ";

	if($model->total_number == "")
		$sql .= " NULL, ";   
	else
		$sql .= " $model->total_number, ";


	if($model->customer_ani == "")             
		$sql .= " NULL, ";   
	else
		$sql .= " '".$model->customer_ani."', ";
		
	if($model->binary_data == "")             
		$sql .= " NULL, ";   
	else
		$sql .= " $model->binary_data, ";

		
	if($model->prefix_detail == "")              
		$sql .= " NULL, ";   
	else
		$sql .= " '".$model->prefix_detail."', ";

		
	if($model->sms_text_view == "")            
		$sql .= " NULL, ";
	else
		$sql .= " '".$model->sms_text_view."', ";

		
	if($model->instant_failed == "")               
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->instant_failed.", "; 

	if($model->tipe_batch == "")           
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->tipe_batch.",";

	if($model->promotion_media == "")               
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->promotion_media.", ";

		
	if($model->sex_category == "")          
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->sex_category.",";

	if($model->age_category == "")             
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->age_category.", ";

		
	if($model->status_category == "")             
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->status_category.", ";

	if($model->filter_prefix == "")          
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->filter_prefix.", ";


	if($model->education_category == "")            
		$sql .= " NULL, ";   
	else
		$sql .= " ".$model->education_category.", ";


	if($model->channel_category == "")       
		$sql .= " NULL, ";      
	else
		$sql .= " ".$model->channel_category.", ";

	if($model->priority_category == "")
		$sql .= " NULL, ";
	else      
		$sql .= " ".$model->priority_category.", ";           

	if($model->flag_deleted == "")
		$sql .= " 0, ";
	else
		$sql .= " ".$model->flag_deleted.", ";

	if($model->ast == "")                
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->ast.", ";

	if($model->cell == "")                         
		$sql .= " NULL, ";
	else
		$sql .= " '".$model->cell."', ";

	if($model->flag_lba == "")                        
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_lba.", ";

	if($model->flag_file == "")                    
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_file.", ";

	if($model->multimedia_type == "")                   
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->multimedia_type.", ";

	if($model->quota == "")             
		$sql .= " 0, ";
	else
		$sql .= " ".$model->quota.", ";

	if($model->profile == "")                       
		$sql .= " 0, ";
	else
		$sql .= " ".$model->profile.", ";

	if($model->batch_type == "")                     
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->batch_type.", ";

	if($model->vip == "")                  
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->vip." , ";

	if($model->increase_counter == "")                         
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->increase_counter.", ";

	if($model->unique_subscriber_per_batch == "")            
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->unique_subscriber_per_batch.", ";

	if($model->flag_hour == "") 
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_hour.", ";                   

	if($model->flag_multi_batch == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_multi_batch.", ";            

	if($model->slot_multi_batch == "")
		$sql .= " NULL, ";
	else
		$sql .= " '".$model->slot_multi_batch."', ";            

	if($model->id_batch_reference == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->id_batch_reference.", ";          

	if($model->black_list_profile == "")
		$sql .= " NULL, ";
	else
		$sql .= " '".$model->black_list_profile."', ";          

	if($model->flag_template_message == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_template_message.", ";       

	if($model->profile_gender == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_gender.", ";              

	if($model->profile_gprs == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_gprs.", ";                

	if($model->profile_kesehatan == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_kesehatan.", ";           

	if($model->profile_usia == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_usia.", ";                

	if($model->profile_lifestyle == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_lifestyle.", ";           

	if($model->profile_sains == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_sains.", ";               

	if($model->profile_travel == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_travel.", ";              

	if($model->profile_arpu == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_arpu.", ";                

	if($model->param_arpu == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->param_arpu.", ";                  

	if($model->param_usia == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->param_usia.", ";                  

	if($model->profile_usia2 == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_usia2.", ";               

	if($model->profile_arpu2 == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_arpu2.", ";               

	if($model->flag_master_batch == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_master_batch.", ";           

	if($model->interface_channel == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->interface_channel.", ";           


	if($model->response_success == "")
		$sql .= " 0, ";
	else
		$sql .= " ".$model->response_success.", ";            

	if($model->response_failed == "")
		$sql .= " 0, ";
	else
		$sql .= " ".$model->response_failed.", ";             

	if($model->profile_production == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->profile_production.", ";          


	if($model->flag_global_whitelist == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->flag_global_whitelist.", ";       

	if($model->max_total_response == "")
		$sql .= " NULL, ";
	else
		$sql .= " ".$model->max_total_response.", ";          

	if($model->list_id_batch_reference == "")
		$sql .= " NULL ";
	else
		$sql .= " '".$model->list_id_batch_reference."' ";

	$sql .= " ) ";     

		
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
		if($command){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function getSlotReferenceByCampaigneId($idBatch){
		/*
			CREATE TABLE `tbl_slot` (
			  `id` int(11) NOT NULL auto_increment,
			  `start` datetime NOT NULL,
			  `stop` datetime NOT NULL,
			  `slot4` varchar(16) default NULL,
			  `slot3` varchar(16) default NULL,
			  `slot2` varchar(16) default NULL,
			  `slot1` varchar(16) default NULL,
			  `id_batch` int(11) NOT NULL,
			  `id_group` int(11) NOT NULL,
			  `profile_type` int(11) NOT NULL,
			  `slot_type` int(11) default NULL,
			  PRIMARY KEY  (`id`)
			) ENGINE=MyISAM
		*/
		
		$sql 		= "select * from tbl_slot where id_batch = ".$idBatch."";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		
		$slot = $row[0]['slot1'].$row[0]['slot2'].$row[0]['slot3'].$row[0]['slot4'];
		return $slot;
		
	}
	
	
	public function checkValidChannelCategoryForCustomer($model){
		if($model->channel_category != "" && $model->id_customer != ""){
			$sql 		= "select * from tbl_channel_category_detail where id_channel = ".$model->channel_category." AND id_customer = ".$model->id_customer."";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			if(count($row) > 0){
				return true;
			}
			else{
				return false;
			}	
		}
		else{
			return false;
		}		
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	 
	 
	 
    public function insertEventCapture($model){
		
		if($model->flagEventCapture == 1){
			$group = BtsGroup::model()->findByPk($model->cell);
			$sql = " INSERT INTO tbl_event_capture ";
			$sql .= " ( ";
			$sql .= " profile_name, ";
			$sql .= " jobs_id, ";
			$sql .= " first_user, ";
			$sql .= " first_ip, ";
			$sql .= " first_update, ";
			$sql .= " last_user, ";
			$sql .= " last_ip, ";
			$sql .= " last_update ";
			$sql .= " ) ";
			$sql .= " VALUES ";
			$sql .= " ( ";
			$sql .= " '[".$group->bts_name."]_".$model->eventCaptureName."', ";
			$sql .= " '".$model->jobs_id."', ";
			$sql .= " '".Yii::app()->user->first_name."', ";
			$sql .= " '".CHttpRequest::getUserHostAddress()."', ";
			$sql .= " '".date("Y-m-d H:i:s")."', ";
			$sql .= " '".Yii::app()->user->first_name."', ";
			$sql .= " '".CHttpRequest::getUserHostAddress()."', ";
			$sql .= " '".date("Y-m-d H:i:s")."' ";
			$sql .= " ) ";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql)->execute();
		}
	}	
	
	public function getDateByStartPeriode($model){
			$array = explode(" ",$model);
			return $array[0];
	}
	
	
	public function updateListIdBatchReference($listIdBatchReference){
		$list = rtrim($listIdBatchReference,",");
		$array = explode(",",$list);
		$connection = Yii::app()->db;
		if( count($array)>0 ){
			foreach($array as $idx => $value){
				$sql = "UPDATE tbl_lba_batch set list_id_batch_reference = '".$list."' where id_batch = ".trim($value)." ";
				$command = $connection->createCommand($sql)->execute();
			}
		}
	
	}
	
	function parseDataBlackWhiteListProfile($model){
		if($model->batch_type == 3 || $model->batch_type == 4){
			if($model->batch_type == 3){
				$model->profile = $model->cell.":".$model->profile;
			}
			if($model->batch_type = 4){
				$model->profile = $model->cell.":".$model->profile;
			}
		}
		else{
			$model->profile = 0;
		}
	}
	
	
	public function createXMLUSSD($model,$idBatch){
		if($model->channel_category == 8){
			$xml = "<?xml version='1.0' encoding='UTF-8' ?>";
			$xml .= '<!DOCTYPE umb SYSTEM "./umb.dtd">';
			$xml .= "<umb>";
			$xml .= "<result>";
			$xml .= "<resultdata>";
			$xml .= $model->sms_text."";
			$xml .= "</resultdata>";
			$xml .= "</result>";
			$xml .= "</umb>";
			$handle = fopen("/projectlbaxl/program/lba_xl/ussd_server/".$idBatch.".xml","w");
			fwrite($handle,$xml);
			fclose($handle);
		}
	}
	
	public function checkTopikMMS($model){
		if($model->channel_category == 5){
			if($model->topik_mms == ""){
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
		
	}
	
	public function checkUrlWap($model){
		if($model->channel_category == 9){
			if($model->url_wap == ""){
				return false;
			}
			else{
				return true;
			}
		
		}else{
			return true;
		}
	}
	
	function createFileMessageMMS($idBatch,$model){
		if($model->channel_category == 5){
			$path = "/projectlbaxl/program/lba_xl/temp/";
			if(file_exists($path.$idBatch."") == false){
				$handle = fopen($path.$idBatch.".txt","a");
				fwrite($handle,$model->sms_text);
				fclose($handle);
			}
		}
	}
	
	function checkCSDescription($model){
		if($model->cs_description == ""){
			return false;
		}
		else{
			return true;
		}
	}
	
	/*
		
			IF urlLocal = 0 THEN
				SELECT 1 as hasil INTO resultUrl;
			ELSE
			   IF url = urlLocal THEN
			      SET resultUrl = 1;
				END IF;
			END IF;
	*/
	
	
	function createStoreProc2($model,$idBatch){
		
		$query = " CREATE  PROCEDURE `".$model->jobs_id."`(IN msisdn varchar(100))";
		$query .= " BEGIN ";
		$query .= " DECLARE resultBlackList INT DEFAULT 0;";
		
		if($model->batch_type == 0){ // normal
			$query .= " SELECT 1 as result;";
		}
		if($model->batch_type == 1){ // blacklist
			$query .= " SELECT count(mdn) FROM tbl_batch listed_".$idBatch." INTO resultBlackList;";
			$query .= " IF resultBlackList = 0 THEN ";
			$query .= " SELECT 1 as result; ";
			$query .= " ELSE ";
			$query .= " SELECT id_batch FROM tbl_lba_batch where id_batch = -1";
			$query .= " END IF;";
		}
		if($model->batch_type == 2){ // whitelist
			$query .= " SELECT count(mdn) FROM tbl_batch listed_".$idBatch." ;";
		}
		$query .= " END";
		$command = Yii::app()->db->createCommand($query)->execute();
	
	}
	
	
	/*
	function createStoreProc($model,$idbatch,$ads_type)
	{
		$jobsid = $model->jobs_id;
$storeProc = array(
	0 => array(
		0 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
            
            IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;  			
	
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType >0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
			SELECT 1 as hasil;
			END IF;
			END IF;
			END
		",
		1=> "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;
            
			
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0  THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
			   SELECT 1 as  hasil into resultUserAgent;
			ELSE
			   select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
			   SELECT 1 as hasil into resultLocation;
			ELSE
			    select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultBlackList = 1 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			
			END IF;
			END
		",
		
		2 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

	
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;

            IF resultGlobalBlackList = 0 THEN
  			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultWhiteList = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		"
	),
	
	1=> array(
		0 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(11), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
			SELECT 1 as hasil;
			END IF;
			END IF;
			END
		",
		1 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;
            
			
			SELECT count(subscriber_number) from tbl_batch_listed_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0  THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;

			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
			   SELECT 1 as  hasil into resultUserAgent;
			ELSE
			   select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
			   SELECT 1 as hasil into resultLocation;
			ELSE
			    select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultBlackList = 1 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			
			END IF;
			END
		",
		2 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

	
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;

            IF resultGlobalBlackList = 0 THEN
  			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultWhiteList = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		"
	),
	
	2=> array(
		0 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			
			IF resultGlobalBlackList = 0 THEN
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF eventType = 2 THEN
			SELECT 1 as hasil INTO resultEventType;
			ELSE
			SELECT 0 as hasil INTO resultEventType;
			END IF;
			IF url = -1 THEN
			SELECT 1 as hasil INTO resultUrl;
			ELSE
			SELECT count(id) from tbl_filter_url_intercept where id = url INTO resultUrl;
			END IF;
			IF keyword = -1 THEN
			SELECT 1 as  hasil into resultKeyword;
			ELSE
			SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			IF userAgentLocal = 0 THEN
			SELECT 1 as  hasil into resultUserAgent;
			ELSE
			select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			IF locationCampaigne = '-1' THEN
			SELECT 1 as hasil into resultLocation;
			ELSE
			select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			IF resultEventType = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUrl = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultKeyword = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUserAgent = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultLocation = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultGlobalBlackList > 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultAll = 256 THEN
			SELECT resultAll as hasil;
			END IF;
			END IF;
			END
		",
		
		1 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url INT(11), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;

			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			
			IF resultGlobalBlackList = 0 THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF eventType = 2 THEN
			SELECT 1 as hasil INTO resultEventType;
			ELSE
			SELECT 0 as hasil INTO resultEventType;
			END IF;
			IF url = -1 THEN
			SELECT 1 as hasil INTO resultUrl;
			ELSE
			SELECT count(id) from tbl_filter_url_intercept where id = url INTO resultUrl;
			END IF;
			IF keyword = -1 THEN
			SELECT 1 as  hasil into resultKeyword;
			ELSE
			SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			IF userAgentLocal = 0 THEN
			SELECT 1 as  hasil into resultUserAgent;
			ELSE
			select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			IF locationCampaigne = '-1' THEN
			SELECT 1 as hasil into resultLocation;
			ELSE
			select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			IF resultEventType = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUrl = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultKeyword = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUserAgent = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultLocation = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultBlackList = 1 THEN
			SET resultAll = 0;
			END IF;
			IF resultGlobalBlackList > 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultAll = 256 THEN
			SELECT resultAll as hasil;
			END IF;
			END IF;
			END
		",
		
		2 => "
			CREATE  PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url INT(11), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF eventType = 2 THEN
			SELECT 1 as hasil INTO resultEventType;
			ELSE
			SELECT 0 as hasil INTO resultEventType;
			END IF;
			IF url = -1 THEN
			SELECT 1 as hasil INTO resultUrl;
			ELSE
			SELECT count(id) from tbl_filter_url_intercept where id = url INTO resultUrl;
			END IF;
			IF keyword = -1 THEN
			SELECT 1 as  hasil into resultKeyword;
			ELSE
			SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			IF userAgentLocal = 0 THEN
			SELECT 1 as  hasil into resultUserAgent;
			ELSE
			select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			IF locationCampaigne = '-1' THEN
			SELECT 1 as hasil into resultLocation;
			ELSE
			select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			IF resultEventType = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUrl = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultKeyword = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultUserAgent = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultLocation = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultGlobalBlackList > 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultWhiteList = 0 THEN
			SET resultAll = 0;
			END IF;
			IF resultAll = 256 THEN
			SELECT resultAll as hasil;
			END IF;
			END IF;
			END
		"
	),
);
	$command = Yii::app()->db->createCommand($storeProc[$ads_type][$model->batch_type])->execute();
	
	}
	*/
	
	function createStoreProc($model,$idbatch,$ads_type)
	{
		$jobsid = $model->jobs_id;
$storeProc = array(
	0 => array(
		0 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
            
            IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;  			
	
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType >0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
			SELECT 1 as hasil;
			END IF;
			END IF;
			END
		",
		1=> "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;
            
			
			SELECT count(subscriber_number) from tbl_batch_listed_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0  THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
			   SELECT 1 as  hasil into resultUserAgent;
			ELSE
			   select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
			   SELECT 1 as hasil into resultLocation;
			ELSE
			    select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultBlackList = 1 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		",
		
		2 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

	
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;

            IF resultGlobalBlackList = 0 THEN
  			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultWhiteList = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		"
	),
	
	1=> array(
		0 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(11), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
			SELECT 1 as hasil;
			END IF;
			END IF;
			END
		",
		1 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;
            
			SELECT count(subscriber_number) from tbl_batch_listed_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0  THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;

			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
			   SELECT 1 as  hasil into resultUserAgent;
			ELSE
			   select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
			   SELECT 1 as hasil into resultLocation;
			ELSE
			    select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultBlackList = 1 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		",
		2 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

	
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;

            IF resultGlobalBlackList = 0 THEN
  			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF eventType < 2 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultWhiteList = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		"
	),
	
	2=> array(
		0 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			
			IF resultGlobalBlackList = 0 THEN
			
			IF eventType = 2 THEN
				SELECT 1 as hasil INTO resultEventType;
			ELSE
				SELECT 0 as hasil INTO resultEventType;
			END IF;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF resultEventType = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE	
				SELECT 1 as hasil;
			END IF;
			
			END IF;
			END
		",
		
		1 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultBlackList INT DEFAULT 0;

			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			
			IF resultGlobalBlackList = 0 THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultBlackList;
			
			IF eventType = 2 THEN
				SELECT 1 as hasil INTO resultEventType;
			ELSE
				SELECT 0 as hasil INTO resultEventType;
			END IF;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF resultEventType = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE IF resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE IF resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE IF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE IF resultBlackList = 1 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE IF resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE
				SELECT 1 as hasil;
			END IF;
			END IF;
			END
		",
		
		2 => "
			CREATE DEFINER=`system_user`@`%` PROCEDURE `".$jobsid."`(IN userAgentLocal INT(11),IN urlLocal INT(11),IN msisdn varchar(100),IN eventType INT(11), IN url varchar(100), IN keyword INT(11),IN userAgent varchar(100),IN location varchar(100),IN locationCampaigne varchar(100))
			BEGIN
			DECLARE resultEventType INT DEFAULT 0;
			DECLARE resultUrl INT DEFAULT 0;
			DECLARE resultkeyword INT DEFAULT 0;
			DECLARE resultUserAgent INT DEFAULT 0;
			DECLARE resultlocation INT DEFAULT 0;
			DECLARE resultGlobalBlackList INT DEFAULT 0;
			DECLARE resultAll INT DEFAULT 1;
			DECLARE resultWhiteList INT DEFAULT 0;

			SELECT count(subscriber_number) from tbl_global_blacklist_asli where subscriber_number = msisdn INTO resultGlobalBlackList;
			IF resultGlobalBlackList = 0 THEN
			
			SELECT count(mdn) from tbl_batch_listed_".$idbatch." where mdn = msisdn INTO resultWhiteList;
			
			IF eventType = 2 THEN
			SELECT 1 as hasil INTO resultEventType;
			ELSE
			SELECT 0 as hasil INTO resultEventType;
			END IF;
			
			IF urlLocal = '0' THEN
				SELECT 1 as hasil into resultUrl;
			ELSE
				select count(*) from (select url as hasil) as a where hasil like CONCAT('%~',urlLocal, '~%') INTO resultUrl;
			END IF;
			
			IF keyword = 0 THEN
				SELECT 1 as  hasil into resultKeyword;
			ELSE
				SELECT count(id) from tbl_param_keyword where id = keyword INTO resultKeyword;
			END IF;
			
			IF userAgentLocal = 0 THEN
				SELECT 1 as  hasil into resultUserAgent;
			ELSE
				select count(*) from (select userAgent as hasil) as a where hasil like CONCAT('%~',userAgentLocal, '~%') INTO resultUserAgent;
			END IF;
			
			IF locationCampaigne = '-1' THEN
				SELECT 1 as hasil into resultLocation;
			ELSE
				select count(*) from (select location as hasil) as a where hasil like CONCAT('%~',locationCampaigne, '~%') INTO resultlocation;
			END IF;
			
			IF resultEventType = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultUrl = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultKeyword = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultUserAgent = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultLocation = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF	resultGlobalBlackList > 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSEIF resultWhiteList = 0 THEN
				SELECT id_batch from tbl_lba_batch where id_batch = -100;
			ELSE	
				SELECT 1 as hasil;
			END IF;
			END IF;
			END
		"
	),
);
	$command = Yii::app()->db->createCommand($storeProc[$ads_type][$model->batch_type])->execute();
	}

	
	public function insertMapTokenCampaign($idBatch,$totalNumber,$token,$tipe){
		$sql = " INSERT INTO tbl_map_token_campaign ";
		$sql .= " ( ";
		$sql .= " id_batch, ";
		$sql .= " total_number, ";
		$sql .= " token, ";
		$sql .= " tipe ";
		$sql .= " ) ";
		$sql .= " VALUES ";
		$sql .= " ( ";
		$sql .= " ".$idBatch.", ";
		$sql .= " ".$totalNumber.", ";
		$sql .= " ".$token.", ";
		$sql .= " ".$tipe." ";
		$sql .= " ) ";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql)->execute();
	}
	
	public function actionCreate()
	{
		$model=new Campaigne;
				
		if(isset($_POST['Campaigne']))
		{
			$model->attributes = $_POST['Campaigne'];
			$booleanGue = true;
			
			//print "<pre>";
			//print_r($_POST);
			//print "</pre>";
			//exit;
			
			$model->timePeriodeUnikSub = $model->time_out_unique_subscriber_per_batch;
			
			$this->parseDefaultCreateDataCampaigne($model);
			//NEEDED MODEL TO VALIDATE
			$customer = Customer::model()->findByPk($model->id_customer);
			$paket = PaketCampaign::model()->findByPk($model->id_paket_campaign);			
			$jobs = Campaigne::model()->findByPk(array('jobs_id'=>$model->jobs_id));
			$windowtime = RuleTemplate::model()->findByPk($model->time_gate);		

			//CEK CUSTOMER KOSONG / BANNED / REJECTED
			if($this->checkValidCustomer($model)==false)
			{
				$model->addError('error','Customer is not valid.');
				return false;
			}
			
			//
			//CEK CUSTOMER SEGMEN BUKAN INTERNAL
			if($customer->segmen_customer == 1)
			{
				//CEK EXPIRED VOCER
				if($this->checkExpiredVoucher($model)){
					$booleanGue = false;
					$model->addError('error','Customer Voucher is expired. Please contact your Administrator or Media Seller');
				}
				
				//CEK BALANCE
				if($this->checkValidBalance($model) == false){
					$booleanGue = false;
					$model->addError('error','Not enough balance for create Campaigne');
				}
			}
			
			//CEK CUSTOMER ALLOWED DI CHANNEL CATEGORY
			if($this->checkValidChannelCategoryForCustomer($model) == false){
				$booleanGue = false;
				$model->addError('error','Customer can not choose this channel category');
			}
			
			//CEK JOBS ID
			if($model->jobs_id == "")
			{
				$booleanGue = false;
				$model->addError('error','Jobs ID is empty.');
			}
			
			//CEK JOBS ID SUDAH ADA ATAU BELUM
			if(count($jobs)>0)
			{
				$booleanGue = false;
				$model->addError('error','Jobs ID exists, not available.');
			}
			
			//CEK START PERIODE
			if( $this->checkValidStartPeriodeCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not Valid Date for start periode');
			}
			
			//CEK CONTENT EXPIRED
			if($this->checkValidContentExpiredCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not Valid Date for content expired');
			}
			
			//CEK RANGE BETWEEN START AND EXPIRED PERIODE
			if($this->checkValidPeriodeCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not valid periode date between start periode and content expired');
			}
			
			//CEK IMAGE TYPE
			if($model->channel_category == "65280"){
				if($paket->advertising_content_type == "0"){
					$imagetype = Yii::app()->params['imagetype'];
					foreach($_FILES['Campaigne']['type']['filenameupload'] as $index=>$type)
					{
						if($type != "")
						{
							if(!in_array($type, $imagetype))
							{
								$booleanGue = false;
								$model->addError('error','Image '.$_FILES['Campaigne']['name']['filenameupload'][$index].' type is not allowed');
							}
						}
						else
						{
							$booleanGue = false;
							$model->addError('error','Please Upload Image');
						}
					}
				}
			}
			
			if($model->channel_category == "5"){
				$imagetype = Yii::app()->params['imagetype'];
				foreach($_FILES['Campaigne']['type']['filenameuploadMMS'] as $index=>$type)
				{
					if(!in_array($type, $imagetype))
					{
						$booleanGue = false;
						$model->addError('error','Image '.$_FILES['Campaigne']['name']['filenameuploadMMS'][$index].' type is not allowed');
					}
				}
			}
			
			if($model->channel_category != "65280"){
				$imagetype = Yii::app()->params['filebroadcasttype'];
				foreach($_FILES['Campaigne']['type']['filenameuploadBroadcast'] as $index=>$type)
				{
					if(!in_array($type, $imagetype))
					{
						$booleanGue = false;
						$model->addError('error','File Broadcast '.$_FILES['Campaigne']['name']['filenameuploadBroadcast'][$index].' type is not allowed');
					}
				}
			}
			
			
			if(empty($model->topik))
			{
				$booleanGue = false;
				$model->addError('error','Topik must be filled.');
			}
			
			$model->flying_click_link = str_replace("&","xyzyx",$model->flying_click_link); 
			$model->flying_click_link = str_replace("+","xyzyy",$model->flying_click_link);
			
			/*
			if(empty($model->flying_click_link))
			{
				if($model->channel_category == "65280"){
					$booleanGue = false;
					$model->addError('error','Redirect value must be filled.');
				}
			}
			*/
			
			if(empty($model->id_paket_campaign))
			{
				$booleanGue = false;
				$model->addError('error','Paket Campaign value must be chosen.');
			}
			
			if(empty($model->unique_subscriber_per_batch) && $model->unique_subscriber_per_batch!=0)
			{
				$booleanGue = false;
				$model->addError('error','Unique subscriber must be chosen.');
			}

			if($model->unique_subscriber_per_batch == 1 && empty($model->time_out_unique_subscriber_per_batch))
			{
				$booleanGue = false;
				$model->addError('error','Unique subscriber timeout must be filled.');
			}
			
			if(((strtotime($model->start_periode) - strtotime($model->content_expired)) * -1) <= 21600)
			{
				$model->unique_subscriber_per_batch = 2;
			}

			//Cek file black white list
			if($this->checkValidBlackWhiteListFile($model) == false){
				$booleanGue = false;
				$model->addError('error','Please input valid file black/white list');
			}
						
			if($booleanGue == true)
			{			
				if($customer->bypass_approval==1)
				{
					$model->approved_time = date("Y-m-d H:i:s");
					$model->status_batch = 1;
				}
				
				$model->sms_success = 0;
				$model->sms_failed = 0;
				$model->deleted = 0;
				$arrayData = explode("_",$model->jobs_id);
				$model->batch_no = $arrayData[1];
				$model->broadcast_type = 0;
				
				if($model->segmenCustomer == 1){
					$this->minBalanceCustomer($model);
				}
				
				$model->total_number = $model->total_number * $paket->harga;
				$totalNumber 		 = $model->total_number; 
				
				
				
				$model->first_ip = CHttpRequest::getUserHostAddress();
				$model->first_user = Yii::app()->user->first_name;
				$model->first_update = date("Y-m-d H:i:s");
				$model->last_ip = CHttpRequest::getUserHostAddress();
				$model->last_user = Yii::app()->user->first_name;
				$model->last_update = date("Y-m-d H:i:s");
				
				$model->booking = $paket->harga;
				$model->instant_failed = "";
				$model->binary_data = 0;
				$model->promotion_media = 0;
				$model->sex_category = 0;
				$model->age_category = 0;
				$model->status_category = 0;
				$model->filter_prefix = 0;
				$model->education_category = 0;
				
				
				$model->priority_category = $paket->priority;
				
				$model->ast = 3;
				
				if($model->cell ==  ""){
					$model->cell = -1;
				}
				
				
				$model->multimedia_type = 0;
				$model->quota = 0;
				$model->profile = 0;
				$model->increase_counter = 0;
				//$model->unique_subscriber_per_batch = 0;
				$model->flag_hour = 0;
				$model->flag_multi_batch = 0;
				$model->flag_multi_batch = 0;
				$model->profile_production = 0;
				$model->profile_gender = 0;
				$model->profile_gprs = 0;
				$model->profile_kesehatan = 0;
				$model->profile_usia = 0;
				$model->profile_lifestyle = 0;
				$model->profile_sains = 0;
				$model->profile_travel = 0;
				$model->profile_arpu = 0;
				$model->param_arpu = 0;
				$model->param_usia = 0;
				$model->profile_usia2 = 0;
				$model->profile_arpu2 = 0;
				$model->flag_master_batch = 0;
				$model->interface_channel = 0;
				$model->response_success = 0;
				$model->response_failed = 0;
				$model->profile_production = 0;
				$model->flag_global_whitelist =0;
				$model->balance_flag = 0;
				$model->timeDelivery = 0;
				
				if($model->channel_category == "65280"){
					
					if($paket->advertising_content_type == "1"){
						$model->ads_start_time 	= $paket->ads_start_time;
						$model->ads_time_out 	= $paket->ads_time_out;
						
						$model->image_width_1 	= explode(' x ',$paket->ukuran_gambar1);
						$model->image_width_1 	= $model->image_width_1[0];
						$model->image_width_2 	= explode(' x ',$paket->ukuran_gambar2);
						$model->image_width_2 	= $model->image_width_2[0];
						$model->image_width_3 	= explode(' x ',$paket->ukuran_gambar3);
						$model->image_width_3 	= $model->image_width_3[0];
						$model->image_height_1 	= explode(' x ',$paket->ukuran_gambar1);
						$model->image_height_1 	= $model->image_height_1[1];
						$model->image_height_2 	= explode(' x ',$paket->ukuran_gambar2);
						$model->image_height_2 	= $model->image_height_2[1];
						$model->image_height_3 	= explode(' x ',$paket->ukuran_gambar3);
						$model->image_height_3 	= $model->image_height_3[1];
						$model->inner_html_1 	= "";
						$model->inner_html_2 	= "";
						$model->inner_html_3 	= "";
						$model->http_intercept_ads_value = $paket->posisi;
						$model->http_intercept_ads_type = $paket->http_intercept_ads_type;
						$model->http_intercept_ads_type = $paket->tipe;
						$model->filename = $model->running_text.",".$model->running_text.",".$model->running_text.",";
					}
					else{
						$model->ads_start_time 	= $paket->ads_start_time;
						$model->ads_time_out 	= $paket->ads_time_out;
						
						
						$model->image_width_1 	= explode(' x ',$paket->ukuran_gambar1);
						$model->image_width_1 	= $model->image_width_1[0];
						$model->image_width_2 	= explode(' x ',$paket->ukuran_gambar2);
						$model->image_width_2 	= $model->image_width_2[0];
						$model->image_width_3 	= explode(' x ',$paket->ukuran_gambar3);
						$model->image_width_3 	= $model->image_width_3[0];
						$model->image_height_1 	= explode(' x ',$paket->ukuran_gambar1);
						$model->image_height_1 	= $model->image_height_1[1];
						$model->image_height_2 	= explode(' x ',$paket->ukuran_gambar2);
						$model->image_height_2 	= $model->image_height_2[1];
						$model->image_height_3 	= explode(' x ',$paket->ukuran_gambar3);
						$model->image_height_3 	= $model->image_height_3[1];
						$model->inner_html_1 	= "";
						$model->inner_html_2 	= "";
						$model->inner_html_3 	= "";
						
						$model->http_intercept_ads_value = $paket->posisi;
						$model->http_intercept_ads_type = $paket->http_intercept_ads_type;
						$model->http_intercept_ads_type = $paket->tipe;
						$paketsize = explode(' x ',$paket['ukuran_gambar'.($index+1)]);
						$paketwidth = $paketsize[0];
						$paketheight = $paketsize[1];
						
						//SET ALL UPLOADED FILE NAME INTO FILENAME RECORD
						$allfile = "";
						$allname = "";
						
							$extensions = array(
							1  => 'gif',
							2  => 'jpeg',
							3  => 'png',
							);
					
						foreach($_FILES['Campaigne']['name']['filenameupload'] as $index => $fname)
						{
							$info = pathinfo($fname);
							$allfile .= $fname.",";
							$allname .= "image".($index+1) .$model->jobs_id.".".$info['extension'].",";
						}
						
						$model->filenameupload = $allfile;
						$model->filename = $allname;
						//UPLOAD FILE
						$webPath=dirname(Yii::app()->basePath);
						$imagePath=$webPath.Yii::app()->params['uploadpath'];
						$picture_file = CUploadedFile::getInstancesByName('Campaigne');
						$index = 0;
						$namefile = explode(',',$allname);
						foreach ($picture_file as $image => $pic) {
							if($pic->type != "text/plain")
							{
								$pic->saveAs($imagePath.'view_'.( ($paket['tipe']==1) ? "cpm" : "cpc" ).'/'.$namefile[$index]);
								copy($imagePath.'view_'.( ($paket['tipe']==1) ? "cpm" : "cpc" ).'/'.$namefile[$index],$imagePath."/".$namefile[$index]);
								$index++;
							}
						}
					}
				
				}
				
				// upload file broadcast
				if($model->channel_category != 65280){
					
					$allfile = "";
						$allname = "";
					
						$extensions = array(
						1  => 'txt',
						);	
						
						foreach($_FILES['Campaigne']['name']['filenameuploadBroadcast'] as $index => $fname)
						{
							$info = pathinfo($fname);
							$allfile .= $fname.",";
							$allname .= $model->jobs_id.".".$info['extension']."";
						}
						
						$model->filenameupload = $allfile;
						$model->filename = $allname;
						//UPLOAD FILE
						$webPath=dirname(Yii::app()->basePath);
						$imagePath=$webPath.Yii::app()->params['uploadpathBroadcast'];
						$picture_file = CUploadedFile::getInstancesByName('Campaigne');
						$index = 0;
						$namefile = explode(',',$allname);
						foreach ($picture_file as $image => $pic) {
							if($pic->type == "text/plain")
							{
								$pic->saveAs($imagePath.$namefile[$index]);
								$index++;
							}
						}
				
				}
				
				if($model->channel_category == 5){
						$allfile = "";
						$allname = "";
					
						$extensions = array(
						1  => 'gif',
						2  => 'jpeg',
						3  => 'png',
						);	
						
						foreach($_FILES['Campaigne']['name']['filenameuploadMMS'] as $index => $fname)
						{
							$info = pathinfo($fname);
							$allfile .= $fname.",";
							$allname .= $model->jobs_id.".".$info['extension']."";
						}
						
						$model->filenameupload = $allfile;
						$model->filename = $allname;
						//UPLOAD FILE
						$webPath=dirname(Yii::app()->basePath);
						$imagePath=$webPath.Yii::app()->params['uploadpathMMS'];
						$picture_file = CUploadedFile::getInstancesByName('Campaigne');
						$index = 0;
						$namefile = explode(',',$allname);
						foreach ($picture_file as $image => $pic) {
							if($pic->type != "text/plain")
							{
								$pic->saveAs($imagePath.$namefile[$index]);
								$index++;
							}
						}
						
						// save file mms
						$handle = fopen($imagePath.$model->jobs_id.".txt","a");
						fwrite($handle,$model->sms_text);
						fclose($handle);
				}
				
				$model->time_gate = $windowtime->pause_time."-".$windowtime->resume_time;
				
				if($model->channel_category == 65280){
					$model->queryMGM = 'call '.$model->jobs_id.' ('.$model->id_user_agent.'#$2C'.$model->id_filter_url_intercept.'#$2C"%3$s"#$2C"%4$s"#$2C"~%10$s~"#$2C0#$2C"%12$s"#$2C"~%11$s~"#$2C"'.$model->cell.'");';
				}
				else{
					$model->queryMGM = 'call '.$model->jobs_id.'(%3$s)';
				}
				
				$ads_type = Yii::app()->params['expandvalue'][$model->http_intercept_ads_value];
				
				//SET SMS SEND LIMIT TO PAKET TOTAL;
				$model->sms_send_limit = $model->total_number;
				//SET MESSAGE TYPE
				$model->message_type = 1;
				//SET MESSAGE BIT
				$model->message_bit  = 7;
				//SET STATUS BATCH SAME AS BYPASS APPROVAL INITIATE
				$model->status_batch = $customer->bypass_approval;
				
				if($model->http_intercept_ads_type == 1){
					$model->CPM = $model->total_number; 	
					$model->total_number = $model->total_number * 1000;
					$time = strtotime($model->start_periode);
					$model->expired_time2 = date("Y-m-d H:i:s",($time+3600));
				}				
				else{
					$model->CPM = 0;
					$model->expired_time2 = $model->start_periode;
				}
				
				$model->flag_reload_campaign = 0;
				
				if($model->save()){
					$this->createTableCampaigneId($model,$id="");
					$lastIdBatch = $model->id_batch;
					
					if($model->channel_category == "65280"){
						$this->insertMapTokenCampaign($lastIdBatch,$totalNumber,$paket->harga,$model->http_intercept_ads_type);
						$this->createStoreProc($model,$lastIdBatch,$ads_type );
					}
					else{
						$this->insertMapTokenCampaign($lastIdBatch,$totalNumber,$paket->harga,0);
						$this->createStoreProc2($model,$lastIdBatch);
					}
					
					if($model->batch_type == 1 || $model->batch_type == 2 || $model->batch_type == 3 || $model->batch_type == 4){
						$this->insertCampaigneListedFile($model,$lastIdBatch);
					}
					
					$lastPrepaidValue = $customer->prepaid_value;
					$this->afterSave($model,$model->id_batch,$lastPrepaidValue,"");
					$this->redirect(array('view','id'=>$model->id_batch));
				}
			}
		}
	
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionOnUploadBrowse()
	{
		$paket = PaketCampaign::model()->findByPk($_POST['Campaigne']['id_paket_campaign']);
		foreach($_FILES['Campaigne']['tmp_name']['filenameupload'] as $index => $img)
		{
			if(!empty($img))
			{
				$paketsize = explode(' x ',$paket['ukuran_gambar'.($index+1)]);
				$paketwidth = $paketsize[0];
				$paketheight = $paketsize[1];
				
				list($width, $height, $type, $attr) = getimagesize($img);

				if($width < $paketwidth-50 || $width > $paketwidth+50)
				{
					echo "<label class='label'>Image $index width is too big or small. are you sure to upload it ?</label></br>";
				}
				if($height < $paketheight-50 || $height > $paketheight+50)
				{
					echo "<label class='label'>Image $index height is too big or small. are you sure to upload it ?</label></br>";
				}
				
			}
		}
		
	}
	
	
	public function parseUpdateDataCampaigne($model,$customer){
		
		$model->flag_reload_campaign = 1;
		
		$paket = PaketCampaign::model()->findByPk($model->id_paket_campaign);	
		if( $paket->http_intercept_ads_type == 1 ){
			$model->sms_send_limit =  ($model->total_number) * ($paket->harga);
			$model->total_number   =  ($model->total_number) * ($paket->harga *1000) ;
		}
		else{
			$model->total_number   = ($model->total_number) * ($paket->harga);
			$model->sms_send_limit = $model->total_number;
		}
		
		if($customer->segmen_customer == 1){
			
			$sql 		= "select sms_send_limit from tbl_lba_batch where jobs_id = '".$model->jobs_id."'";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$row = $dataReader->queryAll();
			$oldToken = ($row[0]['sms_send_limit']) / ($paket->harga);
			$newToken = ($model->sms_send_limit) / ($paket->harga);
			
			if($oldToken > $newToken ){ // kalau panjangnya lebih besar dari yang baru
				$selisih =  $oldToken - $newToken;
				// set sms send limit baru
				// tambahin balancenya
				$sql = "UPDATE tbl_customer  set prepaid_value = prepaid_value + ".$selisih." where id_customer = ".$model->id_customer."";
				$connection = Yii::app()->db;
				$command = $connection->createCommand($sql)->execute();
				
				// update ke history lba batch
				$sql = "UPDATE tbl_history_invoice_lba set date_history = NOW(),first_prepaid = first_prepaid+".$selisih." WHERE id_batch = ".$model->id_batch." ";
				$command = $connection->createCommand($sql)->execute();
			}
			else{  // kalau panjangnya lebih kecil dari pada yang sebelumnya
				// set sms send limit baru
				$selisih 				= $newToken - $oldToken;
				// kurangin balancenya
				$sql = "UPDATE tbl_customer  set prepaid_value = prepaid_value - ".$selisih." where id_customer = ".$model->id_customer."";
				$connection = Yii::app()->db;
				$command = $connection->createCommand($sql)->execute();
				// update ke history lba batch
				$sql = "UPDATE tbl_history_invoice_lba set date_history = NOW(),first_prepaid = first_prepaid - ".$selisih." WHERE id_batch = ".$model->id_batch." ";
				$command = $connection->createCommand($sql)->execute();
			}
		}
	}
	
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Campaigne']))
		{
			$booleanGue = true;
			$model->attributes=$_POST['Campaigne'];
			
			
			$customer = Customer::model()->findByPk($model->id_customer);
			if($customer->segmen_customer == 1){
				// check balance
				if($this->checkValidBalance($model) == false){
					$booleanGue = false;
					$model->addError('error','Not enough balance to create campaigne');
				}
			}
			
			//topic
			if($this->checkValidTopik($model) == false){
				$booleanGue = false;
				$model->addError('error','Please Input Topic');
			}
			
			//start periode
			if( $this->checkValidStartPeriodeCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not Valid Date for start periode');
			}
			// content expired
			if($this->checkValidContentExpiredCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not Valid Date for content expired');
			}
			// periode
			if($this->checkValidPeriodeCampaigne($model) == false){
				$booleanGue = false;
				$model->addError('error','Not valid periode date beetween start periode and content expired');
			}
			
			// total number
			//if( ctype_digit($model->total_number) == false){
				//$booleanGue = false;
				//$model->addError('error','Please input valid total number');	
			//}

			if($model->channel_category == 65280){
					if( count($_FILES['Campaigne']) > 0 ){
						
						$allfile = "";
						$allname = "";
							
							$extensions = array(
							1  => 'gif',
							2  => 'jpeg',
							3  => 'png',
							);
							
						foreach($_FILES['Campaigne']['name']['filenameupload'] as $index => $fname)
						{
							$info = pathinfo($fname);
							$allfile .= $fname.",";
							$allname .= "image".($index+1) .$model->jobs_id.".".$info['extension'].",";
						}
						
						$model->filenameupload = $allfile;
						$model->filename = $allname;
						
						//UPLOAD FILE
						$webPath=dirname(Yii::app()->basePath);
						$imagePath=$webPath.Yii::app()->params['uploadpath'];
						$picture_file = CUploadedFile::getInstancesByName('Campaigne');
						$index = 0;
						$namefile = explode(',',$allname);
						foreach ($picture_file as $image => $pic) {
							if($pic->type != "text/plain")
							{
								$pic->saveAs($imagePath.'view_'.( ($paket['tipe']==1) ? "cpm" : "cpc" ).'/'.$namefile[$index]);
								copy($imagePath.'view_'.( ($paket['tipe']==1) ? "cpm" : "cpc" ).'/'.$namefile[$index],$imagePath."/".$namefile[$index]);
								//echo $imagePath.'view_'.( ($paket['tipe']==1) ? "cpm" : "cpc" ).'/'.$namefile[$index]."<br>";
								$index++;
							}
						}
					}
			}
			else{
				if($this->checkValidSmsText($model) == false){
					$booleanGue = false;
					$model->addError('error','Please input SMS text');
				}
				
				if(strlen($model->sms_text)> 160){
					$booleanGue = false;
					$model->addError('error','MAX input form SMS text is 160 character');
				}
			}
			
			
			if($booleanGue == true){	
				$this->parseUpdateDataCampaigne($model,$customer);
			
				if($model->save()){
					$this->afterUpdate($model);
					$this->redirect(array('view','id'=>$model->id_batch));
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		
			$this->afterDelete($id);
			// we only allow deletion via POST request
			//$this->loadModel($id)->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model= new Campaigne('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Campaigne']))
			$model->attributes=$_GET['Campaigne'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Campaigne('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Campaigne']))
			$model->attributes=$_GET['Campaigne'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Campaigne::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='campaigne-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
