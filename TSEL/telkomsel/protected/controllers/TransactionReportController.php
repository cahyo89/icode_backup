<?php

class TransactionReportController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$id_customer = $_GET['id_customer'];
		
		$criteria = new CDbCriteria;
		$criteria->select='	*
							';
		$criteria->addCondition("id_customer = ".$id_customer." ");
		$criteria->addCondition("status_batch in(0,1,2,3,4,5,6,7,9,11) ");
		
		
		if(isset($_GET['excel'])){
			$model=TransactionReport::model()->findAll($criteria);
			
			Yii::app()->request->sendFile('TransactionReport.xls',
				$this->renderPartial('excel',array(
						'model'=>$model,
					),true)
			);
		}
		else{
			$dataProvider=new CActiveDataProvider('TransactionReport',array(
					'criteria'=>$criteria,
					'pagination'=>array(
						'pageSize'=>Yii::app()->params['paging'],
					),
			));
			$this->render('view',array(
				'dataProvider'=>$dataProvider,
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TransactionReport;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TransactionReport']))
		{
			$model->attributes=$_POST['TransactionReport'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_batch));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionExcel($jobs_id,$flag)
	{
		//$jobs_id = "dodo_1";
		//$flag = "success";
		$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.$jobs_id.'')->queryScalar();
		$sql='SELECT number,tanggal as waktu FROM '.$jobs_id.' where '.$flag.' > 0 ';
		$dataProvider=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'pagination' => array(
                    'pageSize' => 9999999,
			),

		));
		
		
			$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM '.$jobs_id.'')->queryScalar();
			$sql='Select error_code,count(error_code) as jumlah FROM '.$jobs_id.' where '.$flag.' > 0  group by error_code';
			$dataProviderError=new CSqlDataProvider($sql, array(
				'totalItemCount'=>$count,
				'pagination' => array(
                    'pageSize' => 9999999,
				),
			));
		
		

		Yii::app()->request->sendFile('TransactionReport['.$flag.'_number]'.$jobs_id.'.xls',
			$this->renderPartial('excelNumber',array(
					'model'=>$dataProvider,
					'modelError'=>$dataProviderError,
				),true)
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TransactionReport']))
		{
			$model->attributes=$_POST['TransactionReport'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_batch));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new TransactionReport('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TransactionReport'])){
			$model->attributes=$_GET['TransactionReport'];
			$this->redirect(array('view','id_customer'=>$_GET['TransactionReport']['id_customer']));
			
		}
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TransactionReport::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='transaction-report-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
