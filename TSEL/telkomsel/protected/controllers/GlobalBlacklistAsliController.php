<?php

class GlobalBlacklistAsliController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new GlobalBlacklistAsli;
		$modelBlack = new GlobalBlacklist;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GlobalBlacklistAsli']))
		{
			$model->attributes=$_POST['GlobalBlacklistAsli'];
			if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser')  && !Yii::app()->user->checkAccess('CustomerService')){
				$model->id_customer = Yii::app()->user->IdCustomer;
			}
			else{
				$model->id_customer = -1;
				$modelWhite = $this->loadModelWhite($model->subscriber_number);
				if($modelWhite===null){}
				else
					$modelWhite->delete();
			}
			
			$G = GlobalConfigurationSubs::model()->find();
			$modelBlack->attributes=$_POST['GlobalBlacklistAsli'];
			if($model->save()){
				$modelBlack->first_user = Yii::app()->user->first_name;
				$modelBlack->first_ip = CHttpRequest::getUserHostAddress();
				$modelBlack->first_update = date('Y-m-d h:i:s');
				$modelBlack->last_user = Yii::app()->user->first_name;
				$modelBlack->last_ip = CHttpRequest::getUserHostAddress();
				$modelBlack->last_update =  date('Y-m-d h:i:s');
				$modelBlack->save();
				
				$modelHistory = new HistoryGlobalblackwhitelist;
				$modelHistory->msisdn = $model->subscriber_number;
				$modelHistory->shortcode = $G->shortcode;
				$modelHistory->sms_text = $G->keyword_on_blacklist;
				$modelHistory->action = 3;
				$modelHistory->first_user = Yii::app()->user->first_name;
				$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
				$modelHistory->first_update = date('Y-m-d h:i:s');
				$modelHistory->last_user = Yii::app()->user->first_name;
				$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
				$modelHistory->last_update =  date('Y-m-d h:i:s');
				$modelHistory->save();
				
				Controller::afterCreate("GlobalBlacklistAsli",$model->id);
				Controller::addberita("Create Global BlackList : Name : $model->name , Number : $model->subscriber_number");
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelOld=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GlobalBlacklistAsli']))
		{
			
			$model->attributes=$_POST['GlobalBlacklistAsli'];
			
			$modelBlack = $this->loadModelBlack($modelOld->subscriber_number);
			$modelBlack->attributes=$_POST['GlobalBlacklistAsli'];
			
			if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser') && !Yii::app()->user->checkAccess('CustomerService')){
				$model->id_customer = Yii::app()->user->IdCustomer;
				$modelBlack->id_customer = Yii::app()->user->IdCustomer;
			}
			else{
				$model->id_customer = -1;
				$modelBlack->id_customer = -1;
				$modelWhite = $this->loadModelWhite($model->subscriber_number);
				if($modelWhite===null){}
				else
					$modelWhite->delete();
			}
			if($model->save()){
				$modelBlack->last_user = Yii::app()->user->first_name;
				$modelBlack->last_ip = CHttpRequest::getUserHostAddress();
				$modelBlack->last_update =  date('Y-m-d h:i:s');
				$modelBlack->save();
				Controller::addberita("Update Global BlackList : Name : $modelOld->name -> $model->name , Number : $modelOld->subscriber_number -> $model->subscriber_number");
				
				Controller::afterUpdate("GlobalBlacklistAsli",$model->id);
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			Controller::beforeDelete('GlobalBlacklistAsli',$id);
			$G = GlobalConfigurationSubs::model()->find();
			$model=$this->loadModel($id);
			Controller::addberita("Delete Global BlackList : Name : $model->name , Number : $model->subscriber_number");
			
			$modelHistory = new HistoryGlobalblackwhitelist;
			$modelHistory->msisdn = $model->subscriber_number;
			$modelHistory->shortcode = $G->shortcode;
			$modelHistory->sms_text = $G->keyword_off_blacklist;
			$modelHistory->action = 4;
			$modelHistory->first_user = Yii::app()->user->first_name;
			$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
			$modelHistory->first_update = date('Y-m-d h:i:s');
			$modelHistory->last_user = Yii::app()->user->first_name;
			$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
			$modelHistory->last_update =  date('Y-m-d h:i:s');
			$modelHistory->save();
			
			$this->loadModel($id)->delete();
			$this->loadModelBlack($model->subscriber_number)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new GlobalBlacklistAsli('search');
		
		if(isset($_POST['GlobalBlacklistAsli']))
		{
			$model->attributes=$_POST['GlobalBlacklistAsli'];
			$itu=CUploadedFile::getInstance($model,'filee');
			
			if (substr($itu,-3) == "xls") {
				require_once('/nfs_data/projectmobads/program/web_admin/protected/extensions/php-excel-reader-2.21/excel_reader2.php');
				
				
				$path=Yii::app()->basePath.'/dataTemp/'. $itu;
				//$model->image->saveAs(Yii::app()->basePath . '/../images/' . $model->image);
				$itu->saveAs($path);
				$data = new Spreadsheet_Excel_Reader($path);
				$subscriber_number=array();
				$nama=array();
				for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++) 
				{
					if(isset($data->sheets[0]['cells'][$j][1],$data->sheets[0]['cells'][$j][2],$data->sheets[0]['cells'][$j][3],$data->sheets[0]['cells'][$j][4],$data->sheets[0]['cells'][$j][5])){
						$dataExcel[$j]['num'] = $data->sheets[0]['cells'][$j][1];
						$dataExcel[$j]['nama'] = $data->sheets[0]['cells'][$j][2];
						$dataExcel[$j]['address'] = $data->sheets[0]['cells'][$j][3];
						$dataExcel[$j]['request'] = $data->sheets[0]['cells'][$j][4];
						$dataExcel[$j]['description'] = $data->sheets[0]['cells'][$j][5];
					}
				}
				
				if(!Yii::app()->user->checkAccess('Admin') && !Yii::app()->user->checkAccess('superUser')){
					$idCus = Yii::app()->user->IdCustomer;
					
				}
				else{
					$idCus = -1;
				}
				
				$G = GlobalConfigurationSubs::model()->find();
				foreach($dataExcel as $id=>$value)
				{
					if(Yii::app()->user->isAdmin()){
						$modelWhite = $this->loadModelWhite($model->subscriber_number);
						if($modelWhite===null){}
						else
							$modelWhite->delete();
					}
					
					$modelBA=new GlobalBlacklistAsli;
					
					$modelBA->subscriber_number=$value['num'];
					$modelBA->name=$value['nama'];
					$modelBA->address=$value['address'];
					$modelBA->request=$value['request'];
					$modelBA->description=$value['description'];
					$modelBA->id_customer=$idCus;
					$modelBA->first_user = Yii::app()->user->first_name;
					$modelBA->first_ip = CHttpRequest::getUserHostAddress();
					$modelBA->first_update = date('Y-m-d h:i:s');
					$modelBA->last_user = Yii::app()->user->first_name;
					$modelBA->last_ip = CHttpRequest::getUserHostAddress();
					$modelBA->last_update =  date('Y-m-d h:i:s');
					if($modelBA->save()){
					
					$modelB=new GlobalBlacklist;
					
					$modelB->subscriber_number=$value['num'];
					$modelB->id_customer=$idCus;
					$modelB->first_user = Yii::app()->user->first_name;
					$modelB->first_ip = CHttpRequest::getUserHostAddress();
					$modelB->first_update = date('Y-m-d h:i:s');
					$modelB->last_user = Yii::app()->user->first_name;
					$modelB->last_ip = CHttpRequest::getUserHostAddress();
					$modelB->last_update =  date('Y-m-d h:i:s');
					$modelB->save();
					
					$modelHistory = new HistoryGlobalblackwhitelist;
					$modelHistory->msisdn = $value['num'];
					$modelHistory->shortcode = $G->shortcode;
					$modelHistory->sms_text = $G->keyword_on_blacklist;
					$modelHistory->action = 3;
					$modelHistory->first_user = Yii::app()->user->first_name;
					$modelHistory->first_ip = CHttpRequest::getUserHostAddress();
					$modelHistory->first_update = date('Y-m-d h:i:s');
					$modelHistory->last_user = Yii::app()->user->first_name;
					$modelHistory->last_ip = CHttpRequest::getUserHostAddress();
					$modelHistory->last_update =  date('Y-m-d h:i:s');
					$modelHistory->save();
					
					}
			   }
				unlink($path);
			}		
			else
			{
				Yii::app()->user->setFlash('error', "File must Excel 2003(*.xls)!");
			}
			
			$this->redirect(array('index'));
		}
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GlobalBlacklistAsli']))
			$model->attributes=$_GET['GlobalBlacklistAsli'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GlobalBlacklistAsli::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function loadModelBlack($subscriber_number)
	{
		$model=GlobalBlacklist::model()->findByAttributes(array('subscriber_number'=>$subscriber_number));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModelWhite($subscriber_number)
	{
		$model=GlobalWhitelist::model()->findByAttributes(array('subscriber_number'=>$subscriber_number));
		//if($model===null)
			//throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='global-blacklist-asli-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
