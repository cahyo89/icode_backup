<?php

class RevenueController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('dodo'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	protected function Persen($value)
	{
		return $value;
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		ini_set('memory_limit', "500M");
		$dateStart = $_GET['dateStart'];
		$customer = $_GET['customer'];
		$campaign = $_GET['campaign'];
		$startPlusOne = date('Ymd',strtotime($dateStart) - 86400);
		$sql = "";
		if($dateStart != "")
		{
			$sql = "select tbl_lba_batch.*,tbl_paket_campaign.harga, tbl_paket_campaign.nama from tbl_lba_batch left join tbl_paket_campaign on tbl_lba_batch.id_paket_campaign = tbl_paket_campaign.id where 1";
			
			
			if($customer!="")
			{
				$sql .= " and tbl_lba_batch.id_customer = ".$customer;
			}
			if($campaign != "")
			{
				$sql .= " and tbl_lba_batch.id_batch = ".$campaign;
			}
		}
	
		$count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM ('.$sql.') tbl')->queryScalar();
		$dataProvider=new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
		$this->render('view',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionExcel()
	{
		$dateStart = $_GET['dateStart'];
		$customer = $_GET['customer'];
		$campaign = $_GET['campaign'];
		$startPlusOne = date('Ymd',strtotime($dateStart) - 86400);
		$sql = "";
		if($dateStart != "")
		{
			$sql = "select tbl_lba_batch.*,tbl_paket_campaign.harga, tbl_paket_campaign.nama from tbl_lba_batch left join tbl_paket_campaign on tbl_lba_batch.id_paket_campaign = tbl_paket_campaign.id where 1";
			
			
			if($customer!="")
			{
				$sql .= " and tbl_lba_batch.id_customer = ".$customer;
			}
			if($campaign != "")
			{
				$sql .= " and tbl_lba_batch.id_batch = ".$campaign;
			}
		}
		$data = Yii::app()->db->createCommand($sql)->query();
		CsvExport::exportRevenue($data);
		
	}

	public function actionIndex()
	{
		$modelB=new Revenue;
		$modelB->unsetAttributes();  // clear any default values
		if(isset($_GET['Revenue'])){
			if($_GET['Revenue']['campaign'] != "NoBatch"){
				if($_GET['dateStart'] != ""){
						$this->redirect(array('view','dateStart'=>$_GET['dateStart'],'customer'=>$_GET['Revenue']['customer'],'campaign'=>$_GET['Revenue']['campaign'],'msisdn'=>$_GET['msisdn'],'url'=>$_GET['url']));
				}
				else{
					$modelB->addError('dateStart','Date Start can not be empty.');
				}
			}
			else{
					$modelB->addError('campaign','Campaign can not be empty.');
			}
		}
		$this->render('index',array(
			'modelB'=>$modelB,
		));
	}

	public function actionCekDataType()
        {
                //Handler for _form updates when perspective_id is changed.
                if (Yii::app()->request->isAjaxRequest)
                {
                        
                        //pick off the parameter value
                        $dataType = Yii::app()->request->getParam( 'dataType' );
                        if($dataType == 1)
                        {
                        //we are going to hide the insider_div block on _form.
                        //by changing the style attribute on the form block div.
                        echo "display:none";
                        Yii::app()->end();
                        }
                        else
                        {
                        //display the insider_div  block on _form
                        echo "display:block";
                        Yii::app()->end();
                        }
                        
                }
        }
	
/*
		OpenFlashChart2Loader::load();
		$bar = new bar();
		$bar->set_values( array(9,8,7,6,5,4,3,2,1) );
		

		$chart = new open_flash_chart();
		
		$chart->set_title( new title("MyChart") );
		$chart->add_element( $bar );
		for($i=0;$i<10;$i++)
			$datado[] = 'dodo';
		$x = new x_axis();
		$x->set_labels_from_array($datado);
		$chart->set_x_axis( $x );
		$this->render('index',array(
			'model'=>$model,
			'chart'=>$chart,
		));
*/
	public function actionCekCampaign()
	{
		$dataCus = Yii::app()->request->getParam('dataCus');
		$dataDateStart = Yii::app()->request->getParam('dataDateStart');
			
		if($dataCus != "" ){
			
			//$where = "(start_periode between '".$dataDateStart." 00:00:00' AND '".$dataDateStart." 23:59:59' or start_periode <= '".$dataDateStart." 00:00:00') and content_expired >= '".$dataDateStart." 00:00:00'";
			
			$data = Campaigne::model()->findAll('id_customer = '.$dataCus.' and message_type <> 2 and status_batch not in(0,1,11)');
			//echo "'id_customer = '.$dataCus.' and message_type <> 2 and status_batch not in(0,1,11) and  ('.$where.') '";
			
			if(count($data) > 0)
			{
				$dropDownA = CHtml::tag('option',array('value'=>''),CHtml::encode('All'),true);
			}
			
			foreach($data as $name)
			{
			   $dropDownA .= CHtml::tag('option', array('value'=>$name->id_batch),CHtml::encode($name->jobs_id),true);
			}
			
			if($dropDownA == "")
				$dropDownA .= CHtml::tag('option',array('value'=>'NoBatch'),CHtml::encode('--No Batch--'),true);
		}
		else{
			$dropDownA = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('All'),true);
		}
			// return data (JSON formatted)
				echo CJSON::encode(array(
					'campaignD'=>$dropDownA,
				)); 
				  Yii::app()->end();
	}
	
	public function loadModel($id)
	{
		$model=Banned::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='banned-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
