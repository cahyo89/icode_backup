<?php

class SummaryTransactionHourlyController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
	public function actionDetail()
	{
	
	}
	
	public function actionIndex()
	{
		$model=new SummaryTransactionHourly('search');
		
		if(isset($_POST['SummaryTransactionHourly']))
		{			
			$dataType = $_POST['SummaryTransactionHourly']['dataType'];
			$dateStart = $_POST['dateStart'];
			$dateEnd = $_POST['dateEnd'];
			$hourStart = $_POST['SummaryTransactionHourly']['hourStart'];
			$hourEnd = $_POST['SummaryTransactionHourly']['hourEnd'];
			$customer = $_POST['SummaryTransactionHourly']['customer'];
			$campaign = $_POST['SummaryTransactionHourly']['campaign'];
			$tipe = $_POST['SummaryTransactionHourly']['tipe'];

			$flag = true;
			if($campaign == 'NoBatch')
			{
				$model->addError('campaign','Campaign can not be empty.');
				$flag = false;
			}
	
			if($flag)
			{
				$this->redirect(array('view',
					'dataType'=>$dataType,
					'dateStart'=>$dateStart,
					'dateEnd'=>$dateEnd,
					'hourStart'=>$hourStart,
					'hourEnd'=>$hourEnd,
					'customer'=>$customer,
					'campaign'=>$campaign,
					'tipe'=>$tipe,
				));
			}
	
		}
		$this->render('index',array(
			'model'=>$model,
		));
	}
	
	public function actionView()
	{
		$dataType = $_GET['dataType'];
		$dateStart = $_GET['dateStart'];
		$dateEnd = $_GET['dateEnd'];
		$hourStart = $_GET['hourStart'];
		$hourEnd = $_GET['hourEnd'];
		$customer = $_GET['customer'];
		$campaign = $_GET['campaign'];
		$tipe = $_GET['tipe'];
		
		$criteria = new CDbCriteria;
		$sqlExcel ="";

		if($dataType == 1)
		{
			//DATE_FORMAT(tanggal,"%Y-%m-%d") as 
			$criteria->select='	DATE_FORMAT(tanggal,"%Y-%m-%d") tanggal,
								id_batch,
								id_customer,
								tipe,
								sum(total) total
								';
			$criteria->addCondition("tanggal >= DATE_FORMAT('".$dateStart." 00:00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateEnd." 23:59:59','%Y-%m-%d %H:%i:%s')");
			$sqlExcel = "tanggal >= DATE_FORMAT('".$dateStart." 00:00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateEnd." 23:59:59','%Y-%m-%d %H:%i:%s') ";
		
		}
		else
		{
			//DATE_FORMAT(tanggal,"%Y-%m-%d %H:00") as 
			$criteria->select=' DATE_FORMAT(tanggal,"%Y-%m-%d %H:00") tanggal,
								id_batch,
								id_customer,
								tipe,
								sum(total) total
								';
			$sqlExcel = "tanggal >= DATE_FORMAT('".$dateStart." ".$hourStart.":00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateStart." ".$hourEnd.":59:59','%Y-%m-%d %H:%i:%s') ";							
			$criteria->addCondition("tanggal >= DATE_FORMAT('".$dateStart." ".$hourStart.":00:00','%Y-%m-%d %H:%i:%s') AND tanggal <= DATE_FORMAT('".$dateStart." ".$hourEnd.":59:59','%Y-%m-%d %H:%i:%s')");
		}
		
		if(Yii::app()->user->getUserMode()==0 || !Yii::app()->user->checkAccess('Customer'))
		{
			if($customer != "")
			{
				$sqlExcel .= "and  id_batch in (select id_batch from tbl_lba_batch where id_customer = ".$customer.") ";
				$criteria->addCondition("t.id_customer = ".$customer." ");
			}
			else if($customer == "")
			{
				if(Yii::app()->user->getUserMode() == 1)
				{
					/*$criteria->with = array("Customer");
					$custHead = Customer::model()->findAll('id_user = '.Yii::app()->user->id);
					$sqlExcel .= 'approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )';
					$criteria->addCondition('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or t.id_customer = '.$custHead[0]->id_customer.' )');
					*/
					
					$custHead = Customer::model()->findAll('id_user ='.Yii::app()->user->id);
					
					$idCustomer = '';
					foreach($custHead as $rw)
					{
						$idCustomer .= $rw->id_customer.',';
					}
					
					$idCustomer = substr($idCustomer,0,-1);
					
					$sqlExcel .= 'approved = 1 and id_customer in ('.$idCustomer.')';
					$criteria->addCondition('approved = 1 and t.id_customer in ('.$id_customer.')');
				}
			}
		}
		else
		{
			$sqlExcel .= " and  id_batch in (select id_batch from tbl_lba_batch where id_customer = ".Yii::app()->user->getIdCustomer()."  )";
			$criteria->addCondition("t.id_customer = ".Yii::app()->user->getIdCustomer()." ");
		}
		
		if($tipe!="")
		{
			$sqlExcel .= "and tipe = ".$tipe." ";
			$criteria->addCondition("tipe = ".$tipe." ");
		}
		
		if($campaign != "" )
		{
			$sqlExcel .= " and  id_batch = ".$campaign." ";
			$criteria->addCondition("id_batch = ".$campaign." ");
		}
		
		$criteria->group = 'tanggal,tipe,id_batch,t.id_customer';
		
		$dataProvider=new CActiveDataProvider('SummaryTransactionHourly',array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['paging'],
			),
		));
		
		Yii::app()->session['excel'] = $sqlExcel;
		
		$this->render('view',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionExcel()
	{
		//$sqlExcel = Yii::app()->user->getFlash('excel');		
		//Yii::app()->user->setFlash('excel', $sqlExcel);
		
		$sqlExcel = Yii::app()->session['excel'];
		//$data = SummaryTransactionHourly::model()->findAllBySQL('SELECT DATE_FORMAT(tanggal,"%Y-%m-%d") as tanggal, id_batch, a.id_customer, tipe, sum(total ) as total FROM `tbl_summary_transaction_hourly` a join tbl_customer b on a.id_customer = b.id_customer where '.$sqlExcel.' group by tanggal,tipe,id_batch,a.id_customer');
		$data = SummaryTransactionHourly::model()->findAllBySQL('SELECT DATE_FORMAT(tanggal,"%Y-%m-%d") as tanggal, id_batch, id_customer, tipe, sum(total ) as total FROM `tbl_summary_transaction_hourly` where '.$sqlExcel.' group by tanggal,tipe,id_batch,id_customer');
		//print_r($data);
		CsvExport::export($data);
	}
	
	public function actionCekDataType()
	{
			//Handler for _form updates when perspective_id is changed.
			if (Yii::app()->request->isAjaxRequest)
			{
					//pick off the parameter value
					$dataType = Yii::app()->request->getParam( 'dataType' );
					if($dataType == 1)
					{
					//we are going to hide the insider_div block on _form.
					//by changing the style attribute on the form block div.
					echo "display:none";
					Yii::app()->end();
					}
					else
					{
					//display the insider_div  block on _form
					echo "display:block";
					Yii::app()->end();
					}
			}
	}
	
	public function actionCekCampaign()
	{

		$dataCus = Yii::app()->request->getParam('dataCus');
		$dataType = Yii::app()->request->getParam('dataType');
		$dataDateStart = Yii::app()->request->getParam('dataDateStart');
		$dataDateEnd = Yii::app()->request->getParam('dataDateEnd');
		$dateHourStart = Yii::app()->request->getParam('dataHourStart');
		$dateHourEnd = Yii::app()->request->getParam('dateHourEnd');
		$id_group = Yii::app()->request->getParam('id_group');
		$batas = "";
		
		if($dataCus != "" ){
			if($dataType == 1 ){
				//SELECT * FROM `tbl_lba_batch` WHERE id_customer = 2 and message_type <> 2 and status_batch not in(0,1,11)  and  start_periode between '2013-04-03 00:00:00' AND '2013-04-06 00:00:00' and content_expired between '2013-04-03 00:00:00' AND '2013-04-06 00:00:00'
				$where = "(start_periode between '".$dataDateStart." 00:00:00' AND '".$dataDateEnd." 23:59:59' or start_periode <= '".$dataDateStart." 00:00:00') and content_expired >= '".$dataDateStart." 00:00:00'";
			}
			else{
				$where = "(start_periode between '".$dataDateStart." ".$dateHourEnd.":00:00' or start_periode <= '".$dataDateStart." ".$dateHourEnd.":00:00') AND '".$dataDateEnd." ".$dateHourEnd.":59:59' and content_expired >= '".$dataDateStart." ".$dateHourEnd.":00:00'";
			}
			
			if($id_group != "" )
				$batas .= " and cell = '".$id_group."' ";
				
			//echo 'id_customer = '.$dataCus.' and message_type <> 2 and status_batch not in(0,1,11) '.$batas.' and  ('.$where.') ';
			$data = Campaigne::model()->findAll('id_customer = '.$dataCus.' and message_type <> 2 and status_batch not in(0,1,11) '.$batas.' and  ('.$where.') ');
			//$data = LbaBatch::model()->findAll();

		    $data=CHtml::listData($data,'id_batch','jobs_id');
			$dropDownA = CHtml::tag('option',array('value'=>''),CHtml::encode('All'),true);
			$dio = 0;
			foreach($data as $value=>$name){
			   $dropDownA .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
				$dio++;
			}
			if($dio == 0)
				$dropDownA = CHtml::tag('option',array('value'=>'NoBatch'),CHtml::encode('--No Batch--'),true);
						
		}
		else{
			$dropDownA = "";
			$dropDownA .= CHtml::tag('option',array('value'=>''),CHtml::encode('All'),true);
		}
			// return data (JSON formatted)
				echo CJSON::encode(array(
					'campaignD'=>$dropDownA,
				)); 
				  Yii::app()->end();		
	}

}