<?php

class BlacklistChannelCategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BlacklistChannelCategory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlacklistChannelCategory']))
		{
			$model->attributes=$_POST['BlacklistChannelCategory'];
			if($model->save())
				Controller::afterCreate("BlacklistChannelCategory",$model->id);
				$this->redirect(array('view','id'=>$model->id));
			//	print_r($_POST['BlacklistChannelCategory']);
		}

		$this->render('create',array(
			'model'=>$model,
		));
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlacklistChannelCategory']))
		{
			$model->attributes=$_POST['BlacklistChannelCategory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new BlacklistChannelCategory('search');
		//$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BlacklistChannelCategory'])){
			$model->attributes=$_GET['BlacklistChannelCategory'];
			
			$itu=CUploadedFile::getInstance($model,'filee');
			echo $itu;
			if (substr($itu,-3) == "xls") {
				require_once('/projectmobads/program/web_admin/protected/extensions/php-excel-reader-2.21/excel_reader2.php');
				
				
				$path=Yii::app()->basePath.'/dataTemp/'. $itu;
				//$model->image->saveAs(Yii::app()->basePath . '/../images/' . $model->image);
				$itu->saveAs($path);
				$data = new Spreadsheet_Excel_Reader($path);
				$subscriber_number=array();
				$nama=array();
				for ($j = 1; $j <= $data->sheets[0]['numRows']; $j++) 
				{
					if(isset($data->sheets[0]['cells'][$j][1])){
						$dataExcel[$j]['num'] = $data->sheets[0]['cells'][$j][1];
					}
				}
				
				
				
				foreach($dataExcel as $id=>$value)
				{
					
					
					$modelW=new BlacklistChannelCategory;
					
					$modelW->subscriber_number=$value['num'];
					$modelW->first_user = Yii::app()->user->first_name;
					$modelW->first_ip = CHttpRequest::getUserHostAddress();
					$modelW->first_update = date('Y-m-d h:i:s');
					$modelW->last_user = Yii::app()->user->first_name;
					$modelW->last_ip = CHttpRequest::getUserHostAddress();
					$modelW->last_update =  date('Y-m-d h:i:s');
					$modelW->save();
				}
			   
				unlink($path);
			}		
			else
			{
				Yii::app()->user->setFlash('error', "File must Excel 2003(*.xls)!");
			}
			
		
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BlacklistChannelCategory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BlacklistChannelCategory']))
			$model->attributes=$_GET['BlacklistChannelCategory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BlacklistChannelCategory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BlacklistChannelCategory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BlacklistChannelCategory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blacklist-channel-category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
