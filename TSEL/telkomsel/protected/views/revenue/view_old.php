<?php
$this->breadcrumbs=array(
	'Report Batch Lba Filter'=>array('index'),
);
$dataView = $_GET['dataView'];
$dataType = $_GET['dataType'];
$dateStart = $_GET['dateStart'];
$dateEnd = $_GET['dateEnd'];
$hourStart = $_GET['hourStart'];
$hourEnd = $_GET['hourEnd'];
$customer = $_GET['customer'];
$campaign = $_GET['campaign'];
$modeCustomer = CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama');
$modelCampaign = CHtml::listData(LbaBatch::model()->findAll(),'id_batch','jobs_id');
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Report Campaign</h1></td>
<td><div class="operatorRight"><?php  echo CHtml::link('Export Report to CSV',array('staticBatchLba/view',
										'excel'=>'1',
										'dataView' =>$dataView,
										'dataType'=>$dataType,
										'dateStart'=>$dateStart,
										'dateEnd'=>$dateEnd,
										'hourStart'=>$hourStart,
										'hourEnd'=>$hourEnd,
										'customer'=>$customer,
										'campaign'=>$campaign
										));  ?></div></td>
					</tr></table>

<h4>Date : <?php if($dataType == 1) {echo $dateStart." to ".$dateEnd;}else{echo $dateStart." ".$hourStart." to ".$dateStart." ".$hourEnd;}?></h4>

<h4>Customer : <?php echo $customer == "" ?  'All' :  $modeCustomer[$customer] ;  ?></h4>

<h4>Campaign : <?php echo $campaign == "" ?  'All' :  $modelCampaign[$campaign] ;  ?></h4>
<?php if($dataView == 0){ 

	$success =0;
	$failed = 0;
	$queue = 0;
	$total = 0;
	$qr = 0;
	$sr = 0;
	$fr = 0;
	
	$dataM = $dataProvider->getData();
	foreach($dataM as $dataM){
		
		$success += $dataM['sms_success'];
		$failed += $dataM['sms_failed'];
		$queue += $dataM['queue'];
		$total += $dataM['sms_send_limit'];
		
		$qr += $dataM['qr'];
		$sr += $dataM['sr'];
		$fr += $dataM['fr'];
	}
		if($total != 0){
			$qr = round(($queue/$total)*100,2);
    			$sr = round(($success/$total)*100,2);
  			$fr = round(($failed/$total)*100,2);  
		}
		else{
			$qr = 0;
    			$sr = 0;
  			$fr = 0;  
		}
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'static-error-code-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'start_periode',
		'jobs_id',
		array(
			'name'=>'id_customer',
			'type'=>'raw',
			'value'=>'@$data->Customer->nama',
		),
		'sms_send_limit',
		'sms_success',
		'sms_failed',
		'queue',
		'qr',
		'sr',
		'fr',
		/*
		'result',
		'error_code',
		*/
		
	),
)); 

?>
<div class="operatorRight">
	<table width="200px">
		<tr>
			<td><b>Grand Total<b></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Total<b></td>
			<td><?php echo $total;  ?></td>
		</tr>
		<tr>
			<td><b>Success<b></td>
			<td><?php echo $success;  ?></td>
		</tr>
		<tr>
			<td><b>Failed<b></td>
			<td><?php echo $failed;  ?></td>
		</tr>
		<tr>
			<td><b>Queue<b></td>
			<td><?php echo $queue;  ?></td>
		</tr>
		<tr>
			<td><b>Queue Rate<b></td>
			<td><?php echo $qr."%";  ?></td>
		</tr>
		<tr>
			<td><b>Success Rate<b></td>
			<td><?php echo $sr."%";  ?></td>
		</tr>
		<tr>
			<td><b>Failed Rate<b></td>
			<td><?php echo $fr."%";  ?></td>
		</tr>
	</table>
</div>
<?php }else{ ?>
	
	<?php
		$this->widget(
		   'application.extensions.OpenFlashChart2Widget.OpenFlashChart2Widget',
		   array(
		     'chart' => $chart,
		     'width' => '100%'
		   )
		 );
	?>

<?php } ?>
