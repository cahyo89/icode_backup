<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'apply-param-form','method'=>'get',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Apply Parameter</h1>

	<?php if(Yii::app()->user->hasFlash('error')): ?>
	<br></br>
	<div class="flash-info flash flash-block">
	    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
	</div>

	<?php endif; ?>
	
	<div class="row" style="display:none">
		<?php echo $form->labelEx($model,'jobs_id'); ?>
		<?php echo $form->textField($model,'jobs_id'); ?>
		<?php echo $form->error($model,'jobs_id'); ?>
	</div>
	
	<div class="row submit">
		<?php echo CHtml::submitButton('Apply Parameter'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->