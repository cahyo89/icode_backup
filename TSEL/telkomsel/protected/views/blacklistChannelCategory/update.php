<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $model BlacklistChannelCategory */

$this->breadcrumbs=array(
	'Blacklist Channel Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BlacklistChannelCategory', 'url'=>array('index')),
	array('label'=>'Create BlacklistChannelCategory', 'url'=>array('create')),
	array('label'=>'View BlacklistChannelCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BlacklistChannelCategory', 'url'=>array('admin')),
);
?>

<h1>Update BlacklistChannelCategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>