<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $model BlacklistChannelCategory */

$this->breadcrumbs=array(
	'Blacklist Channel Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List BlacklistChannelCategory', 'url'=>array('index')),
	array('label'=>'Create BlacklistChannelCategory', 'url'=>array('create')),
	array('label'=>'Update BlacklistChannelCategory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BlacklistChannelCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BlacklistChannelCategory', 'url'=>array('admin')),
);
?>

<h1>View BlacklistChannelCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'channel_category',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'name',
		'address',
		'request',
		'description',
	),
)); ?>
