<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Blacklist Channel Categories',
);

$this->menu=array(
	array('label'=>'Create BlacklistChannelCategory', 'url'=>array('create')),
	array('label'=>'Manage BlacklistChannelCategory', 'url'=>array('admin')),
);
?>

<h1>Blacklist Channel Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
