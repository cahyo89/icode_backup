<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $model BlacklistChannelCategory */

$this->breadcrumbs=array(
	'Blacklist Channel Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BlacklistChannelCategory', 'url'=>array('index')),
	array('label'=>'Manage BlacklistChannelCategory', 'url'=>array('admin')),
);
?>

<h1>Create BlacklistChannelCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>