<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $model BlacklistChannelCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'blacklist-channel-category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well-small">
		<?php echo $form->labelEx($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'channel_category'); ?>
		<?php 
			$customer = ChannelCategory::model()->findAll();
		 echo $form->dropDownList($model,'channel_category',CHtml::listData($customer, 'id', 'name'),array('class'=>'required','onchange'=>'{onChangeCustomer();}','empty'=>'--Please Select One--'));?>
		<?php echo $form->error($model,'channel_category'); ?>
	</div>

	
	<div class="well-small">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'request'); ?>
		<?php echo $form->textField($model,'request',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'request'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="well-small buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-inverse')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->