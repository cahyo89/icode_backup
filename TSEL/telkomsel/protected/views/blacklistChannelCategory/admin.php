<?php
/* @var $this BlacklistChannelCategoryController */
/* @var $model BlacklistChannelCategory */

$this->breadcrumbs=array(
	'Blacklist Channel Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BlacklistChannelCategory', 'url'=>array('index')),
	array('label'=>'Create BlacklistChannelCategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#blacklist-channel-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Blacklist Channel Categories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'excel-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
	<div class="row">
        <b>File Excel(.xls):</b>
		<?php echo $form->fileField($model,'filee',array('size'=>40,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'filee'); ?>
		<?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-inverse')); ?>
		
	</div>
        
<?php $this->endWidget(); ?>
</div>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'blacklist-channel-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'msisdn',
		'channel_category',
		'first_ip',
		'first_user',
		'first_update',
		/*
		'last_ip',
		'last_user',
		'last_update',
		'name',
		'address',
		'request',
		'description',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
