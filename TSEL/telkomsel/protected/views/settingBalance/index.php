<?php
$this->breadcrumbs=array(
	'Setting Balances',
);

$this->menu=array(
	array('label'=>'Create SettingBalance', 'url'=>array('create')),
	array('label'=>'Manage SettingBalance', 'url'=>array('admin')),
);
?>

<h1>Setting Balances</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
