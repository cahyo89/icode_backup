<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setting-balance-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>30,'maxlength'=>30 ,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipe_paket'); ?>
		<?php 
			if($model->tipe_customer == 1){ // postpaid
				echo $form->dropDownList($model,'tipe_paket',CHtml::listData(Paket::model()->findAll(' (flag_deleted is null or flag_deleted <> 1) and id_paket = 8'), 'id_paket', 'jenis_paket'));
			}else{
				echo $form->dropDownList($model,'tipe_paket',CHtml::listData(Paket::model()->findAll('id_paket not in(8) AND (flag_deleted is null or flag_deleted <> 1)'), 'id_paket', 'jenis_paket'));
			}
		?>
		<?php echo $form->error($model,'tipe_paket'); ?>
	</div>

	<div class="row">
		<input name="token" value="0" id="token" type="text">Token
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->