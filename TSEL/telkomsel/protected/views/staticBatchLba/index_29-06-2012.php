<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-report-form','method'=>'get',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Report Batch Lba Filter</h1>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'dataView'); ?>
		<?php echo $form->dropDownList($model,'dataView',Controller::getConstanta('dataView')); ?>
		<?php echo $form->error($model,'dataView'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'dataType'); ?>
		<?php echo $form->dropDownList($model,'dataType',Controller::getConstanta('dataType'),
				array('ajax'=>array(
						'type'=>'POST',
						'url'=>CController::createUrl('staticErrorCode/cekDataType'),
						'data'=>array('dataType' => 'js:this.value'),
						'success' => 'function(retval){$("#insider_div").attr("style",retval);$("#insider_div1").attr("style",retval == "display:block" ? "display:none" : "display:block" );}',
						'error'=> 'function(){alert(\'Bad AJAX\');}',
						'update'=>'#insider_div',
				))
		); ?>
		<?php echo $form->error($model,'dataType'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'dateStart'); ?>
		<?php
		
		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'start',
			'name'=>'dateStart', // This is how it works for me.
			'value'=>date('Y-m-d'),
			'options'=>array('dateFormat'=>'yy-mm-dd',
			'altFormat'=>'yy-mm-dd',
			'changeMonth'=>'true',
			'changeYear'=>'true',
			'yearRange'=>'1920:2012',
			'showOn'=>'both',
			// 'buttonText'=>'...',
			'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
			'buttonImageOnly' => true,
			),
			'htmlOptions'=>array('size'=>'15')
			));
		?>
		<?php echo $form->error($model,'dateStart'); ?>
	</div>

	<div id="insider_div1">  
	
		<div class="row" >
			<?php echo $form->labelEx($model,'dateEnd'); ?>
			<?php
				$form->widget('zii.widgets.jui.CJuiDatePicker', array(
				//'model'=>$model,
				'attribute'=>'start',
				'name'=>'dateEnd', // This is how it works for me.
				'value'=>date('Y-m-d'),
				'options'=>array('dateFormat'=>'yy-mm-dd',
				'altFormat'=>'yy-mm-dd',
				'changeMonth'=>'true',
				'changeYear'=>'true',
				'yearRange'=>'1920:2012',
				'showOn'=>'both',
				// 'buttonText'=>'...',
				'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				'buttonImageOnly' => true,
				),
				'htmlOptions'=>array('size'=>'15')
				));
			?>
			<?php echo $form->error($model,'dateEnd'); ?>
		</div>
	
	</div>
	
	<div id="insider_div" style="display:none">  
	
		<div class="row">
			<?php echo $form->labelEx($model,'hourStart'); ?>
			<?php echo $form->dropDownList($model,'hourStart',Controller::getConstanta('dataHour')); ?>
			<?php echo $form->error($model,'hourStart'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'hourEnd'); ?>
			<?php echo $form->dropDownList($model,'hourEnd',Controller::getConstanta('dataHour')); ?>
			<?php echo $form->error($model,'hourEnd'); ?>
		</div>
	
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'customer'); ?>
		<?php echo $form->dropDownList($model,'customer',CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama'),
						array('empty'=>'All',
								'ajax'=>array(
												'type'=>'post',
												'dataType'=>'json',
												'url'=>CController::createUrl('cekCampaign'),
												'data'=>array(	'dataCus' => 'js:this.value',
																'dataType' => 'js:$(\'#StaticBatchLba_dataType\').val()',
																'dataDateStart' => 'js:$(\'#dateStart\').val()',
																'dataDateEnd' => 'js:$(\'#dateEnd\').val()',
																'dataHourStart' => 'js:$(\'#hourStart\').val()',
																'dateHourEnd' => 'js:$(\'#hourEnd\').val()'
																),
												'success' => 'function(data)
												{
													$("#StaticBatchLba_campaign").html(data.campaignD);
												}',
												
												'error'=> 'function(){alert(\'Bad AJAX\');}',
										)	
							)	
		
		
		); ?>
		<?php echo $form->error($model,'customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaign'); ?>
		<?php echo $form->dropDownList($model,'campaign',array(''=>'All')); ?>
		<?php echo $form->error($model,'campaign'); ?>
	</div>
	

	<div class="row submit">
		<?php echo CHtml::submitButton('View'); ?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div><!--form-->