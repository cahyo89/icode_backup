<?php
$this->breadcrumbs=array(
	'Customers',
	'Transfer Balance',
);

$this->menu=array(
	array('label'=>'Manage Customer', 'url'=>array('admin')),
);
?>

<h1>Transfer Balance</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transfer-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well-small">
		<?php echo $form->labelEx($model,'Upline')."<b><font color='red'>*</font></b>"; ?>
		<?php
			$parent = @CHtml::listData(Customer::model()->findAll('id_user = :user and approved = 1 and blocked = 0 and (hierarchy_type = 0 or hierarchy_type = 1 or hierarchy_type is null)',array(':user'=>Yii::app()->user->id)), 'id_customer', 'nama');
			if(Yii::app()->user->checkAccess('Admin'))
			{
				$parent = @CHtml::listData(Customer::model()->findAll('approved = 1 and blocked = 0 and (hierarchy_type = 0 or hierarchy_type = 1 or hierarchy_type is null)'), 'id_customer', 'nama');
			}
		?>
		<?php echo $form->dropDownList($model,'parent_id',$parent,array('empty'=>'--Choice Upline--','onchange'=>'{onGetInfo();}')); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'Upline Balance')."<b><font color='red'>*</font></b>"; ?>
		<?php echo CHtml::textField('Customer[balance]','',array('size'=>30,'maxlength'=>30,'disabled'=>'disabled')); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'Downline')."<b><font color='red'>*</font></b>"; ?>
		<?php echo CHtml::dropDownList('Customer[downline]',array(),array('empty'=>'--Choice Downline--')); ?>
		<?php echo CHtml::error($model,'Customer[downline]'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->labelEx($model,'Transfer Amount')."<b><font color='red'>*</font></b>"; ?>
		<?php echo CHtml::textField('Customer[amount]','',array('size'=>30,'maxlength'=>30)); ?>
	</div>
	
	<div class="buttons" class="btn btn-danger">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Transfer' : 'Save', array("class"=>"btn btn-danger")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
	function onGetInfo()
	{
		<?php 
			echo CHtml::ajax(array(
			// the controller/function to call
			'url'=>CController::createUrl('onGetInfo'),

			// Data to be passed to the ajax function
			// Note that the ' should be escaped with \
			// The field id should be prefixed with the model name eg Vehicle_field_name
		   // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
			'data'=>array('parent_id'=>'js:$(\'#Customer_parent_id\').val()'),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>'function(data)
			{
				$("#Customer_downline").html(data.type);
				$("#Customer_balance").attr(\'value\',data.balance);
			}
			',
		))?>;
				
		return false;
	}
</script>