<?php
$this->breadcrumbs=array(
	'Group Area'=>array('index'),
	$model->bts_name,
);


?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Group Area #<?php echo $model->bts_name; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'btsGroup/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'Delete','link'=>'btsGroup/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->bts_name),
	   array('label'=>'New','link'=>'btsGroup/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bts_name',
		'description',
		array(
		'label'=>'Location Status',
		'value'=>$model->id_loc_stat == "" ? "No Profile" : $model->LocationStat->location_stat_name,
		),

	),
)); 
Controller::createInfo($model);
echo "<br/>";
	
	
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bts-group-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
		'name'=>'CI',
		'value'=>'Controller::getCi($data->id)',
		),
	),
));

?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'btsGroup/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'btsGroup/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	    array('label'=>'Delete','link'=>'btsGroup/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->bts_name)
	   
		));  ?>
</div>