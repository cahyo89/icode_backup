<?php
/* @var $this MessageFilterController */
/* @var $model MessageFilter */

$this->breadcrumbs=array(
	'Message Filters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MessageFilter', 'url'=>array('index')),
	array('label'=>'Create MessageFilter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#message-filter-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Message Filters</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'message-filter-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		'banned_text',
		
		array(            // display 'create_time' using an expression
            'name'=>'flag_deleted',
			'cssClassExpression' => '$data->flag_deleted=="1" ? "non" : "aktip"',
            'value'=>'$data->flag_deleted=="1" ? "Inactive " : "Active"' , 
        ),
		'first_user',
		'last_user',
		'first_update',
		/*
		'last_update',
		'first_ip',
		'last_ip',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<style>
	
 
 	.grid-view table.items td.aktip{
		color:#0dc403;
		font-weight:bold;
	}
	.grid-view table.items td.non{
		color:#ff0000;
		font-weight:bold;
	}
</style>

