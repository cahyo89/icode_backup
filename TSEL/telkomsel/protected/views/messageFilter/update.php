<?php
/* @var $this MessageFilterController */
/* @var $model MessageFilter */

$this->breadcrumbs=array(
	'Message Filters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MessageFilter', 'url'=>array('index')),
	array('label'=>'Create MessageFilter', 'url'=>array('create')),
	array('label'=>'View MessageFilter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MessageFilter', 'url'=>array('admin')),
);
?>

<h1>Update MessageFilter <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>