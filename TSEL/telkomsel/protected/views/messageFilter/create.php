<?php
/* @var $this MessageFilterController */
/* @var $model MessageFilter */

$this->breadcrumbs=array(
	'Message Filters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MessageFilter', 'url'=>array('index')),
	array('label'=>'Manage MessageFilter', 'url'=>array('admin')),
);
?>

<h1>Create MessageFilter</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>