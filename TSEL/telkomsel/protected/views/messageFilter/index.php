<?php
/* @var $this MessageFilterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Message Filters',
);

$this->menu=array(
	array('label'=>'Create MessageFilter', 'url'=>array('create')),
	array('label'=>'Manage MessageFilter', 'url'=>array('admin')),
);
?>

<h1>Message Filters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
