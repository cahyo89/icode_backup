<?php
/* @var $this MessageFilterController */
/* @var $model MessageFilter */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	
	<div class="row">
		<?php echo $form->label($model,'banned_text'); ?>
		<?php echo $form->textField($model,'banned_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'flag_deleted'); ?>
		<?php echo $form->textField($model,'flag_deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_user'); ?>
		<?php echo $form->textField($model,'first_user',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_user'); ?>
		<?php echo $form->textField($model,'last_user',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_update'); ?>
		<?php echo $form->textField($model,'first_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_update'); ?>
		<?php echo $form->textField($model,'last_update'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->