<?php
/* @var $this MessageFilterController */
/* @var $model MessageFilter */

$this->breadcrumbs=array(
	'Message Filters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MessageFilter', 'url'=>array('index')),
	array('label'=>'Create MessageFilter', 'url'=>array('create')),
	array('label'=>'Update MessageFilter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MessageFilter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MessageFilter', 'url'=>array('admin')),
);
?>

<h1>View MessageFilter #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'banned_text',
		'flag_deleted',
		'first_user',
		'last_user',
		'first_update',
		'last_update',
		'first_ip',
		'last_ip',
	),
)); ?>
