<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'transaction-report-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_customer'); ?>
		<?php echo $form->textField($model,'id_customer'); ?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jobs_id'); ?>
		<?php echo $form->textField($model,'jobs_id',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'jobs_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_periode'); ?>
		<?php echo $form->textField($model,'start_periode'); ?>
		<?php echo $form->error($model,'start_periode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stop_periode'); ?>
		<?php echo $form->textField($model,'stop_periode'); ?>
		<?php echo $form->error($model,'stop_periode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_send_limit'); ?>
		<?php echo $form->textField($model,'sms_send_limit'); ?>
		<?php echo $form->error($model,'sms_send_limit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_sent'); ?>
		<?php echo $form->textField($model,'sms_sent'); ?>
		<?php echo $form->error($model,'sms_sent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_text'); ?>
		<?php echo $form->textField($model,'sms_text'); ?>
		<?php echo $form->error($model,'sms_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message_type'); ?>
		<?php echo $form->textField($model,'message_type'); ?>
		<?php echo $form->error($model,'message_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message_bit'); ?>
		<?php echo $form->textField($model,'message_bit'); ?>
		<?php echo $form->error($model,'message_bit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_batch'); ?>
		<?php echo $form->textField($model,'status_batch'); ?>
		<?php echo $form->error($model,'status_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		<?php echo $form->textField($model,'filename',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'filename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_ip'); ?>
		<?php echo $form->textField($model,'first_ip',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'first_ip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_user'); ?>
		<?php echo $form->textField($model,'first_user',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'first_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'frist_update'); ?>
		<?php echo $form->textField($model,'frist_update'); ?>
		<?php echo $form->error($model,'frist_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_ip'); ?>
		<?php echo $form->textField($model,'last_ip',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'last_ip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_user'); ?>
		<?php echo $form->textField($model,'last_user',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'last_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_update'); ?>
		<?php echo $form->textField($model,'last_update'); ?>
		<?php echo $form->error($model,'last_update'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'topik'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'topik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved_time'); ?>
		<?php echo $form->textField($model,'approved_time'); ?>
		<?php echo $form->error($model,'approved_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_success'); ?>
		<?php echo $form->textField($model,'sms_success'); ?>
		<?php echo $form->error($model,'sms_success'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_failed'); ?>
		<?php echo $form->textField($model,'sms_failed'); ?>
		<?php echo $form->error($model,'sms_failed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted'); ?>
		<?php echo $form->textField($model,'deleted'); ?>
		<?php echo $form->error($model,'deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'schedule_delivery'); ?>
		<?php echo $form->textField($model,'schedule_delivery'); ?>
		<?php echo $form->error($model,'schedule_delivery'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'process_date'); ?>
		<?php echo $form->textField($model,'process_date'); ?>
		<?php echo $form->error($model,'process_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'retry'); ?>
		<?php echo $form->textField($model,'retry'); ?>
		<?php echo $form->error($model,'retry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_invoice'); ?>
		<?php echo $form->textField($model,'id_invoice'); ?>
		<?php echo $form->error($model,'id_invoice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_no'); ?>
		<?php echo $form->textField($model,'batch_no',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'batch_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_value'); ?>
		<?php echo $form->textField($model,'last_value'); ?>
		<?php echo $form->error($model,'last_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_expired'); ?>
		<?php echo $form->textField($model,'content_expired'); ?>
		<?php echo $form->error($model,'content_expired'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'divre'); ?>
		<?php echo $form->textField($model,'divre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'divre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'filenameupload'); ?>
		<?php echo $form->textField($model,'filenameupload',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'filenameupload'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'broadcast_type'); ?>
		<?php echo $form->textField($model,'broadcast_type'); ?>
		<?php echo $form->error($model,'broadcast_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_number'); ?>
		<?php echo $form->textField($model,'total_number'); ?>
		<?php echo $form->error($model,'total_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'booking'); ?>
		<?php echo $form->textField($model,'booking'); ?>
		<?php echo $form->error($model,'booking'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usage'); ?>
		<?php echo $form->textField($model,'usage'); ?>
		<?php echo $form->error($model,'usage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_filestatus_modif'); ?>
		<?php echo $form->textField($model,'last_filestatus_modif'); ?>
		<?php echo $form->error($model,'last_filestatus_modif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_ani'); ?>
		<?php echo $form->textField($model,'customer_ani',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'customer_ani'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'binary_data'); ?>
		<?php echo $form->textField($model,'binary_data'); ?>
		<?php echo $form->error($model,'binary_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prefix_detail'); ?>
		<?php echo $form->textField($model,'prefix_detail',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'prefix_detail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_text_view'); ?>
		<?php echo $form->textField($model,'sms_text_view'); ?>
		<?php echo $form->error($model,'sms_text_view'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'instant_failed'); ?>
		<?php echo $form->textField($model,'instant_failed'); ?>
		<?php echo $form->error($model,'instant_failed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipe_batch'); ?>
		<?php echo $form->textField($model,'tipe_batch'); ?>
		<?php echo $form->error($model,'tipe_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'promotion_media'); ?>
		<?php echo $form->textField($model,'promotion_media'); ?>
		<?php echo $form->error($model,'promotion_media'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sex_category'); ?>
		<?php echo $form->textField($model,'sex_category'); ?>
		<?php echo $form->error($model,'sex_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_category'); ?>
		<?php echo $form->textField($model,'age_category'); ?>
		<?php echo $form->error($model,'age_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status_category'); ?>
		<?php echo $form->textField($model,'status_category'); ?>
		<?php echo $form->error($model,'status_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'filter_prefix'); ?>
		<?php echo $form->textField($model,'filter_prefix',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'filter_prefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'education_category'); ?>
		<?php echo $form->textField($model,'education_category'); ?>
		<?php echo $form->error($model,'education_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel_category'); ?>
		<?php echo $form->textField($model,'channel_category'); ?>
		<?php echo $form->error($model,'channel_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priority_category'); ?>
		<?php echo $form->textField($model,'priority_category'); ?>
		<?php echo $form->error($model,'priority_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_deleted'); ?>
		<?php echo $form->textField($model,'flag_deleted'); ?>
		<?php echo $form->error($model,'flag_deleted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ast'); ?>
		<?php echo $form->textField($model,'ast'); ?>
		<?php echo $form->error($model,'ast'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cell'); ?>
		<?php echo $form->textField($model,'cell'); ?>
		<?php echo $form->error($model,'cell'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_lba'); ?>
		<?php echo $form->textField($model,'flag_lba'); ?>
		<?php echo $form->error($model,'flag_lba'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_file'); ?>
		<?php echo $form->textField($model,'flag_file'); ?>
		<?php echo $form->error($model,'flag_file'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'multimedia_type'); ?>
		<?php echo $form->textField($model,'multimedia_type'); ?>
		<?php echo $form->error($model,'multimedia_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quota'); ?>
		<?php echo $form->textField($model,'quota'); ?>
		<?php echo $form->error($model,'quota'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile'); ?>
		<?php echo $form->textField($model,'profile'); ?>
		<?php echo $form->error($model,'profile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_type'); ?>
		<?php echo $form->textField($model,'batch_type',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'batch_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vip'); ?>
		<?php echo $form->textField($model,'vip'); ?>
		<?php echo $form->error($model,'vip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'increase_counter'); ?>
		<?php echo $form->textField($model,'increase_counter'); ?>
		<?php echo $form->error($model,'increase_counter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unique_subscriber_per_batch'); ?>
		<?php echo $form->textField($model,'unique_subscriber_per_batch'); ?>
		<?php echo $form->error($model,'unique_subscriber_per_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_hour'); ?>
		<?php echo $form->textField($model,'flag_hour'); ?>
		<?php echo $form->error($model,'flag_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_multi_batch'); ?>
		<?php echo $form->textField($model,'flag_multi_batch'); ?>
		<?php echo $form->error($model,'flag_multi_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'slot_multi_batch'); ?>
		<?php echo $form->textField($model,'slot_multi_batch'); ?>
		<?php echo $form->error($model,'slot_multi_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_batch_reference'); ?>
		<?php echo $form->textField($model,'id_batch_reference'); ?>
		<?php echo $form->error($model,'id_batch_reference'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'black_list_profile'); ?>
		<?php echo $form->textField($model,'black_list_profile'); ?>
		<?php echo $form->error($model,'black_list_profile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_template_message'); ?>
		<?php echo $form->textField($model,'flag_template_message'); ?>
		<?php echo $form->error($model,'flag_template_message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_prouction'); ?>
		<?php echo $form->textField($model,'profile_prouction',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_prouction'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_gender'); ?>
		<?php echo $form->textField($model,'profile_gender',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_gprs'); ?>
		<?php echo $form->textField($model,'profile_gprs',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_gprs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_kesehatan'); ?>
		<?php echo $form->textField($model,'profile_kesehatan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_kesehatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_usia'); ?>
		<?php echo $form->textField($model,'profile_usia',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_usia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_lifestyle'); ?>
		<?php echo $form->textField($model,'profile_lifestyle',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_lifestyle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_sians'); ?>
		<?php echo $form->textField($model,'profile_sians',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_sians'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_travel'); ?>
		<?php echo $form->textField($model,'profile_travel',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_travel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_arpu'); ?>
		<?php echo $form->textField($model,'profile_arpu',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'profile_arpu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'param_arpu'); ?>
		<?php echo $form->textField($model,'param_arpu'); ?>
		<?php echo $form->error($model,'param_arpu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'param_usia'); ?>
		<?php echo $form->textField($model,'param_usia'); ?>
		<?php echo $form->error($model,'param_usia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_usia2'); ?>
		<?php echo $form->textField($model,'profile_usia2'); ?>
		<?php echo $form->error($model,'profile_usia2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_arpu2'); ?>
		<?php echo $form->textField($model,'profile_arpu2'); ?>
		<?php echo $form->error($model,'profile_arpu2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'flag_master_batch'); ?>
		<?php echo $form->textField($model,'flag_master_batch'); ?>
		<?php echo $form->error($model,'flag_master_batch'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'interface_channel'); ?>
		<?php echo $form->textField($model,'interface_channel'); ?>
		<?php echo $form->error($model,'interface_channel'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->