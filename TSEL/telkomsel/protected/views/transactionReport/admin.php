<?php
$this->breadcrumbs=array(
	'Transaction Reports'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TransactionReport', 'url'=>array('index')),
	array('label'=>'Create TransactionReport', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('transaction-report-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Transaction Reports</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'transaction-report-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_batch',
		'id_customer',
		'jobs_id',
		'start_periode',
		'stop_periode',
		'sms_send_limit',
		/*
		'sms_sent',
		'sms_text',
		'message_type',
		'message_bit',
		'status_batch',
		'filename',
		'first_ip',
		'first_user',
		'frist_update',
		'last_ip',
		'last_user',
		'last_update',
		'topik',
		'approved_time',
		'sms_success',
		'sms_failed',
		'deleted',
		'schedule_delivery',
		'process_date',
		'retry',
		'comment',
		'id_invoice',
		'batch_no',
		'last_value',
		'content_expired',
		'divre',
		'filenameupload',
		'broadcast_type',
		'total_number',
		'booking',
		'usage',
		'last_filestatus_modif',
		'customer_ani',
		'binary_data',
		'prefix_detail',
		'sms_text_view',
		'instant_failed',
		'tipe_batch',
		'promotion_media',
		'sex_category',
		'age_category',
		'status_category',
		'filter_prefix',
		'education_category',
		'channel_category',
		'priority_category',
		'flag_deleted',
		'ast',
		'cell',
		'flag_lba',
		'flag_file',
		'multimedia_type',
		'quota',
		'profile',
		'batch_type',
		'vip',
		'increase_counter',
		'unique_subscriber_per_batch',
		'flag_hour',
		'flag_multi_batch',
		'slot_multi_batch',
		'id_batch_reference',
		'black_list_profile',
		'flag_template_message',
		'profile_prouction',
		'profile_gender',
		'profile_gprs',
		'profile_kesehatan',
		'profile_usia',
		'profile_lifestyle',
		'profile_sians',
		'profile_travel',
		'profile_arpu',
		'param_arpu',
		'param_usia',
		'profile_usia2',
		'profile_arpu2',
		'flag_master_batch',
		'interface_channel',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
