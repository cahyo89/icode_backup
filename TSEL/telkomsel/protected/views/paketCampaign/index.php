<?php
/* @var $this PaketCampaignController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Paket Campaigns',
);

$this->menu=array(
	array('label'=>'Create Paket Campaign', 'url'=>array('create')),
	array('label'=>'Manage Paket Campaign', 'url'=>array('admin')),
);
?>

<h1>Paket Campaign</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
