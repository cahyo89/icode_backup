<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Paket Campaigns'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List Paket Campaign', 'url'=>array('index')),
	array('label'=>'Create Paket Campaign', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#paket-campaign-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Paket Campaigns</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'paket-campaign-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		'nama',
		'harga',
		array(
		'name'=>'channel_category',
		'value'=>'Yii::app()->params[\'channelcatagory\'][$data->channel_category]',
		),
		/*
		'ads_start_time',
		'ads_time_out',
		'ukuran_gambar1',
		'ukuran_gambar2',
		'ukuran_gambar3',
		'priority',
		'first_user',
		'first_update',
		'first_ip',
		'last_user',
		'last_update',
		'last_ip',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
