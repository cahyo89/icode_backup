<?php
$this->breadcrumbs=array(
	'Report Summary Campaign Filter'=>array('index'),
	);?>
	<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>

<?php 
if(isset($dataProvider)){
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'summary-single-command-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(            
            'name'=>'trans_date',
            'value'=>'@$data->trans_date',
        ),	array(            
            'name'=>'ani',
            'value'=>'@$data->ani',
        ),
		array(            
            'name'=>'Customer',
            'value'=>'@$data->Customer->nama',
        ),
		array(            
            'name'=>'success',
            'value'=>'@$data->success',
        ),
		array(            
            'name'=>'failed',
            'value'=>'@$data->failed',
        ),
		array(            
            'name'=>'error_code',
            'value'=>'@$data->error_code',
        ),
		
	),
));
}
 ?>
 </div>