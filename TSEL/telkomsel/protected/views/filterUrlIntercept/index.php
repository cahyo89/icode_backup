<?php
/* @var $this FilterUrlInterceptController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Filter Url Intercepts',
);

$this->menu=array(
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
	array('label'=>'Manage FilterUrlIntercept', 'url'=>array('admin')),
);
?>

<h1>Filter Url Intercepts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
