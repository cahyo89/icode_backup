<?php
/* @var $this FilterUrlInterceptController */
/* @var $model FilterUrlIntercept */

$this->breadcrumbs=array(
	'Filter Url Intercepts'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List FilterUrlIntercept', 'url'=>array('index')),
	array('label'=>'Create FilterUrlIntercept', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#filter-url-intercept-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Filter Url Intercepts</h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
					)); ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="well">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'filter-url-intercept-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		'url_name',
		array(
			'name'=>'url_detail',
			'htmlOptions'=>array('width'=>'100px'),
		),
		/*'first_user',
		'first_update',
		'first_ip',
		'last_user',
		'last_update',
		'last_ip',
		'flag_deleted',
		*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nUrl Name :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>

<div>
