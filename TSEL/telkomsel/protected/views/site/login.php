<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <title>WEB INTERCEPT MANAGEMENT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="assets/css/default.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    
    <!-- Le fav and touch icons 
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    -->
	<style>
		body
		{
			background:url('assets/img/gplaypattern.png');
		}
		
		#appendedInputButton
		{
			height:44px;
		}
	</style>
	<!--[if lt IE 8]>
		<link href="assets/css/ie.css" rel="stylesheet">
	<![endif]-->	
  </head>
  
  <body>
	
	<div class="container">
		<div style="" class="well loginform">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>'index.php?r=user/login',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
				'htmlOptions'=>array(
					'class'=>'form-horizontal',
				),
			)); ?>
			  <fieldset>
				<div id="legend">
				  <legend class="">WEB INTERCEPT MANAGEMENT</legend>
				</div>
				<div class="control-group">
				
				  <!-- Username -->
				  <?php echo $form->labelEx($model,'Username',array('class'=>"control-label",'for'=>"username")); ?>
				  <div class="controls">
					<?php echo $form->textField($model,'username',array('id'=>"username",'class'=>"input-xlarge",'placeholder'=>"Type Your Username Here")); ?>
					<?php echo $form->error($model,'username'); ?>
				  </div>
				</div>

				<div class="control-group">
				  <!-- Password-->
				  <?php echo $form->labelEx($model,'Password',array('class'=>"control-label",'for'=>"password")); ?>
				  <div class="controls">
					<?php echo $form->passwordField($model,'password',array('id'=>"password",'class'=>"input-xlarge",'placeholder'=>"Type Your Password Here")); ?>
					<?php echo $form->error($model,'password'); ?>
				  </div>
				</div>          
				<div class="control-group">
				  <!-- Button -->
				  <div align="center">
					<button class="btn btn-danger">Login</button>
				  </div>
				</div>
			  </fieldset>
			<?php $this->endWidget(); ?>
			<footer>
				<p> &copy; Axis Telekom 2012</p>
			</footer>
		</div>
    </div> <!-- /container -->

    <div id="forgotpassword" class="modal hide fade">
		<div class="modal-header">
				<button class="close" data-dismiss="modal">×</button>
				<h3>AXIS TELEKOM - INDONESIA</h3>
		</div>
			<div class="modal-body">
			  <div class="row-fluid">
					<div class="well">
						Enter your email here and we will send you an email with a link you can use to reset your password.
					</div>
					<form>
						<div class="input-append">
						  <input class="span9" id="appendedInputButton" type="email" placeholder="Type Your Email Here">
						  <button class="btn btn-large" type="button">Send Email</button>
						</div>
					</form>
			 </div>
		 </div>
    </div>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>

</html>
