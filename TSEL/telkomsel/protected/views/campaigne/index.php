<?php
$this->breadcrumbs=array(
	'Campaignes',
);

$this->menu=array(
	array('label'=>'Create Campaigne', 'url'=>array('create')),
	array('label'=>'Manage Campaigne', 'url'=>array('admin')),
);
?>

<h1>Campaignes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
