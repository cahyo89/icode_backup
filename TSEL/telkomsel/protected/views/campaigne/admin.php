<?php
$this->breadcrumbs=array(
	'Campaignes'=>array('index'),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('campaigne-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerCss(uniqid(),".grid-view table.items{	font-size:11px;}");

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Campaign</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'campaigne-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('name'=>'jobs_id',
		'type'=>'raw',
		'value'=>'CHtml::link(@str_replace("_","<br>",$data->jobs_id),array("Campaigne/view","id"=>$data->id_batch))'),
		array(
		'name'=>'Status Campaign',
		'value'=>'Yii::app()->params[\'status_batch\'][$data->status_batch]',
		),
		array(
		'name'=>'Customer Name',
		'value'=>'@$data->Customer->nama'
		),
		'topik',
		'start_periode',
		'content_expired',
		'total_number',
		array(
		'name'=>'Channel Type',
		'value'=>'Yii::app()->params[\'channelcatagory\'][$data->channel_category]',
		),
		
		/*
		'sms_sent',
		'sms_text',
		'message_type',
		'message_bit',
		'status_batch',
		'filename',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'topik',
		'approved_time',
		'sms_success',
		'sms_failed',
		'deleted',
		'schedule_delivery',
		'process_date',
		'retry',
		'comment',
		'id_invoice',
		'batch_no',
		'last_value',
		'content_expired',
		'divre',
		'filenameupload',
		'broadcast_type',
		'total_number',
		'booking',
		'usage',
		'last_filestatus_modif',
		'customer_ani',
		'binary_data',
		'prefix_detail',
		'sms_text_view',
		'instant_failed',
		'tipe_batch',
		'promotion_media',
		'sex_category',
		'age_category',
		'status_category',
		'filter_prefix',
		'education_category',
		'channel_category',
		'priority_category',
		'flag_deleted',
		'ast',
		'cell',
		'flag_lba',
		'flag_file',
		'multimedia_type',
		'quota',
		'profile',
		'batch_type',
		'vip',
		'increase_counter',
		'unique_subscriber_per_batch',
		'flag_hour',
		'flag_multi_batch',
		'slot_multi_batch',
		'id_batch_reference',
		'black_list_profile',
		'flag_template_message',
		'profile_prouction',
		'profile_gender',
		'profile_gprs',
		'profile_kesehatan',
		'profile_usia',
		'profile_lifestyle',
		'profile_sains',
		'profile_travel',
		'profile_arpu',
		'param_arpu',
		'param_usia',
		'profile_usia2',
		'profile_arpu2',
		'flag_master_batch',
		'interface_channel',
		*/
		array(
			
			'class'=>'CButtonColumn',
			// 'htmlOptions' => array('style'=>'width:800px'),
			'template'=>'{view}{update}{delete}',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\nCampaign ID :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
			'buttons'=>array
			(
			'view'=>array(
				'label'=>'view',
				'url'=>'Yii::app()->createUrl("Campaigne/view", array("id"=>$data->id_batch))',
					),
			
			'update'=>array(
				'label'=>'update',
				'url'=>'Yii::app()->createUrl("Campaigne/update", array("id"=>$data->id_batch))',
				'visible' => '$data->status_batch == 0|| $data->status_batch == 11 || $data->status_batch == 2 || $data->status_batch == 5',
					),
			'delete'=>array(
				'label'=>'delete',
				'url'=>'Yii::app()->createUrl("Campaigne/delete", array("id"=>$data->id_batch,"name"=>$data->id_batch))',
				'visible' => '$data->status_batch == 0',
					),
				),
				),
	),
)); ?>
<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'Campaigne/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>
