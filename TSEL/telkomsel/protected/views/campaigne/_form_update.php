<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'campaigne-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php 
//  yang bisa diupdate
//  start periode
//  content expired
//  isi sms text
//  topic
//  token
//  gambar
//  url click

	function getDetailPacketCampaign($model){
	
		$arrayUrl = array(0=>"disabled",1=>"enable");
		$arrayLocation = array(0=>"disabled",1=>"enable");
		$arrayUserAgent = array(0=>"disabled",1=>"enable");
		$arrayKeyword = array(0=>"disabled",1=>"enable");
		
		$sql = "SELECT * FROM tbl_paket_campaign where id =".$model->id_paket_campaign." ";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count 		= count($row);
		$x = 0;
		while($x < $count)
		{
			
			$packetName 				= $row[$x]['nama'];
			$packetTotalPerToken		= $row[$x]['harga'];
			$packetPriority				= Yii::app()->params['priority'][$row[$x]['priority']];
			$packetChannelCategory		= Yii::app()->params['channelcatagory'][$row[$x]['channel_category']];
			
			$advertisingType 			= Yii::app()->params['http_intercept_ads_type'][$row[$x]['http_intercept_ads_type']];
			$delayAdvertisingStart		= $row[$x]['ads_start_time'];
			$delayAdvertisingTimeOut	= $row[$x]['ads_time_out'];
			
			$site						= $arrayUrl[$row[$x]['site']];
			$location					= $arrayLocation[$row[$x]['location']];
			$userAgent					= $arrayUserAgent[$row[$x]['user_agent']];
			$keyword					= $arrayKeyword[$row[$x]['keyword']];
			
			$size1						= $row[$x]['ukuran_gambar1'];
			$size2						= $row[$x]['ukuran_gambar2'];
			$size3						= $row[$x]['ukuran_gambar3'];
			$x++;
		}
		
		if($model->channel_category == 65280){
			$result = " <br> <br> Packet Name : ".$packetName." ";
			$result .= " <br> Packet type : ".$packetChannelCategory." ";
			$result .= " <br> total / token : ".$packetTotalPerToken." ";
			$result .= " <br> priority : ".$packetPriority." ";	
			$result .= " <br> advertising type : ".$delayAdvertisingStart." ";
			$result .= " <br> delay start : ".$advertisingType." ";
			$result .= " <br> delay time out : ".$delayAdvertisingTimeOut." ";
			$result .= " <br> site : ".$site." ";
			$result .= " <br> location : ".$location." ";
			$result .= " <br> userAgent : ".$userAgent." ";
			$result .= " <br> keyword : ".$keyword." ";
			$result .= " <br> size1 : ".$size1." ";
			$result .= " <br> size2 : ".$size2." ";
			$result .= " <br> size3 : ".$size3." <br> <br>";
			return $result;
		}
		else{
			$result = " <br> <br>Packet Name : ".$packetName." ";
			$result .= " <br> Packet type : ".$packetChannelCategory." ";
			$result .= " <br> total / token : ".$packetTotalPerToken." ";
			$result .= " <br> priority : ".$packetPriority." <br> <br>";	
			return $result;
			
		}
	}

	function getTotalByIdBatch($model){
		$rate = 0;
		$sql  = "SELECT harga FROM tbl_paket_campaign where id = ".$model->id_paket_campaign." ";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count 		= count($row);
		$x = 0;
		while($x < $count)
		{
			$rate = $row[$x]['harga'];
			$x++;
		}
		return $model->total_number/$rate;
	}
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>
	<div class="well-small">
		<?php echo $form->labelEx($model,'Campaigne Id'); ?>
		<div class="read">
			<?php echo Chtml::activeTextField($model,'jobs_id',array('size'=>60,'maxlength'=>200,'readonly'=>true)); ?>
		</div>
		<?php echo $form->error($model,'jobs_id'); ?>
	</div>

	<div class="well-small">
		<?php echo $form->labelEx($model,'Topic'); ?>
		<?php echo $form->textField($model,'topik',array('size'=>60,'maxlength'=>100))."*"; ?>
		<?php echo $form->error($model,'topik'); ?>
	</div>
	
	<div class="well-small">
		<?php echo "Packet : ".getDetailPacketCampaign($model).""; ?>
		<?php //echo $form->labelEx($model,'Paket Name'); ?>
		<?php //echo $form->textArea($model,'id_paket_campaign',array('value'=>"".getDetailPacketCampaign($model).""))."*"; ?>
		<?php //echo $form->error($model,'id_paket_campaign'); ?>
	</div>
	
	<div class="well-small">
		<?php echo $form->label($model,'Start Periode'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
				    'model'=>$model,
				    'name'=>'start_periode',
				));
				echo "*";
				?>
	</div>

	<div class="well-small">
		<?php echo $form->label($model,'Content Expired'); ?>
		<?php $this->widget('application.extensions.timepicker.timepicker', array(
				    'model'=>$model,
				    'name'=>'content_expired',
				));
				echo "*";
				?>
	</div>
	
	<?php if($model->channel_category != 65280){ ?> 
		<div>
			<?php echo $form->labelEx($model,'Campaign SMS Text'); ?>
			<?php echo $form->textArea($model,'sms_text',array('cols'=>50,'rows'=>10,'onkeyup'=>'{onEventSMSText();}'))."*"; ?>
			<?php echo $form->error($model,'sms_text'); ?>
		</div>
		<div>
			Length SMS <input name="Campaigne[length_text]" value="0" size="3" id="Campaigne_length_text" readonly="true"  type="text">
		</div>
	<?php } ?>
	
	<?php if($model->channel_category == 65280){ ?> 
		<div class="well-small">
			<?php echo $form->labelEx($model,'Redirect Link')."<b><font color='red'>*</font></b>"; ?>
			<?php echo Chtml::activeTextField($model,'flying_click_link',array('maxlength'=>200)); ?>
			<?php echo $form->error($model,'flying_click_link'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 1')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 2')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model, 'Image 3')."<b><font color='red'>*</font></b>"; ?>
			<?php echo $form->fileField($model, 'filenameupload[]',array('onchange'=>'onUploadBrowse(this.form,\''.CController::createUrl('OnUploadBrowse').'\',\'data\');')); ?>
			<?php echo $form->error($model, 'filenameupload'); ?>
		</div>
	<?php }?>
	
	
	<div class="well-small">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
	
	
	<input name="Campaigne[message_type]" value="1" id="Campaigne_message_type" style="display:none" type="text">
	<input name="Campaigne[message_bit]" value="7" id="Campaigne_message_bit" style="display:none" type="text">
	<input name="Campaigne[binary_data]" value="0" id="Campaigne_binary_data" style="display:none" type="text">
	<input name="Campaigne[promotion_media]" value="0" id="Campaigne_promotion_media" style="display:none" type="text">
	<input name="Campaigne[sex_category]" value="0" id="Campaigne_sex_category" style="display:none" type="text">
	<input name="Campaigne[age_category]" value="0" id="Campaigne_age_category" style="display:none" type="text">
	<input name="Campaigne[status_category]" value="0" id="Campaigne_status_category" style="display:none" type="text">
	<input name="Campaigne[filter_prefix]" value="0" id="Campaigne_filter_prefix" style="display:none" type="text">
	<input name="Campaigne[education_category]" value="0" id="Campaigne_education_category" style="display:none" type="text">
	<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
	<input name="Campaigne[flag_lba]" value="0" id="Campaigne_flag_lba" style="display:none" type="text">
	<input name="Campaigne[quota]" value="0" id="Campaigne_quota" style="display:none" type="text">
	<input name="Campaigne[vip]" value="1" id="Campaigne_vip" style="display:none" type="text">
	<input name="Campaigne[increase_counter]" value="1" id="Campaigne_increase_counter" style="display:none" type="text">
	<input name="Campaigne[unique_subscriber_per_batch]" value="1" id="Campaigne_unique_subscriber_per_batch" style="display:none" type="text">
	<input name="Campaigne[flag_hour]" value="0" id="Campaigne_flag_hour" style="display:none" type="text">
	<input name="Campaigne[multimedia_type]" value="0" id="Campaigne_multimedia_type" style="display:none" type="text">
	<input name="Campaigne[flag_file]" value="0" id="Campaigne_flag_file" style="display:none" type="text">
	
	
<script type="text/javascript">
	
	
	
	function onUploadBrowse(form, action_url, div_id)
	{
		return false;
		var iframe = document.createElement("iframe");
		iframe.setAttribute("id", "upload_iframe");
		iframe.setAttribute("name", "upload_iframe");
		iframe.setAttribute("width", "0");
		iframe.setAttribute("height", "0");
		iframe.setAttribute("border", "0");
		iframe.setAttribute("style", "width: 0; height: 0; border: none;");
		// Add to document...
		form.parentNode.appendChild(iframe);
		window.frames['upload_iframe'].name = "upload_iframe";
	 
		iframeId = document.getElementById("upload_iframe");
		
		// Add event...
		var eventHandler = function () {
	 
				if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
				else iframeId.removeEventListener("load", eventHandler, false);
				
				// Message from server...
				if (iframeId.contentDocument) {
					content = iframeId.contentDocument.body.innerHTML;
				} else if (iframeId.contentWindow) {
					content = iframeId.contentWindow.document.body.innerHTML;
				} else if (iframeId.document) {
					content = iframeId.document.body.innerHTML;
				}
	 
				document.getElementById(div_id).innerHTML = content;
	 				
			}
	 
		if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
		if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
	 
		// Set properties of form...
		form.setAttribute("target", "upload_iframe");
		form.setAttribute("action", action_url);
		form.setAttribute("method", "post");
		form.setAttribute("enctype", "multipart/form-data");
		form.setAttribute("encoding", "multipart/form-data");
	 
		// Submit the form...
		form.submit();
	 
		document.getElementById(div_id).innerHTML = "Uploading...";
		return false;
	}
	
	function onEventSMSText(){
		var smsText =  document.getElementById('Campaigne_sms_text').value;
		document.getElementById('Campaigne_length_text').value = smsText.length;
	}
	
	
	function onChangeChannelCategory(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeChannelCategory'),
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_customer_ani").html(data.customer_ani);
	                 }
	                 ',
	                ))?>;
					
					if(document.getElementById('Campaigne_channel_category').value == 5){
						document.getElementById('Campaigne_mmsFile').disabled = false;
					}
					else{
						document.getElementById('Campaigne_mmsFile').disabled = true;
					}
					
	        return false;
	}
	
	function onChangeCampaigneType(){
		if(document.getElementById('Campaigne_batch_type').value == 1 || document.getElementById('Campaigne_batch_type').value == 5){
			document.getElementById('Campaigne_blackWhiteListFile').disabled = false;
		}
		else{
			document.getElementById('Campaigne_blackWhiteListFile').disabled = true;
		}
	}
	
	
	function onChangeCustomer(){
		<?php echo CHtml::ajax(array(
	                // the controller/function to call
	                'url'=>CController::createUrl('onChangeCustomer'),
	 
	                // Data to be passed to the ajax function
	                // Note that the ' should be escaped with \
	                // The field id should be prefixed with the model name eg Vehicle_field_name
	               // 'data'=>array('registration_number'=>'js:$(\'#Vehicle_registration_number\').val()', 
					'data'=>array('id_customer'=>'js:$(\'#Campaigne_id_customer\').val()',//-->is this correct??
					'channel_category'=>'js:$(\'#Campaigne_channel_category\').val()',
	                // To pass multiple fields, just repeat eg:
	                //              'chassis_number'=>'js:$(\'#Vehicle_chassis_number\').val()',
	                                ),
	                'type'=>'post',
	                'dataType'=>'json',
	                'success'=>'function(data)
	                {
						$("#Campaigne_jobs_id").val(data.jobs_id);
						$("#Campaigne_customer_ani").html(data.customer_ani);
	                 }
	                 ',
	                ))?>;
					
	        return false;
	}

</script>

	
<?php $this->endWidget(); ?>
</div><!-- form -->