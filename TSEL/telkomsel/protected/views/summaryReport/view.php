<?php
$this->breadcrumbs=array(
	'Report Summary Campaign Filter'=>array('index'),
	);?>
	<?php if(Yii::app()->user->hasFlash('error')): ?>
  <br></br>
<div class="flash-info flash flash-block">
    <font><strong><?php echo Yii::app()->user->getFlash('error'); ?></strong></font>
</div>

<?php endif; ?>

<h1>Report Summary SMS History Transaction API SINGLE Message</h1>
<div class="well-small" style="float:left">
	<?php echo CHtml::link('Export',array('summaryReport/excel'), array('class'=>'btn btn-danger')); ?>
</div>
<?php 
if(isset($dataProvider)){
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'summary-report-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array( 
            'name'=>'transaction date',
			'value'=>'$data[start_date]'),
		array( 
            'name'=>'transaction id',
			'value'=>'$data[trc_id]'),
		array( 
            'name'=>'customer masking',
			'value'=>'$data[ani]'),
		array( 
            'name'=>'msisdn',
			'value'=>'$data[dnis]'),
		array( 
            'name'=>'sms',
			'value'=>'$data[sms_text]'),
		array( 
            'name'=>'customer',
			'value'=>'$data[nama]'),
		array( 
            'name'=>'status',
			'cssClassExpression' => '$data[result]=="2" ? "aktip" : "non"',
			'value'=>'$data[result]=="2" ? "success" : "error"' ,),
	),
));
}
 ?>
 </div>
 
 <style>
	
 
 	.grid-view table.items td.aktip{
		color:#0dc403;
		font-weight:bold;
	}
	.grid-view table.items td.non{
		color:#ff0000;
		font-weight:bold;
	}
</style>

