<?php
/* @var $this ParamKeywordController */
/* @var $model ParamKeyword */

$this->breadcrumbs=array(
	'Param Keywords'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ParamKeyword', 'url'=>array('index')),
	array('label'=>'Create ParamKeyword', 'url'=>array('create')),
	array('label'=>'Update ParamKeyword', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ParamKeyword', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ParamKeyword', 'url'=>array('admin')),
);
?>

<h1>View ParamKeyword #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'param_name',
		'first_update',
		'first_ip',
		'first_user',
		'last_update',
		'last_ip',
		'last_user',
	),
)); ?>
