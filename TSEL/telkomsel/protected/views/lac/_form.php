<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lac-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'lac_name'); ?>
		<?php echo $form->textField($model,'lac_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lac_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lac_id'); ?>
		<?php echo $form->textField($model,'lac_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'lac_id'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'id_probing'); ?>
		<?php echo $form->dropDownList($model,'id_probing',CHtml::listData(Probing::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array(':deleted'=>1)), 'id_probing', 'probing_name'),array('empty'=>'--Please Select One--')); ?>
		<?php echo $form->error($model,'id_probing'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->