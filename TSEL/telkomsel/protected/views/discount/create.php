<?php
/* @var $this DiscountController */
/* @var $model Discount */

$this->breadcrumbs=array(
	'Discounts'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Discount', 'url'=>array('index')),
	array('label'=>'Manage Discount', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Create Discounts</h1></td>
<td><div class="operatorRight"><?php Controller::createMenu(array(
				   array('label'=>'List','link'=>'admin','img'=>'/images/new.png','id'=>'','conf'=>'')
					)); ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>