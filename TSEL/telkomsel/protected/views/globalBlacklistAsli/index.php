<?php
$this->breadcrumbs=array(
	'Global Blacklist Aslis',
);

$this->menu=array(
	array('label'=>'Create GlobalBlacklistAsli', 'url'=>array('create')),
	array('label'=>'Manage GlobalBlacklistAsli', 'url'=>array('admin')),
);
?>

<h1>Global Blacklist Aslis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
