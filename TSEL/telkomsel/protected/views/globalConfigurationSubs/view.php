<?php
$this->breadcrumbs=array(
	'Global Configuration'=>array('index'),
	$model->shortcode,
);

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View Global Configuration #<?php echo $model->shortcode; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'globalConfigurationSubs/update','img'=>'/images/update.png','id'=>$model->shortcode,'conf'=>''),
	   array('label'=>'Delete','link'=>'globalConfigurationSubs/delete','img'=>'/images/delete.png','id'=>$model->shortcode,'conf'=>$model->shortcode),
	   array('label'=>'New','link'=>'globalConfigurationSubs/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'shortcode',
		'keyword_on_whitelist',
		'keyword_off_whitelist',
		'keyword_on_blacklist',
		'keyword_off_blacklist',
		'response_success_on_whitelist_sub',
		'response_success_off_whitelist_unsub',
		'response_already_on_whitelist',
		'response_already_off_whitelist',
		'response_success_on_blacklist_block',
		'response_success_off_blacklist_unblock',
		'response_already_on_blacklist',
		'response_already_off_blacklist',
		'response_keyword_not_valid',
		'response_shortcode_expired',
		'response_shortcode_already_response',
		'response_shortcode_not_from_lba',
	),
));
Controller::createInfo($model);
 ?>
 
 <div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'globalConfigurationSubs/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'globalConfigurationSubs/update','img'=>'/images/update.png','id'=>$model->shortcode,'conf'=>''),
	    array('label'=>'Delete','link'=>'globalConfigurationSubs/delete','img'=>'/images/delete.png','id'=>$model->shortcode,'conf'=>$model->shortcode)
	   
		));  ?>
</div>
