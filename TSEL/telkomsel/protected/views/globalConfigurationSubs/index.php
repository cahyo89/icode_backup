<?php
$this->breadcrumbs=array(
	'Global Configuration Subs',
);

$this->menu=array(
	array('label'=>'Create GlobalConfigurationSubs', 'url'=>array('create')),
	array('label'=>'Manage GlobalConfigurationSubs', 'url'=>array('admin')),
);
?>

<h1>Global Configuration Subs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
