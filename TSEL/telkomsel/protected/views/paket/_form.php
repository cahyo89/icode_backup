<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paket-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_paket'); ?>
		<?php echo $form->textField($model,'jenis_paket',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'jenis_paket'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga_paket'); ?>
		<?php echo $form->textField($model,'harga_paket'); ?> (Numeric/Rupiah)
		<?php echo $form->error($model,'harga_paket'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_paket'); ?>
		<?php echo $form->textField($model,'sms_paket'); ?> (Numeric)
		<?php echo $form->error($model,'sms_paket'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expired_paket'); ?>
		<?php echo $form->textField($model,'expired_paket'); ?> Days
		<?php echo $form->error($model,'expired_paket'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'flag'); ?>
		<?php echo $form->dropDownList($model,'flag',array(0=>"non promo",1=>"promo")); ?> 
		<?php echo $form->error($model,'flag'); ?>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
