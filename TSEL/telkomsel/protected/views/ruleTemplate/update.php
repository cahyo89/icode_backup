<?php
$this->breadcrumbs=array(
	'Rule Templates'=>array('index'),
	$model->rule_name=>array('view','id'=>$model->id),
	'Update',
);

?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Rule Template #<?php echo $model->rule_name; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'ruleTemplate/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'ruleTemplate/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
					
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'ruleTemplate/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'ruleTemplate/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'ruleTemplate/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>