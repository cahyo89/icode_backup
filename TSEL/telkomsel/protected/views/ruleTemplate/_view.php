<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_customer')); ?>:</b>
	<?php echo CHtml::encode($data->id_customer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pause_time')); ?>:</b>
	<?php echo CHtml::encode($data->pause_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resume_time')); ?>:</b>
	<?php echo CHtml::encode($data->resume_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quota_value')); ?>:</b>
	<?php echo CHtml::encode($data->quota_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rule_name')); ?>:</b>
	<?php echo CHtml::encode($data->rule_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('first_ip')); ?>:</b>
	<?php echo CHtml::encode($data->first_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_user')); ?>:</b>
	<?php echo CHtml::encode($data->first_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_update')); ?>:</b>
	<?php echo CHtml::encode($data->first_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_ip')); ?>:</b>
	<?php echo CHtml::encode($data->last_ip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_user')); ?>:</b>
	<?php echo CHtml::encode($data->last_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_update')); ?>:</b>
	<?php echo CHtml::encode($data->last_update); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason_reject')); ?>:</b>
	<?php echo CHtml::encode($data->reason_reject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('flag')); ?>:</b>
	<?php echo CHtml::encode($data->flag); ?>
	<br />

	*/ ?>

</div>