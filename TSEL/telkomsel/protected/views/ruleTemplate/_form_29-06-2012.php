<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rule-template-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'rule_name'); ?>
		<?php echo $form->textField($model,'rule_name',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'rule_name'); ?>
	</div>
	
<div class="row">
		<?php echo $form->labelEx($model,'id_customer'); ?>
		<?php echo $form->dropDownList($model,'id_customer',CHtml::listData(Customer::model()->findAll('flag_deleted IS NULL or flag_deleted <> :deleted',array('deleted'=>1)),'id_customer','nama')); ?>
		<?php echo $form->error($model,'id_customer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pause_time'); ?>
		<?php                    
		$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
		        'language'=>'',
		        'model'=>$model,                                // Model object
		        'attribute'=>'pause_time', // Attribute name//'minDate'=>date('Y-m-d'),  'hourMin' => (int)date('h')
		        'mode'=>'datetime',                     // Use "time","date" or "datetime" (default)
		        'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'changeMonth'=>'true',
					'changeYear'=>'true',
					'hourGrid'=>5,
					'minuteGrid'=>10,
					'yearRange'=>'1920:2013',
					'showOn'=>'both',
					'buttonImageOnly'=>'true',
					'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				),                       
		        'htmlOptions'=>array('readonly'=>true), // HTML options
		));                             
		?>
		<?php echo $form->error($model,'pause_time'); ?>
	</div>

	<div class="row" >
		<?php echo $form->labelEx($model,'resume_time'); ?>
		<?php 
			$this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
		        'language'=>'',
		        'model'=>$model,                                // Model object
		        'attribute'=>'resume_time', // Attribute name//'minDate'=>date('Y-m-d'),  'hourMin' => (int)date('h')
		        'mode'=>'datetime',                     // Use "time","date" or "datetime" (default)
		        'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					'changeMonth'=>'true',
					'changeYear'=>'true',
					'hourGrid'=>2,
					'minuteGrid'=>10,
					'yearRange'=>'1920:2013',
					'showOn'=>'both',
					'buttonImageOnly'=>'true',
					'buttonImage'=>Yii::app()->request->baseUrl . '/images/calendar.gif',
				),                       
		        'htmlOptions'=>array('readonly'=>true), // HTML options
		));          
		?>
		<?php echo $form->error($model,'resume_time'); ?>
	</div>
	



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->