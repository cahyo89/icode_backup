<?php
/* @var $this ParamUserAgentController */
/* @var $model ParamUserAgent */

$this->breadcrumbs=array(
	'Param User Agents'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List ParamUserAgent', 'url'=>array('index')),
	array('label'=>'Create ParamUserAgent', 'url'=>array('create')),
	array('label'=>'View ParamUserAgent', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Param User Agent # <?php echo $model->id; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'admin','img'=>'/images/list.png','id'=>'','conf'=>''),
				   array('label'=>'Create','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>''),
				   array('label'=>'View','link'=>'view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
					));  ?></div></td>
					</tr></table>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>