<?php
/* @var $this ParamUserAgentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Param User Agents',
);

$this->menu=array(
	array('label'=>'Create ParamUserAgent', 'url'=>array('create')),
	array('label'=>'Manage ParamUserAgent', 'url'=>array('admin')),
);
?>

<h1>Param User Agents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
