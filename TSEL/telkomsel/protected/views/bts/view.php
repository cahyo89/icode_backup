<?php
$this->breadcrumbs=array(
	'CI'=>array('index'),
	$model->bts_name,
);


?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><div class="dodod"><b>View CI #<?php echo $model->bts_name; ?></b></div></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'Update','link'=>'bts/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'Delete','link'=>'bts/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->bts_name),
	   array('label'=>'New','link'=>'bts/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'bts_name',
		'cellname',
		
		'site_id',
		array(
			'name'=>'site_type',
			'type'=>'raw',
			'value'=>@Yii::app()->params["site_type"][$model->site_type],
		),
		'description',
		'bts_id',
		'longitude',
		'latitude',
		'lac',
	),
));
Controller::createInfo($model);
 ?>

<div class="operatorLeft">
<?php  Controller::createMenu(array(
		array('label'=>'New','link'=>'bts/create','img'=>'/images/new.png','id'=>'','conf'=>''),
		array('label'=>'Update','link'=>'bts/update','img'=>'/images/update.png','id'=>$model->id,'conf'=>''),
	    array('label'=>'Delete','link'=>'bts/delete','img'=>'/images/delete.png','id'=>$model->id,'conf'=>$model->bts_name)
	   
		));  ?>
</div>
