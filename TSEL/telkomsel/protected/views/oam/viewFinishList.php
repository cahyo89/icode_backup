<?php
$this->breadcrumbs=array(
	'Monitoring'=>array('index'),
	$model->jobs_id,
);
?>

<h1>View Finish List Campaign #<?php echo $model->jobs_id; ?></h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'oam',
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
)); 
?>

<?php 

function getDetailPacketCampaign($model){
	
		$arrayUrl = array(0=>"disabled",1=>"enable");
		$arrayLocation = array(0=>"disabled",1=>"enable");
		$arrayUserAgent = array(0=>"disabled",1=>"enable");
		$arrayKeyword = array(0=>"disabled",1=>"enable");
		
		$sql = "SELECT * FROM tbl_paket_campaign where id =".$model->id_paket_campaign." ";
		$connection = Yii::app()->db;
		$dataReader = $connection->createCommand($sql);
		$row = $dataReader->queryAll();
		$count 		= count($row);
		$x = 0;
		while($x < $count)
		{
			
			$packetName 				= $row[$x]['nama'];
			$packetTotalPerToken		= $row[$x]['harga'];
			$packetPriority				= Yii::app()->params['priority'][$row[$x]['priority']];
			$packetChannelCategory		= Yii::app()->params['channelcatagory'][$row[$x]['channel_category']];
			
			$advertisingType 			= Yii::app()->params['http_intercept_ads_type'][$row[$x]['http_intercept_ads_type']];
			$delayAdvertisingStart		= $row[$x]['ads_start_time'];
			$delayAdvertisingTimeOut	= $row[$x]['ads_time_out'];
			
			$site						= $arrayUrl[$row[$x]['site']];
			$location					= $arrayLocation[$row[$x]['location']];
			$userAgent					= $arrayUserAgent[$row[$x]['user_agent']];
			$keyword					= $arrayKeyword[$row[$x]['keyword']];
			
			$size1						= $row[$x]['ukuran_gambar1'];
			$size2						= $row[$x]['ukuran_gambar2'];
			$size3						= $row[$x]['ukuran_gambar3'];
			$act						= $row[$x]['advertising_content_type'];
			$x++;
		}
		
		if($model->channel_category == 65280){
			$result = "Packet Name : ".$packetName."| ";
			$result .= "Packet type : ".$packetChannelCategory."| ";
			$result .= "total / token : ".$packetTotalPerToken."| ";
			$result .= "priority : ".$packetPriority."| ";	
			$result .= "advertising type : ".$delayAdvertisingStart."| ";
			$result .= "delay start : ".$advertisingType."| ";
			$result .= "delay time out : ".$delayAdvertisingTimeOut."| ";
			$result .= "site : ".$site."| ";
			$result .= "location : ".$location."| ";
			$result .= "userAgent : ".$userAgent."| ";
			$result .= "keyword : ".$keyword."| ";
			$result .= "size1 : ".$size1."| ";
			$result .= "size2 : ".$size2."| ";
			$result .= "size3 : ".$size3." ";
			if($act == 1){
				$result .= " | content running text :  ".$model->filename." ";
			}
			return $result;
		}
		else{
			$result = "Packet Name : ".$packetName."| ";
			$result .= "Packet type : ".$packetChannelCategory."| ";
			$result .= "total / token : ".$packetTotalPerToken."| ";
			$result .= "priority : ".$packetPriority." ";	
			return $result;
			
		}
	}

function getBatchListed($idBatch){
	if($idBatch != ""){
			$sql 		= "select mdn from `tbl_batch_listed_".$idBatch."`";
			$connection = Yii::app()->db;
			$dataReader = $connection->createCommand($sql);
			$x = $dataReader->query()->getRowCount();
		return $x;
	}
	else{
		return 0;
	}
}

if($model->batch_type > 0){
	$totalBatchListed = " total data black white list : ".getBatchListed($model->id_batch);
}
else{
	$totalBatchListed = "0";
}

$arrayMessageType		= array(1=>"non interactive campaign",2=>"interactive campaign");
$arrayBalancing			= array(""=>"Off",0=>"Off",1=>"On");
$arrayStatusCampaign 	= array(0=>"Queue",1=>"Approved",2=>"Progress",3=>"Resuming",4=>"Pausing",5=>"Paused",6=>"Stopping",7=>"Stopped",8=>"Retry",9=>"Finish",11=>"Scheduled");
$arrayChannelCategory   = array(0=>"SMS",5=>"MMS",6=>"Statistic",7=>"Url interface",8=>"USSD",9=>"WAP");
$arrayAst				= array(0=>" ON Entry ",1=>" On Location");
$arrayVip				= array(0=>" NOT VIP",1=>"VIP");
$arrayPriority			= array(0=>"LOW",1=>"MEDIUM",2=>"HIGH");
$arrayCampaignType		= array(0=>"Normal",1=>"blacklist File",2=>"whitelist File",3=>"blacklist profile",4=>"whitelist profile");
$arrayGlobalWhiteList	= array(""=>"Off",0=>"Off",1=>"On");

$columns[] = array(
				'label'=>'Campaign Id',
				'name'=>'jobs_id'
				);
$columns[] = array(
				'label'=>'Id Batch',
				'name'=>'id_batch'
				);				
$columns[] = array(
				'label'=>'Customer',
				'name'=>'Customer.nama',
				);
$columns[] = array(
				'label'=>'Topic',
				'name'=>'topik'
				);
$columns[] = array(
				'label'=>'Start Periode',
				'name'=>'start_periode'
				);
$columns[] = array(
				'label'=>'Content Expired',
				'name'=>'content_expired'
				);				
$columns[] = array(
				'label'=>'Status Campaign',
				'value'=>Controller::getStatusCampaigne($model->status_batch)
				);
if($model->channel_category == "65280"){
$columns[] = array(
				'label'=>'Cell Group Area',
				'name'=>'BtsGroup.bts_name'
				);	

$columns[] = array(
				'label'=>'Cell Group Area',
				'name'=>'flying_click_link'
				);

				
}
else{

$columns[] = array(
				'label'=>'SMS Text',
				'name'=>'sms_text'
				);

}
$columns[] = array(
				'label'=>'Paket Campaign',
				'value'=>''.getDetailPacketCampaign($model).''
);				

$columns[] = array(
				'label'=>'Campaign Type',
				'value'=>Controller::getCampaigneType($model->batch_type)
				);

$columns[] = array(
				'label'=>'total black/white list file',
				'value'=>$totalBatchListed
);
				
$columns[] = array(
				'label'=>'global whitelist',
				'value'=>$arrayGlobalWhiteList[$model->flag_global_whitelist]
				);
				
				
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$columns,
));

 ?>

 <?php  $this->endWidget();

Controller::createInfo($model);
?>

<?php 
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_batch',
		'id_customer',
		'jobs_id',
		'start_periode',
		'stop_periode',
		'sms_send_limit',
		'sms_sent',
		'sms_text',
		'message_type',
		'message_bit',
		'status_batch',
		'filename',
		'first_ip',
		'first_user',
		'first_update',
		'last_ip',
		'last_user',
		'last_update',
		'topik',
		'approved_time',
		'sms_success',
		'sms_failed',
		'deleted',
		'schedule_delivery',
		'process_date',
		'retry',
		'comment',
		'id_invoice',
		'batch_no',
		'last_value',
		'content_expired',
		'divre',
		'filenameupload',
		'broadcast_type',
		'total_number',
		'booking',
		'usage',
		'last_filestatus_modif',
		'customer_ani',
		'binary_data',
		'prefix_detail',
		'sms_text_view',
		'instant_failed',
		'tipe_batch',
		'promotion_media',
		'sex_category',
		'age_category',
		'status_category',
		'filter_prefix',
		'education_category',
		'channel_category',
		'priority_category',
		'flag_deleted',
		'ast',
		'cell',
		'flag_lba',
		'flag_file',
		'multimedia_type',
		'quota',
		'profile',
		'batch_type',
		'vip',
		'increase_counter',
		'unique_subscriber_per_batch',
		'flag_hour',
		'flag_multi_batch',
		'slot_multi_batch',
		'id_batch_reference',
		'black_list_profile',
		'flag_template_message',
		'profile_prouction',
		'profile_gender',
		'profile_gprs',
		'profile_kesehatan',
		'profile_usia',
		'profile_lifestyle',
		'profile_sians',
		'profile_travel',
		'profile_arpu',
		'param_arpu',
		'param_usia',
		'profile_usia2',
		'profile_arpu2',
		'flag_master_batch',
		'interface_channel',
	),
)); 
*/
?>
