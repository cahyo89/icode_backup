<?php
$this->breadcrumbs=array(
	'Lac Profiles'=>array('index'),
	$model->profile_name=>array('view','id'=>$model->id),
	'Update',
);


?>

<table width="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Update Lac Profile  #<?php echo $model->profile_name; ?></h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
	   array('label'=>'View','link'=>'lacProfile/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'lacProfile/index','img'=>'/images/list.png','id'=>'','conf'=>''),
	   array('label'=>'New','link'=>'lacProfile/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?></div></td>
					</tr></table>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

 <div class="operatorLeft"><?php  Controller::createMenu(array(
	array('label'=>'New','link'=>'lacProfile/create','img'=>'/images/new.png','id'=>'','conf'=>''),
	   array('label'=>'View','link'=>'lacProfile/view','img'=>'/images/view.png','id'=>$model->id,'conf'=>''),
	   array('label'=>'List','link'=>'lacProfile/index','img'=>'/images/list.png','id'=>'','conf'=>'')
	  
		));  ?></div>