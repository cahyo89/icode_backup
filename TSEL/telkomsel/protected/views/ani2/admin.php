<?php
$this->breadcrumbs=array(
	'Customer Shortcode'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ani2-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Customer Shortcode</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'ani2/create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ani2-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name'=>'id_customer',
			'type'=>'raw',
			'value'=>'$data->id_customer != -1 ?CHtml::link(@$data->Customer->nama,array("ani2/view","id"=>$data->id)) :CHtml::link("ALL",array("ani2/view","id"=>$data->id))',
		),	
		'ani',
		array(
			'name'=>'media_type',
			'type'=>'raw',
			'value'=>'$data->ChannelCategory->name',
		),
		
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'Controller::getConstanta("approved",$data->status)',
		),
		/*
		'cp_password',
		'cp_sid',
		'cp_password_trx',
		'flag_deleted',
		'first_user',
		'first_ip',
		'first_update',
		'last_user',
		'last_ip',
		'last_update',
		*/
		array(
			'class'=>'CButtonColumn',
			
			'header'=>'Action',
			'deleteConfirmation'=>"js:'Are you sure you want to delete this item??\\n\\n Ani :' +
        $(this).parents('tr').children(':eq(0)').text() +
        '\\n\\nThis operation cannot be undone.\\nPress \"OK\" to delete, or \"Cancel\" to abort without deleting.\\n\\n'",
		),
	),
)); ?>
<div class="operatorLeft">
<?php  Controller::createMenu(array(
	   array('label'=>'New','link'=>'ani2/create','img'=>'/images/new.png','id'=>'','conf'=>'')
		));  ?>
</div>