<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Bucket Single Command'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List Paket Campaign', 'url'=>array('index')),
	array('label'=>'Create Paket Campaign', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bucket-single-command-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Bucket API Push Single Message</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'New','link'=>'create','img'=>'/images/new.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
	
	function getCustomerName($idCustomer){
		$customer = Customer::model()->findByPk($idCustomer);
		return $customer->nama;
	}

?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bucket-single-command-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
		'name'=>'id_customer',
		'value'=>'getCustomerName($data->id_customer)',
		),
		array(
		'name'=>'channel_category',
		'value'=>'Yii::app()->params[\'channelcatagory\'][$data->channel_category]',
		),
		'total',
		//array(
			//'class'=>'CButtonColumn',
		//),
	),
)); ?>
