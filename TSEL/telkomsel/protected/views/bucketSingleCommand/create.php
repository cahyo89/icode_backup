<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */

$this->breadcrumbs=array(
	'Bucker Single Commands'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Paket Campaign', 'url'=>array('index')),
	array('label'=>'Manage Paket Campaign', 'url'=>array('admin')),
);*/
?>

<table width ="<?php echo Yii::app()->params['widthTableMenu']; ?>"><tr><td><h1>Manage Bucket API Push Single Message</h1></td>
<td><div class="operatorRight"><?php  Controller::createMenu(array(
				   array('label'=>'List','link'=>'index','img'=>'/images/list.png','id'=>'','conf'=>'')
					));  ?></div></td>
					</tr></table>
					
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>