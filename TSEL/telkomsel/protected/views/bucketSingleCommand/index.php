<?php
/* @var $this PaketCampaignController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bucket Single Commands',
);

$this->menu=array(
	array('label'=>'Create Bucket Single Command', 'url'=>array('create')),
	array('label'=>'Manage Bucket Single Command', 'url'=>array('admin')),
);
?>

<h1>Bucket API Push Single Message</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
