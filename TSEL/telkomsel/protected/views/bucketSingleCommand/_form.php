<?php
/* @var $this PaketCampaignController */
/* @var $model PaketCampaign */
/* @var $form CActiveForm */
?>
<?php			
	$this->widget('ext.sformwizard.SFormWizard',array(
		'selector'=>"#bucket-single-command-form",
		'disableUIStyles' => "true",
		'validationEnabled' => "true",
	));
?>
<div class="form well">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bucket-single-command-form',
	'enableAjaxValidation'=>false,
)); ?>
	<div class="step" id="Step1">
		<p class="note">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->errorSummary($model); ?>
		
		<div class="well-small">
		 <p class="note">Fields with <span class="required">*</span> are required.</p>
			<?php echo $form->labelEx($model,'Customer Name')."<b><font color='red'>*</font></b>" ?>
			<?php
				if(Yii::app()->user->getUserMode()==0)
				{
					$customer = Customer::model()->findAll('id_customer in ('.Controller::getIdCustomerByIdUserAndesta().') AND approved = 1 AND flag_deleted IS NULL or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
				}
				else if(Yii::app()->user->getUserMode() == 1)
				{
					$customer = Customer::model()->findAll('id_user = '.Yii::app()->user->id.' and approved = 1 and flag_deleted is null or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
				}
				else
				{
					$custHead = Customer::model()->findAll('id_customer = '.Yii::app()->user->getIdCustomer());
					if($custHead)
					$customer = Customer::model()->findAll('approved = 1 and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					else
					{	
						$user = User::model()->findByPk(Yii::app()->user->id);
						$custHead = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and id_customer = '.$user->id_customer);
						$customer = Customer::model()->findAll('approved = 1 and (flag_deleted is null or flag_deleted <> 1) and ( parent_id = '.$custHead[0]->id_customer.' or id_customer = '.$custHead[0]->id_customer.' )');
					}
				}
			?>
			<?php echo $form->dropDownList($model,'id_customer',CHtml::listData($customer, 'id_customer', 'nama'),array('class'=>'required','empty'=>'--Please Select One--'));?>
			<?php echo $form->error($model,'id_customer'); ?>
		</div>

		<div class="well-small">
			<?php echo $form->labelEx($model,'channel_category'); ?>
			<?php echo $form->dropDownList($model,'channel_category',array(0=>"SMS"),array('class' => 'required')); ?>
			<?php echo $form->error($model,'channel_category'); ?>
		</div>
		<?php
			$packetCampaign = PaketCampaign::model()->findAll('channel_category = 0 and flag_deleted is null or flag_deleted <> :flag_deleted order by nama asc',array(':flag_deleted'=>1));
		?>
		<div class="well-small">
			<?php echo $form->labelEx($model,'idPacketCampaign'); ?>
			<?php echo $form->dropDownList($model,'idPacketCampaign',CHtml::listData($packetCampaign, 'id', 'nama'),array('class' => 'required','onchange'=>'{onChangeChannelCategory();}')); ?>
			<?php echo $form->error($model,'idPacketCampaign'); ?>
		</div>
		
		<div class="well-small">
			<?php echo $form->labelEx($model,'totalBuyToken'); ?>
			<?php echo $form->textField($model,'totalBuyToken',array('class'=>'required digits')); ?>
			<?php echo $form->error($model,'totalBuyToken'); ?>
		</div>

		<div class="well-small buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->