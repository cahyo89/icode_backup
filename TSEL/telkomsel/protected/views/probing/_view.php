 $value
	 * @return string
	 */
	protected static function compile_render($value)
	{
		$pattern = static::matcher('render');

		return preg_replace($pattern, '$1<?php echo render$2; ?>', $value);
	}

	/**
	 * Rewrites Blade @render_each statements into valid PHP.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protected static function compile_render_each($value)
	{
		$pattern = static::matcher('render_each');

		return preg_replace($pattern, '$1<?php echo render_each$2; ?>', $value);
	}

	/**
	 * Rewrites Blade @yield statements into Section statements.
	 *
	 * The Blade @yield statement is a shortcut to the Section::yield method.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protected static function compile_yields($value)
	{
		$pattern = static::matcher('yield');

		return preg_replace($pattern, '$1<?php echo \\Laravel\\Section::yield$2; ?>', $value);
	}

	/**
	 * Rewrites Blade yield section statements into valid PHP.
	 *
	 * @return string
	 */
	protected static function compile_yield_sections($value)
	{
		$replace = '<?php echo \\Laravel\\Section::yield_section(); ?>';

		return str_replace('@yield_section', $replace, $value);
	}

	/**
	 * Rewrites Blade @section statements into Section statements.
	 *
	 * The Blade @section statement is a shortcut to the Section::start method.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protecte