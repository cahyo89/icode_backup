<?php
/*-----------------------------------------------------------------------------------------------------------------
CONFIGURATION
--------------------------------------------------------------------------------------------------------------------*/
//$IP_OCS = "10.9.2.102";
$IP_OCS = "10.9.2.15";
$Port_OCS = "8680";
$URL_SOAP = "/services/CBSInterfaceAccountMgrService";


/*-----------------------------------------------------------------------------------------------------------------
DO NOT CHANGE CODE BELOW !!!
--------------------------------------------------------------------------------------------------------------------*/

function getValueFromTag($string, $tagname) 
{
	$pattern = "/<$tagname ?.*>([Prepaid,Postpaid].*)<\/$tagname><Balance>(.*)<\/Balance>/";
	preg_match($pattern, $string, $matches);
	//echo $pattern;
	return array($matches[1],$matches[2]);
}

function getExpFromTag($string, $tagname) 
{
	
	$pattern = "/<$tagname ?.*>(.*)<\/$tagname>(.*)/";
	preg_match($pattern, $string, $matches);
	//echo $pattern;
	return array($matches[1],$matches[2]);
}


function getValInTag($string, $start, $end)
{
	$str1tmp = strpos($string, $start);
	$str2 = strpos($string, $end);
	
	if(($str1tmp===FALSE) or ($str2===FALSE))
	{
		return "";
	}
	else
	{
		$str1 = $str1tmp+strlen($start);
		return trim(substr($string, $str1, ($str2-$str1)));
	}
}

$msisdn = $_GET['msisdn'] . $_SERVER['argv'][1];
if($msisdn == "")
{
	print "status:FAILED|msisdn:{$msisdn}|type:|bal:";
	exit;
}
else
{
	if(substr($msisdn, 0, 2) == "62")
		$msisdn = substr($msisdn, 2);
	elseif(substr($msisdn, 0, 1) == "0")
		$msisdn = substr($msisdn, 1);
}

$body = '';

$rand = rand(1000, 9999);
$date=mktime();
$date = $date.$rand;

$body .= '<?xml version="1.0" encoding="UTF-8"?>';
$body .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
$body .= '<SOAP-ENV:Body>';
$body .= '  <QueryBalanceRequestMsg xmlns="http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg">';
$body .= '   <RequestHeader xmlns="">';
$body .= '    <ns1:CommandId xmlns:ns1="http://www.huawei.com/bme/cbsinterface/common">QueryBalance</ns1:CommandId>';
$body .= '    <ns2:Version xmlns:ns2="http://www.huawei.com/bme/cbsinterface/common">1</ns2:Version>';
$body .= '    <ns3:TransactionId xmlns:ns3="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns3:TransactionId>';
$body .= '    <ns4:SequenceId xmlns:ns4="http://www.huawei.com/bme/cbsinterface/common"/>';
$body .= '    <ns5:RequestType xmlns:ns5="http://www.huawei.com/bme/cbsinterface/common">Event</ns5:RequestType>';
$body .= '    <ns6:SerialNo xmlns:ns6="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns6:SerialNo>';
$body .= '   </RequestHeader>';
$body .= '   <QueryBalanceRequest xmlns="">';
$body .= '    <ns7:SubscriberNo xmlns:ns7="http://www.huawei.com/bme/cbsinterface/cbs/accountmgr">'.$msisdn.'</ns7:SubscriberNo>';
$body .= '   </QueryBalanceRequest>';
$body .= '  </QueryBalanceRequestMsg>';
$body .= '</SOAP-ENV:Body>';
$body .= '</SOAP-ENV:Envelope>';

	$body_len = strlen($body);
	
	//$header = 'POST /soap/ssmb_rec_channel HTTP/1.0'.chr(10).'';
	//$header .= 'Host: 10.22.197.74:20051'.chr(10).'';
	$header = 'POST http://'.$IP_OCS.':'.$Port_OCS.$URL_SOAP.' HTTP/1.0'.chr(10).'';
    $header .= 'Host: '.$IP_OCS.':'.$Port_OCS.chr(10).'';	
	$header .= 'User-Agent: CDMClient/0.0.1'.chr(10).'';
	$header .= 'Content-Type: application/soap+xml; action=QueryBalance; charset=UTF-8'.chr(10).'';
	//$header .= 'Cache-Control: no-cache'.chr(10).'';
	//$header .= 'Pragma: no-cache'.chr(10).'';
	$header .= 'SOAPAction: ""'.chr(10).'';
	$header .= 'Content-Length: '.$body_len.chr(10).'';
	$header .= 'Connection: Close'.chr(10).''.chr(10).'';
	
	//print $header . $body;
	//print "\r\n";
	
	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket === false) {
		$err = "socket_create() failed: reason: " . socket_strerror(socket_last_error());
	} else {
		//$result = @socket_connect($socket, "10.22.197.74", "20051");
		$result = @socket_connect($socket, $IP_OCS, $Port_OCS);
		if ($result === false) {
			$err = "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket));
		} else {
			socket_write($socket, 
				//$this->header . $this->content_header . $this->smil . $this->attachments_enc . $this->end, 
				//strlen($this->header . $this->content_header . $this->smil . $this->attachments_enc . $this->end));
				$header . $body, 
				strlen($header . $body));
			//echo "OK.\n";

			//echo "Reading response:\n\n";
			
			while ($out = socket_read($socket, 2048)) {
				$return .= $out;
			}

			//echo "Closing socket...";
			socket_close($socket);
		}		
	}
	//print $err . "\r\n";	
	print $return . "\r\n";
	$data = getValueFromTag($return, "BalanceDesc");
	$data2 = getExpFromTag($return, "ExpireTime");
	//$type = getValInTag($return, "<BalanceDesc>", "</BalanceDesc>");
	//$bal = getValInTag($return, "<Balance>", "</Balance>");
print_r($data);
	if((empty($data[0])) or ($data[1]==""))
	{
		print "status:FAILED|msisdn:{$msisdn}|type:{$data[0]}|bal:{$data[1]}";
	}
	else
	{
		//print "status:SUCCESS|msisdn:{$msisdn}|type:{$data[0]}|bal:{$data[1]}";
		print "<span class=\"mp97234_text\"> <h4><b>Pulsa Anda Rp. ".number_format($data[1],0,',','.')."<br>Masa Aktif ".substr($data2[0],0,4)."-".substr($data2[0],4,2)."-".substr($data2[0],6,2)."<br></b></h4></span>";
	}
?>
