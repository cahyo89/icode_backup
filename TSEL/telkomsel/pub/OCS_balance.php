<?php
/*-----------------------------------------------------------------------------------------------------------------
CONFIGURATION
--------------------------------------------------------------------------------------------------------------------*/
//$IP_OCS = "10.9.2.102";
$IP_OCS = "10.22.197.80";
$Port_OCS = "20045";
//$URL_SOAP = "/services/CBSInterfaceAccountMgrService";
$URL_SOAP = "/ws/com.axis.interacct.pub.ws.provider:providerGetSubscriberInfo";
$username = "mobads";
$password = "axis";
$channel = "MOBADS";

/*-----------------------------------------------------------------------------------------------------------------
DO NOT CHANGE CODE BELOW !!!
--------------------------------------------------------------------------------------------------------------------*/

function getValueFromTag($string, $tagname) 
{
	$pattern = "/<$tagname ?.*>([Prepaid,Postpaid].*)<\/$tagname><Balance>(.*)<\/Balance>/";
	preg_match($pattern, $string, $matches);
	//echo $pattern;
	return array($matches[1],$matches[2]);
}

function getExpFromTag($string, $tagname) 
{
	
	$pattern = "/<$tagname ?.*>(.*)<\/$tagname>(.*)/";
	preg_match($pattern, $string, $matches);
	//echo $pattern;
	return array($matches[1],$matches[2]);
}


function getValInTag($string, $start, $end)
{
	$str1tmp = strpos($string, $start);
	$str2 = strpos($string, $end);
	
	if(($str1tmp===FALSE) or ($str2===FALSE))
	{
		return "";
	}
	else
	{
		$str1 = $str1tmp+strlen($start);
		return trim(substr($string, $str1, ($str2-$str1)));
	}
}

$msisdn = $_GET['msisdn'] . $_SERVER['argv'][1];
if($msisdn == "")
{
	print "status:FAILED|msisdn:{$msisdn}";
	exit;
}
else
{
	/*if(substr($msisdn, 0, 2) == "62")
		$msisdn = substr($msisdn, 2);
	else*/
	if(substr($msisdn, 0, 1) == "0"){
		$msisdn = "62" . substr($msisdn, 1);
	}
}

$body = '';

$rand = rand(1000, 9999);
$date=mktime();
$date = $date.$rand;

/*
$body .= '<?xml version="1.0" encoding="UTF-8"?>';
$body .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
$body .= '<SOAP-ENV:Body>';
$body .= '  <QueryBalanceRequestMsg xmlns="http://www.huawei.com/bme/cbsinterface/cbs/accountmgrmsg">';
$body .= '   <RequestHeader xmlns="">';
$body .= '    <ns1:CommandId xmlns:ns1="http://www.huawei.com/bme/cbsinterface/common">QueryBalance</ns1:CommandId>';
$body .= '    <ns2:Version xmlns:ns2="http://www.huawei.com/bme/cbsinterface/common">1</ns2:Version>';
$body .= '    <ns3:TransactionId xmlns:ns3="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns3:TransactionId>';
$body .= '    <ns4:SequenceId xmlns:ns4="http://www.huawei.com/bme/cbsinterface/common"/>';
$body .= '    <ns5:RequestType xmlns:ns5="http://www.huawei.com/bme/cbsinterface/common">Event</ns5:RequestType>';
$body .= '    <ns6:SerialNo xmlns:ns6="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns6:SerialNo>';
$body .= '   </RequestHeader>';
$body .= '   <QueryBalanceRequest xmlns="">';
$body .= '    <ns7:SubscriberNo xmlns:ns7="http://www.huawei.com/bme/cbsinterface/cbs/accountmgr">'.$msisdn.'</ns7:SubscriberNo>';
$body .= '   </QueryBalanceRequest>';
$body .= '  </QueryBalanceRequestMsg>';
$body .= '</SOAP-ENV:Body>';
$body .= '</SOAP-ENV:Envelope>';
*/
/*
$body .= '<?xml version="1.0" encoding="UTF-8"?>'.chr(13).chr(10);
$body .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.chr(13).chr(10);
$body .= '<SOAP-ENV:Body>'.chr(13).chr(10);
$body .= '<q2:IntegrationEnquiryRequestMsg xmlns:q0="http://www.huawei.com/bme/cbsinterface/cbs/businessmgr" xmlns:q1="http://www.huawei.com/bme/cbsinterface/common" xmlns:q2="http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg">'.chr(13).chr(10);
$body .= '           <RequestHeader>'.chr(13).chr(10);
$body .= '                  <q1:CommandId>IntegrationEnquiry</q1:CommandId>'.chr(13).chr(10);
$body .= '                  <q1:Version>1.2</q1:Version>'.chr(13).chr(10);
$body .= '                  <q1:TransactionId>trans001</q1:TransactionId>'.chr(13).chr(10);
$body .= '                  <q1:SequenceId>2232</q1:SequenceId>'.chr(13).chr(10);
$body .= '                  <q1:RequestType>Event</q1:RequestType>'.chr(13).chr(10);
$body .= '                  <q1:SessionEntity>'.chr(13).chr(10);
$body .= '                         <q1:Name>name</q1:Name>'.chr(13).chr(10);
$body .= '                         <q1:Password>password</q1:Password>'.chr(13).chr(10);
$body .= '                         <q1:RemoteAddress>127.0.0.1</q1:RemoteAddress>'.chr(13).chr(10);
$body .= '                  </q1:SessionEntity>'.chr(13).chr(10);
$body .= '                  <q1:SerialNo>'.$date.'</q1:SerialNo>'.chr(13).chr(10);
$body .= '           </RequestHeader>'.chr(13).chr(10);
$body .= '           <IntegrationEnquiryRequest>'.chr(13).chr(10);
$body .= '                  <q0:SubscriberNo>'.$msisdn.'</q0:SubscriberNo>'.chr(13).chr(10);
$body .= '                  <q0:QueryType>0</q0:QueryType>'.chr(13).chr(10);
$body .= '           </IntegrationEnquiryRequest>'.chr(13).chr(10);
$body .= '</q2:IntegrationEnquiryRequestMsg>'.chr(13).chr(10);
$body .= '</SOAP-ENV:Body>'.chr(13).chr(10);
$body .= '</SOAP-ENV:Envelope>'.chr(13).chr(10);
*/
/*
$body .= '<?xml version="1.0" encoding="UTF-8"?>'.chr(13).chr(10);
$body .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.chr(13).chr(10);
$body .= '<SOAP-ENV:Body>'.chr(13).chr(10);
$body .= '<IntegrationEnquiryRequestMsg xmlns="http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg">'.chr(13).chr(10);
$body .= '<RequestHeader xmlns="">'.chr(13).chr(10);
$body .= '    <ns1:CommandId xmlns:ns1="http://www.huawei.com/bme/cbsinterface/common">IntegrationEnquiry</ns1:CommandId>'.chr(13).chr(10);
$body .= '    <ns2:Version xmlns:ns2="http://www.huawei.com/bme/cbsinterface/common">1</ns2:Version>'.chr(13).chr(10);
$body .= '    <ns3:TransactionId xmlns:ns3="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns3:TransactionId>'.chr(13).chr(10);
$body .= '    <ns4:SequenceId xmlns:ns4="http://www.huawei.com/bme/cbsinterface/common"></ns4:SequenceId>'.chr(13).chr(10);
$body .= '    <ns5:RequestType xmlns:ns5="http://www.huawei.com/bme/cbsinterface/common">Event</ns5:RequestType>'.chr(13).chr(10);
$body .= '    <ns6:SerialNo xmlns:ns6="http://www.huawei.com/bme/cbsinterface/common">'.$date.'</ns6:SerialNo>'.chr(13).chr(10);
$body .= '</RequestHeader>'.chr(13).chr(10);
$body .= '<IntegrationEnquiryRequest xmlns="">'.chr(13).chr(10);
$body .= '    <ns7:SubscriberNo xmlns:ns7="http://www.huawei.com/bme/cbsinterface/cbs/businessmgr">'.$msisdn.'</ns7:SubscriberNo>'.chr(13).chr(10);
$body .= '    <ns8:QueryType xmlns:ns8="http://www.huawei.com/bme/cbsinterface/cbs/businessmgr">0</ns8:QueryType>'.chr(13).chr(10);
$body .= '</IntegrationEnquiryRequest>'.chr(13).chr(10);
$body .= '</IntegrationEnquiryRequestMsg>'.chr(13).chr(10);
$body .= '</SOAP-ENV:Body>'.chr(13).chr(10);
$body .= '</SOAP-ENV:Envelope>'.chr(13).chr(10);
*/

$body .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:prov="http://JKMADMDWMIX01/com/axis/ocs/info/pub/provGetSubscriberInfo">'.chr(13).chr(10);
$body .= '   <soapenv:Header/>'.chr(13).chr(10);
$body .= '   <soapenv:Body>'.chr(13).chr(10);
$body .= '      <prov:getSubscriberInfo>'.chr(13).chr(10);
$body .= '         <docReq>'.chr(13).chr(10);
$body .= '            <requestHeader>'.chr(13).chr(10);
$body .= '               <!--type: string-->'.chr(13).chr(10);
$body .= '               <trxid>'.$date.'</trxid>'.chr(13).chr(10);
$body .= '               <auth>'.chr(13).chr(10);
$body .= '                  <!--type: string-->'.chr(13).chr(10);
$body .= '                  <username>'.$username.'</username>'.chr(13).chr(10);
$body .= '                  <!--type: string-->'.chr(13).chr(10);
$body .= '                  <password>'.md5($password).'</password>'.chr(13).chr(10);
$body .= '               </auth>'.chr(13).chr(10);
$body .= '               <!--Optional:-->'.chr(13).chr(10);
$body .= '               <!--type: string-->'.chr(13).chr(10);
$body .= '               <channel>'.$channel.'</channel>'.chr(13).chr(10);
$body .= '            </requestHeader>'.chr(13).chr(10);
$body .= '            <body>'.chr(13).chr(10);
$body .= '               <!--type: string-->'.chr(13).chr(10);
$body .= '               <msisdn>'.$msisdn.'</msisdn>'.chr(13).chr(10);
$body .= '            </body>'.chr(13).chr(10);
$body .= '         </docReq>'.chr(13).chr(10);
$body .= '      </prov:getSubscriberInfo>'.chr(13).chr(10);
$body .= '   </soapenv:Body>'.chr(13).chr(10);
$body .= '</soapenv:Envelope>'.chr(13).chr(10);

	$body_len = strlen($body);
	
	//$header = 'POST /soap/ssmb_rec_channel HTTP/1.0'.chr(10).'';
	//$header .= 'Host: 10.22.197.74:20051'.chr(10).'';
	$header = 'POST http://'.$IP_OCS.':'.$Port_OCS.$URL_SOAP.' HTTP/1.0'.chr(10).'';
    $header .= 'Host: '.$IP_OCS.':'.$Port_OCS.chr(10).'';	
	$header .= 'User-Agent: CDMClient/0.0.1'.chr(10).'';
	$header .= 'Content-Type: application/soap+xml; action=IntegrationEnquiry; charset=UTF-8'.chr(10).'';
	//$header .= 'Cache-Control: no-cache'.chr(10).'';
	//$header .= 'Pragma: no-cache'.chr(10).'';
	$header .= 'SOAPAction: ""'.chr(10).'';
	$header .= 'Content-Length: '.$body_len.chr(10).'';
	$header .= 'Connection: Close'.chr(10).''.chr(10).'';
	
	//print $header . $body;
	//print "\r\n";
	
	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket === false) {
		$err = "socket_create() failed: reason: " . socket_strerror(socket_last_error());
	} else {
		//$result = @socket_connect($socket, "10.22.197.74", "20051");
		$result = @socket_connect($socket, $IP_OCS, $Port_OCS);
		if ($result === false) {
			$err = "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket));
		} else {
			socket_write($socket, 
				//$this->header . $this->content_header . $this->smil . $this->attachments_enc . $this->end, 
				//strlen($this->header . $this->content_header . $this->smil . $this->attachments_enc . $this->end));
				$header . $body, 
				strlen($header . $body));
			//echo "OK.\n";

			//echo "Reading response:\n\n";
			
			while ($out = socket_read($socket, 2048)) {
				$return .= $out;
			}

			//echo "Closing socket...";
			socket_close($socket);
		}		
	}
	//print $err . "\r\n";	
	//print $return . "\r\n";
	//$data = getValueFromTag($return, "BalanceDesc");
	//$data2 = getExpFromTag($return, "ExpireTime");
	//$data2 = getExpFromTag($return, "ActiveStop");
	$data1 = getValInTag($return, "<balance>", "</balance>");
	$data2 = getValInTag($return, "<expiryDate>", "</expiryDate>");
	//$type = getValInTag($return, "<BalanceDesc>", "</BalanceDesc>");
	//$bal = getValInTag($return, "<Balance>", "</Balance>");
	if(($data1 == "") or ($data2=="")) 
	{
		//print "Balance anda: , expire:";
	}
	else
	{
		//print "status:SUCCESS|msisdn:{$msisdn}|type:{$data[0]}|bal:{$data[1]}";
		print "Pulsa Anda Rp. ".number_format($data1,0,',','.').", Masa Aktif ".substr($data2,6,2)."/".substr($data2,4,2)."/".substr($data2,0,4)."";
	}
?>
