/*
Navicat MySQL Data Transfer

Source Server         : 10.10.227.172
Source Server Version : 50530
Source Host           : 10.10.227.172:3306
Source Database       : int_axis

Target Server Type    : MYSQL
Target Server Version : 50530
File Encoding         : 65001

Date: 2013-12-06 14:30:19
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `tbl_message_filter`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_message_filter`;
CREATE TABLE `tbl_message_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banned_text` blob,
  `flag_deleted` int(11) DEFAULT NULL,
  `first_user` varchar(50) DEFAULT NULL,
  `last_user` varchar(50) DEFAULT NULL,
  `first_update` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `first_ip` varchar(50) DEFAULT NULL,
  `last_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flagDeleted` (`flag_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_message_filter
-- ----------------------------
INSERT INTO tbl_message_filter VALUES ('1', 0x496E646F736174, '0', 'dodo', 'dodo', '2013-09-13 11:02:44', '2013-09-13 11:02:44', '10.40.40.16', '10.40.40.16');
INSERT INTO tbl_message_filter VALUES ('2', 0x616E6465737461, '1', 'andesta', 'andesta', '2013-09-14 13:58:11', '2013-09-14 13:58:11', '10.40.40.16', '10.40.40.16');
