<?php
/*
Create by cahyo89@gmail.com
16 September 2013
..........hwaiting.........
     ayo al-hazim
.: Man Jadda Wa Jada :. 
*/

	class schedulerCampaign{
		function __construct(){
			clearstatcache();
			include  "/projectlbaxl_development/utils/schedulerCampaignProsess/config.php";
			require_once  "/projectlbaxl_development/utils/sendMail/sendMail.php";
			$mysqli = new mysqli($hostDb,$userDb,$passDb,$db);

			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error());
			    exit();
			}else{
				
				$this->data_campaign		= array();
				$this->db					= $mysqli;
				$this->fileNameLog 			= $fileNameLog;
				$this->folderLog			= $folderLog;
				$this->varr					= $varr;
				$this->stop_stat			= $stop_stat;
				$this->finis_stat			= $finis_stat;
				$this->sender				= $sender;
			}
		}
		
		function updateScheduler($time){
			
			$query = "UPDATE tbl_scheduler_status set last_exec_time = '".date("Y-m-d H:i:s")."',status = 1,next_execute_time = '".$time."' ";
			$query .= " WHERE description = '".$this->varr."' ";
			
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}else{
				$this->writeLog("query failed : ".$query);
			}
		}
		
		function updateStatus(){
			$query = "UPDATE tbl_scheduler_status set status = 0 ";
			$query .= " WHERE description = '".$this->varr."' ";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success : ".$query);
			}else{
				$this->writeLog("query failed  : ".$query);
			}
		}
		
		
		function cekStatus(){
			 $query  = "SELECT * from tbl_scheduler_status where description = '".$this->varr."'";
			$result = $this->db->query($query);
				if($result){
					$this->writeLog("scheduler Checking....");
					$rw = $result->fetch_array(MYSQLI_ASSOC); 
					
					if($rw['status'] == 1){
						$this->writeLog("scheduler already running");
						return "FALSE";
					}
				    else{
						
						$a = $rw['next_execute_time'];
						$mkTimeNext = strtotime($a);
						
						$data = explode(" ",$a);
						$this->nextExecute = $a; 
						
						$tanggal = explode("-",$data[0]);
						$waktu   = explode(":",$data[1]);
						
						$y = $tanggal[0];
						$m = $tanggal[1];
						$d = $tanggal[2];
						
						$h = $waktu[0];
						$i = $waktu[1];
						$s = $waktu[2];	
						
						// hour menit second month day year
						 $this->endEksekusi=date("Y-m-d H:i:s", mktime(intval($waktu[0]), intval("59"), intval("59"), intval($tanggal[1]), intval($tanggal[2]), intval($tanggal[0])));
						 $this->startEksekusi=$a;
						
						$time = date("Y-m-d H:i:s", mktime(intval($waktu[0]), intval($waktu[1]+2), intval($waktu[2]), intval($tanggal[1]), intval($tanggal[2]), intval($tanggal[0])));
							//echo $time = date("Y-m-d H:i:s",mktime());
						if(time() > $mkTimeNext){
							$this->updateScheduler($time);
							 
							return "TRUE";	
						}else{
							 
							return "FALSE";
						}
					}
				}else{
					$this->writeLog("query FAILED");
					return "FALSE";
				}
		}
		
		
		function send_email($Message,$subject,$emailnya){
			$destination=array("".$emailnya."");
			
			$ayo = new sendEmail();
			$ayo->sendMailnya($this->sender,$subject,$destination,$Message );
		}
		
		
		
		function load_campaign(){
			$query  = "SELECT a.*,b.pic_name as namareseller ,b.pic_email as emailreseller,c.pic_name as namacustomer,c.pic_email as emailcustomer  from tbl_lba_batch a join tbl_reseller b on (a.id_reseller = b.id ) join tbl_customer c on (a.id_customer=c.id_customer) where (a.status_batch =".$this->stop_stat." or a.status_batch=".$this->finis_stat.") and a.last_update >='".$this->startEksekusi."' and a.last_update <='".$this->endEksekusi."'";
			//$query="SELECT a.*,b.pic_name as namareseller ,b.pic_email as emailreseller,c.pic_name as namacustomer,c.pic_email as emailcustomer  from tbl_lba_batch a join tbl_reseller b on (a.id_reseller = b.id ) join tbl_customer c on (a.id_customer=c.id_customer) where (a.status_batch =".$this->stop_stat." or a.status_batch=".$this->finis_stat.") and a.id_reseller=4 and a.id_customer=16";
			$result = $this->db->query($query);
			if($result){
				$this->writeLog("query success :".$query);
				if(($result->num_rows)>0){
					while($rw = $result->fetch_array(MYSQLI_ASSOC)){
						$this->data_campaign[$rw['id_batch']] = $rw;
					}
					$this->writeLog("query success ".$query);
				}
			}
			else{
				$this->writeLog("query failed : ".$query);
			}	
			print_r($this->data_campaign);
		}
		
		
				
		 
		function proses(){
			foreach($this->data_campaign as $c) {
				$message=""; $subject="";
				if($c['status_batch']==$this->stop_stat){
					$subject=" Stopped campaign (Customer : ".$c['namacustomer']."/ Reseller :".$c['namareseller'].")";
				}elseif($c['status_batch']==$this->finis_stat){
					$subject=" Finished campaign (Customer :".$c['namacustomer']."/ Reseller : ".$c['namareseller'].")";
				}
				$message="
Campaign ID                  : ".$c['jobs_id']."
Topik                             : ".$c['topik']."
Start date                      : ".$c['start_periode']."
End Date                       : ".$c['content_expired']."
Potongan isi campaign   : ".$c['sms_text']."
Channel                         : ".$c['channel_category']."
Success count               : ".$c['sms_success']."
Failed Count                   : ".$c['sms_failed']."
				";
				if($this->send_email($message,$subject,$c['emailreseller'])){
					$this->writeLog("Success Send Email Reseller : ".$c['emailreseller']);
				}else{
					$this->writeLog("Failed Send Email Reseller : ".$c['emailreseller']);
				}
				if($this->send_email($message,$subject,$c['emailcustomer'])){
					$this->writeLog("Success Send Email Customer : ".$c['emailcustomer']);
				}else{
					$this->writeLog("Failed Send Email Customer : ".$c['emailcustomer']);
				}
			}
		}
		
		
		function writeLog($message){
			$handle = fopen($this->folderLog.$this->fileNameLog,"a");
			if($handle){
				fwrite($handle,date("Y-m-d H:i:s")." : ".$message."\r\n");
				fclose($handle);
			}
		}
		
		
		
		function __destruct(){
		} 
	}
	
	$ayo = new schedulerCampaign();
		if($ayo->cekStatus() == "TRUE"){
			$ayo->load_campaign();
			$ayo->proses();
			$ayo->updateStatus();
		}
?>